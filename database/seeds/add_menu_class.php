<?php

use Illuminate\Database\Seeder;

class add_menu_class extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\MenuClass::create([
        	'id'=>'17',
        	'name'=>'購物指南',
        	'sort'=>'11',
        	'font_awesome'=>'fa fa-user',
        	'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
	]);
    }
}
