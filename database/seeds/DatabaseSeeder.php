<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Model::unguard();
		$this->call(CommentTableSeeder::class);
		//$this->cell(PostsTableSeeder::class);
		Model::reguard();
		$this -> call('products');
		$this -> call('add_menu_class');
	}
}
