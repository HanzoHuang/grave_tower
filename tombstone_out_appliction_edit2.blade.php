@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!! Html::script(asset('js/ckeditor/ckeditor.js')) !!}
{!! Html::style(asset('backend/plugins/bootstrap-datepicker/css/datepicker3.css'))  !!}
{!! Html::script(asset('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'))!!}
{!! Html::script(asset('js/bootstrap-datetimepicker.js'))!!}
<link rel="stylesheet" href="{{asset('css/lightbox.min.css')}}">
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}


 <?php $tombstoneSys=CabinetSys(); ?>
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    <input class="" placeholder="" name="tombstone_appliction_type" type="hidden" value='1'><div class="form-group">
    <input name="_method" type="hidden" value="PATCH">
    <input class="" placeholder="" name="id" type="hidden" value="{{$tombstone_appliction->id}}">
    {{ csrf_field() }}
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="no" name="no" type="text" readonly="readonly" value="{{$tombstone_appliction->no}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請狀態</label>
        <div class="col-sm-3">
            <input  type="radio" id="status" name="status" value='700' onclick="return false;">已申請
            <input  type="radio" id="status" name="status" value='800' @if($tombstone_appliction->status==700) checked @else onclick="return false;" @endif>已出塔
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="application_date" name="application_date" type="date" value="{{$tombstone_appliction->application_date}}" readonly="" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant" name="applicant" type="text" value="{{$tombstone_appliction->applicant}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant_number" name="applicant_number" type="text" value="{{$tombstone_appliction->applicant_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="household_registration" name="household_registration" type="text" value="{{$tombstone_appliction->household_registration}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="tel" name="tel" type="text" value="{{$tombstone_appliction->tel}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="phone" name="phone" type="text"  value="{{$tombstone_appliction->phone}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="relationship" name="relationship" type="text" value="{{$tombstone_appliction->relationship}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">聯絡地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction->address}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">
            @if(is_file('Upload/tombstoneAppliction/'.$tombstone_appliction->file1))
                {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file1),50) !!}
            @endif
        </div>

    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">相片</label>
        <div class="col-sm-6">
            @if(is_file('Upload/tombstoneAppliction/'.$tombstone_appliction->file2))
                {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file2),50) !!}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            @if(is_file('Upload/tombstoneAppliction/'.$tombstone_appliction->file3))
                {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file3),50) !!}
            @endif
        </div>
    </div>
    
    <div class="form-group">
        <h4>牌位者資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_appliction_user_id" type="hidden" value="{{$tombstone_appliction_user->id}}">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者：</label>
        <div class="col-sm-3">
            {{-- tombstone_appliction_user --}}
            <input class="form-control" placeholder="" id="name" name="name" type="text" value="{{$tombstone_appliction_user->name}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位者身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="name_number" name="name_number" type="text"  value="{{$tombstone_appliction_user->name_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者戶籍地：</label>
        <div class="col-sm-9">

            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction_user->address}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">性別</label>
        <div class="col-sm-9">
            <input  type="radio" id="gender" name="gender" value='1' @if($tombstone_appliction_user->gender=='1') checked @else onclick="return false;" @endif>男
            <input  type="radio" id="gender" name="gender" value='2' @if($tombstone_appliction_user->gender=='2') checked @else onclick="return false;" @endif>女
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">身分</label>
        <div class="col-sm-9">
            <input  type="radio" name="identity" value="0" @if($tombstone_appliction_user->identity=='0') checked @else onclick="return false;" @endif>本鄉鄉民
            <input  type="radio" name="identity" value="1" @if($tombstone_appliction_user->identity=='1') checked @else onclick="return false;" @endif>本縣縣民
            <input  type="radio" name="identity" value="2" @if($tombstone_appliction_user->identity=='2') checked @else onclick="return false;" @endif>外縣市民
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">優待</label>
        <div class="col-sm-9">
            <select name="discount">
                @if($tombstone_appliction_user->discount=='1')<option value='1' selected >復興村13.14鄰 106.8.18以前優待80%</option>@endif
                @if($tombstone_appliction_user->discount=='2')<option value='2' selected >復興村13.14鄰 106.8.19以後優待50%</option>@endif
                @if($tombstone_appliction_user->discount=='3')<option value='3' selected >101.9.1~103.11.30入懷恩堂優待10%</option>@endif
                @if($tombstone_appliction_user->discount=='4')<option value='4' selected >曾設籍本鄉減免5%</option>@endif
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">出生日期：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="birthday" name="birthday" type="date" value="{{$tombstone_appliction_user->birthday}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">預計遷出日期：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="expected_date" name="expected_date" type="date" value="{{$tombstone_appliction_user->expected_date}}" readonly="">
        </div>
    </div>


    <div class="form-group">
        <h4>牌位資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_appliction_data_id" type="hidden" value="{{$tombstone_appliction_data->id}}">
    {{-- tombstone_appliction_data --}}
       <div id="sys_select_div">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">櫃位區域</label>
        <div class="col-sm-3">
            --
        </div>
        <label for="fname" class="col-sm-3 control-label">方位座向</label>
        <div class="col-sm-3">
            --
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">排</label>
        <div class="col-sm-3">
            --
        </div>
        <label for="fname" class="col-sm-3 control-label">位子</label>
        <div class="col-sm-3">
            --
        </div>
    </div>
    <div class="form-group"  >
        <label for="fname" class="col-sm-3 control-label">櫃位位子</label>
        <div class="col-sm-9">
            <input type="hidden" name="tombstone_code" value="{{$tombstone_appliction_data->tombstone_code}}">
                {{$tombstone_appliction_data->tombstone_code}}
        </div>
    </div>
</div>
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_principal_id" type="hidden" value="{{$tombstone_principal->id}}">
    {{-- tombstone_principal --}}
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client" name="client" type="text" value="{{$tombstone_principal->client}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_number" name="client_number" type="text" value="{{$tombstone_principal->client_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_household_registration" name="client_household_registration" value="{{$tombstone_principal->client_household_registration}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_tel" name="client_tel" type="text" value="{{$tombstone_principal->client_household_registration}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_phone" name="client_phone" type="text" value="{{$tombstone_principal->client_phone}}" readonly="">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">聯絡地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_address" name="client_address" type="text" value="{{$tombstone_principal->client_address}}" readonly="">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{$users->name}}
        </div>
    </div>

    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">執行人員：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="principal" name="principal" type="text" required=""  value="{{$tombstone_principal->principal}}>
        </div>
        <label for="fname" class="col-sm-3 control-label">出塔日期：</label>
        <div class="col-sm-3">
            <input class="form-control col-sm-9" placeholder="" id="change_date" name="change_date" type="date" required="" value="{{$tombstone_principal->change_date}}>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">退費金額：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="cost" name="cost" type="number" min='0' value='{{$tombstone_appliction_data->cost}}'  readonly="">
        </div>
    </div>
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50">{{$tombstone_appliction->ext}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-9">
            <button class="btn btn-success" type="submit">送出</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    function SaveData(){
        $.ajax({
            url: "{{ asset('BackTombstoneSysApi') }}",
            data: { 
                "position": $("#position").find("option:selected").val(),
                "floor": $("#floor").find("option:selected").val(),
                "class": $("#class").find("option:selected").val(),
                "row": $("#row").find("option:selected").val(),
             },
            dataType:"html",
            type: "get",
            success: function(data){
                $("#cabinet_code option").remove();
                $("#cabinet_code" ).append(data);
            },
            error: function(){
            }
        });
    }
</script>
@stop
