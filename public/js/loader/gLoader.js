// JavaScript Document
;(function($) {
    $.fn.gLoader = function(target) {
        var _loader = null;
        var _this   = null;

        _this = this;
        _loader = $('<div class="gLoader"></div>');
        $(this).append(_loader);

        this.loading = function() {
            $(_this).addClass('loading');
        }

        this.finish = function() {
            $(_this).removeClass('loading');
        }

        return this;
    };
})(jQuery);
