<?php
if(empty($data=Session::get('data'))){
    $data=DB::table('program_registration')->get();
    Session::put('data',$data);
}
// Home
Breadcrumbs::register('BackHome', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('BackHome.index'));
});


foreach ($data as $key => $value) {
    Breadcrumbs::register($value->program_name.'.index', function($breadcrumbs) use ($value)
    {
        $breadcrumbs->parent('BackHome');
        $breadcrumbs->push($value->name, route($value->program_name.'.index'));
    });
    Breadcrumbs::register($value->program_name.'.create', function($breadcrumbs) use ($value)
    {
        $breadcrumbs->parent($value->program_name.'.index');
        $breadcrumbs->push($value->name."新增", route($value->program_name.'.create'));
    });

    Breadcrumbs::register($value->program_name.'.store', function($breadcrumbs) use ($value)
    {
        $breadcrumbs->parent($value->program_name.'.store');
        $breadcrumbs->push($value->name."新增", route($value->program_name.'.store'));
    });
    Breadcrumbs::register($value->program_name.'.edit', function($breadcrumbs,$id) use ($value)
    {
        $breadcrumbs->parent($value->program_name.'.index');
        $breadcrumbs->push($value->name."修改", route($value->program_name.'.edit',$id));//
    });

    Breadcrumbs::register($value->program_name.'.show', function($breadcrumbs,$id) use ($value)
    {
        $breadcrumbs->parent($value->program_name.'.index');
        $breadcrumbs->push($value->name."詳細", route($value->program_name.'.show',$id));
    });

}