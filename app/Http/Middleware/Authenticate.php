<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Route;

class Authenticate {
	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;
    protected $adminPrefix = 'Back';

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth) {
		$this->auth = $auth;
	}
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if ($this->auth->guest()) {
			if ($request->ajax()) {
				return response('Unauthorized.', 401);
			} else {
				return redirect()->guest('BackLogin');
			}
		} else {
            // 如果使用者已經登入，就判斷目的路徑是不是後台，是就再檢查使用者是不是管理員，不是就導回前台
            if (preg_match('/'.$this->adminPrefix.'.*$/i', Route::getCurrentRoute()->getPath())) {
                $user = $this->auth->user();
                if ($user->type != 'admin') {
                    return redirect('Home');
                }    
            }
        }

		return $next($request);
	}
}
