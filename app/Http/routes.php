<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */
Route::get('/', function () {
    return Redirect::to('BackLogin');
});
Route::get('BBanner', ['as' => 'Back.Banner', 'uses' => 'Reception\Reception\ReceptionController@Banner']);

Route::resource('BackLogin', 'admin\BackLoginController@index');

//vvvvvvvvvvvvvvvvv auth link vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
Route::group(['middleware' => ['auth', 'sessiontimeout']], function () {
    Route::get('BackHome', ['as' => 'BackHome.index', 'uses' => 'admin\BackLoginController@Home',function(){
        return view('BackHome');
    }]);
    Route::group(['middleware' => ['acl']], function () {
        //^^^^^^^^^^^^^^^^基礎資料


        //log管理
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        //服務公告管理
        Route::resource('BackAnnouncementInformation', 'admin\PublicInformation\BackAnnouncementInformationController');
        //公告分類管理
        Route::resource('BackAnnouncementInformationClass', 'admin\PublicInformation\BackAnnouncementInformationClassController');
        //活動花絮
        Route::resource('BackBanner', 'admin\PublicInformation\BackBannerController');
        //塔位查詢
        Route::resource('BackTowerSelect', 'admin\TowerSelect\BackTowerSelectController');
        //ui
        Route::resource('BackUi', 'admin\Select\BackUiController');
        Route::resource('api_select', 'admin\Select\BackUiController@api_select');
        Route::get('api_select_data', ['as' => 'api_select_data', 'uses' => 'admin\Select\BackUiController@api_select_data']);
        Route::get('api_select_data_t', ['as' => 'api_select_data_t', 'uses' => 'admin\Select\BackUiController@api_select_data_t']);

        
        Route::get('api_select_data_s', ['as' => 'api_select_data_s', 'uses' => 'admin\Select\BackUiController@api_select_data_s']);
        //各類櫃位及牌位使用情形及數量統計  
        Route::resource('BackPaper', 'admin\Paper\BackPaperController');
        //各項收入統計
        Route::get('BackPaperShow', ['as' => 'BackPaperShow', 'uses' => 'admin\Paper\BackPaperController@table']);
        Route::resource('BackPaper', 'admin\Paper\BackPaperController');

        Route::resource('BackPaperMoneySum', 'admin\Paper\BackPaperMoneySumController');

        
        Route::get('BackPaperMoneySumShow', ['as' => 'BackPaperMoneySumShow', 'uses' => 'admin\Paper\BackPaperMoneySumController@table']);

        Route::get('BackPaperMoneyDetailSumShow', ['as' => 'BackPaperMoneyDetailSumShow', 'uses' => 'admin\Paper\BackPaperMoneyDetailSumController@table']);



        //各項新增收入及退費金額統計
        Route::resource('BackPaperMoneyDetail', 'admin\Paper\BackPaperMoneyDetailSumController');
        //牌位查詢
        Route::resource('BackTombstoneSelect', 'admin\Select\BackTombstoneSelectController');
        
        Route::get('BackTombstoneSelectChangeSelect', ['as' => 'BackTombstone.ChangeSelect', 'uses' => 'admin\Select\BackTombstoneSelectController@change_select']);

        //櫃位查詢
        Route::resource('BackCabinet', 'admin\Select\BackCabinetController');
        //貴為查詢條件變動
        Route::get('BackCabinetSelect', ['as' => 'BackCabinet.ChangeSelect', 'uses' => 'admin\Select\BackCabinetController@change_select']);
        Route::get('BackCabinetCalculate', ['as' => 'BackCabinet.BackCabinetCalculate', 'uses' => 'admin\CabinetAppliction\BackCabinetApplictionController@calculate']);
        Route::get('BackCabinetChangeCalculate', ['as' => 'BackCabinet.BackCabinetChangeCalculate', 'uses' => 'admin\CabinetAppliction\BackCabinetChangeApplictionController@change_calculate']);

        //圖片刪除
        Route::get('BackRemove', ['as' => 'Back.RemoveFile', 'uses' => 'admin\RemoveFileController@remove_file']);

        //帳號管理
        Route::resource('BackUserManagement', 'admin\Management\BackUserManagementController');
        //role管理
        Route::resource('BackRole', 'admin\management\role\BackRoleController'); 
        //程式註冊
        Route::resource('BackRoleControls', 'admin\Management\role\BackRoleControlsController'); 
        //角色維護
        Route::resource('BackProgramRegistration', 'admin\System\BackProgramRegistrationController');
        //選單註冊
        Route::resource('BackMenuClass', 'admin\System\BackMenuClassController');
        //櫃位管理
        Route::resource('BackCabinetSys', 'admin\CabinetSys\BackCabinetSysController');
        //櫃位管理ajax
        Route::get('BackCabinetSysAddClass',['as'=>'BackCabinetSys.AddClass','uses'=>'admin\CabinetSys\BackCabinetSysController@add_class']);

        Route::get('BackCabinetSysUpdateClass',['as'=>'BackCabinetSys.UpdateClass','uses'=>'admin\CabinetSys\BackCabinetSysController@update_class']);

        Route::get('BackCabinetSysDeleteClass',['as'=>'BackCabinetSys.DeleteClass','uses'=>'admin\CabinetSys\BackCabinetSysController@delete_class'] );

        Route::get('BackCabinetSysGetClass',['as'=>'BackCabinetSys.GetClass','uses'=>'admin\CabinetSys\BackCabinetSysController@get_class'] );

        Route::get('BackCabinetSysChangeStatus',['as'=>'BackCabinetSys.ChangeStatus','uses'=>'admin\CabinetSys\BackCabinetSysController@change_status'] );

        Route::get('BackApplictionC',['as'=>'BackApplictionC.delete','uses'=>'admin\BackApplictionController@c_delete'] );
        Route::get('BackApplictionT',['as'=>'BackApplictionT.delete','uses'=>'admin\BackApplictionController@t_delete'] );

        //櫃位管理ajax end
        //建立櫃位
        Route::get('BackCabinetSysCreateCabinetSys',['as'=>'BackCabinetSys.CreateCabinetSys','uses'=>'admin\CabinetSys\BackCabinetSysController@create_cabinet_sys'] );
        //建立櫃位end
        


        //櫃位開啟/關閉
        Route::get('BackCabinetSysCahngeStatus',['as'=>'BackCabinetSys.CahngeStatus','uses'=>'admin\CabinetSys\BackCabinetSysController@CahngeStatus'] );
        //櫃位費用管理
        Route::resource('BackCabinetCostDiscount', 'admin\CabinetCost\BackCabinetCostDiscountController');
        //取得層
        Route::get('BackCabinetCostDiscountGetRow',['as'=>'BackCabinetCostDiscount.GetRow','uses'=>'admin\CabinetCost\BackCabinetCostDiscountController@get_row']);

        Route::get('BackCabinetCostDiscountGetCost',['as'=>'BackCabinetCostDiscount.GetCost','uses'=>'admin\CabinetCost\BackCabinetCostDiscountController@get_cost']);
        //櫃位其他費用管理
        Route::resource('BackCabinetOtherCost', 'admin\CabinetCost\BackCabinetOtherCostController');
        //牌位管理
        Route::resource('BackTombstoneSys', 'admin\TombstoneSys\BackTombstoneSysController');
        //牌位費用設定
        Route::resource('BackTombstoneCost', 'admin\TombstoneCost\BackTombstoneCostController');
        //牌位其他費用設定
        Route::resource('BackTombstoneOtherCost', 'admin\TombstoneCost\BackTombstoneOtherCostController');

        //牌位開啟/關閉
        Route::get('BackTombstoneSysCahngeStatus',['as'=>'BackTombstone.CahngeStatus','uses'=>'admin\TombstoneSys\BackTombstoneSysController@cahnge_status'] );
        //牌位管理ajax
        Route::get('BackTombstoneSysAddClass',['as'=>'BackTombstone.AddClass','uses'=>'admin\TombstoneSys\BackTombstoneSysController@add_class']);

        Route::get('BackTombstoneSysUpdateClass',['as'=>'BackTombstone.UpdateClass','uses'=>'admin\TombstoneSys\BackTombstoneSysController@update_class']);

        Route::get('BackTombstoneSysDeleteClass',['as'=>'BackTombstone.DeleteClass','uses'=>'admin\TombstoneSys\BackTombstoneSysController@delete_class'] );

        Route::get('BackTombstoneSysGetClass',['as'=>'BackTombstone.GetClass','uses'=>'admin\TombstoneSys\BackTombstoneSysController@get_class'] );

        Route::get('BackTombstoneSysChangeStatus',['as'=>'BackTombstone.ChangeStatus','uses'=>'admin\TombstoneSys\BackTombstoneSysController@change_status'] );
        //牌位管理ajax end
        //建立櫃位
        Route::get('BackTombstoneSysCreateTombstoneSys',['as'=>'BackTombstone.CreateTombstone','uses'=>'admin\TombstoneSys\BackTombstoneSysController@create_sys'] );
        //建立櫃位end
        //逝者管理
        Route::resource('BackDeceasedManger', 'admin\DeceasedManger\BackDeceasedMangerController'); 
        //逝者管理
        Route::resource('BackDeceasedManger2', 'admin\DeceasedManger\BackDeceasedManger2Controller');
        Route::get('BackCabinetApplictionApi', 'admin\CabinetSys\BackCabinetSysController@api');
        Route::get('BackCabinetApplictionPdf', 'admin\CabinetAppliction\BackCabinetApplictionController@Pdf');
        Route::get('BackCabinetApplictionPdf2', 'admin\CabinetAppliction\BackCabinetApplictionController@Pdf2');
        Route::get('BackCabinetApplictionPdf3', 'admin\CabinetAppliction\BackCabinetApplictionController@Pdf3');
        Route::get('BackCabinetApplictionPdf4', 'admin\CabinetAppliction\BackCabinetApplictionController@Pdf4');

        Route::get('BackTombstoneApplictionPdf', 'admin\TombstoneAppliction\BackTombstoneApplictionController@Pdf');
        Route::get('BackTombstoneApplictionPdf2', 'admin\TombstoneAppliction\BackTombstoneApplictionController@Pdf2');
        Route::get('BackTombstoneApplictionPdf3', 'admin\TombstoneAppliction\BackTombstoneApplictionController@Pdf3');
        Route::get('BackTombstoneApplictionPdf4', 'admin\TombstoneAppliction\BackTombstoneApplictionController@Pdf4');


        Route::get('BackTombstoneSysApi', 'admin\TombstoneSys\BackTombstoneSysController@api');
        //櫃位申請流程
        Route::resource('BackCabinetAppliction', 'admin\CabinetAppliction\BackCabinetApplictionController'); 
        //櫃位異動申請流程
        Route::resource('BackCabinetChangeAppliction', 'admin\CabinetAppliction\BackCabinetChangeApplictionController'); 
        Route::get('BackCabinetChangeGetNo',['as'=>'BackCabinetSys.GetNo','uses'=>'admin\CabinetAppliction\BackCabinetChangeApplictionController@get_no'] );
        Route::get('BackTombstoneOutGetNo',['as'=>'BackTombstoneSys.GetNo','uses'=>'admin\TombstoneAppliction\BackTombstoneOutApplictionController@get_no'] );
        //查詢身份證字號
        Route::get('BackGetData','admin\CabinetAppliction\BackCabinetApplictionController@get_data');
        //櫃位遷出申請流程
        Route::resource('BackCabinetOutAppliction', 'admin\CabinetAppliction\BackCabinetOutApplictionController'); 
        Route::get('BackCabinetOutGetNo',['as'=>'BackCabinetSys.GetNo','uses'=>'admin\CabinetAppliction\BackCabinetOutApplictionController@get_no'] );

        //牌位申請流程
        Route::resource('BackTombstoneAppliction', 'admin\TombstoneAppliction\BackTombstoneApplictionController'); 
        //查詢身份證字號
        Route::get('BackGetDataT','admin\TombstoneAppliction\BackTombstoneApplictionController@get_data');
        //牌位異動申請流程
        Route::resource('BackTombstoneChangeAppliction', 'admin\TombstoneAppliction\BackTombstoneChangeApplictionController'); 
        //牌位遷出申請流程
        Route::resource('BackTombstoneOutAppliction', 'admin\TombstoneAppliction\BackTombstoneOutApplictionController'); 

        Route::get('OrientationData', ['as' => 'Admin.OrientationData', 'uses' => 'admin\Orientation\BackOrientationController@change_data']);

        Route::get('BackCabinetApplictionGetArea', 'admin\CabinetAppliction\BackCabinetApplictionController@change_area');

        Route::get('BackTombstoneApplictionGetArea', 'admin\TombstoneAppliction\BackTombstoneApplictionController@change_area');

        Route::get('BackTombstoneApplictionCalculate', 'admin\TombstoneAppliction\BackTombstoneApplictionController@calculate');

        Route::group(['namespace' => 'admin'], function () {
            Route::get('BackReturnList/ReturnCheck', ['as' => 'ReturnList.update', 'uses' => 'ReturnList\BackReturnListController@return_checkon']);
            Route::get('BackReturnList/CancelReturn', ['as' => 'ReturnList.update', 'uses' => 'ReturnList\BackReturnListController@cancel_return']);
        });

    });
});
//^^^^^^^^^^^^^^^^^ auth link ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Route::group(['prefix' => 'auth'], function () {
    //註冊
    Route::get('/register', ['as' => 'register.index', 'uses' => 'Auth\AuthController@getregister']);
    Route::post('/register', ['as' => 'register.process', 'uses' => 'Auth\AuthController@Postregister']);
});
//登入
Route::post('Login', ['as' => 'Auth.authenticate', 'uses' => 'Auth\AuthController@authenticate']);
//登出
Route::get('Logout', ['as' => 'BackLogin.Logout', 'uses' => 'admin\BackLoginController@Logout']);
