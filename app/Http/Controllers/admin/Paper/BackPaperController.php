<?php

namespace App\Http\Controllers\admin\Paper;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\PaperRequest;
use DB;
use Input;
use Session;
class BackPaperController extends BackController {
    //配合route resource
    protected $RouteName="Paper";
    //檔案路徑
    protected $Path="Upload/Paper/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\PublicInformation\Paper";

    //主要table
    protected $Table1="tower_select";
    //關聯table
    protected $Table2="roles";
    
    public function index()
    {
        return view('admin.Table.view',['Url'=>'BackPaperShow']);
    }

    public function table()
    {
        $new_data=array();
        $new_data2=array();
        $new_data3=array();
        $serach=Input::all();
        $new_data_count=0;
        $new_data_count_no=0;
        $new_data_count2=0;
        $new_data_count_no2=0;
        $new_data_count3=0;
        $new_data_count_no3=0;
        $new_data_count4=0;
        $new_data_count_no4=0;
        $new_data_count5=0;
        $new_data_count_no5=0;
        $new_data_count6=0;
        $new_data_count_no6=0;
        $new_data_count7=0;
        $new_data_count_no7=0;

        $a_status_100=0;
        $a_status_200=0;
        $a_status_300=0;
        $a_status_400=0;
        $a_status_500=0;
        $a_status_600=0;
        $a_status_700=0;
        $a_status_800=0;

        $b_status_100=0;
        $b_status_200=0;
        $b_status_300=0;
        $b_status_400=0;
        $b_status_500=0;
        $b_status_600=0;
        $b_status_700=0;
        $b_status_800=0;


        $c_status_100=0;
        $c_status_200=0;
        $c_status_300=0;
        $c_status_400=0;
        $c_status_500=0;
        $c_status_600=0;
        $c_status_700=0;
        $c_status_800=0;

        $d_status_100=0;
        $d_status_200=0;
        $d_status_300=0;
        $d_status_400=0;
        $d_status_500=0;
        $d_status_600=0;
        $d_status_700=0;
        $d_status_800=0;

        $e_status_100=0;
        $e_status_200=0;
        $e_status_300=0;
        $e_status_400=0;
        $e_status_500=0;
        $e_status_600=0;
        $e_status_700=0;
        $e_status_800=0;

        $f_status_100=0;
        $f_status_200=0;
        $f_status_300=0;
        $f_status_400=0;
        $f_status_500=0;
        $f_status_600=0;
        $f_status_700=0;
        $f_status_800=0;

       $g_status_100=0;
       $g_status_200=0;
       $g_status_300=0;
       $g_status_400=0;
       $g_status_500=0;
       $g_status_600=0;
       $g_status_700=0;
       $g_status_800=0;

        $data=db::table('cabinet_sys')->get();
        foreach ($data as $key => $value) {
            $sort=explode('-', $value->code);
            $new_data[$sort[1]][$sort[2]][$sort[3]][]=$value; 

        }
        foreach ($new_data as $key => $value) {
            if($key=='A'||$key=='B'||$key=='C'||$key=='D'){
                foreach ($value as $key2 => $value2) {
                    foreach ($value2 as $key3 => $value3) {
                        foreach ($value3 as $key4 => $value4) {
                            switch ($key3) {
                                case 1:
                                    if($value4->c_no!=''){
                                        $new_data_count=$new_data_count+1;   
                                    }else{
                                        $new_data_count_no=$new_data_count_no+1;   
                                    }
                                    break;
                                case 2:
                                    if($value4->c_no!=''){
                                        $new_data_count=$new_data_count+1;   
                                    }else{
                                        $new_data_count_no=$new_data_count_no+1;   
                                    }
                                    break;
                                default:
                                    if($value4->c_no!=''){
                                        $new_data_count2=$new_data_count2+1;   
                                    }else{
                                        $new_data_count_no2=$new_data_count_no2+1;   
                                    }
                                    break;
                            }
                        }
                    }

                }

            }elseif($key=='F'||$key=='G'||$key=='H'){
                foreach ($value as $key2 => $value2) {
                    foreach ($value2 as $key3 => $value3) {
                        foreach ($value3 as $key4 => $value4) {
                            switch ($key3) {
                                case 1:
                                    
                                    if($value4->c_no!=''){
                                        $new_data_count3=$new_data_count3+1;   
                                    }else{
                                        $new_data_count_no3=$new_data_count_no3+1;   
                                    }
                                    break;
                                case 2:

                                    if($value4->c_no!=''){
                                        $new_data_count3=$new_data_count3+1;   
                                    }else{
                                        $new_data_count_no3=$new_data_count_no3+1;   
                                    }
                                    break;
                                default:
                                    if($value4->c_no!=''){
                                        $new_data_count4=$new_data_count4+1;   
                                    }else{
                                        $new_data_count_no4=$new_data_count_no4+1;   
                                    }
                                    break;
                            }
                        }
                    }
                }
            }elseif($key=='K'||$key=='J'){
                foreach ($value as $key2 => $value2) {
                    foreach ($value2 as $key3 => $value3) {
                        foreach ($value3 as $key4 => $value4) {
                            if($value4->c_no!=''){
                                $new_data_count5=$new_data_count5+1;   
                            }else{
                                $new_data_count_no5=$new_data_count_no5+1;   
                            }
                        }
                    }
                }
            }
        }
        $data2=db::table('cabinet_appliction')
        ->whereBetween('application_date',[$serach['application_start_date'],$serach['application_end_date']])->wherenull('deleted_at')->groupBy('no')->get();
        foreach ($data2 as $key => $value) {
            $code=db::table('cabinet_sys')->where('c_no',$value->id)->first();
            if($value->status==800||$value->status==700){
                $code=db::table('cabinet_appliction_data')->select('cabinet_code as code')->where('c_no',$value->id)->first();
            }
            if(empty($code->code)){
                continue;
            }
            $sort=explode('-', $code->code);
            if(empty($new_data2[$sort[1]][$sort[3]][$value->status])){
                $new_data2[$sort[1]][$sort[3]][$value->status]=0;
            }
            $new_data2[$sort[1]][$sort[3]][$value->status]+=1; 
        }
        foreach ($new_data2 as $key => $value) {
            if($key=='A'||$key=='B'||$key=='C'||$key=='D'){
                foreach ($value as $key2 => $value2) {
                    if($key2==1 or $key2==2){
                        foreach ($value2 as $key3 => $value3) {
                            switch ($key3) {
                                case 100:
                                    $a_status_100=$a_status_100+$value2['100'];
                                    break;
                                case 200:
                                    $a_status_200=$a_status_200+$value2['200'];
                                    break;
                                case 300:
                                    $a_status_300=$a_status_300+$value2['300'];
                                    break;
                                case 350:
                                    $a_status_300=$a_status_300+$value2['350'];
                                    break;
                                case 399:
                                    $a_status_700=$a_status_700+$value2['399'];
                                    break;
                                case 400:
                                    $a_status_400=$a_status_400+$value2['400'];
                                    break;
                                case 500:
                                    $a_status_500=$a_status_500+$value2['500'];
                                    break;
                                case 600:
                                    $a_status_600=$a_status_600+$value2['600'];
                                    break;
                                case 650:
                                    $a_status_600=$a_status_600+$value2['650'];
                                    break;
                                case 699:
                                    $a_status_600=$a_status_600+$value2['699'];
                                    break;
                                case 700:
                                    $a_status_700=$a_status_700+$value2['700'];
                                    break;
                                case 800:
                                    $a_status_800=$a_status_800+$value2['800'];
                                    break;
                            }
                        }
                    }else{
                        foreach ($value2 as $key3 => $value3) {
                            switch ($key3) {
                                case 100:
                                    $b_status_100=$b_status_100+$value2['100'];
                                    break;
                                case 200:
                                    $b_status_200=$b_status_200+$value2['200'];
                                    break;
                                case 300:
                                    $b_status_300=$b_status_300+$value2['300'];
                                    break;
                                case 350:
                                    $b_status_300=$b_status_300+$value2['350'];
                                    break;
                                case 399:
                                    $b_status_700=$b_status_700+$value2['399'];
                                    break;
                                case 400:
                                    $b_status_400=$b_status_400+$value2['400'];
                                    break;
                                case 500:
                                    $b_status_500=$b_status_500+$value2['500'];
                                    break;
                                case 600:
                                    $b_status_600=$b_status_600+$value2['600'];
                                    break;
                                case 650:
                                    $b_status_600=$b_status_600+$value2['650'];
                                    break;
                                case 699:
                                    $b_status_600=$b_status_600+$value2['699'];
                                    break;
                                case 700:
                                    $b_status_700=$b_status_700+$value2['700'];
                                    break;
                                case 800:
                                    $b_status_800=$b_status_800+$value2['800'];
                                    break;
                            }
                        }
                    }
                }

            }elseif($key=='F'||$key=='G'||$key=='H'){
                foreach ($value as $key2 => $value2) {
                    if($key2==1 or $key2==2){
                        foreach ($value2 as $key3 => $value3) {
                            switch ($key3) {
                                case 100:
                                    $c_status_100=$c_status_100+$value2['100']*2;
                                    break;
                                case 200:
                                    $c_status_200=$c_status_200+$value2['200']*2;
                                    break;
                                case 300:
                                    $c_status_300=$c_status_300+$value2['300']*2;
                                    break;
                                case 350:
                                    $c_status_300=$c_status_300+$value2['350']*2;
                                    break;
                                case 399:
                                    $c_status_700=$c_status_700+$value2['399']*2;
                                    break;
                                case 400:
                                    $c_status_400=$c_status_400+$value2['400']*2;
                                    break;
                                case 500:
                                    $c_status_500=$c_status_500+$value2['500']*2;
                                    break;
                                case 600:
                                    $c_status_600=$c_status_600+$value2['600']*2;
                                    break;
                                case 650:
                                    $c_status_600=$c_status_600+$value2['650']*2;
                                    break;
                                case 699:
                                    $c_status_600=$c_status_600+$value2['699']*2;
                                    break;
                                case 700:
                                    $c_status_700=$c_status_700+$value2['700']*2;
                                    break;
                                case 800:
                                    $c_status_800=$c_status_800+$value2['800']*2;
                                    break;
                            }
                        }
                    }else{
                        foreach ($value2 as $key3 => $value3) {
                            switch ($key3) {
                                case 100:
                                    $d_status_100=$d_status_100+$value2['100']*2;
                                    break;
                                case 200:
                                    $d_status_200=$d_status_200+$value2['200']*2;
                                    break;
                                case 300:
                                    $d_status_300=$d_status_300+$value2['300']*2;
                                    break;
                                case 350:
                                    $d_status_300=$d_status_300+$value2['350']*2;
                                    break;
                                case 399:
                                    $d_status_700=$d_status_700+$value2['399']*2;
                                    break;
                                case 400:
                                    $d_status_400=$d_status_400+$value2['400']*2;
                                    break;
                                case 500:
                                    $d_status_500=$d_status_500+$value2['500']*2;
                                    break;
                                case 600:
                                    $d_status_600=$d_status_600+$value2['600']*2;
                                    break;
                                case 650:
                                    $d_status_600=$d_status_600+$value2['650']*2;
                                    break;
                                case 699:
                                    $d_status_600=$d_status_600+$value2['699']*2;
                                    break;
                                case 700:
                                    $d_status_700=$d_status_700+$value2['700']*2;
                                    break;
                                case 800:
                                    $d_status_800=$d_status_800+$value2['800']*2;
                                    break;
                            }
                        }
                    }
                }
            }elseif($key=='K'||$key=='J'){
                 foreach ($value as $key2 => $value2) {
                    foreach ($value2 as $key3 => $value3) {
                        switch ($key3) {
                            case 100:
                                $e_status_100=$e_status_100+$value2['100']*12;
                                break;
                            case 200:
                                $e_status_200=$e_status_200+$value2['200']*12;
                                break;
                            case 300:
                                $e_status_300=$e_status_300+$value2['300']*12;
                                break;
                            case 350:
                                $e_status_300=$e_status_300+$value2['350']*12;
                                break;
                            case 399:
                                $e_status_700=$e_status_700+$value2['399']*12;
                                break;
                            case 400:
                                $e_status_400=$e_status_400+$value2['400']*12;
                                break;
                            case 500:
                                $e_status_500=$e_status_500+$value2['500']*12;
                                break;
                            case 600:
                                $e_status_600=$e_status_600+$value2['600']*12;
                                break;
                            case 650:
                                $e_status_600=$e_status_600+$value2['650']*12;
                                break;
                            case 699:
                                $e_status_600=$e_status_600+$value2['699']*12;
                                break;
                            case 700:
                                $e_status_700=$e_status_700+$value2['700']*12;
                                break;
                            case 800:
                                $e_status_800=$e_status_800+$value2['800']*12;
                                break;
                        }
                    }
                }
            }
        }
        $data2=db::table('tombstone_appliction')
        ->whereBetween('application_date',[$serach['application_start_date'],$serach['application_end_date']])->wherenull('deleted_at')->groupBy('no')->get();
        foreach ($data2 as $key => $value) {
            $code=db::table('tombstone_sys')->where('c_no',$value->id)->first();
            if($value->status==800){
                $code=db::table('tombstone_appliction_data')->select('tombstone_code as code')->where('c_no',$value->id)->first();
            }

            if(empty($code->code)){
                continue;
            }
            
            $sort=explode('-', $code->code);
            if(empty($new_data3[$sort[1]][$value->status])){
                $new_data3[$sort[1]][$value->status]=0;
            }
            $new_data3[$sort[1]][$value->status]+=1; 

        }
        foreach ($new_data3 as $key => $value) {
            if($key=='N'){
                foreach ($value as $key2 => $value2) {
                    switch ($key2) {
                        case 100:
                            $f_status_100=$value2;
                            break;
                        case 200:
                            $f_status_200=$value2;
                            break;
                        case 300:
                            $f_status_300=$value2;
                            break;
                        case 350:
                            $f_status_300=$value2;
                            break;
                        case 399:
                            $f_status_700=$value2;
                            break;
                        case 400:
                            $f_status_400=$value2;
                            break;
                        case 500:
                            $f_status_500=$value2;
                            break;
                        case 600:
                            $f_status_600=$value2;
                            break;
                        case 700:
                            $f_status_700=$value2;
                            break;
                        case 800:
                            $f_status_800=$value2;
                            break;
                    }
                }
            }elseif($key=='S'){
                foreach ($value as $key2 => $value2) {
                    switch ($key2) {
                        case 100:
                            $g_status_100=$value2;
                            break;
                        case 200:
                            $g_status_200=$value2;
                            break;
                        case 300:
                            $g_status_300=$value2;
                            break;
                        case 350:
                            $g_status_300=$value2;
                            break;
                        case 399:
                            $g_status_700=$value2;
                            break;
                        case 400:
                            $g_status_400=$value2;
                            break;
                        case 500:
                            $g_status_500=$value2;
                            break;
                        case 600:
                            $g_status_600=$value2;
                            break;
                        case 700:
                            $g_status_700=$value2;
                            break;
                        case 800:
                            $g_status_800=$value2;
                            break;
                    }
                }
                                
            }
        }
        $new_data1=array($a_status_100,$a_status_200,$a_status_300,$a_status_400,$a_status_500,$a_status_600,$a_status_700,$a_status_800);
        $new_data2=array($b_status_100,$b_status_200,$b_status_300,$b_status_400,$b_status_500,$b_status_600,$b_status_700,$b_status_800);
        $new_data3=array($c_status_100,$c_status_200,$c_status_300,$c_status_400,$c_status_500,$c_status_600,$c_status_700,$c_status_800);
        $new_data4=array($d_status_100,$d_status_200,$d_status_300,$d_status_400,$d_status_500,$d_status_600,$d_status_700,$d_status_800);
        $new_data5=array($e_status_100,$e_status_200,$e_status_300,$e_status_400,$e_status_500,$e_status_600,$e_status_700,$e_status_800);
        $new_data6=array($f_status_100,$f_status_200,$f_status_300,$f_status_400,$f_status_500,$f_status_600,$f_status_700,$f_status_800);
        $new_data7=array($g_status_100,$g_status_200,$g_status_300,$g_status_400,$g_status_500,$g_status_600,$g_status_700,$g_status_800);
        $new_data=array($new_data1,$new_data2,$new_data3,$new_data4,$new_data5,$new_data6,$new_data7);
        $data2=db::table('tombstone_sys')->get();
        foreach ($data2 as $key => $value) {
            $sort=explode('-', $value->code);
                if($value->number!=''){
                   $new_data333[$sort[1]][$sort[3]][$value->status][]=$value; 
               }else{
                   $new_data333[$sort[1]][$sort[3]][$value->status][]=$value; 
               }
         }
        foreach ($new_data333 as $key => $value) {
            if($key=='S'){
                foreach ($value as $key2 => $value2) {
                    foreach ($value2 as $key3 => $value3) {
                        foreach ($value3 as $key4 => $value4) {
                            if($value4->c_no!=''){
                                $new_data_count6=$new_data_count6+1;   
                            }else{
                                $new_data_count_no6=$new_data_count_no6+1;   
                            }
                        }
                    }
                }
            }
            if($key=='N'){
                foreach ($value as $key2 => $value2) {
                    foreach ($value2 as $key3 => $value3) {
                        foreach ($value3 as $key4 => $value4) {
                            if($value4->c_no!=''){
                                $new_data_count7=$new_data_count7+1;   
                            }else{
                                $new_data_count_no7=$new_data_count_no7+1;   
                            }
                        }
                    }

                }

            }
        }


        $new_data_count_amount=array($new_data_count,$new_data_count2,$new_data_count3,$new_data_count4,$new_data_count5,$new_data_count6,$new_data_count7);
        $new_data_count_no_amount=array($new_data_count_no,$new_data_count_no2,$new_data_count_no3,$new_data_count_no4,$new_data_count_no5,$new_data_count_no6,$new_data_count_no7);


        return view('admin.Table.index2',['new_data'=>$new_data,'new_data2'=>$new_data,'new_data_count_amount'=>$new_data_count_amount,'new_data_count_no_amount'=>$new_data_count_no_amount,'serach'=>$serach]);
    }
}