<?php

namespace App\Http\Controllers\admin\Paper;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\PaperMoneyDetailSumRequest;
use DB;
use Input;
use Session;
class BackPaperMoneyDetailSumController extends BackController {
    //配合route resource
    protected $RouteName="PaperMoneyDetailSum";
    //檔案路徑
    protected $Path="Upload/PaperMoneyDetailSum/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\PublicInformation\PaperMoneyDetailSum";

    //主要table
    protected $Table1="tower_select";
    //關聯table
    protected $Table2="roles";
    public function index()
    {
        return view('admin.Table.view',['Url'=>'BackPaperMoneyDetailSumShow    ']);
    }

    public function table()
    {
        $new_data=array();
        $new_data2=array();
        $new_data_year=array();
        $new_data2_year=array();
        $new_data_total=array();
        $new_data2_total=array();
        $serach=Input::all();
        $data=db::table('pd_log')
            ->select('pd_log.no','pd_log.status as log_status','pd_log.code as code','pd_log.application_date')
            ->whereIn('pd_log.status',['800'])
            ->whereBetween('pd_log.application_date',[$serach['application_start_date'],$serach['application_end_date']])
            ->groupBy('pd_log.no')
            ->get();
        $i=array();
        $ii=array();
        $iii=array();
        foreach ($data as $key => $value) {
            if($value->log_status==800){
                $select_data=db::table('cabinet_appliction')->whereNull('deleted_at')->where('no',$value->no)->first();
                if(!$select_data){
                    continue;   
                }
                $value->cost=$select_data->cost;
                $value->other_amount_due=$select_data->other_amount_due;
                $value->identity=db::table('cabinet_appliction_user')->where('c_no',$select_data->id)->first()->identity;

                $sort=explode('-', $value->code);
                if(empty($new_data['S']['1-2'][$value->log_status][$value->identity]['cost'])){
                    $new_data['S']['1-2'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($new_data['S']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data['S']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }

                if(empty($new_data['C']['1-2'][$value->log_status][$value->identity]['cost'])){
                    $new_data['C']['1-2'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($new_data['C']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data['C']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }

                if(empty($new_data['F']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data['F']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($ii[$value->no])){
                    $ii[$value->no]=0;
                }
                if(empty($iii[$value->no])){
                    $iii[$value->no]=0;
                }
                if(empty($i[$value->no])){
                    $i[$value->no]=0;
                }
                if($sort[3]==1||$sort[3]==2){
                    if($sort[1]=='A'||$sort[1]=='B'||$sort[1]=='C'||$sort[1]=='D'){
                        if($i[$value->no]==1){
                           continue;
                        }
                        $new_data['S']['1-2'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $i[$value->no]=$i[$value->no]+1;
                    }elseif($sort[1]=='F'||$sort[1]=='G'||$sort[1]=='H'){
                        if($ii[$value->no]==1){
                           continue;
                        }
                        $new_data['C']['1-2'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $ii[$value->no]=$ii[$value->no]+1;
                    }else{
                        if($iii[$value->no]==1){
                           continue;
                        }
                        $new_data['F']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $iii[$value->no]=$iii[$value->no]+1;
                    }
                }else{
                    if($sort[1]=='A'||$sort[1]=='B'||$sort[1]=='C'||$sort[1]=='D'){
                        if($i[$value->no]==1){
                           continue;
                        }
                        $new_data['S']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $i[$value->no]=$i[$value->no]+1;
                    }elseif($sort[1]=='F'||$sort[1]=='G'||$sort[1]=='H'){
                        if($ii[$value->no]==1){
                           continue;
                        }
                        $new_data['C']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $ii[$value->no]=$ii[$value->no]+1;
                    }else{
                        if($iii[$value->no]==1){
                           continue;
                        }
                        $new_data['F']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $iii[$value->no]=$iii[$value->no]+1;
               
                    }
               
                }
            }
        }

        $year=date("Y",time()) -1911;
        $first=$year."-01-01";
        $end=$year."-12-31";
        $data_year=db::table('pd_log')
            ->select('pd_log.no','pd_log.status as log_status','pd_log.code as code','pd_log.application_date')
            ->whereIn('pd_log.status',['800'])
            ->whereBetween('pd_log.application_date',[$first,$end])
            ->groupBy('pd_log.no')
            ->get();
        $i=array();
        $ii=array();
        $iii=array();
        foreach ($data_year as $key => $value) {
            if($value->log_status==800){
                $select_data=db::table('cabinet_appliction')->whereNull('deleted_at')->where('no',$value->no)->first();
                if(!$select_data){
                    continue;   
                }
                $value->cost=$select_data->cost;
                $value->other_amount_due=$select_data->other_amount_due;
                $value->identity=db::table('cabinet_appliction_user')->where('c_no',$select_data->id)->first()->identity;

                $sort=explode('-', $value->code);
                if(empty($new_data_year['S']['1-2'][$value->log_status][$value->identity]['cost'])){
                    $new_data_year['S']['1-2'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($new_data_year['S']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data_year['S']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }

                if(empty($new_data_year['C']['1-2'][$value->log_status][$value->identity]['cost'])){
                    $new_data_year['C']['1-2'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($new_data_year['C']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data_year['C']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }

                if(empty($new_data_year['F']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data_year['F']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($ii[$value->no])){
                    $ii[$value->no]=0;
                }
                if(empty($iii[$value->no])){
                    $iii[$value->no]=0;
                }
                if(empty($i[$value->no])){
                    $i[$value->no]=0;
                }
                if($sort[3]==1||$sort[3]==2){
                    if($sort[1]=='A'||$sort[1]=='B'||$sort[1]=='C'||$sort[1]=='D'){
                        if($i[$value->no]==1){
                           continue;
                        }
                        $new_data_year['S']['1-2'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $i[$value->no]=$i[$value->no]+1;
                    }elseif($sort[1]=='F'||$sort[1]=='G'||$sort[1]=='H'){
                        if($ii[$value->no]==1){
                           continue;
                        }
                        $new_data_year['C']['1-2'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $ii[$value->no]=$ii[$value->no]+1;
                    }else{
                        if($iii[$value->no]==1){
                           continue;
                        }
                        $new_data_year['F']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $iii[$value->no]=$iii[$value->no]+1;
                    }
                }else{
                    if($sort[1]=='A'||$sort[1]=='B'||$sort[1]=='C'||$sort[1]=='D'){
                        if($i[$value->no]==1){
                           continue;
                        }
                        $new_data_year['S']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $i[$value->no]=$i[$value->no]+1;
                    }elseif($sort[1]=='F'||$sort[1]=='G'||$sort[1]=='H'){
                        if($ii[$value->no]==1){
                           continue;
                        }
                        $new_data_year['C']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $ii[$value->no]=$ii[$value->no]+1;
                    }else{
                        if($iii[$value->no]==1){
                           continue;
                        }
                        $new_data_year['F']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $iii[$value->no]=$iii[$value->no]+1;
               
                    }
               
                }
            }
        }

        $data_total=db::table('pd_log')
            ->select('pd_log.no','pd_log.status as log_status','pd_log.code as code','pd_log.application_date')
            ->whereIn('pd_log.status',['800'])
            ->groupBy('pd_log.no')
            ->get();
        $i=array();
        $ii=array();
        $iii=array();
        foreach ($data_total as $key => $value) {
            if($value->log_status==800){
                $select_data=db::table('cabinet_appliction')->whereNull('deleted_at')->where('no',$value->no)->first();
                if(!$select_data){
                    continue;   
                }
                $value->cost=$select_data->cost;
                $value->other_amount_due=$select_data->other_amount_due;
                $value->identity=db::table('cabinet_appliction_user')->where('c_no',$select_data->id)->first()->identity;

                $sort=explode('-', $value->code);
                if(empty($new_data_total['S']['1-2'][$value->log_status][$value->identity]['cost'])){
                    $new_data_total['S']['1-2'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($new_data_total['S']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data_total['S']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }

                if(empty($new_data_total['C']['1-2'][$value->log_status][$value->identity]['cost'])){
                    $new_data_total['C']['1-2'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($new_data_total['C']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data_total['C']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }

                if(empty($new_data_total['F']['3-8'][$value->log_status][$value->identity]['cost'])){
                    $new_data_total['F']['3-8'][$value->log_status][$value->identity]['cost']=0;
                }
                if(empty($ii[$value->no])){
                    $ii[$value->no]=0;
                }
                if(empty($iii[$value->no])){
                    $iii[$value->no]=0;
                }
                if(empty($i[$value->no])){
                    $i[$value->no]=0;
                }
                if($sort[3]==1||$sort[3]==2){
                    if($sort[1]=='A'||$sort[1]=='B'||$sort[1]=='C'||$sort[1]=='D'){
                        if($i[$value->no]==1){
                           continue;
                        }
                        $new_data_total['S']['1-2'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $i[$value->no]=$i[$value->no]+1;
                    }elseif($sort[1]=='F'||$sort[1]=='G'||$sort[1]=='H'){
                        if($ii[$value->no]==1){
                           continue;
                        }
                        $new_data_total['C']['1-2'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $ii[$value->no]=$ii[$value->no]+1;
                    }else{
                        if($iii[$value->no]==1){
                           continue;
                        }
                        $new_data_total['F']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $iii[$value->no]=$iii[$value->no]+1;
                    }
                }else{
                    if($sort[1]=='A'||$sort[1]=='B'||$sort[1]=='C'||$sort[1]=='D'){
                        if($i[$value->no]==1){
                           continue;
                        }
                        $new_data_total['S']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $i[$value->no]=$i[$value->no]+1;
                    }elseif($sort[1]=='F'||$sort[1]=='G'||$sort[1]=='H'){
                        if($ii[$value->no]==1){
                           continue;
                        }
                        $new_data_total['C']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $ii[$value->no]=$ii[$value->no]+1;
                    }else{
                        if($iii[$value->no]==1){
                           continue;
                        }
                        $new_data_total['F']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
                        $iii[$value->no]=$iii[$value->no]+1;
               
                    }
               
                }
            }
        }


        $data2=db::table('pd_log')
            ->select('pd_log.no','pd_log.status as log_status','pd_log.code as code','pd_log.application_date')
            ->whereIn('pd_log.status',['800'])
            ->whereBetween('pd_log.application_date',[$serach['application_start_date'],$serach['application_end_date']])
            ->groupBy('pd_log.no')
            ->get();
        foreach ($data2 as $key => $value) {
            $select_data=db::table('tombstone_appliction')->whereNull('deleted_at')->where('no',$value->no)->first();
            if(!$select_data){
                continue;   
            }
            $value->cost=$select_data->cost;
            $value->other_amount_due=$select_data->other_amount_due;
            $value->identity=db::table('tombstone_appliction_user')->where('c_no',$select_data->id)->first()->identity;
            $sort=explode('-', $value->code);
            if(empty($new_data2['T']['3-8'][$value->log_status][$value->identity]['cost'])){
                $new_data2['T']['3-8'][$value->log_status][$value->identity]['cost']=0;
            }
            $new_data2['T']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
        }


        $data2_year=db::table('pd_log')
            ->select('pd_log.no','pd_log.status as log_status','pd_log.code as code','pd_log.application_date')
            ->whereIn('pd_log.status',['800'])
            ->whereBetween('pd_log.application_date',[$first,$end])
            ->groupBy('pd_log.no')
            ->get();
        foreach ($data2_year as $key => $value) {
            $select_data=db::table('tombstone_appliction')->whereNull('deleted_at')->where('no',$value->no)->first();
            if(!$select_data){
                continue;   
            }
            $value->cost=$select_data->cost;
            $value->other_amount_due=$select_data->other_amount_due;
            $value->identity=db::table('tombstone_appliction_user')->where('c_no',$select_data->id)->first()->identity;
            $sort=explode('-', $value->code);
            if(empty($new_data2_year['T']['3-8'][$value->log_status][$value->identity]['cost'])){
                $new_data2_year['T']['3-8'][$value->log_status][$value->identity]['cost']=0;
            }
            $new_data2_year['T']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
        }

        $data2_total=db::table('pd_log')
            ->select('pd_log.no','pd_log.status as log_status','pd_log.code as code','pd_log.application_date')
            ->whereIn('pd_log.status',['800'])
            ->groupBy('pd_log.no')
            ->get();
        foreach ($data2_total as $key => $value) {
            $select_data=db::table('tombstone_appliction')->whereNull('deleted_at')->where('no',$value->no)->first();
            if(!$select_data){
                continue;   
            }
            $value->cost=$select_data->cost;
            $value->other_amount_due=$select_data->other_amount_due;
            $value->identity=db::table('tombstone_appliction_user')->where('c_no',$select_data->id)->first()->identity;
            $sort=explode('-', $value->code);
            if(empty($new_data2_total['T']['3-8'][$value->log_status][$value->identity]['cost'])){
                $new_data2_total['T']['3-8'][$value->log_status][$value->identity]['cost']=0;
            }
            $new_data2_total['T']['3-8'][$value->log_status][$value->identity]['cost']+=$value->cost; 
        }
        return view('admin.Table.index3',['new_data'=>$new_data,'new_data_year'=>$new_data_year,'new_data_total'=>$new_data_total,'new_data2'=>$new_data2
            ,'new_data2_year'=>$new_data2_year
            ,'new_data2_total'=>$new_data2_total
            ,'serach'=>$serach]);
        
    }
    
}