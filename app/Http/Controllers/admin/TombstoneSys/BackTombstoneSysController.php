<?php

namespace App\Http\Controllers\admin\TombstoneSys;
use App\Http\Controllers\BackController;
use App\Http\Requests\TombstoneSysRequest;
use Illuminate\Http\Request;
use DB;
use Input;
use Session;
class BackTombstoneSysController extends BackController {
    //配合route resource
    protected $RouteName="BackTombstoneSys";
    //檔案路徑
    protected $Path="Upload/TombstoneSys/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\TomstoneSys\TombstoneSys";

    //主要table
    protected $Table1="tombstone_sys";
    //關聯table
    protected $Table2="roles";
    protected $Table4="tombstone_sys_class";
    public function index()
    {
        // dd(ord('D'));

        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '編碼', 'width' => '10'),
            array('title' => '館別', 'width' => '5'),
            array('title' => '樓層', 'width' => '5'),
            array('title' => '區域', 'width' => '5'),
            array('title' => '排', 'width' => '5'),
            array('title' => '位置', 'width' => '5'),
            array('title' => '方位', 'width' => '5'),
            array('title' => '狀態', 'width' => '5'),
            array('title' => '下架時間', 'width' => '10'),
            array('title' => 'option', 'width' => '20')
        );
        // $Data=DB::table($this->Table1)
        // ->select(
        //     $this->Table1.'.id',
        //     $this->Table1.'.code',
        //     $this->Table1.'.museum',
        //     $this->Table1.'.floor',
        //     'class.code as class_name',
        //     'layer.code as layer_name',
        //     'seat.code as seat_name',
        //     $this->Table1.'.position',
        //     $this->Table1.'.status',
        //     $this->Table1.'.deleted_at'
        // )
        // ->join($this->Table4.' as class',$this->Table1.'.class','=','class.code')
        // ->join($this->Table4.' as seat',$this->Table1.'.seat','=','seat.id')
        // ->join($this->Table4.' as layer',$this->Table1.'.layer','=','layer.id')
        // ->whereNull($this->Table1.'.deleted_at')
        // ->get();
        // foreach($Data as $key =>$value){
        //     switch ($value->status) {
        //         case '1': $value->status='開啟'; break;
        //         case '0': $value->status='關閉'; break;
        //         default:  break;
        //     }
        //     switch ($value->position) {
        //         case '1': $value->position='坐北朝南';break;
        //         case '2': $value->position='坐南朝北';break;
        //         case '3': $value->position='坐東朝西';break;
        //         case '4': $value->position='坐西朝東';break;
        //         default: break;
        //     }
        // }
        $area=db::Table($this->Table4)->whereNull('top_class')->get();
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 0, 'Delete' => 0, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        // $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.tombstone_sys', ['AddUrl'=>$this->RouteName.'/create','area'=>$area]);
    }
    
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "櫃位新增";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CabinetSysRequest $request)
    {   
        $ModelName=$this->ModelName;
        $data=$request->all();
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data['code']=$data['class'].'-'.$data['position'].'-'.$data['row'].'-'.$data['layer'];
        $data=$ModelName::create(RequestForeach($data,$this->Path));
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::find($id));
        //表格標題
        $LayoutTitle = "櫃位修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'SelectValue'=>$data->product_class_id,'SelectValue2'=>json_encode(explode(',',$data->product_specification_id)),'id'=>$id]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CabinetSysRequest $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $data=$request->all();
        $data['code']=$data['class'].'-'.$data['position'].'-'.$data['row'].'-'.$data['layer'];
        $ModelName::find($id)->update(RequestForeach($data,$this->Path));
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    
    public function BodyArray($Data = 1) {
        $status=array(1=>'開啟',0=>'關閉');
        $CabinetSys=CabinetSys();
        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            array('id' => 'museum', 'name' => 'museum', 'CNname' => '館別：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['museum'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['museum'] : null),
            array('id' => 'floor', 'name' => 'floor', 'CNname' => '樓層：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['floor'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['floor'] : null),
            
            array('id' => 'class', 'name' => 'class', 'CNname' => '區域：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>$CabinetSys['class'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['class'] : null),
            
            array('id' => 'position', 'name' => 'position', 'CNname' => '排/棟：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['position'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['position'] : null),
            array('id' => 'row', 'name' => 'row', 'CNname' => '層/棟：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['row'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['row'] : null),
            array('id' => 'layer', 'name' => 'layer', 'CNname' => '位置：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['layer'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['layer'] : null),

            array('id' => 'status', 'name' => 'status', 'CNname' => '狀態：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $status, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['status'] : null),
           );
        return $BodyArray;
    }
    public function correspond($Data)
    {
        $CabinetSys=CabinetSys();
        foreach($Data as $key =>$value){
            $Data[$key]->museum=$CabinetSys['museum'][$value->museum];
            $Data[$key]->class=$CabinetSys['class'][$value->class];
            $Data[$key]->position=$CabinetSys['position'][$value->position];
            switch ($value->status) {
                case 1:
                   $Data[$key]->status='開啟';
                    break;
                default:
                    $Data[$key]->status='關閉';
                    break;
            }
        }
        return $Data;
    }
    public function cahnge_status($value='')
    {
        $id=Input::get('id');
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $data=db::table($this->Table1)->find($id);
        if($data){
            if($data->status==1){
                db::table($this->Table1)->where('id',$id)->update(['status'=>0]);
            }else{
                db::table($this->Table1)->where('id',$id)->update(['status'=>1]);
            }
        }
        return redirect()->back();
    }
    public function api()
    {
        $data=array();
        $data['museum']=1;
        $data['area']=db::Table($this->Table4)->where('id',Input::get('area'))->first()->code;
        $data['layer']=db::Table($this->Table4)->where('id',Input::get('layer'))->first()->code;
        $data['seat']=db::Table($this->Table4)->where('id',Input::get('seat'))->first()->code;
        $code=implode('-',$data);
        $data=db::table($this->Table1)
        ->where('code',$code)
        ->where('number',null)
        ->where('status',1)
        ->get();
        $array=array();
        if(count($data)>0){
            $OtherCost=db::table('tombstone_other_cost')->first();
            $array[0]=1;
            $array['other_cost']=$OtherCost->cost;
            $array['data']=$data;
            return json_encode($array);
        }else{
            return json_encode(array(0));
        }
    }
    public function add_class()
    {
        $data=Input::all();
        $old=db::table($this->Table4)
            ->orderBy('id','desc');
        if(Input::has('top_class')){
           $old=$old->where('top_class',Input::get('top_class')); 
        }else{
            $old=$old->whereNull('top_class');
        }
        $old=$old->first();
        if(!Input::has('top_class')){
            if($old){
                $data['code']=chr(ord($old->code)+1);
            }else{
                $data['code']='A';
            }
        }else{
            $data['code']=1;
            $data['name']=1;
            if($old){
                $data['code']=$old->code+1;
                $data['name']=$old->name+1;
            }
            if(Input::has('position')){
                $data['position']=Input::get('position');
            }
        }
        db::Table($this->Table4)->insert($data);
        //當層資料
        if(Input::has('top_class')){
            $class=db::table($this->Table4)->where('top_class',Input::get('top_class'))->get();
        }else{
            $class=db::table($this->Table4)->whereNull('top_class')->get();

        }
        return view('admin.Select.tombstone_class',['class'=>$class]);
        
    }
    public function update_class()
    {
        $data=Input::all();
        db::table($this->Table4)->where('id',$data['id'])->update($data);
        $class=db::table($this->Table4);
        if(Input::has('top_class')){
            $class=$class->where('top_class',$data['top_class']);
        }else{
            $class=$class->whereNull('top_class');
        }
        $class=$class->get();
        return view('admin.Select.class',['class'=>$class]);
    }
    public function delete_class()
    {
        $data=Input::all();
        db::table($this->Table4)->where('id',$data['id'])->delete();
        $class=db::table($this->Table4);
        if(Input::has('top_class_id')){
            $class=$class->where('top_class',$data['top_class_id']);
        }else{
            $class=$class->whereNull('top_class');
        }
        $class=$class->get();
        return view('admin.Select.class',['class'=>$class]);
    }
    public function get_class()
    {
        $top_class=Input::get('top_class_id');
        $class=db::table($this->Table4)->where('top_class',$top_class)->get();
        return view('admin.Select.class',['class'=>$class]);

    }
    public function change_status()
    {
        $data=Input::all();
        $old=db::Table($this->Table4)->find($data['id']);
        if(empty($old->deleted_at)){
            $status=date('Y-m-d H:i:s');
        }else{
            $status=null;
        }
        db::Table($this->Table4)
            ->where('id',$data['id'])
            ->update(['deleted_at'=>$status]);

        $class=db::table($this->Table4);
        if(Input::has('top_class')){
            $class=$class->where('top_class',$data['top_class']);
        }else{
            $class=$class->whereNull('top_class');
        }
        $class=$class->get();
        return view('admin.Select.class',['class'=>$class]);
    }
    public function create_sys(Request $request)
    {
        $area=db::table($this->Table4)->find($request->area)->code;
        $layer=db::table($this->Table4)->find($request->layer)->code;
        $seat=db::table($this->Table4)->find($request->seat)->code;
        $position=db::table($this->Table4)->find($request->seat)->position;
        $old=db::Table('tombstone_sys')
            ->where('class',$area)
            ->where('layer',$request->layer)
            ->where('seat',$request->seat)
            ->first();
        if($old){
            return '已經存在';
        }
        $data=array();
        //存id
        $data['class']=$area;
        $data['layer']=$request->layer;
        $data['seat']=$request->seat;
        //cdoe 用cdoe組成
        $data['code']=implode('-',array(1,$area,$layer,$seat));
        $data['museum']=1;
        $data['floor']=1;
        $data['status']=1;
        $data['position']=$position;
        db::table('tombstone_sys')->insert($data);
    }
}