<?php

namespace App\Http\Controllers\admin\System;

use App\Http\Controllers\BackController;
use App\Http\Requests\ProgramRegistrationRequest;
use Gate;
use Route;
use DB;
class BackProgramRegistrationController extends BackController
{
    //配合route resource
    protected $RouteName="BackProgramRegistration";
    //檔案路徑
    protected $Path="";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\ProgramRegistration";
    //主要table
    protected $Table1="program_registration";
    //join table
    protected $Table2="menu_class";
    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '分類', 'width' => '9'),
            array('title' => '名稱', 'width' => '9'),
            array('title' => '程式名稱', 'width' => '9'),
            array('title' => '排序', 'width' => '5'),
            array('title' => '選單', 'width' => '5'),
            array('title' => '建立時間', 'width' => '9'),
            array('title' => '修改時間', 'width' => '9'),
            array('title' => '隱藏時間', 'width' => '9'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
            ->select($this->Table1.'.id',$this->Table2.'.name as class_name',$this->Table1.'.name',$this->Table1.'.program_name',$this->Table1.'.sort',$this->Table1.'.menu',$this->Table1.'.created_at',$this->Table1.'.updated_at',$this->Table1.'.deleted_at')
            ->join($this->Table2,$this->Table1.'.menu_class_id','=',$this->Table2.'.id')
            ->orderBy('sort','asc')->get();
        //DataTable中的功能按鈕
        // $Data=getDataTableImages($Data,'Product');
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 1, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "新增程式註冊";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }
    public function store(ProgramRegistrationRequest $request)
    {   

        //use model
        $ModelName=$this->ModelName;
        $data=$ModelName::where('name',$request->name)->get();
        if (count($data)>=1) {
            return redirect()->to($this->RouteName.'/create')->with('status','名稱重複');
        }
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::withTrashed()->find($id));
        //表格標題
        $LayoutTitle = "程式註冊修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'id'=>$id]);
    }
    public function update(ProgramRegistrationRequest $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //以輸入的名稱撈取資料
        $data=$ModelName::withTrashed()->where('name',$request->name)->first();
        //判斷是否有撈到資料
        if (count($data)>=1) {
        //撈取的資料id與當次修改id不同但名稱相同 判定為不同筆資料 名稱重複
            if ($data->id!=$id&&$data->name==$request->name) {
               return redirect()->to($this->RouteName.'/'.$id.'/edit')->with('status','程式名稱重複');
            }
        }
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::withTrashed()->find($id)->update(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        $Table2 = DB::table($this->Table2)
            ->whereNull('deleted_at')
            ->select('id', 'name')
            ->get();
        $menu=array('Y'=>'Y','N'=>'N');
        $BodyArray = array(
             array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
             array('id' => 'menu_class_id', 'name' => 'menu_class_id', 'CNname' => '選單分類：', 'placeholder' => "", 'class' => "full-width", 'readonly' => 'readonly', 'Option' => ArrayToLaravelFormSelectOption($Table2), 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['menu_class_id'] : null),
             array('id' => 'menu', 'name' => 'menu', 'CNname' => '選單：', 'placeholder' => "", 'class' => "full-width", 'readonly' => 'readonly', 'Option' => $menu, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data[$this->Table2.'menu'] : null),
             array('id' => 'name', 'name' => 'name', 'CNname' => '名稱：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name'] : null),
             array('id' => 'program_name', 'name' => 'program_name', 'CNname' => '程式名稱：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['program_name'] : null),
             
             array('id' => 'sort', 'name' => 'sort', 'CNname' => '排序：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['sort'] : null),
        );
        return $BodyArray;
    }
}
