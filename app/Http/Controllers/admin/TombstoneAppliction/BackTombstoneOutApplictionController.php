<?php

namespace App\Http\Controllers\admin\TombstoneAppliction;
use Illuminate\Http\Request;
use App\Http\Controllers\BackController;
use App\Http\Requests\TombstoneApplictionOutRequest;
use DB;
use Input;
use Session;
use Auth;
class BackTombstoneOutApplictionController extends BackController {
    //配合route resource
    protected $RouteName="BackTombstoneOutAppliction";
    //檔案路徑
    protected $Path="Upload/TombstoneAppliction/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\TombstoneAppliction\TombstoneAppliction";

    //主要table
    protected $Table1="tombstone_appliction";
    protected $Table2="tombstone_appliction_user";
    protected $Table3="tombstone_appliction_data";
    protected $Table4="users";
    protected $Table5="tombstone_sys_class";
    protected $Table6="tombstone_sys";
    //關聯table
    // protected $Table2="roles";
    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '編號', 'width' => '5'),
            array('title' => '牌位編號', 'width' => '5'),
            array('title' => '逝者', 'width' => '5'),
            array('title' => '身分證號碼', 'width' => '5'),
            array('title' => '身分', 'width' => '5'),
            array('title' => '申請人', 'width' => '5'),
            array('title' => '身分證號碼', 'width' => '5'),
            
            array('title' => '狀態', 'width' => '5'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select($this->Table1.'.id',
            $this->Table1.'.no',
            $this->Table3.'.tombstone_code',
            $this->Table2.'.name',
            $this->Table2.'.name_number',
            $this->Table2.'.identity',
            $this->Table1.'.applicant',
            $this->Table1.'.applicant_number',
            $this->Table1.'.status'
            // $this->Table1.'.deleted_at'
        )
        ->join($this->Table2,$this->Table2.'.c_no','=',$this->Table1.'.id')
        ->join($this->Table3,$this->Table3.'.c_no','=',$this->Table1.'.id')
        ->join($this->Table4,$this->Table1.'.users','=',$this->Table4.'.id')
        ->whereNull( $this->Table1.'.deleted_at')
        ->where( $this->Table1.'.status','>=',700)
        ->where( $this->Table1.'.status','<',800)
        ->orderBy($this->Table1.'.updated_at','desc')
        ->orderBy($this->Table1.'.created_at','desc')
        ->get();
        $option=CabinetSys();
        foreach($Data as $key =>$val){
            $val->identity=$option['type'][$val->identity];
            switch ($val->status) {
                case 700: $val->status='已申請'; break;
                case 800: $val->status='已出塔'; break;
                default: break;
            }
        }
        // $Data=$this->correspond($Data);
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'ApplictionEdit' => 1, 'Delete' => 0, 'Check' => 0, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0,'tombstoneChangeAppliction'=>1,'Appliction2'=>1,'print2'=>1,'T_ApplictionDeletel'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    public function create()
    {
        //要edit的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        $LayoutTitle = "牌位遷出申請";
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $data=['FormTtile'=> $FormTtile,'url'=>$Url];
        $select_no=Input::get('select_no',0);
        if($select_no!==0){
            $tombstone_appliction=db::table('tombstone_appliction')->find($select_no);
            if(!$tombstone_appliction){
                return redirect()->back()->with('status','查無此身分證');
            }
            $tombstone_appliction_user=db::Table('tombstone_appliction_user')->where('c_no',$tombstone_appliction->id)->first();
            $confirm_payment_user=db::table('users')->find($tombstone_appliction->confirm_payment_user);
            $tombstone_appliction_data=db::Table('tombstone_appliction_data')->where('c_no',$tombstone_appliction_user->c_no)->first();
            $tombstone_principal=db::Table('tombstone_principal')->where('c_no',$tombstone_appliction_user->c_no)->first();
            $tombstone_sys=DB::Table('tombstone_sys')->where('status',1)->get();
            $users=db::Table('users')->find($tombstone_appliction->users);
            $data['url']=$Url;
            $data['tombstone_appliction']=$tombstone_appliction;
            $data['tombstone_appliction_user']=$tombstone_appliction_user;
            $data['tombstone_appliction_data']=$tombstone_appliction_data;
            $data['tombstone_principal']=$tombstone_principal;
            $data['tombstone_sys']=$tombstone_sys;
            $data['users']=$users;
            $data['confirm_payment_user']=$confirm_payment_user;
            $area=array();
            $layer=array();
            $seat=array();
            $position=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
            $area=db::Table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_aera)->whereNull('deleted_at')->get();
            $layer=db::table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_layer)->whereNull('deleted_at')->get();
            $seat=db::table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_seat)->whereNull('deleted_at')->get();
            $sys=db::table($this->Table6)->where('code',$tombstone_appliction_data->tombstone_code)->get();
            $data['position']=$position;
            $data['area']=$area;
            $data['layer']=$layer;
            $data['seat']=$seat;
            $data['sys']=$sys;
            $no=no_select2();
            $data['no']=$no;
        }
        //app/Support/Helpers底下的自訂function
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        return view('admin.Form.tombstone_out_appliction',$data);
    }
    public function store(TombstoneApplictionOutRequest $request)
    {   
        $old_tombstone_appliction=db::table('tombstone_appliction')->find($request->old_tombstone_appliction_id);
        $no=$request->no;
        do{
            $no_select=no_select('tombstone_appliction',$no);
            if(!$no_select){
                $no++;
            }
        } while ( !$no_select );
        db::table('tombstone_appliction')->where('id',$request->old_tombstone_appliction_id)->update(['status'=>399]);
        $tombstone_appliction=array();
        $tombstone_appliction['before_appliction_id']=$request->old_tombstone_appliction_id;
        $tombstone_appliction['tombstone_appliction_type']=$request->tombstone_appliction_type;
        $tombstone_appliction['no']=$no;
        $tombstone_appliction['applicant']=$request->applicant;
        $tombstone_appliction['application_date']=$request->application_date;
        $tombstone_appliction['household_registration']=$request->household_registration;
        $tombstone_appliction['applicant_number']=$request->applicant_number;
        $tombstone_appliction['tel']=$request->tel;
        $tombstone_appliction['phone']=$request->phone;
        $tombstone_appliction['relationship']=$request->relationship;
        $tombstone_appliction['address']=$request->address;
        $tombstone_appliction['ext']=$request->ext;
        $tombstone_appliction['users']=Auth::user()->id;
        $tombstone_appliction_user['application_date']=$request->application_date;
        $tombstone_appliction['status']=700;
        $tombstone_appliction['cost']=$request->cost;
        for ($i = 1; $i < 10; $i++) {
            $name='file'.$i;
            if(Input::hasFile($name)){
            $filename = date('Ymd').rand(11111, 99999);
            $tombstone_appliction[$name] = $filename .".". Input::file('file1')->getClientOriginalExtension();
                Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
            }
        }
        $tombstone_appliction_id=DB::table($this->Table1)->insertGetid($tombstone_appliction);

        $tombstone_appliction_user=array();
        $tombstone_appliction_user['c_no']=$tombstone_appliction_id;
        $tombstone_appliction_user['name']=$request->name;
        $tombstone_appliction_user['name_number']=$request->name_number;
        $tombstone_appliction_user['address']=$request->address;
        $tombstone_appliction_user['gender']=$request->gender;
        $tombstone_appliction_user['identity']=$request->identity;
        // $tombstone_appliction_user['discount']=$request->discount;
        //$tombstone_appliction_user['dead_date']=$request->dead_date;
        //$tombstone_appliction_user['dead_reason']=$request->dead_reason;
        //$tombstone_appliction_user['dead_location']=$request->dead_location;
        $tombstone_appliction_user['birthday']=$request->birthday;
        $tombstone_appliction_user['expected_date']=$request->expected_date;
        DB::table($this->Table2)->insert($tombstone_appliction_user);
        $tombstone_appliction_data=array();
        $tombstone_appliction_data['c_no']=$tombstone_appliction_id;
        //$tombstone_appliction_data['tombstone_type']=$request->new_tombstone_type;
        $tombstone_appliction_data['tombstone_class_position']=$request->tombstone_class_position;
        $tombstone_appliction_data['tombstone_class_floor']=1;
        $tombstone_appliction_data['tombstone_class_aera']=$request->tombstone_class_aera;
        // $tombstone_appliction_data['tombstone_class_row']=$request->tombstone_class_row;
        $tombstone_appliction_data['tombstone_class_layer']=$request->tombstone_class_layer;
        $tombstone_appliction_data['tombstone_class_seat']=$request->tombstone_class_seat;
        $tombstone_appliction_data['tombstone_code']=$request->tombstone_code;
        $tombstone_appliction_data['cost']=$request->cost;
        DB::Table($this->Table3)->insert($tombstone_appliction_data);

        $tombstone_principal=array();
        $tombstone_principal['c_no']=$tombstone_appliction_id;
        $tombstone_principal['client']=$request->client;
        $tombstone_principal['client_number']=$request->client_number;
        $tombstone_principal['client_household_registration']=$request->client_household_registration;
        $tombstone_principal['client_tel']=$request->client_tel;
        $tombstone_principal['client_phone']=$request->client_phone;
        $tombstone_principal['client_address']=$request->client_address;
        DB::table('tombstone_principal')->insert($tombstone_principal);
        Exist('tombstone_sys',
            $request->tombstone_code,
            '700',
            $request->name_number,
            '',
            $tombstone_appliction_id
        );
        no_insert($no);
        ExistLog('牌位',$request->tombstone_class_floor,$request->tombstone_class_aera,$request->tombstone_class_row,$request->tombstone_class_position,$request->tombstone_code,$request->no,$request->applicant_number,$request->name_number,'',$request->application_date,$request->expected_date,'',$request->status,$request->applicant,$request->name,$request->tel,$request->phone,$request->identity);
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        $LayoutTitle='';

        $tombstone_sys=DB::Table('tombstone_sys')->where('status',1)->get();

        $tombstone_appliction=db::table('tombstone_appliction')->find($id);

        $users=db::Table('users')->find($tombstone_appliction->users);

        $confirm_payment_user=db::table('users')->find($tombstone_appliction->confirm_payment_user);

        $tombstone_appliction_user=db::Table('tombstone_appliction_user')->where('c_no',$id)->first();

        $tombstone_appliction_data=db::Table('tombstone_appliction_data')->where('c_no',$id)->first();

        $old_tombstone_appliction_data=db::Table('tombstone_appliction_data')->where('id',$tombstone_appliction_data->old_tombstone_class_seat)->first();
        $tombstone_principal=db::Table('tombstone_principal')->where('c_no',$id)->first();
        if($tombstone_appliction->status=='700'){
            $LayoutTitle = "牌位遷出出塔作業";
        }
        $area=array();
        $layer=array();
        $seat=array();
        $position=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        $area=db::Table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_aera)->whereNull('deleted_at')->get();
        $layer=db::table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_layer)->whereNull('deleted_at')->get();
        $seat=db::table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_seat)->whereNull('deleted_at')->get();
        $sys=db::table($this->Table6)->where('code',$tombstone_appliction_data->tombstone_code)->get();
        $data['position']=$position;
        $data['area']=$area;
        $data['layer']=$layer;
        $data['seat']=$seat;
        $data['sys']=$sys;
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        
        //app/Support/Helpers底下的自訂function
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.tombstone_out_appliction_edit', [
            'FormTtile' => $FormTtile,
            'id'=>$id,'url'=>$Url,
            'tombstone_appliction'=>$tombstone_appliction,
            'tombstone_appliction_user'=>$tombstone_appliction_user,
            'tombstone_appliction_data'=>$tombstone_appliction_data,
            'tombstone_principal'=>$tombstone_principal,
            'tombstone_sys'=>$tombstone_sys,
            'users'=>$users,
            'confirm_payment_user'=>$confirm_payment_user,
            'old_tombstone_appliction_data'=>$old_tombstone_appliction_data,
            'position'=>$position,
            'area'=>$area,
            'layer'=>$layer,
            'seat'=>$seat,
            'sys'=>$sys,
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {   
        $tombstone_appliction=array();
        // $tombstone_appliction['tombstone_appliction_type']=$request->tombstone_appliction_type;
        // $tombstone_appliction['no']=$request->no;
        // $tombstone_appliction['application_date']=$request->application_date;
        $tombstone_appliction['status']=$request->status;
        // $tombstone_appliction['application_date']=$request->application_date;
        // $tombstone_appliction['applicant']=$request->applicant;
        // $tombstone_appliction['household_registration']=$request->household_registration;
        // $tombstone_appliction['applicant_number']=$request->applicant_number;
        // $tombstone_appliction['tel']=$request->tel;
        // $tombstone_appliction['phone']=$request->phone;
        // $tombstone_appliction['relationship']=$request->relationship;
        // $tombstone_appliction['address']=$request->address;
        // $tombstone_appliction['ext']=$request->ext;
        $tombstone_appliction['principal']=$request->principal;
        $tombstone_appliction['change_date']=$request->change_date;
        db::Table('tombstone_appliction')->where('id',$id)->update($tombstone_appliction);
        // $tombstone_appliction_user=array();
        // $tombstone_appliction_user['c_no']=$tombstone_appliction_id;
        // $tombstone_appliction_user['name']=$request->name;
        // $tombstone_appliction_user['name_number']=$request->name_number;
        // $tombstone_appliction_user['address']=$request->address;
        // $tombstone_appliction_user['gender']=$request->gender;
        // $tombstone_appliction_user['identity']=$request->identity;
        // $tombstone_appliction_user['discount']=$request->discount;
        //$tombstone_appliction_user['dead_date']=$request->dead_date;
        //$tombstone_appliction_user['dead_reason']=$request->dead_reason;
        //$tombstone_appliction_user['dead_location']=$request->dead_location;
        // $tombstone_appliction_user['birthday']=$request->birthday;
        // $tombstone_appliction_user['expected_date']=$request->expected_date;
        // DB::table('tombstone_appliction_user')->where('id',$request->id)->update($tombstone_appliction_user);
        
        $tombstone_appliction_data=array();
        // $tombstone_appliction_data['c_no']=$tombstone_appliction_id;
        // $tombstone_appliction_data['tombstone_type']=$request->tombstone_type;
        // $tombstone_appliction_data['tombstone_class_floor']=$request->tombstone_class_floor;
        // $tombstone_appliction_data['tombstone_class_aera']=$request->tombstone_class_aera;
        // $tombstone_appliction_data['tombstone_class_row']=$request->tombstone_class_row;
        // $tombstone_appliction_data['tombstone_class_layer']=$request->tombstone_class_layer;
        //$tombstone_appliction_data['tombstone_class_seat']=$request->tombstone_class_seat;
        // $tombstone_appliction_data['cost']=$request->cost;
        // $tombstone_appliction_data['other_cost']=$request->other_cost;
        // $tombstone_appliction_data['amount_due']=$request->amount_due;
        // DB::Table('tombstone_appliction_data')->where('id',$request->tombstone_appliction_data_id)->update($tombstone_appliction_data);
        $tombstone_principal=array();
        // $tombstone_principal['c_no']=$tombstone_appliction_id;
        // $tombstone_principal['client']=$request->client;
        // $tombstone_principal['client_number']=$request->client_number;
        // $tombstone_principal['client_household_registration']=$request->client_household_registration;
        // $tombstone_principal['client_tel']=$request->client_tel;
        // $tombstone_principal['client_phone']=$request->client_phone;
        // $tombstone_principal['client_address']=$request->client_address;
        // DB::table('tombstone_principal')->where('id',$request->tombstone_principal_id)->update($tombstone_principal);
        // $table,$code,$status,$number='',$new_code=''
        Exist('tombstone_sys',
            $request->tombstone_code,
            '800',
            $request->name_number,
            '',
            $id
        );
        ExistLog('牌位',$request->tombstone_class_floor,$request->tombstone_class_aera,$request->tombstone_class_row,$request->tombstone_class_position,$request->tombstone_code,$request->no,$request->applicant_number,$request->name_number,'',$request->application_date,$request->expected_date,'',$request->status,$request->applicant,$request->name,$request->tel,$request->phone,$request->identity);
        
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    public function get_no()
    {
        $select_number=Input::get('select_number');
        $tombstone_appliction=db::Table('tombstone_appliction')
                ->select('id','no')
                ->where('applicant_number',$select_number)
                ->where('tombstone_appliction_type',1)
                ->where('status',300)
                ->get();
        return json_encode($tombstone_appliction);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    
    public function BodyArray($Data = 1) {
        
        $tombstoneSys=tombstoneSys();

        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            array('id' => 'no', 'name' => 'no', 'CNname' => '申請單號：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['no'] : date('Ymd').rand(1000, 9999)),
            array('id' => 'application_date', 'name' => 'application_date', 'CNname' => '申請日期：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'InputDate', "Value" => (is_object($Data)) ? $Data['application_date'] : ''),
            array('id' => 'applicant', 'name' => 'applicant', 'CNname' => '申請人：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'InputDate', "Value" => (is_object($Data)) ? $Data['applicant'] : ''),
            array('id' => 'applicant_number', 'name' => 'applicant_number', 'CNname' => '申請人身分證：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['applicant_number'] : ''),
            array('id' => 'household_registration', 'name' => 'household_registration', 'CNname' => '申請人戶籍地：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['household_registration'] : ''),
            array('id' => 'tel', 'name' => 'tel', 'CNname' => '聯絡電話：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['tel'] : ''),
            array('id' => 'phone', 'name' => 'phone', 'CNname' => '行動電話：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['phone'] : ''),
            array('id' => 'relationship', 'name' => 'relationship', 'CNname' => '關係：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['relationship'] : ''),
            array('id' => 'address', 'name' => 'address', 'CNname' => '聯絡地址：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['address'] : ''),
            array('id' => 'file1', 'name' => 'file1', 'CNname' => '申請人身分證：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file1'] : ''),
            array('id' => 'file2', 'name' => 'file2', 'CNname' => '火化許可證：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file2'] : ''),
            array('id' => 'file3', 'name' => 'file3', 'CNname' => '死亡證明書：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file3'] : ''),
            array('id' => 'file4', 'name' => 'file4', 'CNname' => '戶籍謄本：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file4'] : ''),
            array('id' => 'file5', 'name' => 'file5', 'CNname' => '亡者除戶謄本：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file5'] : ''),
            array('id' => '', 'name' => '', 'CNname' => '逝者資訊', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'HR', "Value" => ''),
            array('id' => 'name', 'name' => 'name', 'CNname' => '逝者：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name'] : ''),
            array('id' => 'name_number', 'name' => 'name_number', 'CNname' => '逝者：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name_number'] : ''),
            array('id' => 'name_household_registration', 'name' => 'name_household_registration', 'CNname' => '逝者戶籍地：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name_household_registration'] : ''),
            array('id' => 'gender', 'name' => 'gender', 'CNname' => '逝者戶籍地：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['gender'] : ''),
            array('id' => 'identity', 'name' => 'identity', 'CNname' => '身分：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $tombstoneSys['type'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['identity'] : ''),

            // array('id' => 'position', 'name' => 'position', 'CNname' => '方位：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $tombstoneSys['position'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['position'] : null),
            // array('id' => 'floor', 'name' => 'floor', 'CNname' => '樓層：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $tombstoneSys['floor'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['floor'] : null),
            // array('id' => 'class', 'name' => 'class', 'CNname' => '類別：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>$tombstoneSys['class'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['class'] : null),
            // array('id' => 'row', 'name' => 'row', 'CNname' => '排/棟：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $tombstoneSys['row'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['row'] : null),
            // array('id' => 'layer', 'name' => 'layer', 'CNname' => '層：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $tombstoneSys['layer'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['layer'] : null),
            // array('id' => 'code', 'name' => 'code', 'CNname' => '編號：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['code'] : null),
           );
        return $BodyArray;
    }
}