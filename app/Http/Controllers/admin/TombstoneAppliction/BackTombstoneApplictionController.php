<?php

namespace App\Http\Controllers\admin\TombstoneAppliction;
use Illuminate\Http\Request;
use App\Http\Controllers\BackController;
use App\Http\Requests\TombstoneApplictionRequest;
use DB;
use Input;
use Session;
use Auth;
class BackTombstoneApplictionController extends BackController {
    //配合route resource
    protected $RouteName="BackTombstoneAppliction";
    //檔案路徑
    protected $Path="Upload/TombstoneAppliction/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\TombstoneAppliction\TombstoneAppliction";

    //主要table
    protected $Table1="tombstone_appliction";
    protected $Table2="tombstone_appliction_user";
    protected $Table3="tombstone_appliction_data";
    protected $Table4="users";
    protected $Table5="tombstone_sys_class";
    protected $Table6="tombstone_sys";
    //關聯table
    // protected $Table2="roles";
    public function index()
    {
    $area=db::table('tombstone_sys_class')->whereNull('top_class')->whereNull('deleted_at')->get();
        // $data=array();
        // db::table('tombstone_sys')->insert($data);   
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '編號', 'width' => '5'),
            array('title' => '逝者', 'width' => '5'),
            array('title' => '身分證號碼', 'width' => '5'),
            array('title' => '身分', 'width' => '5'),
            array('title' => '申請人', 'width' => '5'),
            array('title' => '身分證號碼', 'width' => '5'),
            array('title' => '狀態', 'width' => '5'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select($this->Table1.'.id',
            $this->Table1.'.no',
            $this->Table2.'.name',
            $this->Table2.'.name_number',
            $this->Table2.'.identity',
            $this->Table1.'.applicant',
            $this->Table1.'.applicant_number',
            $this->Table1.'.status'
            // $this->Table1.'.deleted_at'
        )
        ->join($this->Table2,$this->Table2.'.c_no','=',$this->Table1.'.id')
        ->join($this->Table3,$this->Table3.'.c_no','=',$this->Table1.'.id')
        ->whereNull( $this->Table1.'.deleted_at')
        ->where( $this->Table1.'.status','<',300)
        ->orderBy($this->Table1.'.updated_at','desc')
        ->orderBy($this->Table1.'.created_at','desc')
        ->get();
        $option=CabinetSys();
        foreach($Data as $key =>$val){
            $val->identity=$option['type'][$val->identity];
            switch ($val->status) {
                case 100: $val->status='已申請'; break;
                case 200: $val->status='已繳費'; break;
                case 300: $val->status='已進塔'; break;
                default: break;
            }
        }
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'ApplictionEdit' => 1, 'Delete' => 0, 'Check' => 0, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0,'Appliction'=>1,'ChangeAppliction'=>0,'print2'=>1,'T_ApplictionDeletel'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    
    public function create()
    {
        $tombstone_sys=DB::Table('tombstone_sys')->where('status',1)->get();
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "牌位申請";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        $area=array();
        $layer=array();
        $seat=array();
        $position=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        $area=db::Table($this->Table5)->whereNull('top_class')->whereNull('deleted_at')->get();
        if(count($area)>0){
            $layer=db::table($this->Table5)->where('top_class',$area[0]->id)->whereNull('deleted_at')->get();
        }
        if(count($layer)>0){
            $seat=db::table($this->Table5)->where('top_class',$layer[0]->id)->whereNull('deleted_at')->get();
        }
        //統一使用admin.Form.index，除非特殊狀況
        $no=no_select2();
        return view('admin.Form.tombstone_appliction', ['FormTtile' => $FormTtile,'tombstone_sys'=>$tombstone_sys,'url'=>$Url,'area'=>$area,'layer'=>$layer,'seat'=>$seat,'position'=>$position,'no'=>$no]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TombstoneApplictionRequest $request)
    {   
        if($request->tombstone_code=='目前沒有位置'){
            return redirect()->back()->withInput()->with('status','請選擇有效的牌位');
        }
        $tombstone_cost=db::Table('tombstone_cost')->get();
        $cost=array();
        foreach($tombstone_cost as $val){
            $cost[$val->type]=$val;
        }
        $no=$request->no;
        do{
            $no_select=no_select('tombstone_appliction',$no);
            if(!$no_select){
                $no++;
            }
        } while ( $no_select==false );
        //驗證是否是者已入塔
        $tw_number_select=tw_number_select('tombstone_sys',$request->name_number);
        if(!$tw_number_select){
            return redirect()->back()->withInput()->with('status',$request->name_number.' 已經入塔');
        }
        $tombstone_other_cost=db::Table('tombstone_other_cost')->first();
        $tombstone_appliction=array();
        $tombstone_appliction['tombstone_appliction_type']=$request->tombstone_appliction_type;
        $tombstone_appliction['no']=$no;
        // $tombstone_appliction['application_type']=$request->application_type;
        $tombstone_appliction['application_date']=$request->application_date;
        $tombstone_appliction['household_registration']=$request->household_registration;
        $tombstone_appliction['applicant']=$request->applicant;
        $tombstone_appliction['applicant_number']=$request->applicant_number;
        $tombstone_appliction['tel']=$request->tel;
        $tombstone_appliction['phone']=$request->phone;
        $tombstone_appliction['relationship']=$request->relationship;
        $tombstone_appliction['address']=$request->address;
        $tombstone_appliction['ext']=$request->ext;
        $tombstone_appliction['users']=Auth::user()->id;
        $tombstone_appliction['status']=100;
        $tombstone_appliction['code']=$request->tombstone_code;
        $tombstone_appliction['cost']=$request->cost;
        $tombstone_appliction['other_amount_due']=$request->other_amount_due;
        $tombstone_appliction['amount_due']=$request->amount_due;
        for ($i = 1; $i < 6; $i++) {
            $name='file'.$i;
            if(Input::hasFile($name)){
            $filename = date('Ymd').rand(11111, 99999);
            $tombstone_appliction[$name] = $filename .".". Input::file($name)->getClientOriginalExtension();
                Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
            }
        }
        $tombstone_appliction_id=DB::table('tombstone_appliction')->insertGetid($tombstone_appliction);
        $tombstone_appliction_user=array();
        $tombstone_appliction_user['c_no']=$tombstone_appliction_id;
        $tombstone_appliction_user['name']=$request->name;
        $tombstone_appliction_user['name_number']=$request->name_number;
        $tombstone_appliction_user['address']=$request->name_household_registration;
        $tombstone_appliction_user['gender']=$request->gender;
        $tombstone_appliction_user['identity']=$request->identity;
        // $tombstone_appliction_user['discount']=$request->discount;
        $tombstone_appliction_user['birthday']=$request->birthday;
        $tombstone_appliction_user['expected_date']=$request->expected_date;
        $tombstone_appliction_user['expected_time']=$request->expected_time;
        DB::table('tombstone_appliction_user')->insert($tombstone_appliction_user);
        
        $tombstone_appliction_data=array();
        $tombstone_appliction_data['c_no']=$tombstone_appliction_id;
        $tombstone_appliction_data['tombstone_class_floor']=1;
        $tombstone_appliction_data['tombstone_class_aera']=$request->tombstone_class_aera;
        $tombstone_appliction_data['tombstone_class_layer']=$request->tombstone_class_layer;
        $tombstone_appliction_data['tombstone_class_seat']=$request->tombstone_class_seat;
        $tombstone_appliction_data['tombstone_class_position']=$request->tombstone_class_position;
        $tombstone_appliction_data['tombstone_code']=$request->tombstone_code;
        // $tombstone_appliction_data['tombstone_class_seat']=$request->tombstone_class_seat;
        $tombstone_appliction_data['cost']=$cost[$request->identity]->cost;
        $tombstone_appliction_data['other_cost']=0;
        $tombstone_appliction_data['amount_due']=$cost[$request->identity]->cost;
        $tombstone_appliction_data['tombstone_code']=$request->tombstone_code;
        
        DB::Table('tombstone_appliction_data')->insert($tombstone_appliction_data);
        $tombstone_principal=array();
        $tombstone_principal['c_no']=$tombstone_appliction_id;
        $tombstone_principal['client']=$request->client;
        $tombstone_principal['client_number']=$request->client_number;
        $tombstone_principal['client_household_registration']=$request->client_household_registration;
        $tombstone_principal['client_tel']=$request->client_tel;
        $tombstone_principal['client_phone']=$request->client_phone;
        $tombstone_principal['client_address']=$request->client_address;
        DB::table('tombstone_principal')->insert($tombstone_principal);
        Exist(
            'tombstone_sys',
            $request->tombstone_code,
            $request->status,
            $request->name_number,
            '',
            $tombstone_appliction_id
        );
        ExistLog('牌位',1,$request->tombstone_class_aera,$request->tombstone_class_row,$request->tombstone_class_position,$request->tombstone_code,$no,$request->applicant_number,$request->name_number,'',$request->application_date,$request->expected_date,'',$request->status,$request->applicant,$request->name,$request->tel,$request->phone,$request->identity);
        no_insert($no);
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        $tombstone_sys=DB::Table('tombstone_sys')->where('status',1)->get();
        $tombstone_appliction=db::table('tombstone_appliction')->find($id);
        $users=db::Table('users')->find($tombstone_appliction->users);
        $confirm_payment_user=db::table('users')->find($tombstone_appliction->confirm_payment_user);
        $tombstone_appliction_user=db::Table('tombstone_appliction_user')->where('c_no',$id)->first();
        $tombstone_appliction_data=db::Table('tombstone_appliction_data')->where('c_no',$id)->first();
        $tombstone_principal=db::Table('tombstone_principal')->where('c_no',$id)->first();
        if($tombstone_appliction->status==100){
            $LayoutTitle = "牌位申請繳費";
        }if($tombstone_appliction->status==200){
            $LayoutTitle = "牌位申請入塔";
        }
        $area=array();
        $layer=array();
        $seat=array();
        $position=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        $area=db::Table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_aera)->whereNull('deleted_at')->get();
        $layer=db::table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_layer)->whereNull('deleted_at')->get();
        $seat=db::table($this->Table5)->where('id',$tombstone_appliction_data->tombstone_class_seat)->whereNull('deleted_at')->get();
        $sys=db::table($this->Table6)->where('code',$tombstone_appliction_data->tombstone_code)->get();
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        
        //app/Support/Helpers底下的自訂function
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.tombstone_appliction_edit', [
            'FormTtile' => $FormTtile,
            'id'=>$id,'url'=>$Url,
            'tombstone_appliction'=>$tombstone_appliction,
            'tombstone_appliction_user'=>$tombstone_appliction_user,
            'tombstone_appliction_data'=>$tombstone_appliction_data,
            'tombstone_principal'=>$tombstone_principal,
            'tombstone_sys'=>$tombstone_sys,
            'users'=>$users,
            'confirm_payment_user'=>$confirm_payment_user,
            'area'=>$area,
            'layer'=>$layer,
            'seat'=>$seat,
            'sys'=>$sys,
            'position'=>$position
        ]);
    }

    public function update(Request $request,$id)
    {   
        $tombstone_appliction=array();
        // $tombstone_appliction['no']=$request->no;
        // $tombstone_appliction['tombstone_appliction_type']=$request->tombstone_appliction_type;
        // $tombstone_appliction['application_date']=$request->application_date;
        // $tombstone_appliction['household_registration']=$request->household_registration;
        // $tombstone_appliction['applicant_number']=$request->applicant_number;
        // $tombstone_appliction['applicant']=$request->applicant;
        // $tombstone_appliction['tel']=$request->tel;
        // $tombstone_appliction['phone']=$request->phone;
        // $tombstone_appliction['relationship']=$request->relationship;
        // $tombstone_appliction['address']=$request->address;
        // $tombstone_appliction['ext']=$request->ext;
         // $tombstone_appliction['code']=$request->tombstone_code;
        //要改變成已繳費
        if($request->status==200){
            $tombstone_appliction['confirm_payment_user']=Auth::user()->id;
            $tombstone_appliction['payment_date']=$request->payment_date;
        }
        //要改變成已入塔
        if($request->status==300){
            $tombstone_appliction['principal']=$request->principal;
            $tombstone_appliction['change_date']=$request->change_date;
        }
        $tombstone_appliction['status']=$request->status;
        for ($i = 1; $i < 8; $i++) {
            $name='file'.$i;
            if(Input::hasFile($name)){
            $filename = date('Ymd').rand(11111, 99999);
            $tombstone_appliction[$name] = $filename .".". Input::file($name)->getClientOriginalExtension();
                Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
            }
        }
        db::Table('tombstone_appliction')->where('id',$request->id)->update($tombstone_appliction);

        $tombstone_appliction_user=array();
        // $tombstone_appliction_user['c_no']=$tombstone_appliction_id;
        // $tombstone_appliction_user['name']=$request->name;
        // $tombstone_appliction_user['name_number']=$request->name_number;
        // $tombstone_appliction['applicant']=$request->applicant;
        // $tombstone_appliction['application_type']=$request->application_type;
        // $tombstone_appliction_user['address']=$request->address;
        // $tombstone_appliction_user['gender']=$request->gender;
        // $tombstone_appliction_user['identity']=$request->identity;
        // $tombstone_appliction_user['discount']=$request->discount;
        // $tombstone_appliction_user['birthday']=$request->birthday;
        // $tombstone_appliction_user['expected_date']=$request->expected_date;
        // DB::table('tombstone_appliction_user')->where('id',$request->id)->update($tombstone_appliction_user);
        
        $tombstone_appliction_data=array();
        // $tombstone_appliction_data['c_no']=$tombstone_appliction_id;
        // $tombstone_appliction_data['tombstone_type']=$request->tombstone_type;
        // $tombstone_appliction_data['tombstone_class_floor']=$request->tombstone_class_floor;
        // $tombstone_appliction_data['tombstone_class_aera']=$request->tombstone_class_aera;
        // $tombstone_appliction_data['tombstone_class_row']=$request->tombstone_class_row;
        //$tombstone_appliction_data['tombstone_class_layer']=$request->tombstone_class_layer;
        // $tombstone_appliction_data['tombstone_class_seat']=$request->tombstone_class_seat;
        $tombstone_appliction_data['cost']=$request->cost;
        $tombstone_appliction_data['other_cost']=$request->other_cost;
        $tombstone_appliction_data['amount_due']=$request->amount_due;
        DB::Table('tombstone_appliction_data')->where('id',$request->tombstone_appliction_data_id)->update($tombstone_appliction_data);

        // $tombstone_principal=array();
        // $tombstone_principal['c_no']=$tombstone_appliction_id;
        // $tombstone_principal['client']=$request->client;
        // $tombstone_principal['client_number']=$request->client_number;
        // $tombstone_principal['client_household_registration']=$request->client_household_registration;
        // $tombstone_principal['client_tel']=$request->client_tel;
        // $tombstone_principal['client_phone']=$request->client_phone;
        // $tombstone_principal['client_address']=$request->client_address;
        // DB::table('tombstone_principal')->where('id',$request->tombstone_principal_id)->update($tombstone_principal);
        Exist('tombstone_sys',
            $request->tombstone_code,
            $request->status,
            $request->name_number,
            '',
            $request->id
        );
        ExistLog('牌位',1,$request->tombstone_class_aera,$request->tombstone_class_row,$request->tombstone_class_position,$request->tombstone_code,$request->no,$request->applicant_number,$request->name_number,'',$request->application_date,$request->expected_date,'',$request->status,$request->applicant,$request->name,$request->tel,$request->phone,$request->identity);

        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }     
    }
    
    public function BodyArray($Data = 1) {
        return array( );
    }
    public function change_area()
    {
        $data=Input::all();
        $area =db::Table($this->Table5)->whereNull('top_class')->whereNull('deleted_at')->get();
        $layer=array();
        $seat =array();
        //四個都有
        if(Input::has('layer')){
            $layer=db::table($this->Table5)->where('top_class',$data['area'])->whereNull('deleted_at')->get();
            if(count($layer)>0)
            $seat =db::table($this->Table5)->where('top_class',$data['layer'])->where('position',$data['position'])->whereNull('deleted_at')->get();
        }elseif(Input::has('area')){
            $layer=db::table($this->Table5)->where('top_class',$data['area'])->whereNull('deleted_at')->get();
            if(count($layer)>0)
            $seat =db::table($this->Table5)->where('top_class',$layer[0]->id)->where('position',$data['position'])->whereNull('deleted_at')->get();
        }
        $position=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        return view('admin.Form.tombstone_sys',['area'=>$area,'layer'=>$layer,'seat'=>$seat,'position'=>$position,'request_data'=>$data]);
    }
    public function calculate()
    {
        $data=Input::all();
        $cost=db::Table('tombstone_cost')->where('type',$data['identity'])->first();
        if($data['tombstone_code']==''){
            return json_encode(array(0));
        }
        $price=$cost->cost;
        return json_encode(array($price));
    }
    public function Pdf()
    {   
        $type=array();
        $data=db::table('tombstone_appliction as cp')->where('no',Input::get('no'))
        ->where('applicant_number',Input::get('number'))
        ->select('cp.*','data.*','user.*','p.*','cp.id as id','cp.cost as cost','cp.other_cost as other_cost')
        ->join('tombstone_appliction_data as data','cp.id','=','data.c_no')
        ->join('tombstone_appliction_user as user','cp.id','=','user.c_no')
        ->join('tombstone_principal as p','cp.id','=','p.c_no')
        ->first();
        $data2=db::table('tombstone_appliction_data')
        ->where('c_no',$data->id)
        ->get();
        $user=db::table('tombstone_appliction_user')
        ->where('c_no',$data->id)
        ->get();
        $type=array();
        foreach ($data2 as $key => $value) {
            $number=db::table('tombstone_sys')->where('code',$value->tombstone_code)->first();
            if($user[$key]->name_number==$number->number){
                $new_data[$number->code]=$user[$key];
                $type[$number->code]=0;
            }else{
                $new_data[$number->code]='';
                $type[$number->code]=1;
            }
        }
        $data2=array();
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                if($type[$key]==0){
                    $data2[]=array('gender' => $value->gender,'name' => $value->name,'birthday' => $value->birthday,'code' => $key,'address'=>$value->address,'dead_date'=>'');
                }else{
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
                }
            }
        }else{
            foreach ($type as $key => $value) {
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
            }
        }
        $data2=json_decode(json_encode($data2));;
        $html = view('admin.Pdf.index_1',['data'=>$data,'data2'=>$data2])->render();
        return $html;
    }
    public function Pdf2()
    {   
        $type=array();
        $data=db::table('tombstone_appliction as cp')->where('no',Input::get('no'))
        ->where('applicant_number',Input::get('number'))
        ->select('cp.*','data.*','user.*','p.*','cp.id as id','cp.cost as cost','cp.other_cost as other_cost')
        ->join('tombstone_appliction_data as data','cp.id','=','data.c_no')
        ->join('tombstone_appliction_user as user','cp.id','=','user.c_no')
        ->join('tombstone_principal as p','cp.id','=','p.c_no')
        ->first();
        $data2=db::table('tombstone_appliction_data')
        ->where('c_no',$data->id)
        ->get();
        $user=db::table('tombstone_appliction_user')
        ->where('c_no',$data->id)
        ->get();
        $type=array();
        foreach ($data2 as $key => $value) {
            $number=db::table('tombstone_sys')->where('code',$value->tombstone_code)->first();
            if($user[$key]->name_number==$number->number){
                $new_data[$number->code]=$user[$key];
                $type[$number->code]=0;
            }else{
                $new_data[$number->code]='';
                $type[$number->code]=1;
            }
        }
        $data2=array();
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                if($type[$key]==0){
                    $data2[]=array('gender' => $value->gender,'name' => $value->name,'birthday' => $value->birthday,'code' => $key,'address'=>$value->address,'dead_date'=>'');
                }else{
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
                }
            }
        }else{
            foreach ($type as $key => $value) {
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
            }
        }
        if(count($data2)!=12){
            $count=12-count($data2);
            for ($i=1; $i <=$count ; $i++) { 
                $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>'-','dead_date'=>'','address'=>'');
            }
        }
        $data2=json_decode(json_encode($data2));
        $html = view('admin.Pdf.index2_2',['data'=>$data,'data2'=>$data2])->render();
        return $html;
    }
    public function Pdf3()
    {
        $type=array();
        $data=db::table('tombstone_appliction as cp')->where('no',Input::get('no'))
        ->where('applicant_number',Input::get('number'))
        ->select('cp.*','data.*','user.*','p.*','cp.id as id','cp.cost as cost','cp.other_cost as other_cost')
        ->join('tombstone_appliction_data as data','cp.id','=','data.c_no')
        ->join('tombstone_appliction_user as user','cp.id','=','user.c_no')
        ->join('tombstone_principal as p','cp.id','=','p.c_no')
        ->first();
        $data2=db::table('tombstone_appliction_data')
        ->where('c_no',$data->id)
        ->get();
        $user=db::table('tombstone_appliction_user')
        ->where('c_no',$data->id)
        ->get();
        $type=array();
        foreach ($data2 as $key => $value) {
            $number=db::table('tombstone_sys')->where('code',$value->tombstone_code)->first();
            if($user[$key]->name_number==$number->number){
                $new_data[$number->code]=$user[$key];
                $type[$number->code]=0;
            }else{
                $new_data[$number->code]='';
                $type[$number->code]=1;
            }
        }
        $data2=array();
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                if($type[$key]==0){
                    $data2[]=array('gender' => $value->gender,'name' => $value->name,'birthday' => $value->birthday,'code' => $key,'address'=>$value->address,'dead_date'=>'');
                }else{
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
                }
            }
        }else{
            foreach ($type as $key => $value) {
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
            }
        }
        if(count($data2)!=12){
            $count=12-count($data2);
            for ($i=1; $i <=$count ; $i++) { 
                $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>'-','dead_date'=>'','address'=>'');
            }
        }
        $data2=json_decode(json_encode($data2));
        $html = view('admin.Pdf.index3_3',['data'=>$data,'data2'=>$data2])->render();
        return $html;
    }
    public function Pdf4()
    {
        $type=array();
        $data=db::table('tombstone_appliction as cp')->where('no',Input::get('no'))
        ->where('applicant_number',Input::get('number'))
        ->select('cp.*','data.*','user.*','p.*','cp.id as id','cp.cost as cost','cp.other_cost as other_cost')
        ->join('tombstone_appliction_data as data','cp.id','=','data.c_no')
        ->join('tombstone_appliction_user as user','cp.id','=','user.c_no')
        ->join('tombstone_principal as p','cp.id','=','p.c_no')
        ->first();
        $data2=db::table('tombstone_appliction_data')
        ->where('c_no',$data->id)
        ->get();
        $user=db::table('tombstone_appliction_user')
        ->where('c_no',$data->id)
        ->get();
        $type=array();
        foreach ($data2 as $key => $value) {
            $number=db::table('tombstone_sys')->where('code',$value->tombstone_code)->first();
            if($user[$key]->name_number==$number->number){
                $new_data[$number->code]=$user[$key];
                $type[$number->code]=0;
            }else{
                $new_data[$number->code]='';
                $type[$number->code]=1;
            }
        }
        $data2=array();
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                if($type[$key]==0){
                    $data2[]=array('gender' => $value->gender,'name' => $value->name,'birthday' => $value->birthday,'code' => $key,'address'=>$value->address,'dead_date'=>'');
                }else{
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
                }
            }
        }else{
            foreach ($type as $key => $value) {
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
            }
        }
        $data2=json_decode(json_encode($data2));
        $html = view('admin.Pdf.index4_4',['data'=>$data,'data2'=>$data2])->render();
        return $html;
    }
    public function get_data()
    {
        $data=Input::all();
        switch ($data['type']) {
            case '1':
            $table='tombstone_appliction';
            $data=db::Table($table)
                ->select('applicant','tel','phone','address','household_registration')
                ->where('applicant_number',$data['number'])
                ->orderBy('id','desc')
                ->first();
                break;
            case '2':
            $table='tombstone_principal';
            $data=db::Table($table)
                ->select('client','client_tel','client_address','client_phone','client_household_registration','client_address')
                ->where('client_number',$data['number'])
                ->orderBy('id','desc')
                ->first();
                break;
            default: break;
        }
        if($data){
                return json_encode($data);
        }else{
            return json_encode(array(0));
        }
    }

}