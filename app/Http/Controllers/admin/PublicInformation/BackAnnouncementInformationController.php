<?php

namespace App\Http\Controllers\admin\PublicInformation;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\AnnouncementInformationRequest;
use DB;
use Input;
use Session;
class BackAnnouncementInformationController extends BackController {
    //配合route resource
    protected $RouteName="BackAnnouncementInformation";
    //檔案路徑
    protected $Path="Upload/AnnouncementInformation/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\PublicInformation\AnnouncementInformation";

    //主要table
    protected $Table1="announcement_information";
    //關聯table
    protected $Table2="announcement_information_class";
    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '3'),
            array('title' => '類別', 'width' => '6'),
            array('title' => '標題', 'width' => '10'),
            array('title' => '下架時間', 'width' => '10'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select('id','class_id','title','deleted_at')
        ->get();


        foreach ($Data as $key => $value) {
            $name=DB::table($this->Table2)->whereNull('deleted_at')->where('id',$value->class_id)->first();
            if(isset($name->name)){
                $value->class_id=$name->name;
            }else{
                $name=DB::table($this->Table2)->where('id',$value->class_id)->first();
                $value->class_id=$name->name.'(已下架)';    
            }
        }
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 1, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "服務公告新增";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementInformationRequest $request)
    {   
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$request->all();
        
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::withTrashed()->find($id));
        //表格標題
        $LayoutTitle = "服務公告修改";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH','',$id);
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'SelectValue'=>$data->product_class_id,'SelectValue2'=>json_encode(explode(',',$data->product_specification_id)),'id'=>$id]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnnouncementInformationRequest $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::withTrashed()->find($id)->update(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {

        $option=ArrayToLaravelFormSelectOption(db::table('announcement_information_class')->select('id','name')->get());

        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            array('id' => 'class_id', 'name' => 'class_id', 'CNname' => '分類：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $option, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['class_id'] : null),
            array('id' => 'title', 'name' => 'title', 'CNname' => '標題：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['title'] : null),
            array('id' => 'body', 'name' => 'body', 'CNname' => '內文：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => '', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($Data)) ? $Data['body'] : null),
             array('id' => 'file1', 'name' => 'file1', 'CNname' => '圖片：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '','Path'=>$this->Path, 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file1'] : null,'table'=>$this->Table1,'Path'=>$this->Path)

           );
        return $BodyArray;
    }
}