<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Order\Order;
use Auth;
use Redirect;
use Session;
use Input;
use DB;
class BackApplictionController extends Controller
{
    public function c_delete()
    {

        $data=Input::all();
        $c=db::Table('cabinet_appliction')->where('no',$data['no'])->wherenull('deleted_at')->first();
        db::Table('cabinet_appliction')->where('no',$data['no'])->update(['deleted_at'=>date('Y-m-d H:i:s')]);
        if($c->status<=300){
             $cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('c_no',$c->id)->get();
            foreach($cabinet_appliction_data as $key =>$value){
                db::table('cabinet_sys')->where('code',$value->cabinet_code)->update(['number'=>null,'c_no'=>null]);
            }
        }else if($c->status>=400&&$c->status<=600){
            $before_appliction=db::Table('cabinet_appliction')->where('id',$c->before_appliction_id)->first();
            db::table('cabinet_appliction')->where('id',$c->before_appliction_id)->update(['status'=>$before_appliction->status-50]);
            $data=db::Table('cabinet_appliction_data')->where('c_no',$c->id)->get();
            foreach($data as $key =>$value){
                db::table('cabinet_sys')->where('code',$value->cabinet_code)->update(['number'=>null,'c_no'=>null]);
            }
        }else if($c->status>=700&&$c->status<=800) {
            $before_appliction=db::Table('cabinet_appliction')->where('id',$c->before_appliction_id)->first();
            db::table('cabinet_appliction')->where('id',$c->before_appliction_id)->update(['status'=>$before_appliction->status-99]);
            $data=db::Table('cabinet_appliction_data')->where('c_no',$before_appliction->id)->get();
            $user=db::Table('cabinet_appliction_user')->where('c_no',$before_appliction->id)->get();
        }
       
        return redirect()->back();
    }
    public function t_delete()
    {
        $data=Input::all();
        $t=db::Table('tombstone_appliction')->where('no',$data['no'])->first();
            db::Table('tombstone_appliction')->where('no',$data['no'])->update(['deleted_at'=>date('Y-m-d H:i:s')]);
        if($t->status<=300){
            $tombstone_appliction_data=db::Table('tombstone_appliction_data')->where('c_no',$t->id)->get();
            foreach($tombstone_appliction_data as $key =>$value){
                db::table('tombstone_sys')->where('code',$value->tombstone_code)->update(['number'=>null,'c_no'=>null]);
            } 
        }else if($t->status>=700){
            $before_appliction=db::Table('tombstone_appliction')->where('id',$t->before_appliction_id)->first();
            db::table('tombstone_appliction')->where('id',$t->before_appliction_id)->update(['status'=>300]);
            $data=db::Table('tombstone_appliction_data')->where('c_no',$before_appliction->id)->get();
            $user=db::Table('tombstone_appliction_user')->where('c_no',$before_appliction->id)->get();
        }
        
        return redirect()->back();
    }
    
}
