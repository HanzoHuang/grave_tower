<?php

namespace App\Http\Controllers\admin\TombstoneCost;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\TombstoneOtherCostRequest;
use DB;
use Input;
use Session;
class BackTombstoneOtherCostController extends BackController {
    //配合route resource
    protected $RouteName="BackTombstoneOtherCost";
    //檔案路徑
    protected $Path="Upload/TombstoneOtherCost/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\TombstoneCost\TombstoneOtherCost";

    //主要table
    protected $Table1="tombstone_other_cost";

    public function index()
    {
        $Data=DB::table($this->Table1)
        ->select('id','cost')
        ->first();
        return view('admin.Form.t_cost_other', ['Data' => $Data]);
    }
    public function store(TombstoneOtherCostRequest $request)
    {   
        $ModelName=$this->ModelName;
      
        $data=$ModelName::firstOrCreate ([
            'id'=>$request->id,
            ]);
        $data->cost=$request->cost;
        $data->save();
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }

}