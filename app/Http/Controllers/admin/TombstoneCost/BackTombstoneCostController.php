<?php

namespace App\Http\Controllers\admin\TombstoneCost;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\TombstoneCostRequest;
use DB;
use Input;
use Session;
class BackTombstoneCostController extends BackController {
    //配合route resource
    protected $RouteName="BackTombstoneCost";
    //檔案路徑
    protected $Path="Upload/TombstoneCost/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\TombstoneCost\TombstoneCost";

    //主要table
    protected $Table1="tombstone_cost";

    public function index()
    {
        $Data=DB::table($this->Table1)
        ->select('id','type','cost','optional_cost','deleted_at')
        ->orderBy('type','asc')
        ->get();
        return view('admin.Form.t_cost', ['Data' => $Data]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TombstoneCostRequest $request)
    {   
        $ModelName=$this->ModelName;
        $data1=$ModelName::where('type',0)->first();
        $data1->cost=$request->cost_1;
        $data1->optional_cost=$request->optional_cost_1;
        $data1->save();
        $data2=$ModelName::where('type',1)->first();
        $data2->cost=$request->cost_2;
        $data2->optional_cost=$request->optional_cost_2;
        $data2->save();
        $data3=$ModelName::where('type',2)->first();
        $data3->cost=$request->cost_3;
        $data3->optional_cost=$request->optional_cost_3;
        $data3->save();
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
}