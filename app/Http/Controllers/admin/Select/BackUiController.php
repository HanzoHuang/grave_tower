<?php

namespace App\Http\Controllers\admin\Select;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use DB;
use Input;
use Session;
class BackUiController extends BackController {
    //配合route resource
    protected $RouteName="BackUi";

    public function show($id,$data,$class,$layer )
    {   
        switch ($id) {
            case 'A':
                return view('admin.Ui.area_person',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'B':
                return view('admin.Ui.area_person_01',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'C':
                return view('admin.Ui.area_person_02',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'D':
                return view('admin.Ui.area_person_03',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'F':
                return view('admin.Ui.area_couple',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'G':
                return view('admin.Ui.area_couple_01',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'H':
                return view('admin.Ui.area_couple_02',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'J':
                return view('admin.Ui.area_family_01',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'K':
                return view('admin.Ui.area_family',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'N':
                return view('admin.Ui.area_gad_01',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
            case 'S':
                return view('admin.Ui.area_gad',['data'=>$data,'class'=>$class,'layer'=>$layer]);
                break;
        }
    }

    public function edit($id)
    {   
        $lo_data=explode('-', $id);
        $data=$this->api_select_larye($lo_data[0],$lo_data[1]);
        foreach ($data as $key => $value) {
            $sort=explode('-',$value->code);
            if(empty($sort[4])){
                $new_data[$sort[2]][$sort[3]]=$value;
            }else{
                $new_data[$sort[3]][$sort[4]]=$value;
            }
            
            }
        return $this->show($lo_data[0],$new_data,$lo_data[0],$lo_data[1]);
    }


    public function api_select_larye($class,$layer)
    {   
        $numner='1-'.$class.'-';
        if($layer!=''){
            $numner=$numner.$layer.'-';
        }
        $data=DB::table('cabinet_sys')->select('code','number','c_no')->where('code','like','%'.$numner.'%')->orderby('row','asc')->orderby('seat','asc')->get();  

        if(count($data)==0){
            $numner=str_replace('-1-', '', $numner);
            $data=DB::table('tombstone_sys')->select('code','number','c_no')->where('code','like','%'.$numner.'%')->orderby('seat','asc')->get();  
        }
        return $data;
    }

    public function score_sort($a, $b){
        if($a['type_id'] == $b['type_id']) return 0;
        return ($a['type_id'] > $b['type_id']) ? 1 : -1;
    }

    public function api_select_data()
    {   
        $no=DB::table('cabinet_sys as cs')->where('cs.code',Input::get('code'))
            ->first()->c_no;
        $data=DB::table('cabinet_sys as cs')
            ->select('cs.code','applicant','application_date','household_registration','tel','phone','cau.name','cau.name_number','expected_date','change_date')
            ->join('cabinet_appliction_data as cad','cs.c_no','=','cad.c_no')
            ->join('cabinet_appliction as ca','cad.c_no','=','ca.id')
            ->join('cabinet_appliction_user as cau','ca.id','=','cau.c_no')
            ->where('cs.code',Input::get('code'))
            ->where('cad.c_no',$no)
            ->where('ca.id',$no)
            ->where('cau.c_no',$no)
           ->get();
        $html='<li>';
        $html.='<div class="man_info">申請人：'.$data[0]->applicant.'<br/>申請日期：'.$data[0]->application_date.'<br/>申請人地址：'.$data[0]->household_registration.'<br/>申請人電話：'.$data[0]->tel.'<br/>申請人手機：'.$data[0]->phone.'</div>';
        foreach ($data as $key => $value) {
            $new_data[$data[$key]->name_number]=$value;
        }
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                $new_data[$key]->code=DB::table('cabinet_sys')->where('number',$new_data[$key]->name_number)->first()->code;
            }
            foreach ($new_data as $key => $value) {
                if($key!=''){
                    $html.='<div class="clear_line">---------------------------------------------------------------------</div></li>
                               <li>逝者：'.$new_data[$key]->name.'</li>
                               <li>編碼：'.$new_data[$key]->code.'</li>
                               <li>進塔日期：'.$new_data[$key]->change_date.'</li>
                           ';  
                   }else{
                    $html.='<div class="clear_line">---------------------------------------------------------------------</div></li>
                               <li>逝者：</li>
                               <li>編碼：</li>
                               <li>進塔日期：</li>
                           ';  
                   }
            }
        }else{
            $html.='<div class="clear_line">---------------------------------------------------------------------</div></li>
                       <li>逝者：</li>
                       <li>編碼：</li>
                       <li>進塔日期：</li>
                   ';   
        }   
        return $html;
    }

    public function api_select_data_t()
    {   
        $no=DB::table('tombstone_sys as cs')->where('cs.code',Input::get('code'))
            ->first()->c_no;
        $data=DB::table('tombstone_sys as cs')
             ->select('cs.code','applicant','application_date','household_registration','tel','phone','cau.name','cau.name_number','expected_date','change_date')
            ->join('tombstone_appliction_data as cad','cs.c_no','=','cad.c_no')
            ->join('tombstone_appliction as ca','cad.c_no','=','ca.id')
            ->join('tombstone_appliction_user as cau','ca.id','=','cau.c_no')
            ->where('cs.code',Input::get('code'))
            ->where('cs.code',Input::get('code'))
            ->where('cad.c_no',$no)
            ->where('ca.id',$no)
            ->where('cau.c_no',$no)
            ->first();

        $html='<li>';
        $html.='<div class="man_info">申請人：'.$data->applicant.'<br/>申請日期：'.$data->application_date.'<br/>申請人地址：'.$data->household_registration.'<br/>申請人電話：'.$data->tel.'<br/>申請人手機：'.$data->phone.'</div>';
        $html.='<div class="clear_line">---------------------------------------------------------------------</div></li>
                   <li>逝者：'.$data->name.'</li>
                   <li>身分證字號：'.$data->name_number.'</li>
                   <li>編碼：'.$data->code.'</li>
                   <li>進塔日期：'.$data->change_date.'</li>
               ';   
        return $html;
    }


    public function api_select_data_s()
    {   
        $no=DB::table('cabinet_sys as cs') ->where('cs.code','like',Input::get('code').'%')
            ->first()->c_no;
        $data=DB::table('cabinet_sys as cs')
             ->select('cs.code','applicant','application_date','household_registration','tel','phone','cau.name','cau.name_number','expected_date','change_date')
            ->join('cabinet_appliction_data as cad','cs.c_no','=','cad.c_no')
            ->join('cabinet_appliction as ca','cad.c_no','=','ca.id')
            ->join('cabinet_appliction_user as cau','ca.id','=','cau.c_no')
            ->where('cs.code','like',Input::get('code').'%')
            ->where('cad.c_no',$no)
            ->where('ca.id',$no)
            ->where('cau.c_no',$no)
            ->get();

        $html='<li>';
        $html.='<div class="man_info">申請人：'.$data[0]->applicant.'<br/>申請日期：'.$data[0]->application_date.'<br/>申請人地址：'.$data[0]->household_registration.'<br/>申請人電話：'.$data[0]->tel.'<br/>申請人手機：'.$data[0]->phone.'</div>';
        foreach ($data as $key => $value) {
            if($data[$key]->name_number==''){
                continue;
            }
            $new_data[$data[$key]->name_number]=$value;

        }
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                $new_data[$key]->code=DB::table('cabinet_sys')->where('number',$new_data[$key]->name_number)->first()->code;
            }


            foreach ($new_data as $key => $value) {
            $html.='<div class="clear_line">---------------------------------------------------------------------</div></li>
                       <li>逝者：'.$new_data[$key]->name.'</li>
                       <li>身分證字號：'.$new_data[$key]->name_number.'</li>
                       <li>編碼：'.$new_data[$key]->code.'</li>
                       <li>進塔日期：'.$new_data[$key]->change_date.'</li>
                   ';   
            }
        }else{
            $html.='<div class="clear_line">---------------------------------------------------------------------</div></li>
                       <li>逝者：</li>
                       <li>身分證字號：</li>
                       <li>編碼：</li>
                       <li>進塔日期：</li>
                   ';   
        }
        return $html;
    }

}