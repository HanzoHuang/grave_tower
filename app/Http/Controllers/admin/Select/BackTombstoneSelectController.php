<?php

namespace App\Http\Controllers\admin\Select;
use App\Http\Controllers\BackController;
use Illuminate\Http\Request;
use DB;
use Input;
use Session;
use Auth;
class BackTombstoneSelectController extends BackController {
    //配合route resource
    protected $RouteName="BackTombstoneSelect";
    //檔案路徑
    protected $Path="Upload/TombstoneSelect/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\PublicInformation\TombstoneSelect";

    //主要table
    protected $Table1="pd_log";
    protected $Table3="tombstone_sys_class";
    protected $Table5="tombstone_sys_class";
    public function index()
    {
        $serach=Input::all();
        $BodyArray = array(
            array('title' => 'Id', 'width' => '3'),
            array('title' => '申請編號', 'width' => '6'),
            array('title' => '牌位位置', 'width' => '10'),
            array('title' => '方位', 'width' => '10'),
            array('title' => '牌位者', 'width' => '20'),
            array('title' => '身分證字號', 'width' => '20'),
            array('title' => '申請人', 'width' => '20'),
            array('title' => '身分證字號', 'width' => '20'),
            array('title' => '申請日期', 'width' => '20'),
            array('title' => '連絡電話', 'width' => '20'),
            array('title' => '行動電話', 'width' => '20'),
            array('title' => '作業', 'width' => '20'),
            array('title' => '狀態', 'width' => '20'),
            array('title' => '操作', 'width' => '20'),
        );

        $Data=DB::table($this->Table1)
        ->select('id','no','code','class_position','user_name','user_number','name','number','application_date','tel','phone','status as status2','status')
        ->where('class_type','牌位')
        ->orderBy($this->Table1.'.updated_at','desc')
        ->orderBy($this->Table1.'.created_at','desc');
        $serach_count=count($serach);
        $i=0;
        foreach ($serach as $key => $value) {
            if($value==""){
                continue;
            }
            switch ($key) {
                case 'name':
                    $Data=$Data->where('name',$value);
                    $i++;
                    break;
                case 'user_name':
                    $Data=$Data->where('user_name',$value);
                    $i++;
                     break;
                case 'application_start_date':
                case 'application_end_date':
                     $Data=$Data
                        ->whereBetween($this->Table1.'.application_date',[$serach['application_start_date'],$serach['application_end_date']]);
                        $i++;
                    break;
                case 'expected_date_start_date':
                case 'expected_date_end_date':
                     $Data=$Data
                        ->whereBetween($this->Table1.'.expected_date_date',[$serach['expected_date_start_date'],$serach['expected_date_end_date']]);
                        $i++;
                    break;
                case 'number':
                     $Data=$Data->where('number',$value);
                        $i++;
                    break;
                case 'user_number':
                     $Data=$Data->where('user_number',$value);
                        $i++;
                    break;
                case 'identity':
                     $Data=$Data->where('identity',$value);
                        $i++;
                    break;
                case 'status_type':
                    if($value==0){
                       $Data=$Data->where('status','<',400);
                    }elseif($value==1){
                       $Data=$Data->where('status','>=',400)->where('status','<=',600);
                    }else{
                        $Data=$Data->where('status','>=',700)->where('status','<=',800);
                    }
                        $i++;
                    break;
                case 'status':
                     $Data=$Data->where('status',$value);
                    break;
                case 'class_floor':
                case 'class_area':
                case 'class_layer':
                case 'class_row':
                case 'class_position':
                    $data=array();
                    $data['floor']=1;
                    $area=db::table($this->Table3)->where('id',Input::get('class_area'))->first();
                    if($serach['class_area']!=0){
                        if($area){
                            $data['area']=$area->code;
                            if($serach['class_layer']!=0){
                                $layer=db::table($this->Table3)->where('id',Input::get('class_layer'))->first();
                                if($layer){
                                    $data['layer']=$layer->code;
                                     $row=db::table($this->Table3)->where('id',Input::get('class_row'))->first();
                                     if($row){
                                        $data['row']=$row->code;
                                    }
                                    
                                }
                            }
                        }
                    }
                    $code=implode('-',$data);
                    $Data=$Data->where('code','like','%'.$code.'%');
                        $i++;
                    // $Data=$Data->where('class_position',Input::get('class_position'));
                    break;
                default:
                    break;
            }
        }  
        if($i>0&&$i<=3){
            return redirect()->to($this->RouteName)->with('status','請增加搜尋條件');
        }
        $Data=$Data->get(); 
        $Data=getDataTableImages($Data,$this->Path);
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' =>0, 'Delete' => 0, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0,'Goto2'=>1,'print2'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $sys=array();
        $sys['layer']=array();
        $sys['seat']=array();
        $sys['position']=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        $sys['area']=db::Table($this->Table5)->whereNull('top_class')->whereNull('deleted_at')->get();
        if(count($sys['area'])>0){
            $sys['layer']=db::table($this->Table5)->where('top_class',$sys['area'][0]->id)->whereNull('deleted_at')->get();
        }
        if(count($sys['layer'])>0){
            $sys['seat']=db::table($this->Table5)->where('top_class',$sys['layer'][0]->id)->whereNull('deleted_at')->get();
        }
        $option=CabinetSys();
        foreach($Data as $key =>$value){
            $Data[$key]->status=$option['status'][$Data[$key]->status];
            $Data[$key]->class_position=$option['position'][$Data[$key]->class_position];
            if($Data[$key]->status2<=399){
                $Data[$key]->status2='申請模組';
            }elseif( $Data[$key]->status2>=400 and $Data[$key]->status2<=699){
                $Data[$key]->status2='異動模組';
            }elseif( $Data[$key]->status2>=700 ){
                $Data[$key]->status2='遷出模組';
            }
        }
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2,0,'width="2000px"');
        return view('admin.DataTable.tombstone_select', ['DataTableJs' => $DataTableJs,'Url'=>$this->RouteName,'sys'=>$sys,'serach_count'=>$serach_count]);
    }
    public function change_select($value='')
    {
        $data=Input::all();
        $sys['area'] =db::Table($this->Table5)->whereNull('top_class')->whereNull('deleted_at')->get();
        $sys['layer']=array();
        $sys['seat'] =array();
        if(Input::has('layer')){
            $sys['layer']=db::table($this->Table5)->where('top_class',$data['area'])->whereNull('deleted_at')->get();
            if(count($sys['layer'])>0)
            $sys['seat'] =db::table($this->Table5)->where('top_class',$data['layer'])->where('position',$data['position'])->whereNull('deleted_at')->get();
        }elseif(Input::has('area')){
            $sys['layer']=db::table($this->Table5)->where('top_class',$data['area'])->whereNull('deleted_at')->get();
            if(count($sys['layer'])>0)
            $sys['seat'] =db::table($this->Table5)->where('top_class',$sys['layer'][0]->id)->where('position',$data['position'])->whereNull('deleted_at')->get();
        }
        $sys['position']=array(0=>'無',1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        return view('admin.Form.tombstone_sys_select',['sys'=>$sys,'request_data'=>$data]);
    }
    public function show($no)
    {   
        $tombstone_sys=DB::Table('tombstone_sys')->where('status',1)->get();

        $tombstone_appliction=db::table('tombstone_appliction')->where('no',$no)->where('applicant_number',Input::get('number'))->first();
        $users=db::Table('users')->find($tombstone_appliction->users);
        $confirm_payment_user=db::table('users')->find($tombstone_appliction->confirm_payment_user);
        $tombstone_appliction_user=db::Table('tombstone_appliction_user')->where('c_no',$tombstone_appliction->id)->first();
        $tombstone_appliction_data=db::Table('tombstone_appliction_data')->where('c_no',$tombstone_appliction->id)->first();
        $tombstone_principal=db::Table('tombstone_principal')->where('c_no',$tombstone_appliction->id)->first();
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$no;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function

        switch ($tombstone_appliction->status) {
            case '100':
                $LayoutTitle = "牌位申請-修改模式";
                break;
            case '200':
                $LayoutTitle = "牌位繳費-修改模式";
                break;
            case '300':
                $LayoutTitle = "牌位入塔-修改模式";
                break;
            case '400':
                $LayoutTitle = "牌位異動申請繳費-修改模式";
                break;
            case '500':
                $LayoutTitle = "牌位異動申請繳費-修改模式";
                break;
            case '600':
                $LayoutTitle = "牌位異動-修改模式";
                break;
            case '700':
                $LayoutTitle = "牌位遷出申請-修改模式";
                break;
            case '800':
                $LayoutTitle = "牌位已遷出-修改模式";
                break;
            default:
                $LayoutTitle = "結案-修改模式";
                break;
        }

        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);

        $data=[
            'FormTtile' => $FormTtile,
            'id'=>$tombstone_appliction->id,'url'=>$Url,
            'tombstone_appliction'=>$tombstone_appliction,
            'tombstone_appliction_user'=>$tombstone_appliction_user,
            'tombstone_appliction_data'=>$tombstone_appliction_data,
            'tombstone_principal'=>$tombstone_principal,
            'users'=>$users,
            'confirm_payment_user'=>$confirm_payment_user,
            'show'=>1,
        ];
        switch ($tombstone_appliction->status) {
            case '100':
                $data['LayoutTitle'] = "牌位申請";
                return view('admin.Form.tombstone_appliction_edit2', $data);
                break;
            case '200':

                $data['LayoutTitle'] = "牌位繳費";
                return view('admin.Form.tombstone_appliction_edit2', $data);
                break;
            case '300':
                $data['LayoutTitle'] = "牌位入塔";
                return view('admin.Form.tombstone_appliction_edit2', $data);
                break;
            case '350':
                $data['LayoutTitle'] = "牌位入塔";
                return view('admin.Form.tombstone_appliction_edit2', $data);
                break;
            case '399':
                $data['LayoutTitle'] = "牌位入塔";
                return view('admin.Form.tombstone_appliction_edit2', $data);
                break;
            case '700':
                $data['LayoutTitle'] = "牌位遷出申請";
                return view('admin.Form.tombstone_out_appliction_edit2', $data);
                break;
            case '800':
                $data['LayoutTitle'] = "牌位已遷出";
                return view('admin.Form.tombstone_out_appliction_edit2', $data);
                break;
        }
    }
    public function update(Request $request ,$no)
    {   
        $tombstone_appliction=array();
        // $tombstone_appliction['no']=$request->no;sst
        // $tombstone_appliction['tombstone_appliction_type']=$request->tombstone_appliction_type;
        $tombstone_appliction['application_date']=$request->application_date;
        $tombstone_appliction['household_registration']=$request->household_registration;
        $tombstone_appliction['applicant_number']=$request->applicant_number;
        $tombstone_appliction['applicant']=$request->applicant;
        $tombstone_appliction['tel']=$request->tel;
        $tombstone_appliction['phone']=$request->phone;
        $tombstone_appliction['relationship']=$request->relationship;
        $tombstone_appliction['address']=$request->address;
        $tombstone_appliction['ext']=$request->ext;
        $tombstone_appliction['principal']=$request->principal;
        $tombstone_appliction['change_date']=$request->change_date;
        $tombstone_appliction['other_amount_due']=$request->other_amount_due;
        $tombstone_appliction['amount_due']=$request->amount_due;
        //$tombstone_appliction['code']=$request->cabinet_code;
        //要改變成已繳費
        if($request->status==200){
            $tombstone_appliction['confirm_payment_user']=Auth::user()->id;
            $tombstone_appliction['payment_date']=$request->payment_date;
        }
        //要改變成已入塔
        if($request->status==300){
            $tombstone_appliction['principal']=$request->principal;
            $tombstone_appliction['change_date']=$request->change_date;
        }
        for ($i = 1; $i < 8; $i++) {
            $name='file'.$i;
            if(Input::hasFile($name)){
            $filename = date('Ymd').rand(11111, 99999);
            $tombstone_appliction[$name] = $filename .".". Input::file($name)->getClientOriginalExtension();
                Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
            }
        }
        db::Table('tombstone_appliction')->where('id',$request->id)->update($tombstone_appliction);

        $tombstone_appliction_user=array();
        $tombstone_appliction_user['c_no']=$request->id;
        $tombstone_appliction_user['name']=$request->name;
        $tombstone_appliction_user['name_number']=$request->name_number;
        $tombstone_appliction['applicant']=$request->applicant;
        //$tombstone_appliction['application_type']=$request->application_type;
        $tombstone_appliction_user['address']=$request->address;
        $tombstone_appliction_user['gender']=$request->gender;
        $tombstone_appliction_user['identity']=$request->identity;
        $tombstone_appliction_user['discount']=$request->discount;
        $tombstone_appliction_user['birthday']=$request->birthday;
        $tombstone_appliction_user['expected_date']=$request->expected_date;
        $tombstone_appliction_user['expected_time']=$request->expected_time;
        DB::table('tombstone_appliction_user')->where('id',$request->tombstone_appliction_user_id)->update($tombstone_appliction_user);
        
        $tombstone_appliction_data=array();
        // $tombstone_appliction_data['c_no']=$tombstone_appliction_id;
        // $tombstone_appliction_data['tombstone_type']=$request->tombstone_type;
        // $tombstone_appliction_data['tombstone_class_floor']=$request->tombstone_class_floor;
        // $tombstone_appliction_data['tombstone_class_aera']=$request->tombstone_class_aera;
        // $tombstone_appliction_data['tombstone_class_row']=$request->tombstone_class_row;
        //$tombstone_appliction_data['tombstone_class_layer']=$request->tombstone_class_layer;
        // $tombstone_appliction_data['tombstone_class_seat']=$request->tombstone_class_seat;
        //$tombstone_appliction_data['cost']=$request->cost;
        //$tombstone_appliction_data['other_cost']=$request->other_cost;
        //$tombstone_appliction_data['amount_due']=$request->amount_due;
        //DB::Table('tombstone_appliction_data')->where('id',$request->tombstone_appliction_data_id)->update($tombstone_appliction_data);

        $tombstone_principal=array();
        $tombstone_principal['c_no']=$request->id;
        $tombstone_principal['client']=$request->client;
        $tombstone_principal['client_number']=$request->client_number;
        $tombstone_principal['client_household_registration']=$request->client_household_registration;
        $tombstone_principal['client_tel']=$request->client_tel;
        $tombstone_principal['client_phone']=$request->client_phone;
        $tombstone_principal['client_address']=$request->client_address;
        DB::table('tombstone_principal')->where('id',$request->tombstone_principal_id)->update($tombstone_principal);
        Exist('tombstone_sys',$request->cabinet_code,$request->status,$request->name_number,'',
                    $request->id);
        $no=db::Table('tombstone_appliction')->where('id',$request->id)->first()->no;
        DB::table('pd_log')->where('no',$no)->update(['code'=>$request->tombstone_code,'number'=>$request->applicant_number,'user_number'=>$request->name_number,'application_date'=>$request->application_date,'expected_date'=>$request->expected_date,'name'=>$request->applicant,'user_name'=>$request->name,'tel'=>$request->tel,'phone'=>$request->phone,'identity'=>$request->identity]);
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }

}