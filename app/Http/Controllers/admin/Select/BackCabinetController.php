<?php

namespace App\Http\Controllers\admin\Select;
use Illuminate\Http\Request;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\BackCabinetRequest;
use DB;
use Input;
use Session;
class BackCabinetController extends BackController {
    //配合route resource
    protected $RouteName="BackCabinet";
    //檔案路徑
    protected $Path="Upload/BackCabinet/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\PublicInformation\BackCabinet";

    //主要table
    protected $Table1="pd_log";
    //關聯table
    protected $Table2="roles";
    protected $Table3="cabinet_sys_class";
    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '3'),
            array('title' => '申請編號', 'width' => '6'),
            array('title' => '牌位位置', 'width' => '10'),
            array('title' => '方位', 'width' => '10'),
            array('title' => '櫃位者', 'width' => '20'),
            array('title' => '櫃位者身分證字號', 'width' => '20'),
            array('title' => '申請人', 'width' => '20'),
            array('title' => '申請人身分證字號', 'width' => '20'),
            array('title' => '申請日期', 'width' => '20'),
            array('title' => '連絡電話', 'width' => '20'),
            array('title' => '行動電話', 'width' => '20'),
            array('title' => '作業', 'width' => '20'),
            array('title' => '狀態', 'width' => '20'),
            array('title' => '操作', 'width' => '20'),
        );
        $Data=DB::table($this->Table1)
        ->select('id','no','code','class_position','user_name','user_number','name','number','application_date','tel','phone','status','status as status2')
        ->where('class_type','櫃位')
        ->orderBy($this->Table1.'.updated_at','desc')
        ->orderBy($this->Table1.'.created_at','desc');
        $serach=Input::all();
        $serach_coount=count($serach);
        $i=0;
        foreach ($serach as $key => $value) {
            if($value==""){
                continue;
            }
            switch ($key) {
                case 'name':
                    $Data=$Data->where('name',$value);
                    $i++;
                     break;
                case 'user_name':
                    $Data=$Data->where('user_name',$value);
                    $i++;
                     break;
                case 'application_start_date':
                    $Data=$Data->whereBetween($this->Table1.'.application_date',[$serach['application_start_date'],$serach['application_end_date']]);
                    $i++;
                    break;
                case 'expected_start_date':
                    $Data=$Data->whereBetween($this->Table1.'.application_date',[$serach['application_start_date'],$serach['application_end_date']]);
                    // $Data=$Data->whereBetween($this->Table1.'.application_date',[$start,$end]);
                    $i++;
                    break;
                case 'death_start_date':
                    $Data=$Data->whereBetween($this->Table1.'.death_date',[$serach['death_start_date'],$serach['death_end_date']]);
                    $i++;
                case 'death_end_date':
                    $Data=$Data->whereBetween($this->Table1.'.death_date',[$serach['death_start_date'],$serach['death_end_date']]);
                    $i++;
                    break;
                case 'number':
                     $Data=$Data->where('number',$value);
            $i++;
                    break;
                case 'user_number':
                     $Data=$Data->where('user_number',$value);
            $i++;
                    break;
                case 'identity':
                     $Data=$Data->where('identity',$value);
            $i++;
                    break;
                case 'status_type':
                    if($value==0){
                       $Data=$Data->where('status','<',400);
                    }elseif($value==1){
                       $Data=$Data->whereIn('status',[400,500,600]);
                    }else{
                        $Data=$Data->whereIn('status',[700,800]);
                    }
                $i++;
                    break;
                case 'status':
                     $Data=$Data->where('status',$value);
                    break;
                case 'class_floor':
                case 'class_area':
                case 'class_layer':
                case 'class_row':
                case 'class_position':
                    $data=array();
                    $data['floor']=1;
                    $area=db::table($this->Table3)->where('id',Input::get('class_area'))->first();
                    if($serach['class_area']!=0){
                        if($area){
                            $data['area']=$area->code;
                            if($serach['class_layer']!=0){
                                $layer=db::table($this->Table3)->where('id',Input::get('class_layer'))->first();
                                if($layer){
                                    $data['layer']=$layer->code;
                                    if($serach['class_row']!=0){
                                         $row=db::table($this->Table3)->where('id',Input::get('class_row'))->first();
                                         if($row){
                                            $data['row']=$row->code;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // dd($serach);
                    $code=implode('-',$data);
                    // dd($code);
                    $Data=$Data->where('code','like','%'.$code.'%');
                    $i++;
                    // $Data=$Data->where('class_position',Input::get('class_position'));
                    break;
                default: break;
            }
        }  
        if($i>0&&$i<=3){
            return redirect()->to($this->RouteName)->with('status','請增加搜尋條件');
        }
        $Data=$Data->get();
        $Data=getDataTableImages($Data,$this->Path);
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 0, 'Delete' =>0, 'Check' => 0, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0,'Goto'=>1,'print'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $sys=array();
        $sys['area']=array();
        $sys['layer']=array();
        $sys['row']=array();
        $sys['seat']=array();
        $sys['position']=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        $sys['area']=db::Table('cabinet_sys_class')->whereNull('top_class')->whereNull('deleted_at')->get();
        if(count($sys['area'])>0){
            $sys['layer']=db::table('cabinet_sys_class')->where('top_class',$sys['area'][0]->id)->whereNull('deleted_at')->get();
        }
        if(count($sys['layer'])>0){
            $sys['row']=db::table('cabinet_sys_class')->where('top_class',$sys['layer'][0]->id)->whereNull('deleted_at')->get();
        }
        if(count($sys['row'])>0){
            $sys['seat']=db::table('cabinet_sys_class')->where('top_class',$sys['row'][0]->id)->where('position',1)->whereNull('deleted_at')->get();
        }
        $option=CabinetSys();
        
        foreach($Data as $key =>$value){
            $Data[$key]->status=$option['status'][$Data[$key]->status];
            $Data[$key]->class_position=$option['position'][$Data[$key]->class_position];
            if($Data[$key]->status2<=399){
                $Data[$key]->status2='申請模組';
                
            }elseif( $Data[$key]->status2>=400 and $Data[$key]->status2<=699){
                $Data[$key]->status2='異動模組';
                
            }elseif( $Data[$key]->status2>=700 ){
                $Data[$key]->status2='遷出模組';
            }

           
        }
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2,0,'width="2000px"');
        return view('admin.DataTable.cabinet_select', ['DataTableJs' => $DataTableJs,'Url'=>$this->RouteName,'sys'=>$sys,'serach_coount'=>$serach_coount]);
    }
    public function change_select()
    {
        $data=Input::all();
        $sys =array();
        $sys['area'] =array();
        $sys['layer']=array();
        $sys['row']  =array();
        $sys['seat'] =array();
        $sys['area'] =db::Table('cabinet_sys_class')->whereNull('top_class')->whereNull('deleted_at')->get();
        // 五個都有
        if(Input::has('row')){
            $sys['layer']=db::table('cabinet_sys_class')->where('top_class',$data['area'])->whereNull('deleted_at')->get();
            $sys['row']  =db::table('cabinet_sys_class')->where('top_class',$data['layer'])->whereNull('deleted_at')->get();
            $sys['seat'] =db::table('cabinet_sys_class')->where('top_class',$data['row'])->where('position',$data['position'])->whereNull('deleted_at')->get();
        }elseif(Input::has('layer')){
            $sys['layer']=db::table('cabinet_sys_class')->where('top_class',$data['area'])->whereNull('deleted_at')->get();
            $sys['row']  =db::table('cabinet_sys_class')->where('top_class',$data['layer'])->whereNull('deleted_at')->get();
            if(count($sys['row'])>0)
            $sys['seat'] =db::table('cabinet_sys_class')->where('top_class',$sys['row'][0]->id)->where('position',$data['position'])->whereNull('deleted_at')->get();

        }elseif(Input::has('area')){
            $sys['layer']=db::table('cabinet_sys_class')->where('top_class',$data['area'])->whereNull('deleted_at')->get();
            if(count($sys['layer'])>0)
            $sys['row']  =db::table('cabinet_sys_class')->where('top_class',$sys['layer'][0]->id)->whereNull('deleted_at')->get();
            if(count($sys['row'])>0)
            $sys['seat'] =db::table('cabinet_sys_class')->where('top_class',$sys['row'][0]->id)->where('position',$data['position'])->whereNull('deleted_at')->get();
        }
        $sys['position']=array(0=>'無',1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        return view('admin.Form.cabinet_sys_select',['sys'=>$sys,'request_data'=>$data]);
    }
    public function show($no)
    {   
        $cabinet_appliction=db::table('cabinet_appliction')
            ->where('no',$no)
            ->where('applicant_number',Input::get('number'))
            ->orderby('status','desc')
            ->first();
        $users=db::Table('users')->find($cabinet_appliction->users);
        $confirm_payment_user=db::table('users')->find($cabinet_appliction->confirm_payment_user);
        $cabinet_appliction_user=db::Table('cabinet_appliction_user')->where('c_no',$cabinet_appliction->id)->get();
        $cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('c_no',$cabinet_appliction->id)->get();
        $qty=count($cabinet_appliction_data);
        switch ($qty) {
            case 1 : $type="個人"; break;
            case 2 : $type="夫妻"; break;
            case 12: $type="家族"; break;
            default:
                break;
        }
        $cost_total=0;
        $other_total=0;
        $total=0;
        foreach($cabinet_appliction_data as $value){
            $cabinet_code[]=DB::Table('cabinet_sys')->where('code',$value->cabinet_code)->first();
            $area[]=db::Table('cabinet_sys_class')->where('id',$value->cabinet_class_aera)->whereNull('deleted_at')->first();
            $layer[]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_layer)->whereNull('deleted_at')->first();
            $row[]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_row)->whereNull('deleted_at')->first();
            $seat[]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_seat)->whereNull('deleted_at')->first();
            $cost_total+=$value->cost;
        }
        $cost_total=$cabinet_appliction->cost;
        $other_total=$cabinet_appliction->other_amount_due;
        $total=$cabinet_appliction->amount_due;
        $position=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        $cabinet_principal=db::Table('cabinet_principal')->where('c_no',$cabinet_appliction->id)->first();
        // $old_cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('id',$cabinet_appliction_data->old_cabinet_class_seat)->first();
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$no;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = '';
        $data=[
            'FormTtile' => $FormTtile,
            'id'=>$no,
            'url'=>$Url,
            'cabinet_appliction'=>$cabinet_appliction,
            'cabinet_appliction_user'=>$cabinet_appliction_user,
            'cabinet_appliction_data'=>$cabinet_appliction_data,
            'cabinet_principal'=>$cabinet_principal,
            'users'=>$users,
            'confirm_payment_user'=>$confirm_payment_user,
            'show'=>1,
            'area'=>$area,
            'layer'=>$layer,
            'row'=>$row,
            'seat'=>$seat,
            'cabinet_code'=>$cabinet_code,
            'position'=>$position,
            'cost_total'=>$cost_total,
            'other_total'=>$other_total,
            'total'=>$total,
            'type'=>$type
        ];
        switch ($cabinet_appliction->status) {
            case '100':
                $data['LayoutTitle'] = "櫃位申請";
                // $data['show']='';
                return view('admin.Form.cabinet_appliction_select_edit', $data);
                break;
            case '200':
                $data['LayoutTitle'] = "櫃位繳費";
                // $data['show']='';
                return view('admin.Form.cabinet_appliction_select_edit', $data);
                break;
            case '300':
                $data['LayoutTitle'] = "櫃位入塔";
                $data['show']='';
                return view('admin.Form.cabinet_appliction_select_edit', $data);
                break;
            case '350':
                $data['LayoutTitle'] = "櫃位入塔";
                $data['show']='';
                return view('admin.Form.cabinet_appliction_select_edit', $data);
                break;
            case '399':
                $data['LayoutTitle'] = "櫃位入塔";
                $data['show']='';
                return view('admin.Form.cabinet_appliction_select_edit', $data);
                break;
            case '400':
                $data['LayoutTitle'] ="櫃位異動申請繳費";
                $data=$this->old_cabinet_appliction_data($data,$cabinet_appliction_data);
                // $data['show']='';
                return view('admin.Form.cabinet_change_appliction_select_edit', $data);
                break;
            case '500':
                $data['LayoutTitle'] = "櫃位異動申請繳費";
                $data=$this->old_cabinet_appliction_data($data,$cabinet_appliction_data);
                // $data['show']='';
                return view('admin.Form.cabinet_change_appliction_select_edit', $data);
                break;
            case '600':
                $data['LayoutTitle'] = "櫃位異動";
                $data=$this->old_cabinet_appliction_data($data,$cabinet_appliction_data);
                // $data['show']='';
                return view('admin.Form.cabinet_change_appliction_select_edit', $data);
                break;
            case '650':
                $data['LayoutTitle'] = "櫃位異動";
                $data=$this->old_cabinet_appliction_data($data,$cabinet_appliction_data);
                // $data['show']='';
                return view('admin.Form.cabinet_change_appliction_select_edit', $data);
                break;
            case '699':
                $data['LayoutTitle'] = "櫃位異動";
                $data=$this->old_cabinet_appliction_data($data,$cabinet_appliction_data);
                // $data['show']='';
                return view('admin.Form.cabinet_change_appliction_select_edit', $data);
                break;
            case '700':
                $data['LayoutTitle'] = "櫃位遷出申請";
                // $data['show']='';
                return view('admin.Form.cabinet_out_appliction_select_edit', $data);
                break;
            case '800':
                 $data['LayoutTitle'] = "櫃位已遷出";
                return view('admin.Form.cabinet_out_appliction_select_edit', $data);
                break;
            default:
                $data['LayoutTitle'] = "結案";
                return view('admin.Form.cabinet_appliction_select_edit', $data);
                break;
        }
    }
    public function update(Request $request ,$no)
    {
        switch ($request->cabinet_type) {
            case '個人': $qty=1;break;
            case '夫妻': $qty=2;break;
            case '家族': $qty=12;break;
            default: return redirect()->back();  break;
        }
        $cabinet_appliction=array();
        $cabinet_appliction['cabinet_appliction_type']=$request->cabinet_appliction_type;
        $cabinet_appliction['no']=$request->no;
        $cabinet_appliction['application_type']=$request->application_type;
        $cabinet_appliction['application_date']=$request->application_date;
        $cabinet_appliction['household_registration']=$request->household_registration;
        $cabinet_appliction['applicant']=$request->applicant;
        $cabinet_appliction['applicant_number']=$request->applicant_number;
        $cabinet_appliction['tel']=$request->tel;
        $cabinet_appliction['phone']=$request->phone;
        $cabinet_appliction['relationship']=$request->relationship;
        $cabinet_appliction['address']=$request->address;
        $cabinet_appliction['ext']=$request->ext;
        $cabinet_appliction['principal']=$request->principal;
        $cabinet_appliction['payment_date']=$request->payment_date;
        $cabinet_appliction['change_date']=$request->change_date;
        // $cabinet_appliction['users']=Auth::user()->id;
        // $cabinet_appliction['status']=$request->status;
        $cabinet_appliction['cost']=$request->cost;
        if(!isset($request->other_amount_due)){
            $request->other_amount_due=0;
        }
        if(!isset($request->amount_due)){
            $request->amount_due=0;
        }else{
            $cabinet_appliction['other_amount_due']=$request->other_amount_due;
            $cabinet_appliction['amount_due']=$request->amount_due;
        }
        // $cabinet_appliction['code']=$request->cabinet_code;
        $Path='Upload/CabinetAppliction/';
        for ($i = 1; $i < 10; $i++) {
            $name='file'.$i;
            if(Input::hasFile($name)){
            $filename = date('Ymd').rand(11111, 99999);
            $cabinet_appliction[$name] = $filename .".". Input::file($name)->getClientOriginalExtension();
                Input::file($name)->move($Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
            }
        }
        for ($i=1; $i <=$qty ; $i++) { 
            $name='name'.$i;
            if($request->$name<>''){
                $cabinet_appliction['application_type']=1;
            }
        }
        DB::table('cabinet_appliction')->where('no',$no)->update($cabinet_appliction);
        $cabinet_appliction_user=array();
        for ($i=1; $i <=$qty ; $i++) { 
            $name='name'.$i;
            $name_number='name_number'.$i;
            $name_household_registration='name_household_registration'.$i;
            $gender='gender'.$i;
            $identity='identity'.$i;
            $discount='discount'.$i;
            $dead_date='dead_date'.$i;
            $dead_reason='dead_reason'.$i;
            $dead_location='dead_location'.$i;
            $birthday='birthday'.$i;
            $expected_date='expected_date'.$i;
            $expected_time='expected_time'.$i;
            $cabinet_appliction_user_id='cabinet_appliction_user_id'.$i;
            // $cabinet_appliction_user[$i]['c_no']=1;
            // $cabinet_appliction_user[$i]['c_no']=$cabinet_appliction_id;
            $cabinet_appliction_user[$i]['name']=$request->$name;
            $cabinet_appliction_user[$i]['name_number']=$request->$name_number;
            $cabinet_appliction_user[$i]['address']=$request->$name_household_registration;
            $cabinet_appliction_user[$i]['gender']=$request->$gender;
            $cabinet_appliction_user[$i]['identity']=$request->$identity;
            $cabinet_appliction_user[$i]['discount']=$request->$discount;
            $cabinet_appliction_user[$i]['dead_date']=$request->$dead_date;
            $cabinet_appliction_user[$i]['dead_reason']=$request->$dead_reason;
            $cabinet_appliction_user[$i]['dead_location']=$request->$dead_location;
            $cabinet_appliction_user[$i]['birthday']=$request->$birthday;
            $cabinet_appliction_user[$i]['expected_date']=$request->$expected_date;
            $cabinet_appliction_user[$i]['expected_time']=$request->$expected_time;
            DB::table('cabinet_appliction_user')
            ->where('id',$request->$cabinet_appliction_user_id)
            ->update($cabinet_appliction_user[$i]);
        }
        $cabinet_principal=array();
        // $cabinet_principal['c_no']=$cabinet_appliction_id;
        $cabinet_principal['client']=$request->client;
        $cabinet_principal['client_number']=$request->client_number;
        $cabinet_principal['client_household_registration']=$request->client_household_registration;
        $cabinet_principal['client_tel']=$request->client_tel;
        $cabinet_principal['client_phone']=$request->client_phone;
        $cabinet_principal['client_address']=$request->client_address;
        DB::table('cabinet_principal')
            ->where('id',$request->cabinet_principal_id)
            ->update($cabinet_principal);
        for($i=1;$i<=$qty;$i++){
            $cabinet_code='cabinet_code'.$i;
            $name_number='name_number'.$i;
            // $cabinet_class_floor='cabinet_class_floor'.$i;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $expected_date='expected_date'.$i;
            $dead_date='dead_date'.$i;
            $name='name'.$i;
            $identity='identity'.$i;
        Exist('cabinet_sys',$request->$cabinet_code,$request->status,$request->$name_number,'',
                    $request->id);
        DB::table('pd_log')->where('code',$request->$cabinet_code)
        ->update([
            'death_date'=>$request->$dead_date,
            'number'=>$request->applicant_number,
            'user_number'=>$request->$name_number,
            'application_date'=>$request->application_date,
            'expected_date'=>$request->expected_date,
            'name'=>$request->applicant,
            'user_name'=>$request->$name,
            'tel'=>$request->tel,
            'phone'=>$request->phone,
            'identity'=>$request->$identity
        ]);
        }
        return redirect()->to('BackCabinet');
    }
    public function old_cabinet_appliction_data($data,$cabinet_appliction_data)
    {   
        foreach($cabinet_appliction_data as $key =>$value){
            $old_cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('id',$value->old_cabinet_class_seat)->first();
            $data['old_cabinet_code'][]=DB::Table('cabinet_sys')->where('code',$old_cabinet_appliction_data->cabinet_code)->first();
            $data['old_area'][]=db::Table('cabinet_sys_class')->where('id',$old_cabinet_appliction_data->cabinet_class_aera)->whereNull('deleted_at')->first();
            $data['old_layer'][]=db::table('cabinet_sys_class')->where('id',$old_cabinet_appliction_data->cabinet_class_layer)->whereNull('deleted_at')->first();
            $data['old_row'][]=db::table('cabinet_sys_class')->where('id',$old_cabinet_appliction_data->cabinet_class_row)->whereNull('deleted_at')->first();
            $data['old_seat'][]=db::table('cabinet_sys_class')->where('id',$old_cabinet_appliction_data->cabinet_class_seat)->whereNull('deleted_at')->first();
            $data['old_cabinet_appliction_data'][]=$old_cabinet_appliction_data;

            $data['cabinet_code'][]=DB::Table('cabinet_sys')->where('code',$value->cabinet_code)->first();
            $data['area'][]=db::Table('cabinet_sys_class')->where('id',$value->cabinet_class_aera)->whereNull('deleted_at')->first();
            $data['layer'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_layer)->whereNull('deleted_at')->first();
            $data['row'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_row)->whereNull('deleted_at')->first();
            $data['seat'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_seat)->whereNull('deleted_at')->first();
            //$data['cost']+=$value->cost;
            //$data['other_cost']+=$value->other_cost;
            //$data['amount_due']+=$value->amount_due;
        }
        $data['cost']=$data['cost_total'];
        $data['other_cost']=$data['other_total'];
        $data['amount_due']=$data['total'];
        return $data;
    }
}