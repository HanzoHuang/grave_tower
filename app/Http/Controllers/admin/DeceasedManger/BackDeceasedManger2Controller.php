<?php

namespace App\Http\Controllers\admin\DeceasedManger;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\DeceasedManger2Request;
use DB;
use Input;
use Session;
class BackDeceasedManger2Controller extends BackController {
    //配合route resource
    protected $RouteName="BackDeceasedManger2";
    //檔案路徑
    protected $Path="Upload/DeceasedManger/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\DeceasedManger\DeceasedManger2";

    //主要table
    protected $Table1="tombstone_appliction_user";
    //關聯table
    protected $Table2="roles";
    public function index()
    {
        $BodyArray = array(
            array('title' => 'Id', 'width' => '3'),
            array('title' => '身分證字號', 'width' => '10'),
            array('title' => '姓名', 'width' => '6'),
            array('title' => '身分', 'width' => '8'),
            array('title' => '入塔日期', 'width' => '13'),
            array('title' => '狀態', 'width' => '10'),
            array('title' => '牌位位置', 'width' => '10'),
            array('title' => '操作', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->join('tombstone_appliction as ca',$this->Table1.'.c_no','=','ca.id')
        ->select('ca.id as id',$this->Table1.'.name_number',$this->Table1.'.name',$this->Table1.'.identity',$this->Table1.'.expected_date','ca.status','ca.code')->where( $this->Table1.'.name_number','<>','')
        ->where( $this->Table1.'.name','<>','')
        ->whereNull( $this->Table1.'.deleted_at')
        ->orderby('ca.status','desc')
        ->orderby($this->Table1.'.name_number','desc')
        ->groupby($this->Table1.'.name_number')
        ->get();
        foreach($Data as $key =>$value){
            $new_code=DB::table('tombstone_sys')->where('c_no',$value->id)->where('number',$value->name_number)->first();
            if($new_code){
                $Data[$key]->code=$new_code->code;
            }
            switch ($value->status) {
                case 100:$Data[$key]->status='申請(已申請)';break;
                case 200:$Data[$key]->status='申請(已繳費)';break;
                case 300:$Data[$key]->status='申請(已進塔)';break;
                case 350:$Data[$key]->status='異動(首次異動中)';break;
                case 399:$Data[$key]->status='遷出(遷出中)';break;
                case 400:$Data[$key]->status='異動(已申請)';break;
                case 500:$Data[$key]->status='異動(已繳費)';break;
                case 600:$Data[$key]->status='異動(已入塔)';break;
                case 650:$Data[$key]->status='異動(異動中)';break;
                case 699:$Data[$key]->status='遷出(遷出中)';break;
                case 700:$Data[$key]->status='遷出(已申請)';break;
                case 800:$Data[$key]->status='遷出(已遷出)';break;
                default: break;
            }
            switch ($value->identity) {
                case 0:$Data[$key]->identity='本鄉鄉民';break;
                case 1:$Data[$key]->identity='本縣縣民';break;
                case 2:$Data[$key]->identity='外縣市民';break;
                default: break;
            }
        }
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Edit' => 1, 'Delete' => 0, 'Check' => 1, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'Add'=>2]);
    }
    
    public function create()
    {
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "逝者新增";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeceasedManger2Request $request)
    {   
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案拆出來，額外處理，並存在陣列
        $data=$request->all();
        
        $data=$ModelName::create(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        //use model
        $ModelName=$this->ModelName;
        $BodyArray = $this->BodyArray($data=$ModelName::find($id));
        //表格標題
        $LayoutTitle = "牌位者資料維護";
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.index', ['FormTtile' => $FormTtile, 'FormBody' => $FormBody,'SelectValue'=>$data->product_class_id,'SelectValue2'=>json_encode(explode(',',$data->product_specification_id)),'id'=>$id]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DeceasedManger2Request $request,$id)
    {   
        //use model
        $ModelName=$this->ModelName;
        //將request->all()中的上傳檔案或Array拆出來，額外處理，並存在陣列
        $ModelName::find($id)->update(RequestForeach($request->all(),$this->Path));
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function BodyArray($Data = 1) {
        $gender=array(1=>'男',2=>'女');
        $type=array(0=>'本鄉鄉民',1=>'本縣縣民',2=>'外縣市民');
        $discount=array(1=>'',2=>'');
        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            array('id' => 'name', 'name' => 'name', 'CNname' => '牌位者：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name'] : null),
            array('id' => 'name_number', 'name' => 'name_number', 'CNname' => '身分證字號：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name_number'] : null),
            array('id' => 'address', 'name' => 'address', 'CNname' => '戶籍地：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['address'] : null),
            array('id' => 'identity', 'name' => 'identity', 'CNname' => '身分：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $type, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['identity'] : null),
            array('id' => 'gender', 'name' => 'gender', 'CNname' => '性別：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $gender, 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['gender'] : null),
            array('id' => 'birthday', 'name' => 'birthday', 'CNname' => '出生日期：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'InputDate', "Value" => (is_object($Data)) ? $Data['birthday'] : null),
            array('id' => 'expected_date
                ', 'name' => 'expected_date', 'CNname' => '入塔日期：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'InputDate', "Value" => (is_object($Data)) ? $Data['expected_date'] : null),
           );
        return $BodyArray;
    }
}