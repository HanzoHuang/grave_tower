<?php

namespace App\Http\Controllers\admin\TowerSelect;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\TowerSelectRequest;
use DB;
use Input;
use Session;
class BackTowerSelectController extends BackController {
    //配合route resource
    protected $RouteName="TowerSelect";
    //檔案路徑
    protected $Path="Upload/TowerSelect/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\PublicInformation\TowerSelect";

    //主要table
    protected $Table1="pd_log";
    //關聯table
    protected $Table2="roles";
    public function index()
    {
        return redirect()->to('ui');
    }
}