<?php

namespace App\Http\Controllers\admin\CabinetAppliction;
use Illuminate\Http\Request;
use App\Http\Controllers\BackController;
use App\Http\Requests\CabinetApplictionRequest;
use App\Http\Requests\CabinetApplictionUpdateRequest;
use DB;
use Input;
use Session;
use Auth;
use Vsmoraes\Pdf\Pdf;

class BackCabinetApplictionController extends BackController {
    //配合route resource
    protected $RouteName="BackCabinetAppliction";
    //檔案路徑
    protected $Path="Upload/CabinetAppliction/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\CabinetAppliction\CabinetAppliction";

    //主要table
    protected $Table1="cabinet_appliction";
    protected $Table2="cabinet_appliction_user";
    protected $Table3="cabinet_appliction_data";
    protected $Table4="users";
    protected $Table5="cabinet_sys";
    protected $Table6="cabinet_sys_class";
    protected $Table7="cabinet_cost_discount";
    protected $Table8="cabinet_other_cost";
    //關聯table
    // protected $Table2="roles";
    public function __construct(Pdf $pdf)
    {
        $this->pdf = $pdf;
    }
    public function index()
    {       
        $no=Input::get('no',0);
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '編號', 'width' => '5'),
            array('title' => '申請人', 'width' => '5'),
            array('title' => '申請人身分證', 'width' => '5'),
            array('title' => '申請日期', 'width' => '5'),
            array('title' => '逝者', 'width' => '5'),
            array('title' => '逝者身分證', 'width' => '5'),
            array('title' => '櫃位', 'width' => '5'),
            array('title' => '狀態', 'width' => '5'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select($this->Table1.'.id',
            $this->Table1.'.no',
            $this->Table1.'.applicant',
            $this->Table1.'.applicant_number',
            $this->Table1.'.application_date',
            $this->Table1.'.id as d_user',
            $this->Table1.'.id as d_number',
            $this->Table1.'.id as d_data',
            // $this->Table3.'.cabinet_code',
            $this->Table1.'.status'
        )
        ->leftjoin($this->Table2,$this->Table2.'.c_no','=',$this->Table1.'.id')
        ->leftjoin($this->Table3,$this->Table3.'.c_no','=',$this->Table1.'.id')
        ->leftjoin($this->Table4,$this->Table1.'.users','=',$this->Table4.'.id')
        ->whereNull( $this->Table1.'.deleted_at')
        ->groupBy($this->Table1.'.id')
        ->where( $this->Table1.'.status','<',300)
        ->orderBy($this->Table1.'.updated_at','desc')
        ->orderBy($this->Table1.'.created_at','desc');
        if($no!=0){
            $Data->where($this->Table1.'.no',$no);
        }
        $Data=$Data->get();
        $option=CabinetSys();
        foreach($Data as $key =>$val){
            $Data[$key]->status=$option['status'][$Data[$key]->status];
            $user=db::Table('cabinet_appliction_user')->where('c_no',$val->id)->first();
            $val->d_user='';
            $val->d_number='';
            if($user){
                $val->d_user=$user->name;
                $val->d_number=$user->name_number;
            }
            $data=db::Table('cabinet_appliction_data')->where('c_no',$val->id)->first();
            $val->d_data='';
            if($data){
                $val->d_data=$data->cabinet_code;
            }
        }
        // $Data=$this->correspond($Data);
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'ApplictionEdit' => 1, 'Delete' => 0, 'Check' => 0, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0,'Appliction'=>1,'ChangeAppliction'=>0,'print'=>1,'C_ApplictionDeletel'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create']);
    }
    
    public function create()
    {
        $cabinet_type=Input::get('cabinet_type');
        $area=array();
        $layer=array();
        $row=array();
        $seat=array();
        $qty=0;
        switch ($cabinet_type) {
            case '個人': $qty=1; break;
            case '夫妻': $qty=2; break;
            case '家族': $qty=12; break;
            default: break;
        }
        $area=db::Table('cabinet_sys_class')->where('name','like','%'.$cabinet_type.'%')->whereNull('deleted_at')->get();
        $position=array();
        $sys_data=array();

        // if(count($area)>0){
        //     $layer=db::table('cabinet_sys_class')->where('top_class',$area[0]->id)->whereNull('deleted_at')->get();
        // }
        // if(count($layer)>0){
        //     $row=db::table('cabinet_sys_class')->where('top_class',$layer[0]->id)->whereNull('deleted_at')->get();
        // }
        // if(count($row)>0){
        //     $seat=db::table('cabinet_sys_class')->where('top_class',$row[0]->id)->whereNull('deleted_at')->get();
        // }
        // $area_code  =db::Table('cabinet_sys_class')->where('id',$area[0]->id)->whereNull('deleted_at')->first();
        // $layer_code =db::Table('cabinet_sys_class')->where('id',$layer[0]->id)->whereNull('deleted_at')->first();
        // $row=$this->check_row($row,$area_code,$layer_code);
        // $row_code =db::Table('cabinet_sys_class')->where('id',$row[0]->id)->whereNull('deleted_at')->first();
        // $sys=array();
        // if($row_code){
        //     foreach($seat as $seats){
        //         $sys[]='1-'.$area_code->code.'-'.$layer_code->code.'-'.$row_code->code.'-'.$seats->code;
        //     }
        // }
        // $sys_data=db::Table('cabinet_sys')
        //     ->whereIn('code',$sys)
        //     ->orderBy('seat','asc')
        //     ->whereNull('number')
        //     ->get();
        // $position_data=array(0=>'無',1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        // $sys_data=db::Table('cabinet_sys')
        //     ->whereIn('code',$sys)
        //     ->orderBy('seat','asc')
        //     ->whereNull('number')
        //     ->get();
        // $position=array();
        // foreach($sys_data as $key =>$value){
        //     $position[$value->position]=$position_data[$value->position];
        //     $sys_data[$key]->position_name=$position_data[$value->position];
        // }
        // $position[0]='無';
        $other_cost=db::table('cabinet_other_cost')->first();
        $BodyArray = $this->BodyArray();
        //表格標題
        $LayoutTitle = "櫃位申請";
        //要store的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $FormBody = FormBody($BodyArray, $Url,'','POST');
        //統一使用admin.Form.index，除非特殊狀況
        $no=no_select2();
        return view('admin.Form.cabinet_appliction', ['FormTtile' => $FormTtile,'url'=>$Url,'area'=>$area,'layer'=>$layer,'row'=>$row,'position'=>$position,'seat'=>$seat,'qty'=>$qty,'no'=>$no,'sys'=>$sys_data,'other_cost'=>$other_cost->other_cost]);
    }
    public function store(CabinetApplictionRequest $request)
    {   
        switch ($request->cabinet_type) {
            case '個人': $qty=1;break;
            case '夫妻': $qty=2;break;
            case '家族': $qty=12;break;
            default: return redirect()->back();  break;
        }
        for ($i=1; $i <=$qty ; $i++) { 
            $cabinet_code='cabinet_code'.$i;
            $identity='identity1';
            $name_number='name_number'.$i;
            $tw_number_select=tw_number_select('cabinet_sys',$request->$name_number);
            if(!$tw_number_select){
                return redirect()->back()->withInput()->with('status',$request->$name_number.' 已經入塔');
            }
            $cost_data=explode('-', $request->$cabinet_code);
            if($cost_data[0]=='0'){
                return redirect()->back()->withInput()->with('status','請選擇櫃位');
            }
            $cost=db::Table($this->Table7)
                ->where('type',$request->$identity)
                ->where('area',$cost_data[1])
                ->where('row',$cost_data[3])
                ->first();
            if(!$cost){
                return redirect()->back()->withInput()->with('status',$request->$cabinet_code.'沒有設定費用');
            }
        }
        $no=$request->no;
        do{
            $no_select=no_select('cabinet_appliction',$no);
            if(!$no_select){
                $no++;
            }
        } while ( !$no_select );

        $cabinet_appliction=array();
        $cabinet_appliction['cabinet_appliction_type']=$request->cabinet_appliction_type;
        $cabinet_appliction['no']=$no;
        $cabinet_appliction['application_type']=$request->application_type;
        $cabinet_appliction['application_date']=$request->application_date;
        $cabinet_appliction['household_registration']=$request->household_registration;
        $cabinet_appliction['applicant']=$request->applicant;
        $cabinet_appliction['applicant_number']=$request->applicant_number;
        $cabinet_appliction['tel']=$request->tel;
        $cabinet_appliction['phone']=$request->phone;
        $cabinet_appliction['relationship']=$request->relationship;
        $cabinet_appliction['address']=$request->address;
        $cabinet_appliction['ext']=$request->ext;
        $cabinet_appliction['users']=Auth::user()->id;
        $cabinet_appliction['status']=$request->status;
        $cabinet_appliction['cost']=$request->cost;
        $cabinet_appliction['other_amount_due']=$request->other_amount_due;
        $cabinet_appliction['amount_due']=$request->amount_due;
        // $cabinet_appliction['code']=$request->cabinet_code;
        for ($i = 1; $i < 6; $i++) {
            $name='file'.$i;
            if(Input::hasFile($name)){
            $filename = date('Ymd').rand(11111, 99999);
            $cabinet_appliction[$name] = $filename .".". Input::file($name)->getClientOriginalExtension();
                Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
            }
        }
        $cabinet_appliction_id=DB::table('cabinet_appliction')->insertGetid($cabinet_appliction);
        $cabinet_appliction_user=array();
        for ($i=1; $i <=$qty ; $i++) { 
            $name='name'.$i;
            $name_number='name_number'.$i;
            $name_household_registration='name_household_registration'.$i;
            $gender='gender'.$i;
            $identity='identity1';
            $discount='discount1';
            $dead_date='dead_date'.$i;
            $dead_reason='dead_reason'.$i;
            $dead_location='dead_location'.$i;
            $birthday='birthday'.$i;
            $expected_date='expected_date'.$i;
            $expected_time='expected_time'.$i;
            // $cabinet_appliction_user[$i]['c_no']=1;
            $cabinet_appliction_user[$i]['c_no']=$cabinet_appliction_id;
            $cabinet_appliction_user[$i]['name']=$request->$name;
            $cabinet_appliction_user[$i]['name_number']=$request->$name_number;
            $cabinet_appliction_user[$i]['address']=$request->$name_household_registration;
            $cabinet_appliction_user[$i]['gender']=$request->$gender;
            $cabinet_appliction_user[$i]['identity']=$request->$identity;
            $cabinet_appliction_user[$i]['discount']=$request->$discount;
            $cabinet_appliction_user[$i]['dead_date']=$request->$dead_date;
            $cabinet_appliction_user[$i]['dead_reason']=$request->$dead_reason;
            $cabinet_appliction_user[$i]['dead_location']=$request->$dead_location;
            $cabinet_appliction_user[$i]['birthday']=$request->$birthday;
            $cabinet_appliction_user[$i]['expected_date']=$request->$expected_date;
            $cabinet_appliction_user[$i]['expected_time']=$request->$expected_time;
        }
        $cabinet_appliction_user_id=DB::table('cabinet_appliction_user')->insert($cabinet_appliction_user);
        $cabinet_appliction_data=array();
        for ($i=1; $i <=$qty ; $i++) { 
            $cabinet_code='cabinet_code'.$i;
            $identity='identity1';
            $cabinet_type='cabinet_type'.$i;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_layer='cabinet_class_layer'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_seat='cabinet_class_seat'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $dead_date='dead_date'.$i;
            $dead_reason='dead_reason'.$i;
            $dead_location='dead_location'.$i;
            $birthday='birthday'.$i;
            $expected_date='expected_date'.$i;
            $cabinet_appliction_user[$i]['c_no']=1;

            $other_cost=db::Table($this->Table8)->first();
            $cost_data=explode('-', $request->$cabinet_code);
            $cost=db::Table($this->Table7)
                ->where('type',$request->$identity)
                ->where('area',$cost_data[1])
                ->where('row',$cost_data[3])
                ->first();
            
            $cabinet_appliction_data[$i]['c_no']=$cabinet_appliction_id;
            // $cabinet_appliction_data[$i]['c_no']=1;
            $cabinet_appliction_data[$i]['cabinet_type']=$request->cabinet_type;
            $cabinet_appliction_data[$i]['cabinet_class_floor']=1;
            $cabinet_appliction_data[$i]['cabinet_class_aera']=$request->cabinet_class_aera1;
            $cabinet_appliction_data[$i]['cabinet_class_layer']=$request->cabinet_class_layer1;
            $cabinet_appliction_data[$i]['cabinet_class_row']=$request->cabinet_class_row1;
            // $cabinet_appliction_data[$i]['cabinet_class_seat']=$request->cabinet_class_seat1;
            $cabinet_appliction_data[$i]['cabinet_class_position']=$request->cabinet_class_position1;
            $cabinet_appliction_data[$i]['cabinet_code']=$request->$cabinet_code;
            $cabinet_appliction_data[$i]['cost']=$cost->price;
            $cabinet_appliction_data[$i]['other_cost']=0;
            switch ($request->$discount) {
                case '1':
                    if($cost->discount1==1){
                        $cabinet_appliction_data[$i]['cost']=$cost->price*0.2;
                    }
                    break; 
                case '2':
                    if($cost->discount2==1){
                        $cabinet_appliction_data[$i]['cost']=$cost->price*0.5;
                    }
                    break;
                case '3':
                    if($cost->discount3==1){
                        $cabinet_appliction_data[$i]['cost']=$cost->price*0.9;
                    }
                    break;
                case '4':
                    if($cost->discount4==1){
                        $cabinet_appliction_data[$i]['cost']=$cost->price*0.95;
                    }
                    break;
                default:
                    break;
            }
            $cabinet_appliction_data[$i]['amount_due']=$cabinet_appliction_data[$i]['cost']+$cabinet_appliction_data[$i]['other_cost'];
        }
        DB::Table('cabinet_appliction_data')->insert($cabinet_appliction_data);
        $cabinet_principal=array();
        $cabinet_principal['c_no']=$cabinet_appliction_id;
        $cabinet_principal['client']=$request->client;
        $cabinet_principal['client_number']=$request->client_number;
        $cabinet_principal['client_household_registration']=$request->client_household_registration;
        $cabinet_principal['client_tel']=$request->client_tel;
        $cabinet_principal['client_phone']=$request->client_phone;
        $cabinet_principal['client_address']=$request->client_address;
        DB::table('cabinet_principal')->insert($cabinet_principal);
        for($i=1;$i<=$qty;$i++){
            $cabinet_code='cabinet_code'.$i;
            $name_number='name_number'.$i;
            $cabinet_class_floor='cabinet_class_floor'.$i;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $expected_date='expected_date'.$i;
            $dead_date='dead_date'.$i;
            $name='name'.$i;
            $identity='identity1';
            if($request->application_type==2){
                Exist('cabinet_sys',
                  $request->$cabinet_code,
                  $request->status,
                  $request->$name_number,
                  '',
                  $cabinet_appliction_id,
                  1
                );
            }else{
                Exist('cabinet_sys',
                    $request->$cabinet_code,
                    $request->status,
                    $request->$name_number,
                    '',
                    $cabinet_appliction_id
                );
            }
            //類別(櫃位或牌位),樓,區域,排,方位,編碼,申請號碼,申請人身分證字號,死者身分證字號,櫃位類型(一般,長生),申請時間,進塔時間,死亡時間,狀態
            ExistLog('櫃位',1,$request->cabinet_class_aera1,$request->cabinet_class_row1,$request->cabinet_class_position1,$request->$cabinet_code,$no,$request->applicant_number,$request->$name_number,$request->application_type,$request->application_date,$request->$expected_date,$request->$dead_date,$request->status,$request->applicant,$request->$name,$request->tel,$request->phone,$request->$identity);
        };
        no_insert($no);
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function show($id)
    {
    }
    public function edit($id)
    {   
        //展資料用
        //單頭
        $cabinet_appliction=db::table('cabinet_appliction')->find($id);
        $users=db::Table('users')->find($cabinet_appliction->users);
        //確認繳費人員
        $confirm_payment_user=db::table('users')->find($cabinet_appliction->confirm_payment_user);
        //侍者資料
        $cabinet_appliction_user=db::Table('cabinet_appliction_user')->where('c_no',$id)->get();
        //櫃位資料
        $cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('c_no',$id)->get();
        //委託人
        $cabinet_principal=db::Table('cabinet_principal')->where('c_no',$id)->first();

        if($cabinet_appliction->status==100){
            $LayoutTitle = "櫃位申請繳費";
        }if($cabinet_appliction->status==200){
            $LayoutTitle = "櫃位申請入塔";
        }
        $area=array();
        $layer=array();
        $row=array();
        $seat=array();
        $cabinet_code=array();
        $cost_total=0;
        $other_total=0;
        $total=0;
        foreach($cabinet_appliction_data as $value){
            $cabinet_code[]=DB::Table('cabinet_sys')->where('code',$value->cabinet_code)->first();
            $area[]=db::Table('cabinet_sys_class')->where('id',$value->cabinet_class_aera)->whereNull('deleted_at')->first();
            $layer[]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_layer)->whereNull('deleted_at')->first();
            $row[]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_row)->whereNull('deleted_at')->first();
            $seat[]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_seat)->whereNull('deleted_at')->first();
            $cost_total+=$value->cost;
            $other_total+=$value->other_cost;
            $total+=$value->amount_due;
        }
        $position=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //app/Support/Helpers底下的自訂function
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.cabinet_appliction_edit', [
            'FormTtile' => $FormTtile,
            'id'=>$id,
            'url'=>$Url,
            'cabinet_appliction'=>$cabinet_appliction,
            'cabinet_appliction_user'=>$cabinet_appliction_user,
            'cabinet_appliction_data'=>$cabinet_appliction_data,
            'cabinet_principal'=>$cabinet_principal,
            'users'=>$users,
            'confirm_payment_user'=>$confirm_payment_user,
            'area'=>$area,
            'layer'=>$layer,
            'row'=>$row,
            'seat'=>$seat,
            'position'=>$position,
            'cabinet_code'=>$cabinet_code,
            'cost_total'=>$cost_total,
            'other_total'=>$other_total,
            'total'=>$total

        ]);
    }
   
    public function update(CabinetApplictionUpdateRequest $request,$id)
    {   
        switch ($request->cabinet_type) {
            case '個人':$qty=1; break;
            case '夫妻':$qty=2; break;
            case '家族':$qty=12; break;
            default:break;
        }
        $cabinet_appliction=array();
        $required='';
        $cabinet_appliction['status']=$request->status;
        $cabinet_appliction['confirm_payment_user']=Auth::user()->id;
        //已繳費表單才驗證
        if($request->status==200){
            $cabinet_appliction['payment_date']=$request->payment_date;
            for ($i = 6; $i < 8; $i++) {
                $name='file'.$i;
                if(Input::hasFile($name)){
                $filename = date('Ymd').rand(11111, 99999);
                $cabinet_appliction[$name] = $filename .".". Input::file($name)->getClientOriginalExtension();
                    Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
                }
            }
        }elseif($request->status==300){
            $cabinet_appliction['principal']=$request->principal;
            $cabinet_appliction['change_date']=$request->change_date;
        }
        $cabinet_appliction_user=array();
        $cabinet_appliction_data=array();
        if(empty($required)){
            DB::Table('cabinet_appliction')->where('id',$request->id)->update($cabinet_appliction);
            // if($request->status==200){
            //     DB::Table('cabinet_appliction_data')->where('id',$request->cabinet_appliction_data_id)->update($cabinet_appliction_data);
            // }
           for($i=1;$i<=$qty;$i++){
            $cabinet_code='cabinet_code'.$i;
            $name_number='name_number'.$i;
            $cabinet_class_floor='cabinet_class_floor'.$i;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $expected_date='expected_date'.$i;
            $dead_date='dead_date'.$i;
            $name='name'.$i;
            $identity='identity'.$i;
            if($request->application_type==2){
                Exist('cabinet_sys',
                    $request->$cabinet_code,
                    $request->status,
                    $request->$name_number,
                    '',
                    $request->id,
                    1);
            }else{
                Exist('cabinet_sys',
                    $request->$cabinet_code,
                    $request->status,
                    $request->$name_number,
                    '',
                    $request->id);
            }
            //類別(櫃位或牌位),樓,區域,排,方位,編碼,申請號碼,申請人身分證字號,死者身分證字號,櫃位類型(一般,長生),申請時間,進塔時間,死亡時間,狀態
            if($request->status==200){
                ExistLog('櫃位',$request->$cabinet_class_floor,$request->$cabinet_class_aera,$request->$cabinet_class_row,$request->$cabinet_class_position,$request->$cabinet_code,$request->no,$request->applicant_number,$request->$name_number,$request->application_type,$request->application_date,$request->$expected_date,$request->$dead_date,$request->status,$request->applicant,$request->$name,$request->tel,$request->phone,$request->$identity);
            }else{
                ExistLog('櫃位',$request->$cabinet_class_floor,$request->$cabinet_class_aera,$request->$cabinet_class_row,$request->$cabinet_class_position,$request->$cabinet_code,$request->no,$request->applicant_number,$request->$name_number,$request->application_type,$request->application_date,$request->change_date,$request->$dead_date,$request->status,$request->applicant,$request->$name,$request->tel,$request->phone,$request->$identity);
            }
            
        }
            return redirect()->to($this->RouteName)->with('status','成功');
        }else{
            return redirect()->back()->with('status',$required)->withInput();
        }
        //類別(櫃位或牌位),方位,區域,樓,編碼,申請號碼,申請人身分證字號,死者身分證字號,櫃位類型(長生、一般),申請時間,進塔時間,死亡時間
        //
// function ExistLog($class_type,$class_floor,$class_area,$class_row,$code,$no,$number,$user_number,$type,$application_date,$expected_date,$death_date)
// 
        // $cabinet_principal=array();
        // $cabinet_principal['c_no']=$cabinet_appliction_id;
        // $cabinet_principal['client']=$request->client;
        // $cabinet_principal['client_number']=$request->client_number;
        // $cabinet_principal['client_household_registration']=$request->client_household_registration;
        // $cabinet_principal['client_tel']=$request->client_tel;
        // $cabinet_principal['client_phone']=$request->client_phone;
        // $cabinet_principal['client_address']=$request->client_address;
        // DB::table('cabinet_principal')->where('id',$request->cabinet_principal_id)->update($cabinet_principal);
       
    }
    
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }  
    }
    
    public function change_area()
    {
        $data=Input::all();
        $area =array();
        $layer=array();
        $row  =array();
        $seat =array();
        switch ($data['cabinet_type']) {
            case '個人': $qty=1; break;
            case '夫妻': $qty=2; break;
            case '家族': $qty=12; break;
            default:  break;
        }
        $area =db::Table('cabinet_sys_class')->where('name','like','%'.$data['cabinet_type'].'%')->whereNull('deleted_at')->get();
        $area_code =db::Table('cabinet_sys_class')->where('id',$data['area'])->whereNull('deleted_at')->first();
        $sys=array();
        switch ($data['type']) {
            case 'area':
                $layer=db::table('cabinet_sys_class')->where('top_class',$data['area'])->whereNull('deleted_at')->get();
                return json_encode($layer);
                break;
            case 'layer':
                $layer_code=db::table('cabinet_sys_class')->where('id',$data['layer'])->whereNull('deleted_at')->first();
                $row=db::table('cabinet_sys_class')->where('top_class',$data['layer'])->whereNull('deleted_at')->get();
                $row=$this->check_row($row,$area_code,$layer_code);
                if(count($row)==0){
                    return json_encode(array(0));
                }
                return json_encode($row);
                break;
            case 'row':
                $layer_code=db::table('cabinet_sys_class')->where('id',$data['layer'])->whereNull('deleted_at')->first();
                $row_code=db::table('cabinet_sys_class')->where('id',$data['row'])->whereNull('deleted_at')->first();
                $seat =db::table('cabinet_sys_class')->where('top_class',$data['row'])->whereNull('deleted_at')->get();
                $sys=array();
                foreach ($seat as $seats) {
                    $sys[]='1-'.$area_code->code.'-'.$layer_code->code.'-'.$row_code->code.'-'.$seats->code;
                }
                $sys_data=db::Table('cabinet_sys')
                    ->whereIn('code',$sys)
                    ->orderBy('seat','asc')
                    ->whereNull('number')
                    ->get();
                $array=array();
                $array['sys']=$sys_data;
                $position=array();
                $position_data=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
                foreach($sys_data as $key =>$value){
                    $position[$value->position]=$position_data[$value->position];
                }
                $array['position']=$position;
                return json_encode($array);
                break;
            case 'code':
            $sys=db::Table('cabinet_sys')->where('code',$data['cabinet_code'])->first();
            default:
                # code...
                break;
        }
        
        
        $sys_data=db::Table('cabinet_sys')
            ->whereIn('code',$sys)
            ->orderBy('seat','asc')
            ->whereNull('number')
            ->get();
        $position=array();
        foreach($sys_data as $key =>$value){
            $position[$value->position]=$position_data[$value->position];
            $sys_data[$key]->position_name=$position_data[$value->position];
            if(Input::has('position')){
                if(Input::get('position')!=0){
                    if($value->position!=$data['position']){
                    unset($sys_data[$key]);
                    }
                }
                
            }
        }
        if(count($sys_data)==0){
            return 0;
        }
        
        $position_data=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        // dd($sys);
        $sys_data=db::Table('cabinet_sys')
            ->whereIn('code',$sys)
            ->orderBy('seat','asc')
            ->whereNull('number')
            ->get();

        $position=array();
        foreach($sys_data as $key =>$value){
            $position[$value->position]=$position_data[$value->position];
            $sys_data[$key]->position_name=$position_data[$value->position];
            if(Input::has('position')){
                if(Input::get('position')!=0){
                    if($value->position!=$data['position']){
                    unset($sys_data[$key]);
                    }
                }
            }
        }
        if(count($sys_data)==0){
            return 0;
        }
        $view_data=['area'=>$area,'layer'=>$layer,'row'=>$row,'seat'=>$seat,'position'=>$position,'request_data'=>$data,'sys'=>$sys_data,'qty'=>$qty];

        return view('admin.Form.sys',$view_data);
    }
    public function check_row($row,$area_code,$layer_code)
    {
        foreach($row as $key =>$value){
            $row_sys='1-'.$area_code->code.'-'.$layer_code->code.'-'.$value->code.'-';
            $sys_data=db::Table('cabinet_sys')
                ->where('code','like','%'.$row_sys.'%')
                ->orderBy('seat','asc')
                ->whereNull('number')
                ->get();
            if(count($sys_data)==0){
                unset($row[$key]);
            }
        }
        $row=array_values($row);
        if(count($row)==0){
            $row=array();
        }
        return $row;
    }
    public function calculate()
    {
        $data=Input::all();
        $db=array();
        $total=0;
        for ($i=1; $i <=$data['qty'] ; $i++) { 
            $cost_data=explode('-',$data['cabinet_code'.$i]);
            if($cost_data[0]==''||$cost_data[0]=='0'){
                $data[$i]['price']=0;
            }else{
                $cost=db::Table($this->Table7)
                    ->where('area',$cost_data[1])
                    ->where('row',$cost_data[3]);
                if($data['identity'.$i]!==0){
                    $cost->where('type',$data['identity'.$i]);
                }
                $cost=$cost->first();
                if(!$cost){
                    $db[$i]['price']=0;
                }else{
                    if($data['qty']!=12){
                        switch ($data['discount'.$i]) {
                            case '1':
                                if($cost->discount1==1){
                                     $db[$i]['price']=$cost->price*0.2;
                                }else{
                                     $db[$i]['price']=$cost->price;
                                }
                                break; 
                            case '2':
                                if($cost->discount2==1){
                                     $db[$i]['price']=$cost->price*0.5;
                                }else{
                                     $db[$i]['price']=$cost->price;
                                }
                                break;
                            case '3':
                                if($cost->discount3==1){
                                     $db[$i]['price']=$cost->price*0.9;
                                }else{
                                     $db[$i]['price']=$cost->price;
                                }
                                break;
                            case '4':
                                if($cost->discount4==1){
                                     $db[$i]['price']=$cost->price*0.95;
                                }else{
                                     $db[$i]['price']=$cost->price;
                                }
                                break;
                             case '5':
                                if($cost->discount5==1){
                                     $db[$i]['price']=$cost->price*0.8;
                                }else{
                                     $db[$i]['price']=$cost->price;
                                }
                                break;
                            default:
                                 $db[$i]['price']=$cost->price;
                                break;
                        }
                    }else{
                        $db[$i]['price']=$cost->price;
                    }
                }
            }
        }
        return json_encode($db);
    }
    public function Pdf()
    {   

        $data=db::table('cabinet_appliction as cp')->where('no',Input::get('no'))
        ->where('applicant_number',Input::get('number'))
        ->select('cp.*','data.*','user.*','p.*','cp.id as id','cp.cost as cost','cp.other_cost as other_cost')
        ->join('cabinet_appliction_data as data','cp.id','=','data.c_no')
        ->join('cabinet_appliction_user as user','cp.id','=','user.c_no')
        ->join('cabinet_principal as p','cp.id','=','p.c_no')
        ->first();
        $data2=db::table('cabinet_appliction_data')
        ->where('c_no',$data->id)
        ->get();
        $user=db::table('cabinet_appliction_user')
        ->where('c_no',$data->id)
        ->get();
        $type=array();
        foreach ($data2 as $key => $value) {
            $number=db::table('cabinet_sys')->where('code',$value->cabinet_code)->first();

                if($user[$key]->name_number==$number->number){
                    $new_data[$number->code]=$user[$key];
                    $type[$number->code]=0;
                }else{
                    $new_data[$number->code]='';
                    $type[$number->code]=1;
                }
        }

        $data2=array();
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                if($type[$key]==0){
                    $data2[]=array('gender' => $value->gender,'name' => $value->name,'birthday' => $value->birthday,'code' => $key,'address'=>$value->address,'dead_date'=>$value->dead_date);
                }else{
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
                }
            }
        }else{
            foreach ($type as $key => $value) {
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
            }
        }
        if(count($data2)!=12){
            $count=12-count($data2);
            for ($i=1; $i <=$count ; $i++) {    
                $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>'  &nbsp;','dead_date'=>'','address'=>'');
            }
        }
        $data2=json_decode(json_encode($data2));
        $html = view('admin.Pdf.index',['data'=>$data,'data2'=>$data2])->render();
        return $html;
    }
    public function Pdf2()
    {   
        $data=db::table('cabinet_appliction as cp')->where('no',Input::get('no'))
            ->where('applicant_number',Input::get('number'))
            ->select('cp.*','data.*','user.*','p.*','cp.id as id','cp.cost as cost','cp.other_cost as other_cost')
            ->join('cabinet_appliction_data as data','cp.id','=','data.c_no')
            ->join('cabinet_appliction_user as user','cp.id','=','user.c_no')
            ->join('cabinet_principal as p','cp.id','=','p.c_no')
            ->first();
        $data2=db::table('cabinet_appliction_data')
            ->where('c_no',$data->id)
            ->get();
        $user=db::table('cabinet_appliction_user')
            ->where('c_no',$data->id)
            ->get();
        $type=array();
        $new_data=array();
        foreach ($data2 as $key => $value) {
            $number=db::table('cabinet_sys')->where('code',$value->cabinet_code)->first();

                if($user[$key]->name_number==$number->number){
                    $new_data[$number->code]=$user[$key];
                    $type[$number->code]=0;
                }else{
                    $new_data[$number->code]='';
                    $type[$number->code]=1;
                }
        }

        $data2=array();
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                if($type[$key]==0){
                    $data2[]=array('gender' => $value->gender,'name' => $value->name,'birthday' => $value->birthday,'code' => $key,'address'=>$value->address,'dead_date'=>$value->dead_date);
                }else{
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
                }
            }
        }else{
            foreach ($type as $key => $value) {
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
            }
        }
        if(count($data2)!=12){
            $count=12-count($data2);
            for ($i=1; $i <=$count ; $i++) {    
                $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>'  &nbsp;','dead_date'=>'','address'=>'');
            }
        }
        $data2=json_decode(json_encode($data2));
        $html = view('admin.Pdf.index2',['data'=>$data,'data2'=>$data2])->render();
        return $html;
    }
    public function Pdf3()
    {
        $data=db::table('cabinet_appliction as cp')->where('no',Input::get('no'))
        ->where('applicant_number',Input::get('number'))
        ->select('cp.*','data.*','user.*','p.*','cp.id as id','cp.cost as cost','cp.other_cost as other_cost')
        ->join('cabinet_appliction_data as data','cp.id','=','data.c_no')
        ->join('cabinet_appliction_user as user','cp.id','=','user.c_no')
        ->join('cabinet_principal as p','cp.id','=','p.c_no')
        ->first();
        $data2=db::table('cabinet_appliction_data')
        ->where('c_no',$data->id)
        ->get();
        $user=db::table('cabinet_appliction_user')
        ->where('c_no',$data->id)
        ->get();
        foreach ($data2 as $key => $value) {
            $number=db::table('cabinet_sys')->where('code',$value->cabinet_code)->first();

                if($user[$key]->name_number==$number->number){
                    $new_data[$number->code]=$user[$key];
                    $type[$number->code]=0;
                }else{
                    $new_data[$number->code]='';
                    $type[$number->code]=1;
                }
        }

        $data2=array();
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                if($type[$key]==0){
                    $data2[]=array('gender' => $value->gender,'name' => $value->name,'birthday' => $value->birthday,'code' => $key,'address'=>$value->address,'dead_date'=>$value->dead_date);
                }else{
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
                }
            }
        }else{
            foreach ($type as $key => $value) {
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
            }
        }
        if(count($data2)!=12){
            $count=12-count($data2);
            for ($i=1; $i <=$count ; $i++) {    
                $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>'  &nbsp;','dead_date'=>'','address'=>'');
            }
        }
        $data2=json_decode(json_encode($data2));
        $html = view('admin.Pdf.index3',['data'=>$data,'data2'=>$data2])->render();
        return $html;
    }
    public function Pdf4()
    {
        $data=db::table('cabinet_appliction as cp')->where('no',Input::get('no'))
        ->where('applicant_number',Input::get('number'))
        ->select('cp.*','data.*','user.*','p.*','cp.id as id','cp.cost as cost','cp.other_cost as other_cost')
        ->join('cabinet_appliction_data as data','cp.id','=','data.c_no')
        ->join('cabinet_appliction_user as user','cp.id','=','user.c_no')
        ->join('cabinet_principal as p','cp.id','=','p.c_no')
        ->first();
        $data2=db::table('cabinet_appliction_data')
        ->where('c_no',$data->id)
        ->get();
        $user=db::table('cabinet_appliction_user')
        ->where('c_no',$data->id)
        ->get();
        $type=array();
        foreach ($data2 as $key => $value) {
            $number=db::table('cabinet_sys')->where('code',$value->cabinet_code)->first();

                if($user[$key]->name_number==$number->number){
                    $new_data[$number->code]=$user[$key];
                    $type[$number->code]=0;
                }else{
                    $new_data[$number->code]='';
                    $type[$number->code]=1;
                }
        }

        $data2=array();
        if(isset($new_data)){
            foreach ($new_data as $key => $value) {
                if($type[$key]==0){
                    $data2[]=array('gender' => $value->gender,'name' => $value->name,'birthday' => $value->birthday,'code' => $key,'address'=>$value->address,'dead_date'=>$value->dead_date);
                }else{
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
                }
            }
        }else{
            foreach ($type as $key => $value) {
                    $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>$key,'dead_date'=>'','address'=>'');
            }
        }
        if(count($data2)!=12){
            $count=12-count($data2);
            for ($i=1; $i <=$count ; $i++) {    
                $data2[]=array('gender' =>'','name' =>'','birthday' =>'','code' =>'  &nbsp;','dead_date'=>'','address'=>'');
            }
        }
        $data2=json_decode(json_encode($data2));
        $html = view('admin.Pdf.index4',['data'=>$data,'data2'=>$data2])->render();
        return $html;
    }
    public function get_data()
    {
        $data=Input::all();
        switch ($data['type']) {
            case '1':
            $table='cabinet_appliction';
            $data=db::Table($table)
                ->select('applicant','tel','phone','address','household_registration')
                ->where('applicant_number',$data['number'])
                ->orderBy('id','desc')
                ->first();
                break;
            case '2':
            $table='cabinet_principal';
            $data=db::Table($table)
                ->select('client','client_tel','client_address','client_phone','client_household_registration','client_address')
                ->where('client_number',$data['number'])
                ->orderBy('id','desc')
                ->first();
                break;
            default: break;
        }
        if($data){
                return json_encode($data);
        }else{
            return json_encode(array(0));
        }
    }
    public function BodyArray($Data = 1) {
        
        $CabinetSys=CabinetSys();

        $BodyArray = array(
            array('id' => 'id', 'name' => 'id', 'CNname' => '', 'placeholder' => "", 'class' => "", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $Data['id'] : null),
            array('id' => 'no', 'name' => 'no', 'CNname' => '申請單號：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['no'] : date('Ymd').rand(1000, 9999)),
            array('id' => 'application_type', 'name' => 'application_type', 'CNname' => '申請類別：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['application_type'], 'Type' => 'radio', "Value" => (is_object($Data)) ? $Data['application_type'] : ''),
            array('id' => 'application_date', 'name' => 'application_date', 'CNname' => '申請日期：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'InputDate', "Value" => (is_object($Data)) ? $Data['application_date'] : ''),
            array('id' => 'applicant', 'name' => 'applicant', 'CNname' => '申請人：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'InputDate', "Value" => (is_object($Data)) ? $Data['applicant'] : ''),
            array('id' => 'applicant_number', 'name' => 'applicant_number', 'CNname' => '申請人身分證：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['applicant_number'] : ''),
            array('id' => 'household_registration', 'name' => 'household_registration', 'CNname' => '申請人戶籍地：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['household_registration'] : ''),
            array('id' => 'tel', 'name' => 'tel', 'CNname' => '聯絡電話：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['tel'] : ''),
            array('id' => 'phone', 'name' => 'phone', 'CNname' => '行動電話：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['phone'] : ''),
            array('id' => 'relationship', 'name' => 'relationship', 'CNname' => '關係：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['relationship'] : ''),
            array('id' => 'address', 'name' => 'address', 'CNname' => '聯絡地址：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['address'] : ''),
            array('id' => 'file1', 'name' => 'file1', 'CNname' => '申請人身分證：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file1'] : ''),
            array('id' => 'file2', 'name' => 'file2', 'CNname' => '火化許可證：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file2'] : ''),
            array('id' => 'file3', 'name' => 'file3', 'CNname' => '死亡證明書：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file3'] : ''),
            array('id' => 'file4', 'name' => 'file4', 'CNname' => '戶籍謄本：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file4'] : ''),
            array('id' => 'file5', 'name' => 'file5', 'CNname' => '亡者除戶謄本：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'FileBox', "Value" => (is_object($Data)) ? $Data['file5'] : ''),
            array('id' => '', 'name' => '', 'CNname' => '逝者資訊', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'HR', "Value" => ''),
            array('id' => 'name', 'name' => 'name', 'CNname' => '逝者：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name'] : ''),
            array('id' => 'name_number', 'name' => 'name_number', 'CNname' => '逝者：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name_number'] : ''),
            array('id' => 'name_household_registration', 'name' => 'name_household_registration', 'CNname' => '逝者戶籍地：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['name_household_registration'] : ''),
            array('id' => 'gender', 'name' => 'gender', 'CNname' => '逝者戶籍地：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => '', 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['gender'] : ''),
            array('id' => 'identity', 'name' => 'identity', 'CNname' => '身分：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['type'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['identity'] : ''),

            // array('id' => 'position', 'name' => 'position', 'CNname' => '方位：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['position'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['position'] : null),
            // array('id' => 'floor', 'name' => 'floor', 'CNname' => '樓層：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['floor'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['floor'] : null),
            // array('id' => 'class', 'name' => 'class', 'CNname' => '類別：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' =>$CabinetSys['class'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['class'] : null),
            // array('id' => 'row', 'name' => 'row', 'CNname' => '排/棟：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['row'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['row'] : null),
            // array('id' => 'layer', 'name' => 'layer', 'CNname' => '層：', 'placeholder' => "", 'class' => "form-control", 'readonly' => '', 'Option' => $CabinetSys['layer'], 'Type' => 'Select', "Value" => (is_object($Data)) ? $Data['layer'] : null),
            // array('id' => 'code', 'name' => 'code', 'CNname' => '編號：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Text', "Value" => (is_object($Data)) ? $Data['code'] : null),
           );
        return $BodyArray;
    }
}