<?php

namespace App\Http\Controllers\admin\CabinetAppliction;
use Illuminate\Http\Request;
use App\Http\Controllers\BackController;
use App\Http\Requests\CabinetChangeApplictionRequest;
use App\Http\Requests\CabinetChangeApplictionUpdateRequest;
use DB;
use Input;
use Session;
use Auth;
class BackCabinetChangeApplictionController extends BackController {
    //配合route resource
    protected $RouteName="BackCabinetChangeAppliction";
    //檔案路徑
    protected $Path="Upload/CabinetAppliction/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\CabinetAppliction\CabinetAppliction";

    //主要table
    protected $Table1="cabinet_appliction";
    protected $Table2="cabinet_appliction_user";
    protected $Table3="cabinet_appliction_data";
    protected $Table4="users";
    protected $Table7="cabinet_cost_discount";
    protected $Table8="cabinet_other_cost";
    //關聯table
    // protected $Table2="roles";
    public function index()
    {
        $no=Input::get('no',0);
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '編號', 'width' => '5'),
            array('title' => '逝者', 'width' => '5'),
            array('title' => '身分證號碼', 'width' => '5'),
            array('title' => '身分', 'width' => '5'),
            array('title' => '申請人', 'width' => '5'),
            array('title' => '身分證號碼', 'width' => '5'),
            array('title' => '申請日期', 'width' => '5'),
            array('title' => '狀態', 'width' => '5'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select($this->Table1.'.id',
            $this->Table1.'.no',
            $this->Table1.'.id as d_user',
            $this->Table1.'.id as d_number',
            $this->Table1.'.id as d_id',
            $this->Table1.'.applicant',
            $this->Table1.'.applicant_number',
            $this->Table1.'.application_date',
            $this->Table1.'.status'
        )
        ->whereNull( $this->Table1.'.deleted_at')
        ->where( $this->Table1.'.status','>=',400)
        ->where( $this->Table1.'.status','<',600)
        ->groupBy($this->Table1.'.id')
        ->orderBy($this->Table1.'.updated_at','desc')
        ->orderBy($this->Table1.'.created_at','desc')
        ->get();
        $option=CabinetSys();
        foreach($Data as $key =>$val){
            $Data[$key]->status=$option['status'][$Data[$key]->status];
            $user=db::Table('cabinet_appliction_user')->where('c_no',$val->id)->first();
            if($user){
                $val->d_user=$user->name;
                $val->d_number=$user->name_number;
                $val->d_id=$option['type'][$user->identity];
            }

        }
        // $Data=$this->correspond($Data);
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'ApplictionEdit' => 1, 'Delete' => 0, 'Check' => 0, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0,'ApplictionChange'=>1,'print'=>1,'C_ApplictionDeletel'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        $no=no_select2();
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create','no']);
    }
    public function create()
    {
        //要edit的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        $LayoutTitle = "櫃位異動申請";
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $data=['FormTtile'=> $FormTtile,'url'=>$Url];
        $select_applicant_number=Input::get('select_applicant_number','');
        $select_no=Input::get('select_no','');
        if($select_applicant_number!=''&&$select_no!=''){
            $cabinet_appliction=db::Table('cabinet_appliction')
                ->where('id',$select_no)
                ->first();
            $cabinet_appliction_user=db::table('cabinet_appliction_user')
                ->where('c_no',$cabinet_appliction->id)
                ->get();
            $cabinet_appliction_data=db::Table('cabinet_appliction_data')
                ->where('c_no',$cabinet_appliction->id)
                ->get();
            $cabinet_principal=db::Table('cabinet_principal')
                ->where('c_no',$cabinet_appliction->id)
                ->first();
            $users=db::Table('users')->find($cabinet_appliction->users);
            $confirm_payment_user=db::Table('users')->find($cabinet_appliction->confirm_payment_user);

            $sys_status=db::table('cabinet_sys')->where('c_no',$cabinet_appliction->id)->first();
            //old data
            foreach($cabinet_appliction_data as $key =>$value){
                $data['old_cabinet_code'][]=DB::Table('cabinet_sys')->where('code',$value->cabinet_code)->first();
                $data['old_area'][]=db::Table('cabinet_sys_class')->where('id',$value->cabinet_class_aera)->whereNull('deleted_at')->first();
                $data['old_layer'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_layer)->whereNull('deleted_at')->first();
                $data['old_row'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_row)->whereNull('deleted_at')->first();
                $data['old_seat'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_seat)->whereNull('deleted_at')->first();
            }
            switch (count($cabinet_appliction_user)) {
                case 1: $type='個人'; $qty=1; break;
                case 2: $type='夫妻'; $qty=2; break;
                case 12: $type='家族'; $qty=12; break;
                default:  break;
            }
            $change_cost=db::table('cabinet_other_cost')->first();

            //新櫃位選項
            $new_area=db::Table('cabinet_sys_class')->where('name','like','%'.$type.'%')->whereNull('deleted_at')->get();
            if(count($new_area)>0){
                $new_layer=db::table('cabinet_sys_class')->where('top_class',$new_area[0]->id)->whereNull('deleted_at')->get();
            }
            if(count($new_layer)>0){
                $new_row=db::table('cabinet_sys_class')->where('top_class',$new_layer[0]->id)->whereNull('deleted_at')->get();
            }
            if(count($new_row)>0){
                $new_seat=db::table('cabinet_sys_class')->where('top_class',$new_row[0]->id)->whereNull('deleted_at')->get();
            }
            $sys=array();
            $area_code  =db::Table('cabinet_sys_class')->where('id',$new_area[0]->id)->whereNull('deleted_at')->first();
            $layer_code =db::Table('cabinet_sys_class')->where('id',$new_layer[0]->id)->whereNull('deleted_at')->first();
            $row_code =db::Table('cabinet_sys_class')->where('id',$new_row[0]->id)->whereNull('deleted_at')->first();

            foreach($new_seat as $seats){
                $sys[]='1-'.$area_code->code.'-'.$layer_code->code.'-'.$row_code->code.'-'.$seats->code;
            }
            $position_data=array(0=>'無',1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
            $sys_data=db::Table('cabinet_sys')
                ->whereIn('code',$sys)
                ->orderBy('seat','asc')
                ->whereNull('number')
                ->get();
            $position=array();
            foreach($sys_data as $key =>$value){
                $position[$value->position]=$position_data[$value->position];
                $sys_data[$key]->position_name=$position_data[$value->position];
            }

            $position[0]='無';
            $data['position']=$position;
            $data['url']=$Url;
            $data['type']=$type;
            $data['cabinet_appliction']=$cabinet_appliction;
            $data['cabinet_appliction_user']=$cabinet_appliction_user;
            $data['cabinet_appliction_data']=$cabinet_appliction_data;
            $data['cabinet_principal']=$cabinet_principal;
            $data['users']=$users;
            $data['confirm_payment_user']=$confirm_payment_user;
            $data['area']=$new_area;
            $data['layer']=$new_layer;
            $data['row']=$new_row;
            $data['seat']=$new_seat;
            $data['qty']=$qty;
            $data['position_data']=$position_data;
            $data['change_cost']=$change_cost->change_cost;
        
            switch ($type) {
                case '個人':
                     $data['change_cost']=$change_cost->change_cost;
                    break;
                case '夫妻':
                     $data['change_cost']=$change_cost->change_cost;
                    break;
                 case '家族':
                    $data['change_cost']=$change_cost->family_change_cost;
                    break;
                default:
                    # code...
                    break;
            }
           
            $data['other_cost']=$change_cost->other_cost;
            if($sys_status->tag==1){
                $data['change_cost']=0;
                $data['other_cost']=0;
            }
            $data['no']=no_select2();
            $data['sys']=$sys_data;
            $data['tag']=$sys_status->tag;
        }
        return view('admin.Form.cabinet_change_appliction',$data);
    }
    //CabinetChangeAppliction
    public function store(Request $request)
    {  
        switch ($request->cabinet_type) {
            case '個人': $qty=1;break;
            case '夫妻': $qty=2;break;
            case '家族': $qty=12;break;
            default: return redirect()->back();  break;
        }
        for ($i=1; $i <=$qty ; $i++) { 
            $cabinet_code='cabinet_code'.$i;
            $cost_data=explode('-', $request->$cabinet_code);
            if($cost_data[0]==0||$cost_data[0]==''){
                return redirect()->back()->withInput()->with('status',$request->$cabinet_code.'請選擇櫃位');
            }
        }
        $no=(int)$request->no;
        do{
            $no_select=no_select('cabinet_appliction','1070091');
            if(!$no_select){
                $no+=1;
            }
        } while ($no_select==false);
        $cabinet_appliction=array();
        $cabinet_appliction['before_appliction_id']=$request->cabinet_appliction_id;
        $cabinet_appliction['cabinet_appliction_type']=$request->cabinet_appliction_type;
        $cabinet_appliction['no']=$no;
        $cabinet_appliction['applicant']=$request->applicant;
        $cabinet_appliction['number']=$request->applicant_number;
        $cabinet_appliction['application_type']=$request->application_type;
        $cabinet_appliction['application_date']=$request->application_date;
        $cabinet_appliction['household_registration']=$request->household_registration;
        $cabinet_appliction['applicant_number']=$request->applicant_number;
        $cabinet_appliction['tel']=$request->tel;
        $cabinet_appliction['phone']=$request->phone;
        $cabinet_appliction['relationship']=$request->relationship;
        $cabinet_appliction['address']=$request->address;
        $cabinet_appliction['ext']=$request->ext;
        $cabinet_appliction['users']=Auth::user()->id;
        $cabinet_appliction['confirm_payment_user']=Auth::user()->id;
        $cabinet_appliction['cost']=$request->cost;
        $cabinet_appliction['other_amount_due']=$request->other_amount_due;
        $cabinet_appliction['amount_due']=$request->amount_due;
        $cabinet_appliction['status']=400;
        // $cabinet_appliction['code']=$request->cabinet_code;
        for ($i = 1; $i < 10; $i++) {
            $name='file'.$i;
            if(Input::hasFile($name)){
            $filename = date('Ymd').rand(11111, 99999);
            $cabinet_appliction[$name] = $filename .".". Input::file($name)->getClientOriginalExtension();
                Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
            }
        }
        // dd($cabinet_appliction);
        $cabinet_appliction_id=DB::table('cabinet_appliction')->insertGetid($cabinet_appliction);
        $cabinet_appliction_user=array();
        for ($i=1; $i <=$qty ; $i++) { 
            $name='name'.$i;
            $name_number='name_number'.$i;
            $name_household_registration='name_household_registration'.$i;
            $gender='gender'.$i;
            $identity='identity'.$i;
            $discount='discount'.$i;
            $dead_date='dead_date'.$i;
            $dead_reason='dead_reason'.$i;
            $dead_location='dead_location'.$i;
            $birthday='birthday'.$i;
            $expected_date='expected_date'.$i;
            $expected_time='expected_time'.$i;
            // $cabinet_appliction_user[$i]['c_no']=1;
            $cabinet_appliction_user[$i]['c_no']=$cabinet_appliction_id;
            $cabinet_appliction_user[$i]['name']=$request->$name;
            $cabinet_appliction_user[$i]['name_number']=$request->$name_number;
            $cabinet_appliction_user[$i]['address']=$request->$name_household_registration;
            $cabinet_appliction_user[$i]['gender']=$request->$gender;
            $cabinet_appliction_user[$i]['identity']=$request->$identity;
            $cabinet_appliction_user[$i]['discount']=$request->$discount;
            $cabinet_appliction_user[$i]['dead_date']=$request->$dead_date;
            $cabinet_appliction_user[$i]['dead_reason']=$request->$dead_reason;
            $cabinet_appliction_user[$i]['dead_location']=$request->$dead_location;
            $cabinet_appliction_user[$i]['birthday']=$request->$birthday;
            $cabinet_appliction_user[$i]['expected_date']=$request->$expected_date;
            $cabinet_appliction_user[$i]['expected_time']=$request->$expected_time;
        }
        DB::table('cabinet_appliction_user')->insert($cabinet_appliction_user);
        $cabinet_appliction_data=array();
        for ($i=1; $i <=$qty ; $i++) {
            $old_cabinet_class_seat='old_cabinet_class_data'.$i;
            $cabinet_code='cabinet_code'.$i;
            $old_cabinet_code='old_cabinet_code'.$i;
            $identity='identity'.$i;
            $cabinet_type='cabinet_type'.$i;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_layer='cabinet_class_layer'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_seat='cabinet_class_seat'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $dead_date='dead_date'.$i;
            $dead_reason='dead_reason'.$i;
            $dead_location='dead_location'.$i;
            $birthday='birthday'.$i;
            $expected_date='expected_date'.$i;
            $cabinet_appliction_user[$i]['c_no']=1;
            $identity='identity'.$i;
            $discount='discount'.$i;

            $other_cost=db::Table($this->Table8)->first();
            $cost_data=explode('-', $request->$cabinet_code);
            $cabinet_appliction_data[$i]['c_no']=$cabinet_appliction_id;
            // $cabinet_appliction_data[$i]['c_no']=1;
            $cabinet_appliction_data[$i]['old_cabinet_class_seat']=$request->$old_cabinet_class_seat;
            $cabinet_appliction_data[$i]['cabinet_type']=$request->cabinet_type;
            $cabinet_appliction_data[$i]['cabinet_class_floor']=1;
            $cabinet_appliction_data[$i]['cabinet_class_aera']=$request->cabinet_class_aera1;
            $cabinet_appliction_data[$i]['cabinet_class_layer']=$request->cabinet_class_layer1;
            $cabinet_appliction_data[$i]['cabinet_class_row']=$request->cabinet_class_row1;
            $cabinet_appliction_data[$i]['cabinet_class_seat']=$request->cabinet_class_seat1;
            $cabinet_appliction_data[$i]['cabinet_class_position']=$request->cabinet_class_position1;
            $cabinet_appliction_data[$i]['cabinet_code']=$request->$cabinet_code;
            $cost_data=explode('-', $request->$cabinet_code);
            $old_cost_data=explode('-', $request->$old_cabinet_code);
            $cost=db::Table($this->Table7)
                ->where('type',$request->$identity)
                ->where('area',$cost_data[1])
                ->where('row',$cost_data[3])
                ->first();

            $old_cost=db::Table($this->Table7)
                ->where('type',$request->$identity)
                ->where('area',$old_cost_data[1])
                ->where('row',$old_cost_data[3])
                ->first();
            $old_price=0;
            $new_price=0;
            switch ($request->$discount) {
                    case '1':
                        if($cost->discount1==1){
                            $new_price=$cost->price*0.2;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($old_cost->discount1==1){
                            $old_price=$old_cost->price*0.2;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break; 
                    case '2':
                        if($cost->discount2==1){
                            $new_price=$cost->price*0.5;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($old_cost->discount2==1){
                            $old_price=$old_cost->price*0.5;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break;
                    case '3':
                        if($cost->discount3==1){
                            $new_price=$cost->price*0.9;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($cost->discount3==1){
                            $old_price=$old_cost->price*0.9;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break;
                    case '4':
                        if($cost->discount4==1){
                            $new_price=$cost->price*0.95;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($old_cost->discount4==1){
                            $old_price=$old_cost->price*0.95;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break;
                    default:
                        $new_price=$cost->price;
                        $old_price=$old_cost->price;
                        break;
                }
                $price=$new_price-$old_price;
                $cabinet_appliction_data[$i]['cost']=$other_cost->change_cost;
                $cabinet_appliction_data[$i]['other_cost']=$price;
                $cabinet_appliction_data[$i]['amount_due']=$cabinet_appliction_data[$i]['cost']+$price;
            }
        DB::Table('cabinet_appliction_data')->insert($cabinet_appliction_data);

        $cabinet_principal=array();
        $cabinet_principal['c_no']=$cabinet_appliction_id;
        // $cabinet_principal['c_no']=1;
        $cabinet_principal['client']=$request->client;
        $cabinet_principal['client_number']=$request->client_number;
        $cabinet_principal['client_household_registration']=$request->client_household_registration;
        $cabinet_principal['client_tel']=$request->client_tel;
        $cabinet_principal['client_phone']=$request->client_phone;
        $cabinet_principal['client_address']=$request->client_address;
        DB::table('cabinet_principal')->insert($cabinet_principal);
        //改變原申請單的狀態
        $old_cabinet_appliction=db::Table('cabinet_appliction')->where('id',$request->cabinet_appliction_id)->first();
        db::Table('cabinet_appliction')->where('id',$request->cabinet_appliction_id)->update(['status'=>350]);
        for($i=1;$i<=$qty;$i++){
            $cabinet_code='cabinet_code'.$i;
            $name_number='name_number'.$i;
            $cabinet_class_floor=1;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $expected_date='expected_date'.$i;
            $dead_date='dead_date'.$i;
            $name='name'.$i;
            $identity='identity'.$i;
            Exist('cabinet_sys',
                $request->$cabinet_code,
                $request->status,
                $request->$name_number,
                '',
                $cabinet_appliction_id
            );
            ExistLog(
                '櫃位',
                1,
                $request->cabinet_class_aera1,
                $request->cabinet_class_row1,
                $request->cabinet_class_position1,
                $request->cabinet_code1,
                $no,
                $request->applicant_number,
                $request->$name_number,
                $request->application_type,
                $request->application_date,
                $request->$expected_date,
                $request->$dead_date,
                $request->status,
                $request->applicant,
                $request->$name,
                $request->tel,
                $request->phone,
                $request->$identity
            );
        }
        no_insert($no);
        return redirect()->to($this->RouteName)->with('status','新增成功');
    } 
    public function edit($id)
    {   
        $Url = $this->RouteName."/".$id;
        $LayoutTitle='';

        $cabinet_sys=DB::Table('cabinet_sys')->where('status',1)->get();

        $cabinet_appliction=db::table('cabinet_appliction')->find($id);

        $users=db::Table('users')->find($cabinet_appliction->users);

        $confirm_payment_user=db::table('users')->find($cabinet_appliction->confirm_payment_user);

        $cabinet_appliction_user=db::table('cabinet_appliction_user')
                ->where('c_no',$cabinet_appliction->id)
                ->get();

        $cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('c_no',$id)->get();

        // $old_cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('id',$cabinet_appliction_data->old_cabinet_class_seat)->get();
        $cabinet_principal=db::Table('cabinet_principal')->where('c_no',$id)->first();
        if($cabinet_appliction->status=='400'){
            $LayoutTitle = "櫃位異動申請繳費";
        }if($cabinet_appliction->status=='500'){
            $LayoutTitle = "櫃位異動";
        }
            $users=db::Table('users')->find($cabinet_appliction->users);
            $confirm_payment_user=db::Table('users')->find($cabinet_appliction->confirm_payment_user);
            //old data
            $data['cost']=0;
            $data['other_cost']=0;
            $data['amount_due']=0;
            foreach($cabinet_appliction_data as $key =>$value){
                $old_cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('id',$value->old_cabinet_class_seat)->first();
                $data['old_cabinet_code'][]=DB::Table('cabinet_sys')->where('code',$old_cabinet_appliction_data->cabinet_code)->first();
                $data['old_area'][]=db::Table('cabinet_sys_class')->where('id',$old_cabinet_appliction_data->cabinet_class_aera)->whereNull('deleted_at')->first();
                $data['old_layer'][]=db::table('cabinet_sys_class')->where('id',$old_cabinet_appliction_data->cabinet_class_layer)->whereNull('deleted_at')->first();
                $data['old_row'][]=db::table('cabinet_sys_class')->where('id',$old_cabinet_appliction_data->cabinet_class_row)->whereNull('deleted_at')->first();
                $data['old_seat'][]=db::table('cabinet_sys_class')->where('id',$old_cabinet_appliction_data->cabinet_class_seat)->whereNull('deleted_at')->first();
                $data['old_cabinet_appliction_data'][]=$old_cabinet_appliction_data;

                $data['cabinet_code'][]=DB::Table('cabinet_sys')->where('code',$value->cabinet_code)->first();
                $data['area'][]=db::Table('cabinet_sys_class')->where('id',$value->cabinet_class_aera)->whereNull('deleted_at')->first();
                $data['layer'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_layer)->whereNull('deleted_at')->first();
                $data['row'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_row)->whereNull('deleted_at')->first();
                $data['seat'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_seat)->whereNull('deleted_at')->first();
                $data['cost']+=$value->cost;
                $data['other_cost']+=$value->other_cost;
                $data['amount_due']+=$value->amount_due;
            }
            switch (count($cabinet_appliction_user)) {
                case 1: $type='個人';  break;
                case 2: $type='夫妻';  break;
                case 12: $type='家族';  break;
                default:  break;
            }
            $data['position']=array(0=>'無',1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
            $data['url']=$Url;
            $data['type']=$type;
            $data['tag']=$data['old_cabinet_code'][0]->tag;
            $data['cabinet_appliction']=$cabinet_appliction;
            $data['cabinet_appliction_user']=$cabinet_appliction_user;
            $data['cabinet_appliction_data']=$cabinet_appliction_data;
            $data['cabinet_principal']=$cabinet_principal;
            $data['users']=$users;
            $data['confirm_payment_user']=$confirm_payment_user;
            $data['id']=$id;
        //要edit的路由 配合route resource
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        // $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $data['FormTtile'] = '';
        return view('admin.Form.cabinet_change_appliction_edit',$data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CabinetChangeApplictionUpdateRequest $request,$id)
    {   
        $required="";
        $cabinet_appliction=array();
        // $cabinet_appliction['cabinet_appliction_type']=$request->cabinet_appliction_type;
        // $cabinet_appliction['no']=$request->no;
        // $cabinet_appliction['application_date']=$request->application_date;
        
        // $cabinet_appliction['application_date']=$request->application_date;
        // $cabinet_appliction['applicant']=$request->applicant;
        // $cabinet_appliction['household_registration']=$request->household_registration;
        // $cabinet_appliction['applicant_number']=$request->applicant_number;
        // $cabinet_appliction['tel']=$request->tel;
        // $cabinet_appliction['phone']=$request->phone;
        // $cabinet_appliction['relationship']=$request->relationship;
        // $cabinet_appliction['address']=$request->address;
        $cabinet_appliction['change_date']=$request->change_date;
        $cabinet_appliction['principal']=$request->principal;
        $cabinet_appliction['status']=$request->status;
        if($request->status==500){
            $cabinet_appliction['payment_date']=$request->payment_date;
            for ($i = 6; $i < 8; $i++) {
            $name='file'.$i;
                if(Input::hasFile($name)){
                $filename = date('Ymd').rand(11111, 99999);
                $cabinet_appliction[$name] = $filename .".". Input::file($name)->getClientOriginalExtension();
                    Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
                }
            }
        }
        // $cabinet_appliction_user=array();
        // $cabinet_appliction_user['c_no']=$cabinet_appliction_id;
        // $cabinet_appliction_user['name']=$request->name;
        // $cabinet_appliction_user['name_number']=$request->name_number;
        // $cabinet_appliction_user['address']=$request->address;
        // $cabinet_appliction_user['gender']=$request->gender;
        // $cabinet_appliction_user['identity']=$request->identity;
        // $cabinet_appliction_user['discount']=$request->discount;
        // $cabinet_appliction_user['dead_date']=$request->dead_date;
        // $cabinet_appliction_user['dead_reason']=$request->dead_reason;
        // $cabinet_appliction_user['dead_location']=$request->dead_location;
        // $cabinet_appliction_user['birthday']=$request->birthday;
        // $cabinet_appliction_user['expected_date']=$request->expected_date;
        // DB::table('cabinet_appliction_user')->where('id',$request->id)->update($cabinet_appliction_user);
        
        // $cabinet_appliction_data=array();
        // $cabinet_appliction_data['c_no']=$cabinet_appliction_id;
        // $cabinet_appliction_data['cabinet_type']=$request->cabinet_type;
        // $cabinet_appliction_data['cabinet_class_floor']=$request->new_cabinet_class_floor;
        // $cabinet_appliction_data['cabinet_class_aera']=$request->new_cabinet_class_aera;
        // $cabinet_appliction_data['cabinet_class_row']=$request->new_cabinet_class_row;
        // $cabinet_appliction_data['cabinet_class_layer']=$request->new_cabinet_class_layer;
        // $cabinet_appliction_data['cabinet_code']=$request->new_cabinet_code;
        // $cabinet_appliction_data['cabinet_class_seat']=$request->cabinet_class_seat;
        // $cabinet_appliction_data['cost']=$request->cost;
        // $cabinet_appliction_data['other_cost']=$request->other_cost;
        // $cabinet_appliction_data['amount_due']=$request->amount_due;
        // $cabinet_principal=array();

        // $cabinet_principal['c_no']=$cabinet_appliction_id;
        // $cabinet_principal['client']=$request->client;
        // $cabinet_principal['client_number']=$request->client_number;
        // $cabinet_principal['client_household_registration']=$request->client_household_registration;
        // $cabinet_principal['client_tel']=$request->client_tel;
        // $cabinet_principal['client_phone']=$request->client_phone;
        // $cabinet_principal['client_address']=$request->client_address;
         switch ($request->cabinet_type) {
            case '個人': $qty=1;break;
            case '夫妻': $qty=2;break;
            case '家族': $qty=12;break;
            default: return redirect()->back();  break;
        }
        if(empty($required)){
            DB::Table('cabinet_appliction')->where('id',$request->id)->update($cabinet_appliction);
                // DB::Table('cabinet_appliction_data')->where('id',$request->cabinet_appliction_data_id)->update($cabinet_appliction_data);
            // dd($request->all());
            // Exist('cabinet_sys',$request->old_cabinet_code,$request->status,$request->name_number,$request->cabinet_code);
            // ExistLog('櫃位',$request->cabinet_class_floor,$request->cabinet_class_aera,$request->cabinet_class_row,$request->cabinet_class_position,$request->cabinet_code,$request->no,$request->applicant_number,$request->name_number,$request->application_type,$request->application_date,$request->expected_date,$request->dead_date,$request->status,$request->applicant,$request->name,$request->tel,$request->phone,$request->identity);
            for($i=1;$i<=$qty;$i++){
                $old_cabinet_code='old_cabinet_code'.$i;
                $cabinet_code='cabinet_code'.$i;
                $name_number='name_number'.$i;
                $cabinet_class_floor=1;
                $cabinet_class_aera='cabinet_class_aera'.$i;
                $cabinet_class_row='cabinet_class_row'.$i;
                $cabinet_class_position='cabinet_class_position'.$i;
                $expected_date='expected_date'.$i;
                $dead_date='dead_date'.$i;
                $name='name'.$i;
                $identity='identity'.$i;
                Exist('cabinet_sys',
                    $request->$cabinet_code,
                    $request->status,
                    $request->$name_number,
                    $request->$old_cabinet_code,
                    $request->id
                );
                if($request->status==500){
                    ExistLog('櫃位',
                        1,
                        $request->$cabinet_class_aera,
                        $request->$cabinet_class_row,
                        $request->$cabinet_class_position,
                        $request->$cabinet_code,
                        $request->no,
                        $request->applicant_number,
                        $request->$name_number,
                        $request->application_type,
                        $request->application_date,
                        $request->payment_date,
                        $request->$dead_date,
                        $request->status,
                        $request->applicant,
                        $request->$name,
                        $request->tel,
                        $request->phone,
                        $request->$identity
                    );
                }else{
                    ExistLog('櫃位',
                        1,
                        $request->$cabinet_class_aera,
                        $request->$cabinet_class_row,
                        $request->$cabinet_class_position,
                        $request->$cabinet_code,
                        $request->no,
                        $request->applicant_number,
                        $request->$name_number,
                        $request->application_type,
                        $request->application_date,
                        $request->change_date,
                        $request->$dead_date,
                        $request->status,
                        $request->applicant,
                        $request->$name,
                        $request->tel,
                        $request->phone,
                        $request->$identity
                    );
                }
                
            }
            return redirect()->to($this->RouteName)->with('status','成功');
        }else{
            return redirect()->back()->with('status',$required)->withInput();
        }

        
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function get_no()
    {
        $select_applicant_number=Input::get('select_applicant_number');
        $cabinet_appliction=db::Table('cabinet_appliction')
                ->select('id','no')
                ->where('applicant_number',$select_applicant_number)

                ->whereIn('status',[200,300,600])
                ->get();
        return json_encode($cabinet_appliction);
    }
    public function change_calculate($value='')
    {
        $data=Input::all();
        $array=array();
        for ($i=1; $i <=$data['qty'];$i++) { 
            $cost_data=explode('-', $data['cabinet_code'.$i]);
            $old_cost_data=explode('-', $data['old_cabinet_code'.$i]);
            $cost=db::Table($this->Table7)
                ->where('type',$data['identity'.$i])
                ->where('area',$cost_data[1])
                ->where('row',$cost_data[3])
                ->first();
            $old_cost=db::Table($this->Table7)
                ->where('type',$data['identity'.$i])
                ->where('area',$old_cost_data[1])
                ->where('row',$old_cost_data[3])
                ->first();
            if(!$cost){
                return json_encode(array('未設定費用,請勿送出'));
            }
            $old_price=0;
            $new_price=0;
            if($data['qty']!=12){
                switch ($data['discount'.$i]) {
                    case '1':
                        if($cost->discount1==1){
                            $new_price=$cost->price*0.2;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($old_cost->discount1==1){
                            $old_price=$old_cost->price*0.2;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break; 
                    case '2':
                        if($cost->discount2==1){
                            $new_price=$cost->price*0.5;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($old_cost->discount2==1){
                            $old_price=$old_cost->price*0.5;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break;
                    case '3':
                        if($cost->discount3==1){
                            $new_price=$cost->price*0.9;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($cost->discount3==1){
                            $old_price=$old_cost->price*0.9;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break;
                    case '4':
                        if($cost->discount4==1){
                            $new_price=$cost->price*0.95;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($old_cost->discount4==1){
                            $old_price=$old_cost->price*0.95;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break;
                    case '5':
                        if($cost->discount5==1){
                            $new_price=$cost->price*0.8;
                        }else{
                            $new_price=$cost->price;
                        }
                        if($old_cost->discount5==1){
                            $old_price=$old_cost->price*0.8;
                        }else{
                             $old_price=$old_cost->price;
                        }
                        break;
                    default:
                        $new_price=$cost->price;
                        $old_price=$old_cost->price;
                        break;
                }
            }else{
                $new_price=$cost->price;
                $old_price=$old_cost->price;
            }

            $array[$i]['price']=$new_price-$old_price;
        }
        $other_cost=db::Table('cabinet_other_cost')->first();
        $array['other_cost']=$other_cost->other_cost;
        return json_encode($array);
    }
}