<?php

namespace App\Http\Controllers\admin\CabinetAppliction;
use Illuminate\Http\Request;
use App\Http\Controllers\BackController;
use App\Http\Requests\CabinetOutApplictionRequest;
use DB;
use Input;
use Session;
use Auth;
class BackCabinetOutApplictionController extends BackController {
    //配合route resource
    protected $RouteName="BackCabinetOutAppliction";
    //檔案路徑
    protected $Path="Upload/CabinetOutAppliction/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\CabinetAppliction\CabinetOutAppliction";

    //主要table
    protected $Table1="cabinet_appliction";
    protected $Table2="cabinet_appliction_user";
    protected $Table3="cabinet_appliction_data";
    protected $Table4="users";
    //關聯table
    // protected $Table2="roles";
    public function index()
    {
        $no=Input::get('no',0);
        $BodyArray = array(
            array('title' => 'Id', 'width' => '5'),
            array('title' => '編號', 'width' => '5'),
            array('title' => '逝者', 'width' => '5'),
            array('title' => '身分證號碼', 'width' => '5'),
            array('title' => '身分', 'width' => '5'),
            array('title' => '申請人', 'width' => '5'),
            array('title' => '身分證號碼', 'width' => '5'),
            array('title' => '狀態', 'width' => '5'),
            array('title' => 'option', 'width' => '20')
        );
        $Data=DB::table($this->Table1)
        ->select($this->Table1.'.id',
            $this->Table1.'.no',
            $this->Table1.'.id as d_user',
            $this->Table1.'.id as d_number',
            $this->Table1.'.id as d_id',
            $this->Table1.'.applicant',
            $this->Table1.'.applicant_number',
            $this->Table1.'.status'
        )
        ->join($this->Table2,$this->Table2.'.c_no','=',$this->Table1.'.id')
        ->join($this->Table3,$this->Table3.'.c_no','=',$this->Table1.'.id')
        ->join($this->Table4,$this->Table1.'.users','=',$this->Table4.'.id')
        ->whereNull( $this->Table1.'.deleted_at')
        ->where( $this->Table1.'.status','>=',700)
        ->where( $this->Table1.'.status','<',800)
        ->groupBy($this->Table1.'.id')
        ->orderBy($this->Table1.'.updated_at','desc')
        ->orderBy($this->Table1.'.created_at','desc');
        if($no!=0){
            $Data->where($this->Table1.'.no',$no);
        }
        $Data=$Data->get();
        $option=CabinetSys();
        foreach($Data as $key =>$val){
            $Data[$key]->status=$option['status'][$Data[$key]->status];
            $user=db::Table('cabinet_appliction_user')->where('c_no',$val->id)->first();
            $val->d_user=$user->name;
            $val->d_number=$user->name_number;
            $val->d_id=$option['type'][$user->identity];
            $data=db::Table('cabinet_appliction_data')->where('c_no',$val->id)->first();
            
        }
        // $Data=$this->correspond($Data);
        //DataTable中的功能按鈕
        //'Img' => 縮圖, 'Edit' => 修改, 'Delete' => 刪除, 'Check' => 審核狀態, 'SmartSort' => 智能排序, 'Add' =>增加子頁
        $Option = ['Img' => 0, 'Appliction2' => 1, 'Delete' => 0, 'Check' => 0, 'SmartSort' => 0, 'Add' => 0, 'Read' => 0,'ChangeStatus'=>0,'Appliction2'=>1,'print'=>1,'C_ApplictionDeletel'=>1];
        //Controller的名稱
        $ControllerName = $this->RouteName;
        $DataTableJs    = DataTableJs("tableWithSearch", $BodyArray, $Data, $Option, $ControllerName, 2);
        $no=no_select2();
        return view('admin.DataTable.index', ['DataTableJs' => $DataTableJs,'AddUrl'=>$this->RouteName.'/create','no'=>$no]);
    }
    public function create()
    {
        //要edit的路由 配合route resource
        $Url = $this->RouteName;
        //表格簡介
        $LayoutBody = "";
        $LayoutTitle = "櫃位遷出申請";
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);
        $data=['FormTtile'=> $FormTtile,'url'=>$Url];
        $select_applicant_number=Input::get('select_applicant_number','');
        $select_no=Input::get('select_no','');
        if($select_applicant_number!=''&&$select_no!=''){
            $cabinet_appliction=db::Table('cabinet_appliction')
                ->where('id',$select_no)
                ->where('applicant_number',$select_applicant_number)
                ->first();
            $cabinet_appliction_user=db::table('cabinet_appliction_user')
                ->where('c_no',$cabinet_appliction->id)
                ->get();
            $cabinet_appliction_data=db::Table('cabinet_appliction_data')
                ->where('c_no',$cabinet_appliction->id)
                ->get();
            $cabinet_principal=db::Table('cabinet_principal')
                ->where('c_no',$cabinet_appliction->id)
                ->first();
            $users=db::Table('users')->find($cabinet_appliction->users);
            $confirm_payment_user=db::Table('users')->find($cabinet_appliction->confirm_payment_user);
            //old data
            foreach($cabinet_appliction_data as $key =>$value){
                $data['cabinet_code'][]=DB::Table('cabinet_sys')->where('code',$value->cabinet_code)->first();
                $data['area'][]=db::Table('cabinet_sys_class')->where('id',$value->cabinet_class_aera)->whereNull('deleted_at')->first();
                $data['layer'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_layer)->whereNull('deleted_at')->first();
                $data['row'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_row)->whereNull('deleted_at')->first();
            }
            switch (count($cabinet_appliction_user)) {
                case 1: $type='個人';  break;
                case 2: $type='夫妻';  break;
                case 12: $type='家族';  break;
                default:  break;
            }
            $data['position']=array(0=>'無',1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
            $data['url']=$Url;
            $data['type']=$type;
            $data['cabinet_appliction']=$cabinet_appliction;
            $data['cabinet_appliction_user']=$cabinet_appliction_user;
            $data['cabinet_appliction_data']=$cabinet_appliction_data;
            $data['cabinet_principal']=$cabinet_principal;
            $data['users']=$users;
            $data['confirm_payment_user']=$confirm_payment_user;
            
        }
        $data['no']=no_select2();
        //app/Support/Helpers底下的自訂function
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        return view('admin.Form.cabinet_out_appliction',$data);
    }
    public function store(CabinetOutApplictionRequest $request)
    {   
        switch ($request->cabinet_type) {
            case '個人': $qty=1;break;
            case '夫妻': $qty=2;break;
            case '家族': $qty=12;break;
            default: return redirect()->back();  break;
        }
        $no=$request->no;
        do{
            $no_select=no_select('cabinet_appliction',$no);
            if(!$no_select){
                $no++;
            }
        } while ( !$no_select );
        $old_cabinet_appliction=db::Table('cabinet_appliction')->find($request->cabinet_appliction_id);
        $cabinet_appliction=array();
        $cabinet_appliction['before_appliction_id']=$request->cabinet_appliction_id;
        $cabinet_appliction['cabinet_appliction_type']=$request->cabinet_appliction_type;
        $cabinet_appliction['no']=$no;
        $cabinet_appliction['applicant']=$request->applicant;
        $cabinet_appliction['application_type']=$request->application_type;
        $cabinet_appliction['application_date']=$request->application_date;
        $cabinet_appliction['household_registration']=$request->household_registration;
        $cabinet_appliction['applicant_number']=$request->applicant_number;
        $cabinet_appliction['tel']=$request->tel;
        $cabinet_appliction['phone']=$request->phone;
        $cabinet_appliction['relationship']=$request->relationship;
        $cabinet_appliction['address']=$request->address;
        $cabinet_appliction['ext']=$request->ext;
        $cabinet_appliction['users']=Auth::user()->id;
        // $cabinet_appliction_user['application_date']=$request->application_date;
        // $cabinet_appliction['code']=$request->cabinet_code;
        $cabinet_appliction['status']=$request->status;
        $cabinet_appliction['cost']=$request->cost;
        for ($i = 1; $i < 10; $i++) {
            $name='file'.$i;
            if(Input::hasFile($name)){
            $filename = date('Ymd').rand(11111, 99999);
            $cabinet_appliction[$name] = $filename .".". Input::file('file1')->getClientOriginalExtension();
                Input::file($name)->move($this->Path, $filename . "." . Input::file($name)->getClientOriginalExtension());
            }
        }
        $cabinet_appliction_id=DB::table('cabinet_appliction')->insertGetid($cabinet_appliction);
        for ($i=1; $i <=$qty ; $i++) { 
            $name='name'.$i;
            $name_number='name_number'.$i;
            $name_household_registration='name_household_registration'.$i;
            $gender='gender'.$i;
            $identity='identity'.$i;
            $discount='discount'.$i;
            $dead_date='dead_date'.$i;
            $dead_reason='dead_reason'.$i;
            $dead_location='dead_location'.$i;
            $birthday='birthday'.$i;
            $expected_date='expected_date'.$i;
            $expected_time='expected_time'.$i;
            // $cabinet_appliction_user[$i]['c_no']=1;
            $cabinet_appliction_user[$i]['c_no']=$cabinet_appliction_id;
            $cabinet_appliction_user[$i]['name']=$request->$name;
            $cabinet_appliction_user[$i]['name_number']=$request->$name_number;
            $cabinet_appliction_user[$i]['address']=$request->$name_household_registration;
            $cabinet_appliction_user[$i]['gender']=$request->$gender;
            $cabinet_appliction_user[$i]['identity']=$request->$identity;
            $cabinet_appliction_user[$i]['discount']=$request->$discount;
            $cabinet_appliction_user[$i]['dead_date']=$request->$dead_date;
            $cabinet_appliction_user[$i]['dead_reason']=$request->$dead_reason;
            $cabinet_appliction_user[$i]['dead_location']=$request->$dead_location;
            $cabinet_appliction_user[$i]['birthday']=$request->$birthday;
            $cabinet_appliction_user[$i]['expected_date']=$request->$expected_date;
            $cabinet_appliction_user[$i]['expected_time']=$request->$expected_time;
        }
        DB::table('cabinet_appliction_user')->insert($cabinet_appliction_user);
       $cabinet_appliction_data=array();
        for ($i=1; $i <=$qty ; $i++) { 
            $old_cabinet_class_seat='old_cabinet_class_data'.$i;
            $cabinet_code='cabinet_code'.$i;
            $identity='identity'.$i;
            $cabinet_type='cabinet_type'.$i;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_layer='cabinet_class_layer'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_seat='cabinet_class_seat'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $dead_date='dead_date'.$i;
            $dead_reason='dead_reason'.$i;
            $dead_location='dead_location'.$i;
            $birthday='birthday'.$i;
            $expected_date='expected_date'.$i;
            $cabinet_appliction_user[$i]['c_no']=1;

            // $other_cost=db::Table($this->Table8)->first();
            // $cost_data=explode('-', $request->$cabinet_code);
            $cabinet_appliction_data[$i]['c_no']=$cabinet_appliction_id;
            // $cabinet_appliction_data[$i]['c_no']=1;
            $cabinet_appliction_data[$i]['cabinet_type']=$request->cabinet_type;
            $cabinet_appliction_data[$i]['cabinet_class_floor']=1;
            $cabinet_appliction_data[$i]['cabinet_class_aera']=$request->$cabinet_class_aera;
            $cabinet_appliction_data[$i]['cabinet_class_layer']=$request->$cabinet_class_layer;
            $cabinet_appliction_data[$i]['cabinet_class_row']=$request->$cabinet_class_row;
            $cabinet_appliction_data[$i]['cabinet_class_seat']=$request->$cabinet_class_seat;
            $cabinet_appliction_data[$i]['cabinet_class_position']=$request->$cabinet_class_position;
            $cabinet_appliction_data[$i]['cabinet_code']=$request->$cabinet_code;
            $cabinet_appliction_data[$i]['cost']=$request->cost;
            // $cabinet_appliction_data[$i]['other_cost']=$other_cost->other_cost;
            // $cabinet_appliction_data[$i]['amount_due']=$cabinet_appliction_data[$i]['cost']+$other_cost->other_cost;
        }
        DB::Table('cabinet_appliction_data')->insert($cabinet_appliction_data);
        $cabinet_principal=array();
        $cabinet_principal['c_no']=$cabinet_appliction_id;
        $cabinet_principal['client']=$request->client;
        $cabinet_principal['client_number']=$request->client_number;
        $cabinet_principal['client_household_registration']=$request->client_household_registration;
        $cabinet_principal['client_tel']=$request->client_tel;
        $cabinet_principal['client_phone']=$request->client_phone;
        $cabinet_principal['client_address']=$request->client_address;
        DB::table('cabinet_principal')->insert($cabinet_principal);

        db::Table('cabinet_appliction')->where('id',$request->cabinet_appliction_id)->update(['status'=>$old_cabinet_appliction->status+99]);
        for($i=1;$i<=$qty;$i++){
            $cabinet_code='cabinet_code'.$i;
            $name_number='name_number'.$i;
            $cabinet_class_floor=1;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $expected_date='expected_date'.$i;
            $dead_date='dead_date'.$i;
            $name='name'.$i;
            $identity='identity'.$i;
            Exist('cabinet_sys',
                    $request->$cabinet_code,
                    '700',
                    $request->$name_number,
                    '',
                    $cabinet_appliction_id
            );
            //類別(櫃位或牌位),樓,區域,排,方位,編碼,申請號碼,申請人身分證字號,死者身分證字號,櫃位類型(一般,長生),申請時間,進塔時間,死亡時間,狀態
            ExistLog('櫃位',1,$request->$cabinet_class_aera,$request->$cabinet_class_row,$request->$cabinet_class_position,$request->$cabinet_code,$request->no,$request->applicant_number,$request->$name_number,$request->application_type,$request->application_date,$request->application_date,$request->$dead_date,$request->status,$request->applicant,$request->$name,$request->tel,$request->phone,$request->$identity);
        }
        no_insert($request->no);
        return redirect()->to($this->RouteName)->with('status','新增成功');
    }
    public function edit($id)
    {   
        $LayoutTitle='';
        //要edit的路由 配合route resource
        $Url = $this->RouteName."/".$id;
        //表格簡介
        $LayoutBody = "";
        //app/Support/Helpers底下的自訂function
        $FormTtile = FormTtile($LayoutTitle, $LayoutTitle, $LayoutBody);

        $cabinet_sys=DB::Table('cabinet_sys')->where('status',1)->get();

        $cabinet_appliction=db::table('cabinet_appliction')->find($id);

        $users=db::Table('users')->find($cabinet_appliction->users);

        $confirm_payment_user=db::table('users')->find($cabinet_appliction->confirm_payment_user);

        $cabinet_appliction_user=db::Table('cabinet_appliction_user')->where('c_no',$id)->get();

        $cabinet_appliction_data=db::Table('cabinet_appliction_data')->where('c_no',$id)->get();

        $users=db::Table('users')->find($cabinet_appliction->users);
        
        $cabinet_principal=db::Table('cabinet_principal')->where('c_no',$id)->first();
        if($cabinet_appliction->status=='700'){
            $LayoutTitle = "櫃位遷出出塔作業";
        }
        //退貨總額
        $data['total']=0;
        foreach($cabinet_appliction_data as $key =>$value){
            $data['cabinet_code'][]=DB::Table('cabinet_sys')->where('code',$value->cabinet_code)->first();
            $data['area'][]=db::Table('cabinet_sys_class')->where('id',$value->cabinet_class_aera)->whereNull('deleted_at')->first();
            $data['layer'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_layer)->whereNull('deleted_at')->first();
            $data['row'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_row)->whereNull('deleted_at')->first();
            $data['seat'][]=db::table('cabinet_sys_class')->where('id',$value->cabinet_class_seat)->whereNull('deleted_at')->first();
            $data['total']+=$value->cost;
        }

        $data['url']=$Url;
        $data['cabinet_appliction']=$cabinet_appliction;
        $data['cabinet_appliction_user']=$cabinet_appliction_user;
        $data['cabinet_appliction_data']=$cabinet_appliction_data;
        $data['cabinet_principal']=$cabinet_principal;
        $data['cabinet_sys']=$cabinet_sys;
        $data['users']=$users;
        $data['confirm_payment_user']=$confirm_payment_user;
        $position=array(1=>'座北朝南',2=>'座南朝北',3=>'座東朝西',4=>'座西朝東');
        $data['position']=$position;
        $data['id']=$id;
        $data['FormTtile']=$FormTtile;
        switch (count($cabinet_appliction_user)) {
                case 1: $type='個人';  break;
                case 2: $type='夫妻';  break;
                case 12: $type='家族';  break;
                default:  break;
            }
        $data['type']=$type;

        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        
        //app/Support/Helpers底下的自訂function
        // $FormBody = FormBody($BodyArray, $Url,'','PATCH');
        //統一使用admin.Form.index，除非特殊狀況
        return view('admin.Form.cabinet_out_appliction_edit', $data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {   
        switch ($request->cabinet_type) {
            case '個人': $qty=1;break;
            case '夫妻': $qty=2;break;
            case '家族': $qty=12;break;
            default: return redirect()->back();  break;
        }
        $required='';
        $cabinet_appliction=array();
        $cabinet_appliction['status']=$request->status;
        $cabinet_appliction['principal']=$request->principal;
        $cabinet_appliction['change_date']=$request->change_date;

        db::Table('cabinet_appliction')->where('id',$id)->update($cabinet_appliction);
        for($i=1;$i<=$qty;$i++){
            $cabinet_code='cabinet_code'.$i;
            $name_number='name_number'.$i;
            $cabinet_class_floor=1;
            $cabinet_class_aera='cabinet_class_aera'.$i;
            $cabinet_class_row='cabinet_class_row'.$i;
            $cabinet_class_position='cabinet_class_position'.$i;
            $expected_date='expected_date'.$i;
            $dead_date='dead_date'.$i;
            $name='name'.$i;
            $identity='identity'.$i;
            Exist('cabinet_sys',
                $request->$cabinet_code,
                $request->status,
                $request->$name_number,
                '',
                $id
            );
            //類別(櫃位或牌位),樓,區域,排,方位,編碼,申請號碼,申請人身分證字號,死者身分證字號,櫃位類型(一般,長生),申請時間,進塔時間,死亡時間,狀態
            if($request->status==800){
                ExistLog('櫃位',1,$request->$cabinet_class_aera,$request->$cabinet_class_row,$request->$cabinet_class_position,$request->$cabinet_code,$request->no,$request->applicant_number,$request->$name_number,$request->application_type,$request->application_date,$request->change_date,$request->$dead_date,$request->status,$request->applicant,$request->$name,$request->tel,$request->phone,$request->$identity);
            }
            if($request->status==700){
                ExistLog('櫃位',1,$request->$cabinet_class_aera,$request->$cabinet_class_row,$request->$cabinet_class_position,$request->$cabinet_code,$request->no,$request->applicant_number,$request->$name_number,$request->application_type,$request->application_date,$request->$expected_date,$request->$dead_date,$request->status,$request->applicant,$request->$name,$request->tel,$request->phone,$request->$identity);
            }
        }
        return redirect()->to($this->RouteName)->with('status','修改成功');
        // $cabinet_appliction['cabinet_appliction_type']=$request->cabinet_appliction_type;
        // $cabinet_appliction['no']=$request->no;
        // $cabinet_appliction['application_date']=$request->application_date;
        // $cabinet_appliction['status']=$request->status;
        // $cabinet_appliction['application_date']=$request->application_date;
        // $cabinet_appliction['applicant']=$request->applicant;
        // $cabinet_appliction['household_registration']=$request->household_registration;
        // $cabinet_appliction['applicant_number']=$request->applicant_number;
        // $cabinet_appliction['tel']=$request->tel;
        // $cabinet_appliction['phone']=$request->phone;
        // $cabinet_appliction['relationship']=$request->relationship;
        // $cabinet_appliction['address']=$request->address;
        // $cabinet_appliction['ext']=$request->ext;

        // $cabinet_appliction_user=array();
        // $cabinet_appliction_user['c_no']=$cabinet_appliction_id;
        // $cabinet_appliction_user['name']=$request->name;
        // $cabinet_appliction_user['name_number']=$request->name_number;
        // $cabinet_appliction_user['address']=$request->address;
        // $cabinet_appliction_user['gender']=$request->gender;
        // $cabinet_appliction_user['identity']=$request->identity;
        // $cabinet_appliction_user['discount']=$request->discount;
        // $cabinet_appliction_user['dead_date']=$request->dead_date;
        // $cabinet_appliction_user['dead_reason']=$request->dead_reason;
        // $cabinet_appliction_user['dead_location']=$request->dead_location;
        // $cabinet_appliction_user['birthday']=$request->birthday;
        // $cabinet_appliction_user['expected_date']=$request->expected_date;
        // DB::table('cabinet_appliction_user')->where('id',$request->id)->update($cabinet_appliction_user);
        
        // $cabinet_appliction_data=array();
        // $cabinet_appliction_data['c_no']=$cabinet_appliction_id;
        // $cabinet_appliction_data['cabinet_type']=$request->cabinet_type;
        // $cabinet_appliction_data['cabinet_class_floor']=$request->cabinet_class_floor;
        // $cabinet_appliction_data['cabinet_class_aera']=$request->cabinet_class_aera;
        // $cabinet_appliction_data['cabinet_class_row']=$request->cabinet_class_row;
        // $cabinet_appliction_data['cabinet_class_layer']=$request->cabinet_class_layer;
        //$cabinet_appliction_data['cabinet_class_seat']=$request->cabinet_class_seat;
        // $cabinet_appliction_data['cost']=$request->cost;
        // $cabinet_appliction_data['other_cost']=$request->other_cost;
        // $cabinet_appliction_data['amount_due']=$request->amount_due;
        // DB::Table('cabinet_appliction_data')->where('id',$request->cabinet_appliction_data_id)->update($cabinet_appliction_data);
        // $cabinet_principal=array();
        // $cabinet_principal['c_no']=$cabinet_appliction_id;
        // $cabinet_principal['client']=$request->client;
        // $cabinet_principal['client_number']=$request->client_number;
        // $cabinet_principal['client_household_registration']=$request->client_household_registration;
        // $cabinet_principal['client_tel']=$request->client_tel;
        // $cabinet_principal['client_phone']=$request->client_phone;
        // $cabinet_principal['client_address']=$request->client_address;
        // DB::table('cabinet_principal')->where('id',$request->cabinet_principal_id)->update($cabinet_principal);
        // 
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //use model
        $ModelName=$this->ModelName;
        $Model = $ModelName::withTrashed()->where('id',$id)->first();
        if ($Model->trashed()) {
            $Model->restore();
             return response()->json(0);
        }else{
            $Model=$ModelName::withTrashed()->where('id',$id)->delete();
            return response()->json(1);
        }
       
    }
    public function get_no()
    {
        $select_applicant_number=Input::get('select_applicant_number');
        $cabinet_appliction=db::Table('cabinet_appliction')
                ->select('id','no')
                ->where('applicant_number',$select_applicant_number)
                // ->where('application_type',1)
                ->whereIn('status',[300,600])
                ->get();
        return json_encode($cabinet_appliction);
    }
}