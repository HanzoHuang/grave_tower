<?php

namespace App\Http\Controllers\admin\CabinetCost;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\CabinetCostDiscountRequest;
use Illuminate\Http\Request;
use DB;
use Input;
use Session;
class BackCabinetCostDiscountController extends BackController {
    //配合route resource
    protected $RouteName="BackCabinetCostDiscount";
    //檔案路徑
    protected $Path="Upload/CabinetCost/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\CabinetCost\CabinetCostDiscount";
    //主要table
    protected $Table1="cabinet_cost_discount";
    //關聯table
    protected $Table2="roles";
    protected $Table3="cabinet_sys_class";
    public function index()
    {
        $area=db::table($this->Table3)->whereNull('top_class')->get();
        $layer=db::Table($this->Table3)->where('top_class',$area[0]->id)->get();
        $layer_array=array();
        foreach ($layer as $key => $value) {
            $layer_array[]=$value->id;
        }
        $row=db::Table($this->Table3)->groupBy('code')->whereIn('top_class',$layer_array)->get();
        $ModelName=$this->ModelName;
        $cost_data=$ModelName::where('area',$area[0]->code)->where('row',$row[0]->code)->get();
        $cost=array();
        foreach($cost_data as $value){
            $cost[$value->type]=$value;
        }
        return view('admin.Form.cost', ['area'=>$area,'row'=>$row,'cost'=>$cost]);
    }
    public function get_row()
    {
        $area_code=Input::get('area');
        $area=db::Table($this->Table3)->where('code',$area_code)->whereNull('top_class')->first();
        $layer=db::Table($this->Table3)->where('top_class',$area->id)->get();
        $layer_array=array();
        foreach ($layer as $key => $value) {
            $layer_array[]=$value->id;
        }
        $row=db::Table($this->Table3)
        ->groupBy('code')
        ->whereIn('top_class',$layer_array)
        ->orderBy('id','asc')
        ->get();
        $cabinet_cost_discount=db::table('cabinet_cost_discount')->where('type',Input::get('type'))->where('area',$area_code)->where('row',$row[0]->code)->first();
        $data=array();
        if(count($row)>0){
            $data['row']=$row;
        }else{
            $data['row']=0;
        }
        if($cabinet_cost_discount){
            $data['discount']=$cabinet_cost_discount;
        }else{
            $data['discount']=0;
        }
        return json_encode($data);
        // return view('admin.Form.cost_discount_get_row',['row'=>$row]);
    }
    public function get_cost($value='')
    {
        $type=Input::get('no');
        $area_code=Input::get('area');
        $row_code=Input::get('row');
        $area=db::Table($this->Table3)->where('code',$area_code)->whereNull('top_class')->first()->code;
        $row=db::Table($this->Table3)->where('code',$row_code)->first()->code;
        $ModelName=$this->ModelName;
        $cost_data=$ModelName::where('area',$area)->where('row',$row)->where('type',$type)->first();
        return json_encode($cost_data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $ModelName=$this->ModelName;
        $type=$request->type;
        $data=[
            'type'=>$type,
            'area'=>Input::get('area_'.$type),
            'row'=>Input::get('row_'.$type),
            'price'=>Input::get('price_'.$type),
            'discount1'=>Input::get('discount1_'.$type),
            'discount2'=>Input::get('discount2_'.$type),
            'discount3'=>Input::get('discount3_'.$type),
            'discount4'=>Input::get('discount4_'.$type),
            'discount5'=>Input::get('discount5_'.$type)
            ];
        $old=$ModelName::where(['type'=>$type,'area'=>Input::get('area_'.$type),'row'=>Input::get('row_'.$type)])->first();
        if($old){
            $ModelName::where('id',$old->id)->update($data);
        }else{
            $ModelName::create($data);
        }
        return redirect()->to($this->RouteName.'?type='.$type.'&area='.Input::get('area_'.$type).'&row='.Input::get('row_'.$type))->with('status','修改成功');
    }
}