<?php

namespace App\Http\Controllers\admin\CabinetCost;
use App\Http\Controllers\BackController;
use App\Http\Requests\UserRequest;
use App\Http\Requests\CabinetOtherCostRequest;
use DB;
use Input;
use Session;
class BackCabinetOtherCostController extends BackController {
    //配合route resource
    protected $RouteName="BackCabinetOtherCost";
    //檔案路徑
    protected $Path="Upload/CabinetOtherCost/";
    //將use改寫在這裡，後面直接代入
    protected $ModelName="App\Models\CabinetCost\CabinetOtherCost";

    //主要table
    protected $Table1="cabinet_other_cost";
    //關聯table
    protected $Table2="roles";
    public function index()
    {
        $Data=DB::table($this->Table1)
        // ->select('id','change_cost','other_cost','family_change_cost')
        ->first();
        return view('admin.Form.cost_other', ['Data' => $Data]);
    }
    public function store(CabinetOtherCostRequest $request)
    {   
        $ModelName=$this->ModelName;
        $data=$ModelName::firstOrCreate ([
            'id'=>$request->id,
            ]);
        $data->other_cost=$request->other_cost; 
        $data->change_cost=$request->change_cost;
        $data->family_change_cost=$request->family_change_cost;
        $data->save();
        return redirect()->to($this->RouteName)->with('status','修改成功');
    }
}