<?php

namespace App\Http\Controllers\Reception\reception;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Mail;
use UploadHandler;
use DB;
use Excel;
class ReceptionController extends Controller
{
    protected $ModelName="App\User";
    protected $Table1="banner";
    //主要table
    protected $Table2="announcement_information";
    //關聯table
    protected $Table3="announcement_information_class";
    private $pdf;
    
    public function index()
    {
       
    }
    // Banenr
    public function Banner()
    {
        $banner=DB::table($this->Table1)->whereNull('deleted_at')->get();
        $announcement_information=DB::table($this->Table2)->select('title','announcement_information.file1','body','name')->whereNull($this->Table2.'.deleted_at')->whereNull($this->Table3.'.deleted_at')->join($this->Table3,$this->Table2.'.class_id','=',$this->Table3.'.id')->get();
        return view('admin.Banner.banner', ['banner'=>$banner,'announcement_information'=>$announcement_information]);
    }
}