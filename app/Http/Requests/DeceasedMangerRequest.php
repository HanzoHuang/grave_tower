<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DeceasedMangerRequest  extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    return [
	        'name' => 'required',
	        'name_number' => 'required',
	        'address' => 'required',
	        'gender' => 'required',
	        'identity' => 'required',
	        'dead_date' => 'required',
	        'dead_reason' => 'required',
	        'dead_location' => 'required',
	        'birthday' => 'required',
	        // 'cabinet_id' => 'required',
	    ];
    }	
    public function messages()
	{
	    return [
	        'name.required'  => '姓名是必填的',
	        'number.required'  => '身分證字號是必填的',
	        'name_number.unique'  => '此身分證字號已經存在',
	        'address.required'  => '戶籍是必填的',
	        'gender.required'  => '性別是必填的',
	        'identity.required'  => '身分是必填的',
	        'dead_date.required'  => '死亡日期是必填的',
	        'dead_reason.required'  => '死亡原因是必填的',
	        'dead_location.required'  => '死亡地點是必填的',
	        'birthday.required'  => '生日是必填的',
	        // 'cabinet_id.required'  => '內容是必填的',

	    ];
	}


}
