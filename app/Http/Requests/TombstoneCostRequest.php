<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TombstoneCostRequest  extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    return [
	        'cost_1' => 'required',
	        'optional_cost_1' => 'required',
	        'cost_2' => 'required',
	        'optional_cost_2' => 'required',
	        'cost_3' => 'required',
	        'optional_cost_3' => 'required'
	    ];
    }	
    public function messages()
	{
	    return [
	        'cost_1.required'  => '價格是必填的',
	        'optional_cost_1.required'  => '自選價格是必填的',
	        'cost_2.required'  => '價格是必填的',
	        'optional_cost_2.required'  => '自選價格是必填的',
	        'cost_3.required'  => '價格是必填的',
	        'optional_cost_3.required'  => '自選價格是必填的',

	    ];
	}
//          'file1' => 'sometimes|mimes:jpeg,bmp,png,gif,jpg|max:1024',
//	        'img.image'		 =>'驗證欄位檔案必須為圖片格式（ jpeg、png、bmp、gif、 或 svg ）',


}
