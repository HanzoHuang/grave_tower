<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CabinetCostDiscountRequest  extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    return [
	        'price_1' => 'required',
	        'price_2' => 'required',
	        'price_3' => 'required',
	    ];
    }	
    public function messages()
	{
	    return [
	        'price_1.required'  => '本鄉鄉民價格是必填的',
	        'price_2.required'  => '本縣縣民價格是必填的',
			'price_3.required'  => '外縣市民價格是必填的',


	    ];
	}
//          'file1' => 'sometimes|mimes:jpeg,bmp,png,gif,jpg|max:1024',
//	        'img.image'		 =>'驗證欄位檔案必須為圖片格式（ jpeg、png、bmp、gif、 或 svg ）',


}
