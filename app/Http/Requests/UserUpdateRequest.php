<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserUpdateRequest   extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    return [
	        'name' => 'required',
	        'account' => 'required|',
	        'password' => '',
	        'password_confirmation' =>'same:password',
	        'email' => 'required|email',
	    ];
    }	
    public function messages()
	{
	    return [
	        'name.required'  => '姓名是必填的',
	        'account.required'  => '帳號是必填的',
	        'password.required'  => '密碼是必填的',
	        'password_confirmation.required'  => '密碼確認是必填的',
	        'password.confirmed'  => '請重新確認密碼',
	        'email.required'  => '信箱是必填的',
	        'email.email'  => 'email格式錯誤',
	    ];
	}


}
