<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CabinetOtherCostRequest  extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    return [
	        'change_cost' => 'required',
	        'other_cost' => 'required'
	    ];
    }	
    public function messages()
	{
	    return [
	        'change_cost.required'  => '換位費用是必填的',
	        'other_cost.required'  => '其他費用是必填的',

	    ];
	}
//          'file1' => 'sometimes|mimes:jpeg,bmp,png,gif,jpg|max:1024',
//	        'img.image'		 =>'驗證欄位檔案必須為圖片格式（ jpeg、png、bmp、gif、 或 svg ）',


}
