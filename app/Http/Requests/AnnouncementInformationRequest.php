<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AnnouncementInformationRequest   extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    return [
	        'title' => 'required',
	        'body' => 'required',
	    ];
    }	
    public function messages()
	{
	    return [
	        'title.required'  => '標題是必填的',
	        'body.required'  => '內文是必填的',

	    ];
	}
//          'file1' => 'sometimes|mimes:jpeg,bmp,png,gif,jpg|max:1024',
//	        'img.image'		 =>'驗證欄位檔案必須為圖片格式（ jpeg、png、bmp、gif、 或 svg ）',


}
