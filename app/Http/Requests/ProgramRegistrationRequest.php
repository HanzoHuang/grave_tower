<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input;

class ProgramRegistrationRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		
	    return [
	    	'name'=>'required',
	    	'program_name'=>'required',
	    	'sort'=>'required'
	    ];
    }	
    public function messages()
	{
	    return [
	        'name.required'  => '名稱是必填的',
	        'program_name.required'  => '程式名稱是必填的',
	        'sort.required'  => '排序是必填的',
	    ];
	}


}
