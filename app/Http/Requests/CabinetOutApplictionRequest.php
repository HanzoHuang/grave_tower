<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CabinetOutApplictionRequest   extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    return [
	    	'tel' => 'required|regex:/^[0][1-9]{1,3}[0-9]{6,8}$/',
	        'phone' => 'sometimes:regex:/^[0][1-9]{1,3}[0-9]{6,8}$/',
	        'client_tel' => 'regex:/^[0][1-9]{1,3}[0-9]{6,8}$/',
	        'client_phone' => 'sometimes:regex:/^[0][1-9]{1,3}[0-9]{6,8}$/',
			//'no' => 'unique:cabinet_appliction',
	        // 'cabinet_appliction_type' => 'required',
	        // 'application_type' => 'required',
	        // 'application_date' => 'required',
	        // 'applicant' => 'required',
	        // 'applicant_number' => 'required',
	        // 'household_registration' => 'required',
	        // 'tel' => 'required',
	        // 'phone' => 'required',
	        // 'relationship' => 'required',
	        // 'name' => 'required',
	        // 'address' => 'required',
	        // 'name_number' => 'required',
	        // 'name_household_registration' => 'required',
	        // 'gender' => 'required',
	        // 'identity' => 'required',
	        // 'discount' => 'required',
	        // 'dead_date' => 'required',
	        // 'dead_reason' => 'required',
	        // 'dead_location' => 'required',
	        // 'birthday' => 'required',
	        // 'expected_date' => 'required',
	        // 'cabinet_type' => 'required',
	        // 'cabinet_class_position' => 'required',
	        // 'cabinet_class_aera' => 'required',
	        // 'cabinet_class_row' => 'required',
	        // 'cabinet_code' => 'required',
	        // 'client' => 'required',
	        // 'client_number' => 'required',
	        // 'client_household_registration' => 'required',
	        // 'client_tel' => 'required',
	        // 'client_phone' => 'required',
	        // 'client_address' => 'required',
	        'file1' => 'sometimes:mimes:doc,docx,dot,pdf,xls,xlm,xla,xlc,xlt,xlw|max:2048',
	        // 'file4' => 'required|mimes:jpeg,bmp,png,gif,jpg|max:2048',
	        // 'file5' => 'required|mimes:jpeg,bmp,png,gif,jpg|max:2048',
	        'file8' => 'sometimes:mimes:doc,docx,dot,pdf,xls,xlm,xla,xlc,xlt,xlw|max:2048',
	        'file9' => 'sometimes:mimes:doc,docx,dot,pdf,xls,xlm,xla,xlc,xlt,xlw|max:2048'
	    ];
    }	
    public function messages()
	{
	    return [
	    	'tel.regex' => '申請人電話不符',
	        'phone.regex' => '申請人手機不符',
	    	'client_tel.regex' => '委託人電話不符',
	        'client_phone.regex' => '委託人手機不符',
	        //'no.unique' => '表單編號重複',
	        // 'cabinet_appliction_type.required' => '1',
	        // 'application_type.required' => '2',
	        // 'application_date.required' => '請填申請日期',
	        // 'applicant.required' => '請填申請人',
	        // 'applicant_number.required' => '請填申請人身分證',
	        // 'household_registration.required' => '請填申請人戶籍',
	        // 'tel.required' => '請填申請人電話',
	        // 'phone.required' => '請填申請人行動電話',
	        // 'relationship.required' => '請填關係',
	        // 'address.required' => '請填地址',
	        // 'name.required' => '請填逝者姓名',
	        // 'name_number.required' => '請填逝者身分證',
	        // 'name_household_registration.required' => '請填逝者戶籍',
	        // 'gender.required' => '3',
	        // 'identity.required' => '4',
	        // 'discount.required' => '5',
	        // 'dead_date.required' => '請填逝者死亡日期',
	        // 'dead_reason.required' => '請填逝者死亡原因',
	        // 'dead_location.required' => '請填逝者死亡地點',
	        // 'birthday.required' => '請填逝者生日',
	        // 'expected_date.required' => '請填預計遷出日期', 
	        // 'cabinet_class_position.required' => '6',
	        // 'cabinet_class_aera.required' => '8',
	        // 'cabinet_class_row.required' => '9',
	        // 'cabinet_code.required' => '請搜尋空位並選擇',
	        // 'client.required' => '請填委託人姓名',
	        // 'client_number.required' => '請填委託人身分證',
	        // 'client_household_registration.required' => '請填委託人戶籍',
	        // 'client_tel.required' => '請填委託人電話',
	        // 'client_phone.required' => '請填委託人行動電話',
	        // 'client_address.required' => '請填委託人行動地址',
	        // 'file1.required' => '請上傳申請人身分證',
	        'file1.mimes' => '申請人身分證必須是doc,docx,dot,pdf,xls,xlm,xla,xlc,xlt,xlw檔,並且小於2048KB(2MB)',
	        // 'file8.required' => '請上傳切結書',
	        'file8.mimes' => '切結書必須是doc,docx,dot,pdf,xls,xlm,xla,xlc,xlt,xlw檔,並且小於2048KB(2MB)',
	        // 'file9.required' => '請上傳申請書',
	        'file9.mimes' => '申請書必須是doc,docx,dot,pdf,xls,xlm,xla,xlc,xlt,xlw檔,並且小於2048KB(2MB)',
	        // 'file4.required' => '請上傳戶籍謄本',
	        // 'file4.mimes' => '戶籍謄本必須是jpeg,bmp,png,gif,jpg檔,並且小於2048KB(2MB)',
	        // 'file5.required' => '請上傳亡者除戶謄本',
	        // 'file5.mimes' => '亡者除戶謄本必須是jpeg,bmp,png,gif,jpg檔,並且小於2048KB(2MB)',
	    ];
	}
//          'file1' => 'sometimes|mimes:jpeg,bmp,png,gif,jpg|max:1024',
//	        'img.image'		 =>'驗證欄位檔案必須為圖片格式（ jpeg、png、bmp、gif、 或 svg ）',


}
