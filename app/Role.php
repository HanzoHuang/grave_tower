<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model {
	use SoftDeletes;
	protected $table = 'roles';
	protected $fillable = ['name', 'sort', 'description'];
	protected $dates = ['deleted_at'];
	public function users() {
		return $this->hasMany('App\User', 'role_id', 'id');
	}
	public function RoleControls() {
		return $this->hasOne('App\RoleControls', 'foreign_key');
	}
}
?>