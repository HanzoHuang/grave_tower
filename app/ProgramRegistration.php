<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgramRegistration extends Model
{
    use SoftDeletes;
    protected $table    = 'program_registration';
    protected $fillable = ['name', 'sort', 'menu_class_id', 'program_name','menu','top_program_id'];
    protected $dates    = ['deleted_at'];
    public function MenuClass()
    {
        return $this->belongsTo('App\MenuClass');
    }
    public function RoleControls()
    {
        return $this->hasOne('App\RoleControls', 'foreign_key');
    }
}
