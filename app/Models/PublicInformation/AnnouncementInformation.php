<?php

namespace App\Models\PublicInformation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnnouncementInformation extends Model {
	use SoftDeletes;
	protected $table = 'announcement_information';
	protected $fillable  = ['class_id','title','body','file1'];

}
