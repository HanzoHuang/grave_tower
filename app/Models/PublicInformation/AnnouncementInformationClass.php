<?php

namespace App\Models\PublicInformation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnnouncementInformationClass extends Model {
	use SoftDeletes;
	protected $table = 'announcement_information_class';
	protected $fillable  = ['name'];

}
