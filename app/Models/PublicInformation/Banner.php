<?php

namespace App\Models\PublicInformation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model {
	use SoftDeletes;
	protected $table = 'banner';
	protected $fillable  = ['title','file1','ext'];

}
