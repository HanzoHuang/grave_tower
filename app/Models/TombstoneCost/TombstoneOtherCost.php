<?php

namespace App\Models\TombstoneCost;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TombstoneOtherCost extends Model {
	use SoftDeletes;
	protected $table = 'tombstone_other_cost';
	protected $fillable  = ['name','class','cost'];

}
