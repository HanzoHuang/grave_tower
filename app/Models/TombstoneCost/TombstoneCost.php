<?php

namespace App\Models\TombstoneCost;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TombstoneCost extends Model {
	use SoftDeletes;
	protected $table = 'tombstone_cost';
	protected $fillable  = ['name','type','cost','optional_cost'];

}
