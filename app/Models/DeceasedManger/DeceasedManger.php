<?php

namespace App\Models\DeceasedManger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeceasedManger extends Model {
	use SoftDeletes;
	protected $table = 'cabinet_appliction_user';
	protected $fillable  = ['name','mame_number','address','gender','type','dead_date','dead_reason','dead_location','birthday','cabinet_id','registration_date'];

}
