<?php

namespace App\Models\TombstoneSys;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TombstoneSys extends Model {
	use SoftDeletes;
	protected $table = 'tombstone_sys';
	protected $fillable  = ['museum','class','floor','row','layer','code','status','position'];

}
