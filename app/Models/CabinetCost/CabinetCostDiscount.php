<?php

namespace App\Models\CabinetCost;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CabinetCostDiscount extends Model {
	use SoftDeletes;
	protected $table = 'cabinet_cost_discount';
	protected $fillable  = ['type','name','class','area','row','price','discount1','discount2','discount3','discount4'];

}
