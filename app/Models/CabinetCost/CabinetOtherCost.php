<?php

namespace App\Models\CabinetCost;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CabinetOtherCost extends Model {
	use SoftDeletes;
	protected $table = 'cabinet_other_cost';
	protected $fillable  = ['change_cost','cost'];

}
