<?php

namespace App\Models\CabinetSys;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CabinetSys extends Model {
	use SoftDeletes;
	protected $table = 'cabinet_sys';
	protected $fillable  = ['museum','class','floor','row','layer','code','status','position','number'];

}
