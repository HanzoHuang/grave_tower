@extends('layouts.admin.master')
@section('title','後臺帳號管理')
@section('LevelOne',$LevelOne)
@section('LevelTwo',$LevelTwo)
@section('content')
{!!	Html::style('Plugin/EasyUI/easyui.css')  !!}
{!!	Html::script('Plugin/EasyUI/jquery.easyui.min.js')  !!}
<table id="dg" title="帳號管理" class="easyui-datagrid" style="width:800px;height:500px" method="GET"
            url="GetUserData"
            toolbar="#toolbar" pagination="true"
            rownumbers="true" singleSelect="true">
        <thead>
            <tr>
                <th field="id" width="20" hidden=true>ID</th>
                <th field="name" width="80">姓名</th>
                <th field="account" width="120">帳號</th>
                <th field="tel" width="120">電話</th>
                <th field="mobile" width="120">手機</th>
                <th field="email" width="300">Email</th>
            </tr>
        </thead>
    </table>
    <div id="toolbar">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">新增帳號</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">編輯帳號</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">移除帳號</a>
    </div>
    
    <!-- S:新增 dlg -->
    <div id="dlg" class="easyui-dialog" style="width:400px;height:400px;padding:10px 20px"
            closed="true" buttons="#dlg-buttons">
        <div class="ftitle">User Information</div>
        <form id="fm" method="post" novalidate>
            <input type="hidden" name="_token" id="_token" />
            <div class="fitem">
                <label>姓名:</label>
                <input name="name" class="easyui-textbox" required="true">
            </div>
            <div class="fitem">
                <label>帳號:</label>
                <input name="account" class="easyui-textbox" required="true">
            </div>
            <div class="fitem">
                <label>電話:</label>
                <input name="tel" class="easyui-textbox">
            </div>
            <div class="fitem">
                <label>手機:</label>
                <input name="mobile" class="easyui-textbox">
            </div>
            <div class="fitem">
                <label>email:</label>
                <input name="email" class="easyui-textbox" required="true">
            </div>
            <div class="fitem">
                <label>密碼:</label>
                <input name="password" class="easyui-textbox" required="true">
            </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
    </div>
    <!-- E:新增 dlg -->



    <!-- S:編輯 dlg -->
    <div id="dlg_edit" class="easyui-dialog" style="width:400px;height:300px;padding:10px 20px"
            closed="true" buttons="#dlg-edit-buttons">
        <div class="ftitle">User Information</div>
        <form id="fm_edit" method="post" novalidate>
            <input type="hidden" name="_token" id="_token_edit" />
            <input type="hidden" name="id" />
            <div class="fitem">
                <label>姓名:</label>
                <input name="name" class="easyui-textbox" required="true">
            </div>
            <div class="fitem">
                <label>帳號:</label>
                <input name="account" class="easyui-textbox" required="true">
            </div>
            <div class="fitem">
                <label>電話:</label>
                <input name="tel" class="easyui-textbox">
            </div>
            <div class="fitem">
                <label>手機:</label>
                <input name="mobile" class="easyui-textbox">
            </div>
            <div class="fitem">
                <label>email:</label>
                <input name="email" class="easyui-textbox" required="true">
            </div>
        </form>
    </div>
    <div id="dlg-edit-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="updateUser()" style="width:90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg_edit').dialog('close')" style="width:90px">Cancel</a>
    </div>
    <!-- E:編輯 dlg -->


    <script type="text/javascript">
        var url;
        function newUser(){
            $('#dlg').dialog('open').dialog('center').dialog('setTitle','New User');
            $('#fm').form('clear');
            $('#_token').val('{{ Session::token() }}');
            url = 'SaveUserData';
        }
        function editUser(){
            var row = $('#dg').datagrid('getSelected');
            if (row){
                $('#dlg_edit').dialog('open').dialog('center').dialog('setTitle','Edit User');
                $('#fm_edit').form('load',row);
                $('#_token_edit').val('{{ Session::token() }}');
                url = 'UpdateUserData';
            }
        }
        function saveUser(){
            $('#fm').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
                        $('#dlg').dialog('close');        // close the dialog
                        $('#dg').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        function updateUser(){
            $('#fm_edit').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
                        $('#dlg_edit').dialog('close');        // close the dialog
                        $('#dg').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        function destroyUser(){
            var row = $('#dg').datagrid('getSelected');
            if (row){
                $.messager.confirm('Confirm','確定要刪除此帳號?',function(r){
                    if (r){
                        $.post('DestroyUserData',{id:row.id,_token:'{{ Session::token() }}'},function(result){
                            if (result.success){
                                $('#dg').datagrid('reload');    // reload the user data
                            } else {
                                $.messager.show({    // show error message
                                    title: 'Error',
                                    msg: result.errorMsg
                                });
                            }
                        },'json');
                    }
                });
            }
        }
    </script>
    <style type="text/css">
        #fm{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
        .fitem input{
            width:160px;
        }
    </style>
    
@stop