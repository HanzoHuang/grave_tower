<!DOCTYPE html>
<html>
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>@font-face { font-family: 'Firefly Sung'; font-style: normal; font-weight: 400; src: url(http://eclecticgeek.com/dompdf/fonts/cjk/fireflysung.ttf) format('truetype'); } * { font-family: Firefly Sung, DejaVu Sans, sans-serif; }
    	body {
	        height: 842px;
	        width: 720px;
	        /* to centre page on screen*/
	        margin-left: auto;
	        margin-right: auto;
          font-size: 20px;
    		}
    </style>
    </head>
  
  <body>
    <div>
	    <div style="float:left; width:100%;">
	      <div style="float:left;width:75%;text-align:right;">
        <b>宜蘭縣壯圍鄉生命紀念館使用規費繳款單</b></div>
	      <div style="width:25%;float:right; text-align:right;">序號： {{$data->no}}</div>
	    </div>
    </div>
    <div style="text-align: right;">
      <p>轉帳日期： {{$data->payment_date}}</p>
    </div>
    <div>
      <table border="1" cellspacing="0" cellpadding="0" align="" style="width: 100%">
        <tbody>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              項目</td>
            <td width="454" valign="top" style="text-align: center;">
             說明</td>
            <td width="85" valign="top" style="text-align: center;">
              備註</td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              繳款人姓名</td>
            <td width="454" valign="top">
              {{$data->applicant}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              使用類別</td>
            <td width="454" valign="top">
              @if($data->status==100)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位申請
              @elseif($data->status==200)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位繳費
              @elseif($data->status==300 || $data->status==350||$data->status==399 )
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位入塔
              @elseif($data->status==400)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位申請異動
              @elseif($data->status==500)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位異動繳費
              @elseif($data->status==600 || $data->status==650||$data->status==699 )
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位異動遷出
              @elseif($data->status==700)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位遷出申請
              @elseif($data->status==800)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位遷出出塔
              @endif
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              櫃位編號</td>
            <td width="454" valign="top">
              
              <?php $ii=1?>
              @foreach ($data2 as $datas2)
                @if($ii%4==0)
                    <div style="float: left;width:25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @else
                    <div style="float: left;width: 25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @endif
                @if($ii%4==0)
                    <br>
                @endif 
                <?php $ii=$ii+1?>
              @endforeach
            </td>
            <td width="85" valign="top">
              {{$data->cabinet_type}}
            </td>
          </tr>
          <tr>
            <td width="104" valign="top"  style="text-align: center;">
              繳納金額</td>
            <td width="454" valign="top">
              {{number_format($data->cost+$data->other_amount_due)}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=" float:left; width:100%;"> 
    	<div style="float:left;width:70%;">第一聯公所留存</div>
    	<div style="width:30%;float:right; text-align:right;">序號： {{$data->no}}</div>
    </div>
    <div style=" float:left; width:100%;"> 
      <div style="float:left;width:40%;">承辦人：</div>
      <div style="width:30%;float:left;">農會經辦：</div>
      <div style="width:30%;float:left;">農會主任：</div>
    </div>
                                    
     <br>
    <hr size="1" noshade="noshade" style="border:1px #cccccc dotted;"/>

    <div>
      <div style="float:left; width:100%;">
        <div style="float:left;width:75%;text-align:right;">
        <b>宜蘭縣壯圍鄉生命紀念館使用規費繳款單</b></div>
        <div style="width:25%;float:right; text-align:right;">序號： {{$data->no}}</div>
      </div>
    </div>
    <div style="text-align: right;">
      <p>轉帳日期： {{$data->payment_date}}</p>
    </div>
    <div>
      <table border="1" cellspacing="0" cellpadding="0" align="" style="width: 100%">
        <tbody>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              項目</td>
            <td width="454" valign="top" style="text-align: center;">
             說明</td>
            <td width="85" valign="top" style="text-align: center;">
              備註</td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              繳款人姓名</td>
            <td width="454" valign="top">
              {{$data->applicant}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
          
            <td width="104" valign="top" style="text-align: center;">
              使用類別</td>
            <td width="454" valign="top">
              @if($data->status==100)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位申請
              @elseif($data->status==200)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位繳費
              @elseif($data->status==300 || $data->status==350||$data->status==399 )
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位入塔
              @elseif($data->status==400)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位申請異動
              @elseif($data->status==500)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位異動繳費
              @elseif($data->status==600 || $data->status==650||$data->status==699 )
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位異動遷出
              @elseif($data->status==700)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位遷出申請
              @elseif($data->status==800)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位遷出出塔
              @endif
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              櫃位編號</td>
            <td width="454" valign="top">
             <?php $ii=1?>
              @foreach ($data2 as $datas2)
                @if($ii%4==0)
                    <div style="float: left;width:25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @else
                    <div style="float: left;width: 25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @endif
                @if($ii%4==0)
                    <br>
                @endif 
                <?php $ii=$ii+1?>
              @endforeach
            </td>
            <td width="85" valign="top">
              {{$data->cabinet_type}}
            </td>
          </tr>
          <tr>
            <td width="104" valign="top"  style="text-align: center;">
              繳納金額</td>
            <td width="454" valign="top">
              {{number_format($data->cost+$data->other_amount_due)}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=" float:left; width:100%;"> 
    	<div style="float:left;width:70%;">第二聯由鄉公所送回生命紀念館開立正式收據</div>
    	<div style="width:30%;float:right; text-align:right;">序號： {{$data->no}}</div>
    </div>
     <br>
    <hr size="1" noshade="noshade" style="border:1px #cccccc dotted;"/>


    <div>
      <div style="float:left; width:100%;">
        <div style="float:left;width:75%;text-align:right;">
        <b>宜蘭縣壯圍鄉生命紀念館使用規費繳款單</b></div>
        <div style="width:25%;float:right; text-align:right;">序號： {{$data->no}}</div>
      </div>
    </div>
    <div style="text-align: right;">
      <p>轉帳日期： {{$data->payment_date}}</p>
    </div>
    <div>
      <table border="1" cellspacing="0" cellpadding="0" align="" style="width: 100%">
        <tbody>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              項目</td>
            <td width="454" valign="top" style="text-align: center;">
             說明</td>
            <td width="85" valign="top" style="text-align: center;">
              備註</td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              繳款人姓名</td>
            <td width="454" valign="top">
              {{$data->applicant}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              使用類別</td>
            <td width="454" valign="top">
              @if($data->status==100)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位申請
              @elseif($data->status==200)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位繳費
              @elseif($data->status==300 || $data->status==350||$data->status==399 )
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位入塔
              @elseif($data->status==400)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位申請異動
              @elseif($data->status==500)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位異動繳費
              @elseif($data->status==600 || $data->status==650||$data->status==699 )
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位異動遷出
              @elseif($data->status==700)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位遷出申請
              @elseif($data->status==800)
                @if($data->application_type==1)納骨 @else 長生 @endif 櫃位遷出出塔
              @endif
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              櫃位編號</td>
            <td width="454" valign="top">
             <?php $ii=1?>
              @foreach ($data2 as $datas2)
                @if($ii%4==0)
                    <div style="float: left;width:25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @else
                    <div style="float: left;width: 25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @endif
                @if($ii%4==0)
                    <br>
                @endif 
                <?php $ii=$ii+1?>
              @endforeach
            </td>
            <td width="85" valign="top">
              {{$data->cabinet_type}}
            </td>
          </tr>
          <tr>
            <td width="104" valign="top"  style="text-align: center;">
              繳納金額</td>
            <td width="454" valign="top">
              {{number_format($data->cost+$data->other_amount_due)}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=" float:left; width:100%;"> 
    	<div style="float:left;width:70%;">第三聯由繳款人留存</div>
    	<div style="width:30%;float:right; text-align:right;">序號： {{$data->no}}</div>
    </div>
    </body>

</html>
<script>
     window.onload = function ()
    {
        window.print();
        window.close();
    }
</script>