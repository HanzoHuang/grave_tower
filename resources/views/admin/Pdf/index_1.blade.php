<!DOCTYPE html>
<html>
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>@font-face { font-family: 'Firefly Sung'; font-style: normal; font-weight: 400; src: url(http://eclecticgeek.com/dompdf/fonts/cjk/fireflysung.ttf) format('truetype'); } * { font-family: Firefly Sung, DejaVu Sans, sans-serif; }
    	body {
	        height: 842px;
	        width: 720px;
	        /* to centre page on screen*/
	        margin-left: auto;
	        margin-right: auto;
    		}
    </style>
    </head>
  
  <body>
    <div>
      <div style="float:left; width:100%;">
        <div style="float:left;width:70%;text-align:right;;font-size: 24px">
        宜蘭縣壯圍鄉公所轉帳收據（生命紀念館專用）</div>
        <div style="width:30%;float:right; text-align:right;">序號： {{$data->no}}</div>
      </div>
    </div>
    <div style="text-align: right;">
      <p>轉帳日期： {{$data->payment_date}}</p>
    </div>
    <div>
      <table border="1" cellspacing="0" cellpadding="0" align="" style="width: 100%">
        <tbody>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              項目</td>
            <td width="454" valign="top" style="text-align: center;">
             說明</td>
            <td width="85" valign="top" style="text-align: center;">
              備註</td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              繳款人姓名</td>
            <td width="454" valign="top">
              {{$data->applicant}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              使用類別</td>
            <td width="454" valign="top">
              @if($data->status==100)
               牌位申請
              @elseif($data->status==200)
               牌位繳費
              @elseif($data->status==300 || $data->status==350 || $data->status==399)
               牌位入塔
              @elseif($data->status==400)
               牌位申請異動
              @elseif($data->status==500)
               牌位異動繳費
              @elseif($data->status==600 || $data->status==650 || $data->status==699)
               牌位異動遷出
              @elseif($data->status==700)
               牌位遷出申請
              @elseif($data->status==800)
               牌位遷出出塔
              @endif
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              櫃位編號</td>
            <td width="454" valign="top">
              <?php $ii=1?>
              @foreach ($data2 as $datas2)
                @if($ii%4==0)
                    <div style="float: left;width:25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @else
                    <div style="float: left;width: 25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @endif
                @if($ii%4==0)
                    <br>
                @endif 
                <?php $ii=$ii+1?>
              @endforeach
            </td>
            <td width="85" valign="top">
            </td>
          </tr>
          <tr>
            <td width="104" valign="top"  style="text-align: center;">
              繳納金額</td>
            <td width="454" valign="top">
              {{number_format($data->cost+$data->other_amount_due)}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=" float:left; width:100%;"> 
    	<div style="float:left;width:70%;">第一聯由繳款人收執</div>
    	<div style="width:30%;float:right; text-align:right;">序號：{{$data->no}}</div>
    </div>
    <div style=" float:left; width:100%">
		<div style="float:left;width:25%;">經手人：</div>
		<div style="float:left;width:25%;">主辦出納： </div>
		<div style="float:left;width:25%;">主辦會計：</div>
		<div>機關長官：</div>
    </div>
     <br>
    <hr size="1" noshade="noshade" style="border:1px #cccccc dotted;"/>


    <div>
      <div style="float:left; width:100%;">
        <div style="float:left;width:70%;text-align:right;">
        宜蘭縣壯圍鄉公所轉帳收據（生命紀念館專用）</div>
        <div style="width:30%;float:right; text-align:right;">序號： {{$data->no}}</div>
      </div>
    </div>
    <div style="text-align: right;">
      <p>轉帳日期： {{$data->payment_date}}</p>
    </div>
    <div>
      <table border="1" cellspacing="0" cellpadding="0" align="" style="width: 100%">
        <tbody>
           <tr>
            <td width="104" valign="top" style="text-align: center;">
              項目</td>
            <td width="454" valign="top" style="text-align: center;">
             說明</td>
            <td width="85" valign="top" style="text-align: center;">
              備註</td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              繳款人姓名</td>
            <td width="454" valign="top">
              {{$data->applicant}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              使用類別</td>
            <td width="454" valign="top">
              @if($data->status==100)
               牌位申請
              @elseif($data->status==200)
               牌位繳費
              @elseif($data->status==300 || $data->status==350 || $data->status==399)
               牌位入塔
              @elseif($data->status==400)
               牌位申請異動
              @elseif($data->status==500)
               牌位異動繳費
              @elseif($data->status==600 || $data->status==650 || $data->status==699)
               牌位異動遷出
              @elseif($data->status==700)
               牌位遷出申請
              @elseif($data->status==800)
               牌位遷出出塔
              @endif
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              櫃位編號</td>
            <td width="454" valign="top">
             <?php $ii=1?>
              @foreach ($data2 as $datas2)
                @if($ii%4==0)
                    <div style="float: left;width:25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @else
                    <div style="float: left;width: 25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @endif
                @if($ii%4==0)
                    <br>
                @endif 
                <?php $ii=$ii+1?>
              @endforeach
            </td>
            <td width="85" valign="top">
              
            </td>
          </tr>
          <tr>
            <td width="104" valign="top"  style="text-align: center;">
              繳納金額</td>
            <td width="454" valign="top">
              {{number_format($data->cost+$data->other_amount_due)}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=" float:left; width:100%;"> 
    	<div style="float:left;width:70%;">第二聯通知會計單位登帳</div>
    	<div style="width:30%;float:right; text-align:right;">序號：{{$data->no}}</div>
    </div>
    <br>
    <hr size="1" noshade="noshade" style="border:1px #cccccc dotted;"/>


    <div>
      <div style="float:left; width:100%;">
        <div style="float:left;width:70%;text-align:right;">
        宜蘭縣壯圍鄉公所轉帳收據（生命紀念館專用）</div>
        <div style="width:30%;float:right; text-align:right;">序號： {{$data->no}}</div>
      </div>
    </div>
    <div style="text-align: right;">
      <p>轉帳日期： {{$data->payment_date}}</p>
    </div>
    <div>
      <table border="1" cellspacing="0" cellpadding="0" align="" style="width: 100%">
        <tbody>
           <tr>
            <td width="104" valign="top" style="text-align: center;">
              項目</td>
            <td width="454" valign="top" style="text-align: center;">
             說明</td>
            <td width="85" valign="top" style="text-align: center;">
              備註</td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              繳款人姓名</td>
            <td width="454" valign="top">
              {{$data->applicant}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              使用類別</td>
            <td width="454" valign="top">
              @if($data->status==100)
               牌位申請
              @elseif($data->status==200)
               牌位繳費
              @elseif($data->status==300 || $data->status==350 || $data->status==399)
               牌位入塔
              @elseif($data->status==400)
               牌位申請異動
              @elseif($data->status==500)
               牌位異動繳費
              @elseif($data->status==600 || $data->status==650 || $data->status==699)
               牌位異動遷出
              @elseif($data->status==700)
               牌位遷出申請
              @elseif($data->status==800)
               牌位遷出出塔
              @endif
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              櫃位編號</td>
            <td width="454" valign="top">
             <?php $ii=1?>
              @foreach ($data2 as $datas2)
                @if($ii%4==0)
                    <div style="float: left;width:25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @else
                    <div style="float: left;width: 25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @endif
                @if($ii%4==0)
                    <br>
                @endif 
                <?php $ii=$ii+1?>
              @endforeach
            </td>
            <td width="85" valign="top">
              
            </td>
          </tr>
          <tr>
            <td width="104" valign="top"  style="text-align: center;">
              繳納金額</td>
            <td width="454" valign="top">
              {{number_format($data->cost+$data->other_amount_due)}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=" float:left; width:100%;"> 
    	<div style="float:left;width:70%;">第三聯報查由填發單位報財政單位</div>
    	<div style="width:30%;float:right; text-align:right;">序號：{{$data->no}}</div>
    </div>
     <br>
    <hr size="1" noshade="noshade" style="border:1px #cccccc dotted;"/>


    <div>
      <div style="float:left; width:100%;">
        <div style="float:left;width:70%;text-align:right;">
        宜蘭縣壯圍鄉公所轉帳收據（生命紀念館專用）</div>
        <div style="width:30%;float:right; text-align:right;">序號： {{$data->no}}</div>
      </div>
    </div>
    <div style="text-align: right;">
      <p>轉帳日期： {{$data->payment_date}}</p>
    </div>
    <div>
      <table border="1" cellspacing="0" cellpadding="0" align="" style="width: 100%">
        <tbody>
           <tr>
            <td width="104" valign="top" style="text-align: center;">
              項目</td>
            <td width="454" valign="top" style="text-align: center;">
             說明</td>
            <td width="85" valign="top" style="text-align: center;">
              備註</td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              繳款人姓名</td>
            <td width="454" valign="top">
              {{$data->applicant}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              使用類別</td>
            <td width="454" valign="top">
              @if($data->status==100)
               牌位申請
              @elseif($data->status==200)
               牌位繳費
              @elseif($data->status==300)
               牌位入塔
              @elseif($data->status==400)
               牌位申請異動
              @elseif($data->status==500)
               牌位異動繳費
              @elseif($data->status==600)
               牌位異動遷出
              @elseif($data->status==700)
               牌位遷出申請
              @elseif($data->status==800)
               牌位遷出出塔
              @endif
            </td>
            <td width="85" valign="top"></td>
          </tr>
          <tr>
            <td width="104" valign="top" style="text-align: center;">
              櫃位編號</td>
            <td width="454" valign="top">
             <?php $ii=1?>
              @foreach ($data2 as $datas2)
                @if($ii%4==0)
                    <div style="float: left;width:25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @else
                    <div style="float: left;width: 25%">
                        <span >{{$datas2->code}}</span>
                    </div>
                @endif
                @if($ii%4==0)
                    <br>
                @endif 
                <?php $ii=$ii+1?>
              @endforeach
            </td>
            <td width="85" valign="top">
              
            </td>
          </tr>
          <tr>
            <td width="104" valign="top"  style="text-align: center;">
              繳納金額</td>
            <td width="454" valign="top">
              {{number_format($data->cost+$data->other_amount_due)}}
            </td>
            <td width="85" valign="top"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=" float:left; width:100%;"> 
    	<div style="float:left;width:70%;">第四聯生命紀念館留存</div>
    	<div style="width:30%;float:right; text-align:right;">序號：{{$data->no}}</div>
    </div>
    </body>

</html>
<script>
     window.onload = function ()
    {
        window.print();
        window.close();
    }
</script>