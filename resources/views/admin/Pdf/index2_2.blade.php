<style type="text/css">
    @page {
        margin: 0.5cm; /*print邊界*/
        size: landscape;
    }
    @media print {
        body{
             font-size: 4px
        }
        table {
            border: 1px solid #000; border-collapse: collapse;
             font-size: 4px
        }

        tr, td {
            border: 1px solid #000;
        }

          /* ... the rest of the rules ... */
    }

</style>
<div>
    <div style="float:right;">
    申請日期：{{$data->application_date}}
    </div>
           
    <div style="float:left; width:100%;">
        <div style="float:left;width:65%;text-align:right;font-size: 24px" >
                宜蘭縣壯圍鄉生命紀念館申請單
        </div>
        <div  style="width:35%;float:right; text-align:right;">
                序 號：{{$data->no}}
        </div>
    </div>
    <div style="float:right;">
    申請人蓋章：_________
    </div>       
</div>       
<br>
<br> 
<br>        
<div>
    一、申請人
</div>
<table border="0" >
    <tbody>
        <tr>
            <td width="104" nowrap="" valign="top">
                
                    姓 名
                
            </td>
            <td width="123" nowrap="" valign="top">
            {{$data->applicant}}
            </td>
            <td width="104" nowrap="" valign="top">
                
                    戶籍地址
                
            </td>
            <td width="161" nowrap="" valign="top">
                {{$data->household_registration}}
            </td>
            <td width="134" nowrap="" valign="top">
                
                    電 話
                
            </td>
            <td width="147" nowrap="" valign="top">
                {{$data->tel}}
            </td>
            <td width="87" nowrap="" valign="top">
                
                    與逝者關係
                
            </td>
            <td width="71" nowrap="" valign="top">
                {{$data->relationship}}
            </td>
        </tr>
    </tbody>
</table>
<p>
    二、受委託人

<table border="0"  width="775">
    <tbody>
        <tr>
            <td width="104" nowrap="" valign="top">
                
                    姓 名
                
            </td>
            <td width="126" nowrap="" valign="top">
                {{$data->client}}
            </td>
            <td width="101" nowrap="" valign="top">
                
                    戶籍地址
                
            </td>
            <td width="161" nowrap="" valign="top">
                {{$data->client_household_registration}}
            </td>
            <td width="132" nowrap="" valign="top">
                
                    電 話
                
            </td>
            <td width="151" nowrap="" valign="top">
                {{$data->client_tel}}
            </td>
        </tr>
    </tbody>
</table>
<p>
    三、申請項目

<table border="0"  width="926">
    <tbody>
        <tr>
            <td width="94" nowrap="" valign="top">
                
                    項 目
                
            </td>
            <td width="135" nowrap="" valign="top">
                
                    申請內容
                
            </td>
            <td width="148" nowrap="" valign="top">
                
                    使用(或遷出)日期
                
            </td>
            <td width="161" nowrap="" valign="top">
                
                    地點
                
            </td>
            <td width="175" nowrap="" valign="top">
                
                    櫃位或牌位編號
                
            </td>
            <td width="212" nowrap="" valign="top">
                
                    繳費金額
                
            </td>
        </tr>
        <tr>
            <td width="94" nowrap="">
                
                    長生位
                
            </td>
            <td width="135" nowrap="">
                    <input type="checkbox">預購
                
            </td>
            <td width="148" nowrap="" valign="top">
            </td>
            <td width="161" nowrap="" rowspan="5">
                
                    壯圍鄉生命紀念館
                
            </td>
            <td width="175" nowrap="" rowspan="5">  
                    <?php $ii=1?>
                    @for ($i = 0; $i <=11; $i++)
                        @if(isset($data2[$i]))
                            @if($ii%2==0)
                                <div style="float: left;width: 50%">
                                    <span >{{$data2[$i]->code}}</span>
                                </div>
                            @else
                                <div style="float: left;width: 50%">
                                    <span >{{$data2[$i]->code}}</span>
                                </div>
                            @endif
                        @endif
                        @if($ii%2==0)
                            <br>
                        @endif 
                        <?php $ii=$ii+1?>
                    @endfor
                
            </td>
            <td width="212" rowspan="3" valign="top">
                <p>
                    牌位：@if($data->status<=399){{number_format($data->cost)}}@endif
                    <br/>
                    換位：@if($data->status>=400 and $data->status<=699) {{number_format($data->cost)}} @endif
                    <br/>
                    其他：{{number_format($data->other_amount_due)}}
                
            </td>
        </tr>
        <tr>
            <td width="94" nowrap="" rowspan="2">
                
                    櫃位
                
            </td>
            <td width="135" nowrap="" valign="top">
                <p>
                     <input type="checkbox" >入館
                
            </td>
            <td width="148" nowrap="" valign="top">
                    
            </td>
        </tr>
        <tr>
            <td width="135" nowrap="" valign="top">
                <p>
                    <input type="checkbox" >遷出
                
            </td>
            <td width="148" nowrap="" valign="top">
                <p>
                     
                
            </td>
        </tr>
        <tr>
            <td width="94" nowrap="" rowspan="2">
                
                    牌位
                
            </td>
            <td width="135" nowrap="" valign="top">
                <p>
                   <input type="checkbox" @if($data->status<=399 ) checked="" @endif>入館
                
            </td>
            <td width="148" nowrap="" valign="top">
                @if($data->status!=100)
                 @if($data->status<=399) @if($data->expected_date!='0000-00-00') {{$data->expected_date}} @endif @endif
                @endif
            </td>
            <td width="212" rowspan="2" valign="top">
                <p>
                    總金額：{{number_format($data->cost+$data->other_amount_due)}}
                
            </td>
        </tr>
        <tr>
            <td width="135" nowrap="" valign="top">
                <p>
                   <input type="checkbox" @if($data->status>=700) checked="" @endif>遷出
                
            </td>
            <td width="148" nowrap="" valign="top">
                @if($data->status!=100)
                @if($data->status>=700) @if($data->expected_date!='0000-00-00') {{$data->change_date}} @endif @endif
                @endif


            </td>
        </tr>
    </tbody>
</table>

<p>
    四、逝者資料

<div style="float:left; width:100%;">
    <div style="float:left;width:50%;text-align:right;">
        <table border="0">
    <tbody>
        <tr>
            <td width="94" nowrap="" valign="top">
                
                    姓 名
                
            </td>
            <td width="66" nowrap="" valign="top">
                
                    姓 別
                
            </td>
            <td width="95" nowrap="" valign="top">
                
                    戶籍地
                
            </td>
            <td width="76" nowrap="" valign="top">
                
                    出生日期
                
            </td>
            <td width="76" nowrap="" valign="top">
                
                    亡故日期
                
            </td>
            <td valign="top">
                
                    櫃(牌)位
                
            </td>
        </tr>
        @for ($i = 0; $i <= 5; $i++)
            @if(isset($data2[$i]))
             <tr>
                <td width="94" nowrap="" valign="top">
                        {{$data2[$i]->name}}
                </td>
                <td width="66" nowrap="" valign="top">
                    @if($data2[$i]->name)
                        @if($data2[$i]->gender==1)
                            <input type="checkbox" checked="">男
                        @elseif($data2[$i]->gender==2)
                            <input type="checkbox" checked="">女
                        @endif
                    @endif
                </td>
                <td width="95" nowrap="" valign="top">
                    <span style="font-size: 2px">{{$data2[$i]->address}}</span>
                </td>
                <td width="76" nowrap="" valign="top">
                    
                        @if($data2[$i]->birthday!='0000-00-00')
                        {{$data2[$i]->birthday}}
                        @endif
                    
                </td>
                <td width="76" nowrap="" valign="top">
                    
                        -                    
                </td>
                <td>
                        @if(isset($data2[$i]))
                            {{$data2[$i]->code}}
                        @endif
                </td>
             </tr>
            @endif
        @endfor
    </tbody>
        </table>
    </div>
    <div style="width:50%;float:right; text-align:right;">
        <table border="0"> 
   <tbody> 
        <tr>
            <td width="94" nowrap="" valign="top">
                
                    姓 名
                
            </td>
            <td width="66" nowrap="" valign="top">
                
                    姓 別
                
            </td>
            <td width="95" nowrap="" valign="top">
                
                    戶籍地
                
            </td>
            <td width="76" nowrap="" valign="top">
                
                    出生日期
                
            </td>
            <td width="76" nowrap="" valign="top">
                
                    亡故日期
                
            </td>
            <td valign="top">
                
                    櫃(牌)位
                
            </td>
        </tr>
    @for ($i = 6; $i <=  11; $i++)
            @if(isset($data2[$i]))
             <tr>
                <td width="94" nowrap="" valign="top">
                        {{$data2[$i]->name}}
                </td>
                <td width="66" nowrap="" valign="top">
                    @if($data2[$i]->name)
                        @if($data2[$i]->gender==1)
                            <input type="checkbox" checked="">男
                        @elseif($data2[$i]->gender==2)
                            <input type="checkbox" checked="">女
                        @endif
                    @endif
                </td>
                <td width="95" nowrap="" valign="top">
                    <span style="font-size: 2px">{{$data2[$i]->address}}</span>
                </td>
                <td width="76" nowrap="" valign="top">
                        @if($data2[$i]->birthday!='0000-00-00')
                        {{$data2[$i]->birthday}}
                        @endif
                    
                </td>
                <td width="76" nowrap="" valign="top">
                    
                    -
                </td>
                <td>
                       {{$data2[$i]->code}}
                    
                </td>
             </tr>
            @endif
        @endfor
   
   </tbody> 
        </table>
    </div>
</div>
<br>
<br>
<p>
    上列入館事宜願遵守壯圍鄉公所生命紀念館使用管理自治條例之規定,倘有違反或造假,願受沒收所繳費用及禁止使用之處置,絕無異議。

<div style="float:left; width:100%;">
    <div style="float:left;width:60%;text-align:right;font-size:24px">
            此致 宜蘭縣壯圍鄉鄉公所
    </div>
    <div  style="width:40%;float:right; text-align:center;">
            經辦人蓋章：
    </div>
</div>


<p>
    注意事項：

<p>
    一、取得入館許可證後，憑證依管理人員引導辦理進館事宜。

<p>
    二、凡經核准使用本鄉生命紀念館者，其進館使用之骨灰罐尺寸，不得超過館內骨灰櫃位設備之標準。

<p>
    三、本證一式三聯,第1聯申請書由鄉公所存查,第2聯由申請人持向生命紀念館辦理入館事宜,第3聯由申請人收執。

<p>
    四、申請人應先閱畢及了解申請書內容事項。
<script>
     window.onload = function ()
    {
        window.print();
        window.close();
    }
</script>