@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!!	Html::style('https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css')  !!}
{!!	Html::style('https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css')  !!}
{!!	Html::script('https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js')  !!}
{!!	Html::script('https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js')  !!}
{!!	Html::script('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js')  !!}
{!!	Html::script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js')  !!}
{!!	Html::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js')  !!}
{!!	Html::script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js')  !!}
{!!	Html::script('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js')  !!}
{!!	Html::style('backend/plugins/bootstrap-datepicker/css/datepicker3.css')  !!}
{!!	Html::script('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
@if (isset($DetailName))
    <div class="alert alert-success" style="margin-top: 20px;">
        您現在所在的頁面：{{ $DetailName}}

    </div>
@endif
	<form action="{{$Url}}" method="get" >
    <label class="">申請日期區間：</label>
    {!!Form::text('application_start_date',Input::get('start_date',''), array('id' => 'application_start_date' , 'class' => '', 'placeholder' => ''))!!}
         <label class="">-</label>

    {!!Form::text('application_end_date',Input::get('end_date',''), array('id' => 'application_end_date' , 'class' => '', 'placeholder' => ''))!!}


    <label class="">入塔日期區間：</label>
    {!!Form::text('expected_start_date',Input::get('start_date',''), array('id' => 'expected_start_date' , 'class' => '', 'placeholder' => ''))!!}
     <label class="">-</label>
    {!!Form::text('expected_end_date',Input::get('end_date',''), array('id' => 'expected_end_date' , 'class' => '', 'placeholder' => ''))!!}
	<br>
	<br>
	<label>申請人身分證號碼：</label>
	<input class="" type="text" name="number" placeholder="">
	<label>牌位者身分證號碼：</label>
	<input class="" type="text" name="user_number" placeholder="申請人身分證號碼">

	<label>身份：</label>
	<select name="identity">
		<option value="0">本鄉鄉民</option>
		<option value="1">本縣縣民</option>
		<option value="2">外縣市民</option>
	</select>
	<label>作業：</label>
	<select name="status_type">
		<option value="0">申請</option>
		<option value="1">異動</option>
		<option value="2">遷出</option>
	</select>
	<label>狀態：</label>
	<select name="status">
		<option value="100">申請</option>
		<option value="200">繳費</option>
		<option value="300">入塔</option>
		<option value="400">異動申請</option>
		<option value="500">異動繳費</option>
		<option value="600">異動換位置</option>
		<option value="700">遷出申請</option>
		<option value="800">遷出</option>
	</select>
	<br>
	<br>
	<label>樓層：</label>
	<select name="class_floor">
		<option value="">01 1樓</option>
		<option value="">02 2樓</option>
		<option value="">03 3樓</option>
	</select>	
	<label>區域：</label>
	<select name="class_area">
		<option value="A">A A區</option>
		<option value="B">B B區</option>
		<option value="C">C C區</option>
	</select>
	<label>方位：</label>
	<select name="class_position">
		<option value="1">座南朝北</option>
		<option value="2">座北朝南</option>
		<option value="3">座東朝西</option>
	</select>
	<label>排：</label>
	<select name="class_row">
		<option value="">01 1排</option>
		<option value="">02 2排</option>
		<option value="">03 3排</option>
		<option value="">04 4排</option>
	</select>
	<label>層：</label>
	<select>
		<option value="1">01 1位置</option>
		<option value="2">02 2位置</option>
		<option value="3">03 3位置</option>
		<option value="4">04 4位置</option>
		<option value="5">05 5位置</option>
		<option value="6">06 6位置</option>
		<option value="7">07 7位置</option>
		<option value="8">08 8位置</option>
		<option value="9">09 9位置</option>
		<option value="10">10 10位置</option>
		<option value="11">11 11位置</option>
		<option value="12">12 12位置</option>
	</select>
	<button type="submit">搜尋</button>
	</form> 
<br>
<br>
<br>
@if (session('status'))
    <div class="alert alert-success">
        {!! session('status') !!}
    </div>
@endif
{!!$DataTableJs!!}
<br>

@stop

@section('script')
<?php echo Html::script(asset('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>

<script>

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
})
$(function () {
    	$.fn.datepicker.defaults.format = "yyyy-mm-dd";
        $("#application_start_date").datepicker({autoclose:true});
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        $("#application_end_date").datepicker({autoclose:true});
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        $("#expected_start_date").datepicker({autoclose:true});
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        $("#expected_end_date").datepicker({autoclose:true});
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        useCurrent: false //Important! See issue #1075
});
</script>
@stop