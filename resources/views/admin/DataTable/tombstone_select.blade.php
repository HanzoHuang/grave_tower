@extends('layouts.admin.master2')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!! Html::style('backend/data/1.10.11/css/jquery.dataTables.min.css')  !!}
{!! Html::script('backend/data/1.10.11/js/jquery.dataTables.min.js')  !!}


{!! Html::style('backend/data/buttons/1.1.2/css/buttons.dataTables.min.css')  !!}
{!! Html::script('backend/data/buttons/1.1.2/js/dataTables.buttons.min.js')  !!}
{!! Html::script('backend/data/ajax/libs/jszip/2.5.0/jszip.min.js')  !!}
{!! Html::script('backend/data/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js')  !!}


{!! Html::script('backend/data/buttons/1.1.2/js/buttons.flash.min.js')  !!}
{!! Html::script('backend/data/buttons/1.1.2/js/buttons.html5.min.js')  !!}
{!! Html::script('backend/data/buttons/1.1.2/js/buttons.print.min.js')  !!}

@if (isset($DetailName))
    <div class="alert alert-success" style="margin-top: 20px;">
        您現在所在的頁面：{{$DetailName}}
    </div>
@endif
	<form action="{{$Url}}" method="get" >
    <label class="">申請日期區間：</label>
    {!!Form::text('application_start_date',Input::get('start_date',''), array('id' => 'application_start_date' , 'class' => 'datepickerTW','readonly'=>'readonly', 'placeholder' => ''))!!}
         <label class="">-</label>
    {!!Form::text('application_end_date',Input::get('end_date',''), array('id' => 'application_end_date' , 'class' => 'datepickerTW','readonly'=>'readonly', 'placeholder' => ''))!!}

    <label class="">入塔日期區間：</label>
    {!!Form::text('expected_start_date',Input::get('start_date',''), array('id' => 'expected_start_date' , 'class' => 'datepickerTW','readonly'=>'readonly', 'placeholder' => ''))!!}
     <label class="">-</label>
    {!!Form::text('expected_end_date',Input::get('end_date',''), array('id' => 'expected_end_date' , 'class' => 'datepickerTW','readonly'=>'readonly', 'placeholder' => ''))!!}
	<br>
	<br>
	<label>申請人身分證號碼：</label>
	<input class="" type="text" id="number" name="number" placeholder="">
	<label>牌位者身分證號碼：</label>
	<input class="" type="text" name="user_number" id="user_number" placeholder="申請人身分證號碼">

	<label>身份：</label>
	<select name="identity" id="identity">
		<option value="">無</option>
		<option value="0">本鄉鄉民</option>
		<option value="1">本縣縣民</option>
		<option value="2">外縣市民</option>
	</select>
	<label>作業：</label>
	<select name="status_type" id="status_type">
		<option value="">無</option>
		<option value="0">申請</option>
		<option value="2">遷出</option>
	</select>
	<label>狀態：</label>
	<select name="status" id="status">
		<option value="">無</option>
	</select>
	<br>
	<br>
	<div id="sys_select_div">
		<label>樓層：</label>
		<select name="class_floor">
			<option value="1">1樓</option>

		</select>	
		<label>區域：</label>
		<select name="class_area" id="area" onchange="change_sys_select('area')">
			<option value='0'>無</option>
			@foreach($sys['area'] as $val)
			<option value="{{$val->id}}">{{$val->code.' '.$val->name}}</option>
			@endforeach
		</select>
		
		<label>排：</label>
		<select name="class_layer" id="layer" onchange="change_sys_select('layer')">
			<option value="0">無</option>	
			@foreach($sys['layer'] as $val)
				<option value="{{$val->id}}">{{$val->code.'排 '}}</option>
			@endforeach
		</select>
		<label>方位：</label>
		<select name="class_position" id="position" onchange="change_sys_select('position')">
			<option value="0">無</option>
			@foreach($sys['position'] as $key=> $val)
			<option value="{{$key}}">{{$val}}</option>
			@endforeach
		</select>
     
	</div>
   <label>申請人名稱</label>
    <input class="" type="text" name="name" id="name" placeholder="申請人名稱">
      <label>逝者名稱</label>
    <input class="" type="text" name="user_name" id="user_name" placeholder="逝者名稱">
	<button type="submit">搜尋</button>
	</form> 
<br>
<br>
<br>
@if (session('status'))
    <div class="alert alert-success">
        {!! session('status') !!}
    </div>
@endif
@if($serach_count>0)
{!!$DataTableJs!!}
@endif
<br>

@stop

@section('script')
<?php echo Html::script(asset('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>

<script>

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
})
/*
function change_sys_select(type) {
    area=$('#area').find(':selected').val();
    position=$('#position').find(':selected').val();
    layer=$('#layer').find(':selected').val();
    seat=$('#seat').find(':selected').val();
    if(type=='area'){
    	if(area==0){ return ''; }
        data={area:area,position:position};
    }else if(type=='layer'){
    	if(layer==0){ return ''; }
        data={area:area,layer:layer,position:position};
    }else if(type=='seat'){
    	if(layer==0){ return ''; }
        data={area:area,layer:layer,position:position,seat:seat};
    }else if(type=='position'){
    	if(position==0){ return ''; }
        data={area:area,layer:layer,position:position};
    }
    $.ajax({
        url: "{{ route('BackTombstone.ChangeSelect') }}",
        data: data,
        dataType:"html",
        type: "get",
        success: function(data){
            $('#sys_select_div').html(data);
        },
        error: function(){
        }
    });
}
*/
@foreach(Input::all() as $key =>$val)
    $('#{{$key}}').val('{{$val}}');
    if($('#{{$key}}').prop('tagName') =='select'){
    	 $("#{{$key}}").val('{{$val}}');
    }
@endforeach 
 $("#status_type").change(function(){
      //此處用switch case來作為判斷式
      //並以sel各個Option的Value作為判斷條件
      //注意這邊有用parseInt將value值轉為整數型態否則會出現錯誤
      switch (parseInt($(this).val())){
      //默認行為，可以不寫
      default:
         $("#status option").remove();
          var array = {"":"無"};
          //利用each遍歷array中的值並將每個值新增到Select中
          $.each(array, function(i, val) {
            $("#status").append($("<option value='" + i + "'>" + array[i] + "</option>"));
          });  
          break;
      case 0: 
          $("#status option").remove();
          var array = {"100":"申請", "200":"繳費", "300":"入塔"};
          //利用each遍歷array中的值並將每個值新增到Select中
          $.each(array, function(i, val) {
            $("#status").append($("<option value='" + i + "'>" + array[i] + "</option>"));
          });      
          break;
          break;
      //當value值為1時刪除sel2的Option Item 
      //並用陣列及each迴圈新增sel2的Option Item選項
      case 1: 
          $("#status option").remove();
          var array = {"400":"異動申請", "500":"異動繳費", "600":"異動換位"};
          //利用each遍歷array中的值並將每個值新增到Select中
          $.each(array, function(i, val) {
            $("#status").append($("<option value='" + i + "'>" + array[i] + "</option>"));
          });      
          break;
      case 2: 
          $("#status option").remove();
          var array = {"700":"遷出申請", "800":"遷出"};
          //利用each遍歷array中的值並將每個值新增到Select中
          $.each(array, function(i, val) {
            $("#status").append($("<option value='" + i + "'>" + array[i] + "</option>"));
          });      
          break;
     }
    });
</script>
@stop