@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!!	Html::style('css/dataTable/jquery.dataTables.min.css')  !!}
{!!	Html::script('js/dataTable/jquery.dataTables.min.js')  !!}
{!!	Html::style('backend/plugins/bootstrap-datepicker/css/datepicker3.css')  !!}
{!!	Html::script('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
{!!	Html::script('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
	<br>
@if(isset($HeadDataTableJs))
	{!!$HeadDataTableJs!!}
	<br>
@endif
<div class="form-group">
<label class="col-sm-2">館別:<select><option>生命紀念館</option></select></label>
<label class="col-sm-2">樓層:<select id='fool'><option value="1">一樓</option><option value="2">二樓</option><option value="3">三樓</option><option value="4">四樓</option></select></label></div>
<br>
<div class="form-group">
    <div class="col-sm-4" >
        <div id="area_div"> 
            區域
            <select name='area_id' id="area_id" class="form-control class_select" size='5' class_type='area'>
                @foreach($area as $key =>$value)
                <option value="{{$value->id}}" code="{{$value->code}}" class_name="{{$value->name}}">
                    {{$value->code.' '.$value->name}} 
                    @if(!empty($value->deleted_at))
                        關閉
                    @endif
                </option>
                @endforeach
            </select>
        </div>
        <br>
        <label class="col-sm-3">名稱</label>
        <div class="col-sm-9">
            <input id="area_class_name"  placeholder="名稱">
        </div>
        <a class="btn col-sm-3" onclick="add_select('area');">新增</a>
        <a class="btn col-sm-3" onclick="update_select('area');">修改</a>
        <a class="btn col-sm-3" onclick="delete_select('area');">刪除</a>
        <a class="btn col-sm-3" onclick="change_status('area');">關/開</a>
    </div>
    <div class="col-sm-4" id="layer_id_div">
        <div id="layer_div"> 
            排
            <select name="layer_id" id="layer_id" class="form-control class_select" size='5' class_type='layer'>
               
            </select>
        </div>
        <br>
        <a class="btn" onclick="add_select('layer');">新增</a>
        {{-- <a class="btn" onclick="update_select('layer');">修改</a> --}}
        <a class="btn" onclick="delete_select('layer');">刪除</a>
        <a class="btn" onclick="change_status('layer');">關/開</a>
    </div>
    <div class="col-sm-4" id="seat_id_div">
        <div id="seat_div"> 
            位置
            <select name='seat_id' id="seat_id" class="form-control class_select"  class_type='seat' size='5'>
            </select>
        </div>
        <br>
        <label class="col-sm-3">方位</label>
        <div class="col-sm-9">
            <select name='position' id="position" class="form-control"  class_type='seat'>
               <option value="1">座北朝南</option>
               <option value="2">座南朝北</option>
               <option value="3">座東朝西</option>
               <option value="4">座西朝東</option>
            </select>
        </div>
        <a class="btn col-sm-3" onclick="add_select('seat');">新增</a>
        <a class="btn col-sm-3" onclick="update_select('seat');">修改</a>
        <a class="btn col-sm-3" onclick="delete_select('seat');">刪除</a>
        <a class="btn col-sm-3" onclick="change_status('seat');">關/開</a>
    </div>
</div>
<br>

<div class="form-group" style="text-align: right;">
    <a class="btn btn-success" onclick="add_cabinet_sys();">新增櫃位</a>
</div>
@if (isset($DetailName))
    <div class="alert alert-success" style="margin-top: 20px;">
        您現在所在的頁面：{{ $DetailName}}
    </div>
@endif
@if (session('status'))
    <div class="alert alert-success">
        {!! session('status') !!}
    </div>
@endif

<br>

@stop

@section('script')
<script>
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
$(document).on('click', '.ask-delete', function(e) {
	var modalElem = $('#modalSlideUp');
	var $this = $(this);
    e.preventDefault(); // does not go through with the link.
    $('#modalSlideUp').modal('show');
    $(document).on('click', '.model_delete', function(e) {
        $.ajax({
	       type: "POST",
	       url: $this.attr('href'),
	       data: {
	          _method: $this.data('method')
	       },
	       // or data: plaindata, // If 'plaindata' is an object.
	       dataType: 'json',
	       success: function(data) {
	       	  if(data==0){
	       	  	  $('#modalSlideUp_UP').modal('show');
	       	  }else{
	       	      $('#modalSlideUp_Down').modal('show');
	       	  }
	       	  $('#modalSlideUp').modal('hide');
	          console.log(data); // As moonwave99 said
	       },
	       error: function() {
	          //error condition code
	       }
    	});

    });
 });

$(document).on('click', '.model_delete_type', function(e) {
	location.reload();
});
function add_select(type) {
        var name=$('#'+type+'_class_name').val();
        if(type=='area'){
            code=$('#area_class_code').val();
            if(!name.length){
                alert('新增選項不得空白');
                return '';
            }
            var data={name:name};
        }else if(type=='seat'){
            top_class=$('#'+type+'_id').attr('top_class_id');
            position=$('#position :selected').val();
            var data={top_class:top_class,position:position};
            if(!top_class){
                alert('請先選擇上層');
                return '';
            }
        }else{
            top_class=$('#'+type+'_id').attr('top_class_id');
            if(!top_class){
                alert('請先選擇上層');
                return '';
            }
            var data={top_class:top_class};
        }
        $.ajax({
            url: '{{route('BackTombstone.AddClass')}}',
            data:data,
            type:"get",
            dataType:'html',
            success: function(data){
                if(data.length>30){
                    $('#'+type+'_id').html(data);
                }else{
                    alert(data);
                }
                check_area(type);
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                
            }
        });
    }
    
    function update_select(type){
        class_id=$('#'+type+'_id :selected').val();
        if(!class_id){
            alert('請選擇修改的選項');
            return '';
        }
        if(type=='area'){
            name=$("#"+type+"_class_name").val();
            if(!name.length){
                alert('修改不得空白');
                return '';
            }
            var data={id:class_id,name:name};
        }else if(type=='seat'){
            top_class=$('#'+type+'_id').attr('top_class_id');
            position=$('#position :selected').val();
            var data={
                id:class_id,
                top_class:top_class,
                position:position
            };
        }else{
            top_class_id=$('#'+type+'_id').attr('top_class_id');
            var data={
                id:class_id,
                top_class_id:top_class_id
            };
        }
         $.ajax({
            url: '{{route('BackTombstone.UpdateClass')}}',
            data:data,
            type:"get",
            dataType:'html',
            success: function(data){
                if(data.length>30){
                    $('#'+type+'_id').html(data);
                }else{
                    alert(data);
                }
                check_area(type);
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                
            }
        });
    }  
    function delete_select(type) {
        class_id=$('#'+type+'_id :selected').val();
        name=$("#"+type+"_class_name").val();
        top_class_id=$('#'+type+'_id').attr('top_class_id');
        if(!class_id){
            alert('請選擇修改的選項');
            return '';
        }
        if(type=='area'){
            var data={id:class_id,name:name};
        }else{
            var data={id:class_id,top_class_id:top_class_id};
        }
        $.ajax({
            url: '{{route('BackTombstone.DeleteClass')}}',
            data:data,
            type:"get",
            dataType:'html',
            success: function(data){
                $('#'+type+'_id').html(data);
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                alert('錯誤');
            }
        });
    }
    function change_status(type) {
        class_id=$('#'+type+'_id :selected').val();
        top_class=$('#'+type+'_id').attr('top_class_id');
        if(!class_id){
            alert('請選擇修改的選項');
            return '';
        }
        if(type=='area'){
            var data={id:class_id};
        }else{
            var data={id:class_id,top_class:top_class};
        }
        $.ajax({
            url: '{{route('BackTombstone.ChangeStatus')}}',
            data:data,
            type:"get",
            dataType:'html',
            success: function(data){
                $('#'+type+'_id').html(data);
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                alert('錯誤');
            }
        });
    }
    $('.class_select').change(function(){
        type=$(this).attr('class_type');
        if(type=='area'){
            $('#layer_id option').remove();
            $('#row_id option').remove();
            $('#seat_id option').remove();

            $('#layer_id').removeAttr('top_class_id');
            $('#row_id').removeAttr('top_class_id');
            $('#seat_id').removeAttr('top_class_id');
        }else if(type=='layer'){
            $('#row_id option').remove();
            $('#seat_id option').remove();

            $('#row_id').removeAttr('top_class_id');
            $('#seat_id').removeAttr('top_class_id');
        }else if(type=='row'){
            $('#seat_id option').remove();
            
            $('#seat_id').removeAttr('top_class_id');
        }
        class_name=$(this).find(":selected").attr('class_name');
        $('#'+type+'_class_name').val(class_name);
        if(type=='seat'){
            return '';
        }
        class_id=$(this).find(":selected").val();
        class_code=$(this).find(":selected").attr('code');
        $('#'+type+'_class_code').val(class_code);
        if(type=='area'){
            $('#layer_id').attr('top_class_id',class_id);
            last_type='layer_id';
        }else if(type=='layer'){
            $('#seat_id').attr('top_class_id',class_id);
            last_type='seat_id';
        }
        $.ajax({
            url: '{{route('BackTombstone.GetClass')}}',
            data:{top_class_id:class_id},
            type:"get",
            dataType:'html',
            success: function(data){
                $('#'+last_type).html(data);
            },
            beforeSend:function(){
            },
            complete:function(){
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                alert('錯誤');
            }
        });
    });

    function add_cabinet_sys() {
        area_id=$('#area_id :selected').val();
        layer_id=$('#layer_id :selected').val();
        seat_id=$('#seat_id :selected').val();
        if(typeof(area_id)=='undefined'||typeof(layer_id)=='undefined'||typeof(seat_id)=='undefined'){
            alert('請選擇區域、層、位置');
            return '';
        }
        $.ajax({
            url: '{{route('BackTombstone.CreateTombstone')}}',
            data:{
                area:area_id,
                layer:layer_id,
                seat:seat_id,
            },
            type:"get",
            dataType:'html',
            success: function(data){
                if(data=='已經存在'){
                    alert(data);
                }else{
                    location.reload();
                }
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                alert('錯誤');
            }
        });
    }
    $("#fool").change(function(){
      if($("#fool").val()!=1){
        $("#area_id option").hide();
        $("#layer_id  option").hide();
        $("#row_id  option").hide();
        $("#seat_id  option").hide();
      }else{
        $("#area_id option").show();
      }
    });

</script>
@stop