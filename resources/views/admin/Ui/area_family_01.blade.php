﻿<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>宜蘭縣壯圍鄉生命紀念館</title>
<link href="{{asset('css/boilerplate.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/the-modal.css')}}"  rel="stylesheet" type="text/css" media="all">
<!-- 
若要深入了解檔案頂端 html 標籤周圍的條件式註解:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

如果您使用自訂的 Modernizr 組建 (http://www.modernizr.com/)，請執行下列動作:
* 在這裡將連結插入您的 js
* 將下列連結移至 html5shiv
* 將「no-js」類別新增至頂端的 html 標籤
* 如果您在 Modernizr 組建中包含 MQ Polyfill，也可以將連結移至 respond.min.js 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="{{asset('js/jquery-latest.min.js')}}"></script>
<script src="{{asset('js/respond.min.js')}}"></script>
<script src="{{asset('js/jquery.the-modal.js')}}"></script>

<!--彈跳視窗-->
<script type="text/javascript">
jQuery(function($){
      // bind event handlers to modal triggers
      $('body').on('click', '.trigger', function(e){
        e.preventDefault();
        code=$(this).attr('code');
        $.ajax({
            url: '{{route('api_select_data_s')}}',
            data:{code:code},
            type:"get",
            dataType:'html',
            success: function(data){
                $('#tab').html(data);
                $('#test-modal').modal().open();
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                alert('查無資料');
             }
        });

      });
      // attach modal close handler
      $('.modal .close , .modal .close2').on('click', function(e){
        e.preventDefault();
        $.modal().close();
      });

      // below isn't important (demo-specific things)
      $('.modal .more-toggle').on('click', function(e){
        e.stopPropagation();
        $('.modal .more').toggle();
      });
});
</script>


</head>
<body>
<div class="modal more_width" id="test-modal" style="display: none">            
    <div class="more">
      <div class="model_title"><a href="#" class="close">X</a></div>
      <div class="window_info">
           <div class="window_table">
                   <ul class="tab" id="tab">
                       
                    </ul>
                </div>
      </div>
      <div class="clear_line"></div>
    </div>
</div>



<div class="gridContainer clearfix">
  <header id="header">
    <div class="index_title">
        <div class="f_number">1</div>
        <div class="f_area"><div>樓</div>
        <div class="f_sec">家族{{$class}}區</div><div class="f_sec1">第{{$layer}}排</div></div>
        <div class="clear_line"></div>
        <div class="back"><a href="{{asset('ui/level_1.html')}}"></a></div>
        <div class="clear_line"></div>
    </div>
        
    <div class="remark_area2">
        <ul class="select_area_j">
          <li class="a_01"><em class="a_title5">5排</em><a href="{{asset('BackUi/J-5/edit')}}"></a></li>
          <li class="a_02"><em class="a_title4">4排</em><a href="{{asset('BackUi/J-4/edit')}}"></a></li>
          <li class="a_03"><em class="a_title3">3排</em><a href="{{asset('BackUi/J-3/edit')}}"></a></li>
          <li class="a_04"><em class="a_title2">2排</em><a href="{{asset('BackUi/J-2/edit')}}"></a></li>
          <li class="a_05"><em class="a_title1">1排</em><a href="{{asset('BackUi/J-1/edit')}}"></a></li>
          <div class="clear_line"></div>
        </ul>
    </div>
    
    <div class="sec_title"><h2>格位選擇</h2>
       <ul>
          <li><span class="color_area O"></span><span class="color_word">已售出</span></li>
          <li><span class="color_area P"></span><span class="color_word">未售出</span></li>
          <div class="clear_line"></div>
        </ul>
      
    </div>
    <div class="clear_line"></div>
  </div>
  <article id="inside_area">
    <div class="level_sec">
       <div class="a_area">
         <div class="family_area">
         <ul class="a02">
            <?php krsort($data)?>
           @foreach($data as $key =>$value)
           <li><div class="f_area_title1">{{$key}}棟</div></li>
           @endforeach 
         </ul>
         <div class="clear_line"></div>
         <ul class="a02">
           @foreach($data as $key =>$value)
           <li><a @if($value[1]->c_no!=null) class="stop trigger" @else class=" trigger" @endif  code="{{substr($value[1]->code,0,-2)}}"y  href=""></a><font>{{substr($value[1]->code,0,-2)}}</font></li>

           @endforeach 
           <li><div class="k_area_arrew"></div></li>
         </ul>
         <div class="clear_line"></div>
    
         <div class="clear_line"></div>
         </div>
         
         <div class="clear_line"></div>
      </div>
    </div>
  </article>
</div>
 </body>
</html>
