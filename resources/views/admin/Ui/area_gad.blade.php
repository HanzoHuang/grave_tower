<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>宜蘭縣壯圍鄉生命紀念館</title>
<link href="{{asset('css/boilerplate.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/the-modal.css')}}"  rel="stylesheet" type="text/css" media="all">
<!-- 
若要深入了解檔案頂端 html 標籤周圍的條件式註解:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

如果您使用自訂的 Modernizr 組建 (http://www.modernizr.com/)，請執行下列動作:
* 在這裡將連結插入您的 js
* 將下列連結移至 html5shiv
* 將「no-js」類別新增至頂端的 html 標籤
* 如果您在 Modernizr 組建中包含 MQ Polyfill，也可以將連結移至 respond.min.js 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="{{asset('js/jquery-latest.min.js')}}"></script>
<script src="{{asset('js/respond.min.js')}}"></script>
<script src="{{asset('js/jquery.the-modal.js')}}"></script>

<!--彈跳視窗-->



</head>
<body>
<div class="modal more_width" id="test-modal" style="display: none">            
    <div class="more">
      <div class="model_title"><a class="close">X</a></div>
      <div class="window_info">
           <div class="window_table">
                   <ul class="tab" id="tab">
                       
                    </ul>
                </div>
      </div>
      <div class="clear_line"></div>
    </div>
</div>


<div class="gridContainer clearfix">
  <header id="header">
    <div class="index_title">
        <div class="f_number">1</div>
        <div class="f_area"><div>樓</div><div class="f_sec">牌位南區</div><div class="f_sec1"></div></div>
        <div class="clear_line"></div>
        <div class="back"><a href="{{asset('ui/level_1.html')}}"></a></div>
        <div class="clear_line"></div></div>
        
    <div class="remark_area2">
    </div>
    
    <div class="sec_title"><h2>格位選擇</h2>
       <ul>
          <li><span class="color_area O"></span><span class="color_word">已售出</span></li>
          <li><span class="color_area P"></span><span class="color_word">未售出</span></li>
        </ul>
      
    </div>
    <div class="clear_line"></div>
  </div>
  <article id="inside_area">
    <div class="level_sec">
       <div class="a_area">
          <?php krsort($data)?>
           @foreach($data as $key =>$value)
           <ul class="a03">
             <li><div class="a_area_title1">{{$key}}層</div></li>
             @foreach($data[$key] as $key2 =>$value2)
             <li><a @if($value2->c_no!=null)  class="stop trigger" @else class="trigger" @endif  style="
    margin-left: 5px;
    margin-right: 5px;
" code="{{$value2->code}}">{{$key2}}</a><font>{{$value2->code}}</font></li>
             @endforeach
            <div class="clear_line"></div>
            </ul>
         @endforeach
      </div>
    </div>
  </article>
</div>
<script type="text/javascript">
jQuery(function(){
      // bind event handlers to modal triggers
      $('body').on('click', '.trigger', function(e){
        // e.preventDefault();
        code=$(this).attr('code');
        $.ajax({
            url: '{{route('api_select_data_t')}}',
            data:{code:code},
            type:"get",
            dataType:'html',
            success: function(data){
                $('#tab').html(data);
                $('#test-modal').modal().open();
            },
            beforeSend:function(){
                
            },
            complete:function(){
                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                alert('查無資料');
             }
        });

      });
      // attach modal close handler
      $('.modal .close , .modal .close2').on('click', function(e){
        e.preventDefault();
        $.modal().close();
      });

      // below isn't important (demo-specific things)
      $('.modal .more-toggle').on('click', function(e){
        e.stopPropagation();
        $('.modal .more').toggle();
      });
});
</script>
</body>
</html>
