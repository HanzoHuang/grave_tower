@foreach($class as $key =>$value)
    <option value="{{$value->id}}" code="{{$value->code}}" class_name="{{$value->name}}">{{$value->code.' '.$value->name}} 
    	@if(!empty($value->position))
    		@if($value->position==1)
    		座北朝南
    		@elseif($value->position==2)
    		座南朝北
    		@elseif($value->position==3)
    		座東朝西
    		@elseif($value->position==4)
    		座西朝東
    		@endif
    	@endif
    	@if(!empty($value->deleted_at))
    		關閉
    	@endif
    </option>
@endforeach