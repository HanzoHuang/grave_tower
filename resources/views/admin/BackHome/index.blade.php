@extends('layouts.admin.master')
@section('title','後臺管理系統')
@section('content')

	<div class="col-md-4">
		<div data-pages="portlet" class="panel panel-default" id="portlet-basic">
			<div class="panel-heading ">
				<div class="panel-title">
					系統廠商聯絡資訊
				
				</div>
				<div class="panel-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="portlet-collapse" href="#"><i class="portlet-icon portlet-icon-collapse"></i></a>
						</li>
						<li>
							<a data-toggle="refresh" class="portlet-refresh" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
						</li>
						<li>
							<a data-toggle="close" class="portlet-close" href="#"><i class="portlet-icon portlet-icon-close"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
				<h3><span class="semi-bold"></span></h3>
				永廷科技有限公司<br>
				服務電話：04-22376009<br>
				服務信箱：service@yourteam.tw<br>
				LINE帳號：@cjc1806b<br>
				公司地址：台中市北區錦祥里進化路577-8號二樓之七<br>
			
			</div>
		</div>
	</div>
	<br>
	@if (session('status'))
	    <div class="alert alert-success">
	        {!! session('status') !!}
	    </div>
	@endif
@stop
