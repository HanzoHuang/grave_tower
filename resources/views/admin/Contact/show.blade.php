@extends('layouts.admin.master')
@section('title','後臺帳號管理')
@section('content')
{!!	Html::style('Plugin/farbtastic/farbtastic.css')  !!}
{!!	Html::script('Plugin/farbtastic/farbtastic.js')  !!}
<div class="container-fluid container-fixed-lg">
	<div class="panel panel-transparent">
		<div>
			<div class="row">
				<div class="col-sm-10">
					<h3>線上客服內容</h3><p><hr></p><br>
						<input name="_token" type="hidden" value="{{ Session::token() }}">
						<div class="form-group">
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">編號：</label>
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">{{$Data->id}}</label>
						</div>
						<br><br>
						<div class="form-group">
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">姓名：</label>
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">{{$Data->name}}</label>
						</div>
						<br><br>
						<div class="form-group">
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">電話：</label>
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">{{$Data->tel}}</label>
						</div>
						<br><br>
						<div class="form-group">
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">EMail：</label>
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">{{$Data->email}}</label>
						</div>
						<br><br>
						<div class="form-group">
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">留言內容：</label>
							<label for="fname" class="col-sm-2 control-label" style="font-size:16px;">{{$Data->body}}</label>
						</div>
						<br><br>

						<input class="" placeholder="" name="id" type="hidden" value="<?php if(isset($template_data)) echo $template_data->id;?>">
						<div class="row">
							<div class="col-sm-9">
								<button class="btn btn-success" type="button" onclick="javascript:backList();">回列表</button>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function backList(){
	location.href="<?php echo $_SERVER['HTTP_REFERER']; ?>";
}

</script>
@stop

