<html>
 <head>
  <style type="text/css">
    body {
        height: 842px;
        width: 595px;
        /* to centre page on screen*/
        margin-left: auto;
        margin-right: auto;
    }
    table {
        border-collapse: collapse;
    }
     
    table, tr, td {
        border: 1px solid black;
    }
    td {
        text-align: center;
        height: 26px; /*这里需要自己调整，根据自己的需求调整高度*/
        position: relative;
        font-size: 10px;
    }
    td[class=first]{
        width: 100px;
    }
    td[class=first]:before {
        content: "";
        position: absolute;
        width: 1px;
        height: 104px;
        top: 0;
        left: 0;
        background-color: #000;
        display: block;
        transform: rotate(-75deg);
        transform-origin: top;
        -ms-transform: rotate(-75deg);
        -ms-transform-origin: top;
    }
    td[class=first]:after {
        content: "";
        position: absolute;
        width: 1px;
        height: 75px;
        top: 0;
        left: 0;
        background-color: #000;
        display: block;
        transform: rotate(-45deg);
        transform-origin: top;
        -ms-transform: rotate(-45deg);
        -ms-transform-origin: top;
    }
    .title1{
        position: absolute;
        top: 0px;
        right:0px;
    }
    .title2{
        position: absolute;
        top: 26px;
        right:0px;
    }
    .title3{
        position: absolute;
        top: 30px;
        left:0px;
    }
</style> 
 </head>
 <body>
 <?php
$date = new DateTime("now");
$date->modify("-1911 year")
 ?>
  <div align="center"> 
    <p> 壯圍鄉生命紀念館107年各類櫃位及牌位使用情形及數量統計</p>
    <p>{{$serach['application_start_date']}}~{{$serach['application_end_date']}}</p>
    <p align="right" style="font-size: 10px"> 製表日期：{{ ltrim($date->format("Y-m-d"),"0")}} </p>
    <p> 櫃位使用情形及數量 </p>
   <table border="1" cellspacing="0" cellpadding="0" width="600" > 
    <tbody> 
     <tr> 
      <td rowspan="2"> 櫃位類別 </td> 
      <td class="first" rowspan="2" > 
          <span class="title1">狀況區分</span><br /> 
          <span class="title2">數量</span><br /> 
          <span class="title3">層組</span> 
      </td> 
      <td rowspan="2"> 櫃位數量 </td> 
      <td rowspan="2"> 使用數量 </td> 
      <td rowspan="2"> 剩餘數量 </td> 
      <td colspan="3"> 櫃位安置 </td> 
      <td colspan="3"> 櫃位異動 </td> 
      <td colspan="2"> 櫃位遷出 </td> 
     </tr> 
     <tr> 
      <td> 申請 </td> 
      <td> 繳費 </td> 
      <td> 進塔 </td> 
      <td> 申請 </td> 
      <td> 繳費 </td> 
      <td> 進塔 </td> 
      <td> 申請 </td> 
      <td> 遷出 </td>
     </tr> 
     <tr> 
      <td rowspan="2"> 個人 </td> 
      <td> 第一 、二層 </td> 
      <td> {{$new_data_count_amount[0]+$new_data_count_no_amount[0]}}

      </td> 
      <td>
        {{$new_data_count_amount[0]}}
      </td> 
      <td> {{$new_data_count_no_amount[0]}} </td> 
      <td>  {{($new_data[0][0])}} </td> 
      <td> {{($new_data[0][1])}} </td> 
      <td> {{($new_data[0][2])}} </td> 
      <td> {{($new_data[0][3])}} </td> 
      <td> {{($new_data[0][4])}} </td> 
      <td> {{($new_data[0][5])}} </td> 
      <td> {{($new_data[0][6])}} </td> 
      <td> {{($new_data[0][7])}} </td> 
     </tr> 
     <tr> 
      <td> 第三 ~ 八層 </td> 
      <td>  {{$new_data_count_amount[1]+$new_data_count_no_amount[1]}} </td> 
      <td>  {{$new_data_count_amount[1]}} </td> 
      <td>  {{$new_data_count_no_amount[1]}} </td> 
      <td>  {{($new_data[1][0])}} </td> 
      <td> {{($new_data[1][1])}} </td> 
      <td> {{($new_data[1][2])}} </td> 
      <td> {{($new_data[1][3])}} </td> 
      <td> {{($new_data[1][4])}} </td> 
      <td> {{($new_data[1][5])}} </td> 
      <td> {{($new_data[1][6])}} </td> 
      <td> {{($new_data[1][7])}} </td> 
     </tr> 
     <tr> 
      <td rowspan="2"> 夫妻 </td> 
      <td> 第一 、二層 </td> 
      <td> {{$new_data_count_amount[2]+$new_data_count_no_amount[2]}} </td> 
      <td> {{$new_data_count_amount[2]}} </td> 
      <td> {{$new_data_count_no_amount[2]}} </td> 
      <td> {{($new_data[2][0])}} </td> 
      <td> {{($new_data[2][1])}} </td> 
      <td> {{($new_data[2][2])}} </td> 
      <td> {{($new_data[2][3])}} </td> 
      <td> {{($new_data[2][4])}} </td> 
      <td> {{($new_data[2][5])}} </td> 
      <td> {{($new_data[2][6])}} </td> 
      <td> {{($new_data[2][7])}} </td> 
     </tr> 
     <tr> 
      <td> 第三 ~ 八層 </td> 
      <td> {{$new_data_count_amount[3]+$new_data_count_no_amount[3]}} </td> 
      <td> {{$new_data_count_amount[3]}} </td> 
      <td> {{$new_data_count_no_amount[3]}} </td> 
      <td> {{($new_data[3][0])}} </td> 
      <td> {{($new_data[3][1])}} </td> 
      <td> {{($new_data[3][2])}} </td> 
      <td> {{($new_data[3][3])}} </td> 
      <td> {{($new_data[3][4])}} </td> 
      <td> {{($new_data[3][5])}} </td> 
      <td> {{($new_data[3][6])}} </td> 
      <td> {{($new_data[3][7])}} </td> 
     </tr> 
     <tr> 
      <td> 家族 </td> 
      <td> 整組 </td> 
      <td> {{$new_data_count_amount[4]+$new_data_count_no_amount[4]}} </td> 
      <td> {{$new_data_count_amount[4]}} </td> 
      <td> {{$new_data_count_no_amount[4]}} </td> 
      <td> {{($new_data[4][0])}} </td> 
      <td> {{($new_data[4][1])}} </td> 
      <td> {{($new_data[4][2])}} </td> 
      <td> {{($new_data[4][3])}} </td> 
      <td> {{($new_data[4][4])}} </td> 
      <td> {{($new_data[4][5])}} </td> 
      <td> {{($new_data[4][6])}} </td> 
      <td> {{($new_data[4][7])}} </td> 
     </tr> 
     <tr> 
      <td colspan="2"> 總 計 </td> 
      <td> {{
             $new_data_count_amount[0]+$new_data_count_no_amount[0]+
             $new_data_count_amount[1]+$new_data_count_no_amount[1]+
             $new_data_count_amount[2]+$new_data_count_no_amount[2]+
             $new_data_count_amount[3]+$new_data_count_no_amount[3]+
             $new_data_count_amount[4]+$new_data_count_no_amount[4]}} </td> 
      <td> {{$new_data_count_amount[0]+$new_data_count_amount[1]+$new_data_count_amount[2]+$new_data_count_amount[3]+$new_data_count_amount[4]}} </td> 
      <td> {{$new_data_count_no_amount[0]+$new_data_count_no_amount[1]+$new_data_count_no_amount[2]+$new_data_count_no_amount[3]+$new_data_count_no_amount[4]}} </td> 
      <td> {{$new_data[0][0]+$new_data[1][0]+$new_data[2][0]+$new_data[3][0]+$new_data[4][0]}} </td> 
      <td> {{$new_data[0][1]+$new_data[1][1]+$new_data[2][1]+$new_data[3][1]+$new_data[4][1]}} </td> 
      <td> {{$new_data[0][2]+$new_data[1][2]+$new_data[2][2]+$new_data[3][2]+$new_data[4][2]}} </td> 
      <td> {{$new_data[0][3]+$new_data[1][3]+$new_data[2][3]+$new_data[3][3]+$new_data[4][3]}} </td> 
      <td> {{$new_data[0][4]+$new_data[1][4]+$new_data[2][4]+$new_data[3][4]+$new_data[4][4]}} </td> 
      <td> {{$new_data[0][5]+$new_data[1][5]+$new_data[2][5]+$new_data[3][5]+$new_data[4][5]}}</td> 
      <td> {{$new_data[0][6]+$new_data[1][6]+$new_data[2][6]+$new_data[3][6]+$new_data[4][6]}} </td> 
      <td> {{$new_data[0][7]+$new_data[1][7]+$new_data[2][7]+$new_data[3][7]+$new_data[4][7]}} </td> 
     </tr> 
    </tbody> 
   </table> 
  
   <p>牌位使用情形及數量</p>
  <table border="1" cellspacing="0" cellpadding="0" width="569"> 
    <tr> 
     <td rowspan="2"> 牌位區 </td> 
     <td rowspan="2"> 牌位數量 </td> 
     <td rowspan="2"> 使用數量 </td> 
     <td rowspan="2"> 剩餘數量 </td> 
     <td colspan="3"> 牌位安置 </td> 
     <td colspan="2"> 牌位遷出 </td> 
    </tr> 
    <tr> 
     <td> 申請 </td> 
     <td> 繳費 </td> 
     <td> 進塔 </td> 
     <td> 申請 </td> 
     <td> 遷出 </td> 
    </tr> 
    <tr> 
     <td> 牌位北區 </td> 
     <td > {{$new_data_count_amount[6]+$new_data_count_no_amount[6]}} </td> 
     <td > {{$new_data_count_amount[6]}} </td> 
     <td>  {{ $new_data_count_no_amount[6]}} </td> 
     <td> {{($new_data[5][0])}} </td> 
     <td> {{($new_data[5][1])}} </td> 
     <td> {{($new_data[5][2])}} </td> 
     <td> {{($new_data[5][6])}} </td> 
     <td> {{($new_data[5][7])}} </td> 
    </tr> 
    <tr> 
     <td> 牌位南區 </td> 
     <td > {{$new_data_count_amount[5]+$new_data_count_no_amount[5]}} </td> 
     <td > {{$new_data_count_amount[5]}} </td> 
     <td>  {{ $new_data_count_no_amount[5]}} </td> 
     <td> {{($new_data[6][0])}} </td> 
     <td> {{($new_data[6][1])}} </td> 
     <td> {{($new_data[6][2])}} </td> 
     <td> {{($new_data[6][6])}} </td> 
     <td> {{($new_data[6][7])}} </td> 
    </tr> 
    <tr> 
     <td> 總 計 </td> 
     <td> {{
             $new_data_count_amount[5]+$new_data_count_no_amount[5]+
             $new_data_count_amount[6]+$new_data_count_no_amount[6]}} </td> 
     <td> {{$new_data_count_amount[5]+$new_data_count_amount[6]}} </td> 
     <td> {{$new_data_count_no_amount[5]+$new_data_count_no_amount[6]}} </td> 
     <td> {{$new_data[5][0]+$new_data[6][0]}} </td> 
      <td> {{$new_data[5][1]+$new_data[6][1]}} </td> 
      <td> {{$new_data[5][2]+$new_data[6][2]}} </td> 
      <td> {{$new_data[5][6]+$new_data[6][6]}} </td> 
      <td> {{$new_data[5][7]+$new_data[6][7]}} </td> 
    </tr> 
  </table>
  </div> 
 </body>
</html>