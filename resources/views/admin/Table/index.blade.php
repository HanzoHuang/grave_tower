<style type="text/css">
    
    table {
        border-collapse: collapse;
    }
     
    table, tr, td {
        border: 1px solid black;
    }
    td {
        text-align: center;
        height: 60px; /*这里需要自己调整，根据自己的需求调整高度*/
        position: relative;
        font-size: 18px;
    }
    td[class=first]{
        width: 100px;
    }
    td[class=first]:before {
        content: "";
        position: absolute;
        width: 1px;
        height: 125px;
        top: 0;
        left: 0;
        background-color: #000;
        display: block;
        transform: rotate(-75deg);
        transform-origin: top;
        -ms-transform: rotate(-75deg);
        -ms-transform-origin: top;
    }
    td[class=first]:after {
        content: "";
        position: absolute;
        width: 1px;
        height: 85px;
        top: 0;
        left: 0;
        background-color: #000;
        display: block;
        transform: rotate(-45deg);
        transform-origin: top;
        -ms-transform: rotate(-45deg);
        -ms-transform-origin: top;
    }
    .title1{
        position: absolute;
        top: 0px;
        right:0px;
    }
    .title2{
        position: absolute;
        top: 36px;
        right:0px;
    }
    .title3{
        position: absolute;
        top: 30px;
        left:0px;
    }
</style> 
<p align="center">
    壯圍鄉生命紀念館107年各項收入金額統計
</p>
<p align="center">{{$serach['application_start_date']}}~{{$serach['application_end_date']}}</p>
<p align="right">
    製表日期：{{date('Y')-1911}}/{{date('m/d')}}
</p>
    <?php 
    $sum1=0;
    $sum2=0;
    $sum3=0;
    $sum4=0;
    $sum5=0;
    $sum6=0;
    $sum7=0;
    $sum8=0;
    $sum9=0;
    $sum10=0;
    $sum11=0;
    $sum12=0;
    $sum13=0;
    $sum14=0;
    $sum15=0;
    $sum16=0;
    $sum17=0;
    $sum18=0;
    $sum19=0;
    $sum20=0;
    $sum21=0;
    $sum22=0;
    $sum23=0;
    $sum24=0;
    $sum25=0;
    $sum26=0;
    $sum27=0;
    $sum28=0;
    $sum29=0;
    $sum30=0;
    $sum31=0;
    $sum32=0;
    $sum33=0;
    $sum34=0;
    $sum35=0;
    $sum36=0;
    ?>
<div align="center">
    <table border="1" cellspacing="0" cellpadding="0" width="1467">
        <tbody>
            <tr>
                <td width="113">
                    <p align="center">
                        櫃位類別
                    </p>
                </td>
                <td class="first""> 
                  <span class="title1">身份</span><br /> 
                  <span class="title2">金額</span><br /> 
                  <span class="title3">層組</span> 
                </td>
                <td width="227" colspan="3">
                    <p align="center">
                        本鄉鄉民
                    </p>
                </td>
                <td width="255" colspan="3">
                    <p align="center">
                        本縣縣民
                    </p>
                </td>
                <td width="255" colspan="3">
                    <p align="center">
                        外縣市民
                    </p>
                </td>
                <td width="236" colspan="3">
                    <p align="center">
                        本年度累計
                    </p>
                    <p align="center">
                        金額
                    </p>
                </td>
                <td width="258" colspan="3">
                    <p align="center">
                        總累計
                    </p>
                    <p align="center">
                        金額
                    </p>
                </td>
            </tr>
            <tr>
                <td width="113">
                </td>
                <td width="123">
                </td>
                <td width="76">
                    <p align="center">
                        櫃位
                    </p>
                </td>
                <td width="76">
                    <p align="center">
                        換位
                    </p>
                </td>
                <td width="76">
                    <p align="center">
                        其他
                    </p>
                </td>
                <td width="85">
                    <p align="center">
                        櫃位
                    </p>
                </td>
                <td width="85">
                    <p align="center">
                        換位
                    </p>
                </td>
                <td width="85">
                    <p align="center">
                        其他
                    </p>
                </td>
                <td width="85">
                    <p align="center">
                        櫃位
                    </p>
                </td>
                <td width="85">
                    <p align="center">
                        換位
                    </p>
                </td>
                <td width="85">
                    <p align="center">
                        其他
                    </p>
                </td>
                <td width="85">
                    <p align="center">
                        櫃位
                    </p>
                </td>
                <td width="76">
                    <p align="center">
                        換位
                    </p>
                </td>
                <td width="76">
                    <p align="center">
                        其他
                    </p>
                </td>
                <td width="95">
                    <p align="center">
                        櫃位
                    </p>
                </td>
                <td width="85">
                    <p align="center">
                        換位
                    </p>
                </td>
                <td width="78">
                    <p align="center">
                        其他
                    </p>
                </td>
            </tr>
            <tr>
                <td width="113" rowspan="2">
                    <p align="center">
                        個人骨灰櫃位
                    </p>
                </td>
                <td width="123">
                    <p align="center">
                        第一 、二層
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['200']['0']['cost']))
                        {{number_format($new_data['S']['1-2']['200']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>               

                <td width="76">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['500']['0']['cost']))
                        {{number_format($new_data['S']['1-2']['500']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                                

                <td width="76">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['other_total']['0']['other_amount_due']))

                        {{number_format($new_data['S']['1-2']['other_total']['0']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['200']['1']['cost']))
                        {{number_format($new_data['S']['1-2']['200']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['500']['1']['cost']))
                        {{number_format($new_data['S']['1-2']['500']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['other_total']['1']['other_amount_due']))
                        {{number_format($new_data['S']['1-2']['other_total']['1']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['200']['2']['cost']))
                        {{number_format($new_data['S']['1-2']['200']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['500']['2']['cost']))
                        {{number_format($new_data['S']['1-2']['500']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['1-2']['other_total']['2']['other_amount_due']))
                        {{number_format($new_data['S']['1-2']['other_total']['2']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_year['S']['1-2']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum1=$sum1+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum1}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['S']['1-2']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum2=$sum2+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum2}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['S']['1-2']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum3=$sum3+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum3}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_total['S']['1-2']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum4=$sum4+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum4}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['S']['1-2']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum5=$sum5+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum5}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['S']['1-2']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum6=$sum6+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum6}}
                    </p>
                </td>
            </tr>
            <tr>
                <td width="123">
                    <p align="center">
                        第三 ~ 八層
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @if(isset($new_data['S']['3-8']['200']['0']['cost']))
                        {{number_format($new_data['S']['3-8']['200']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="76">
                    <p align="right">

                        @if(isset($new_data['S']['3-8']['500']['0']['cost']))
                        {{number_format($new_data['S']['3-8']['500']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @if(isset($new_data['S']['3-8']['other_total']['0']['other_amount_due']))
                        {{number_format($new_data['S']['3-8']['other_total']['0']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['3-8']['200']['1']['cost']))
                        {{number_format($new_data['S']['3-8']['200']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['3-8']['500']['1']['cost']))
                        {{number_format($new_data['S']['3-8']['500']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['3-8']['other_total']['1']['other_amount_due']))
                        {{number_format($new_data['S']['3-8']['other_total']['1']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>

                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['3-8']['200']['2']['cost']))
                        {{number_format($new_data['S']['3-8']['200']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['3-8']['500']['2']['cost']))
                        {{number_format($new_data['S']['3-8']['500']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['S']['3-8']['other_total']['2']['other_amount_due']))
                        {{number_format($new_data['S']['3-8']['other_total']['2']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_year['S']['3-8']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum7=$sum7+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum7}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['S']['3-8']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum8=$sum8+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum8}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['S']['3-8']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum9=$sum9+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum9}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_total['S']['3-8']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum10=$sum10+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum10}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['S']['3-8']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum11=$sum11+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum11}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['S']['3-8']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum12=$sum12+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum12}}
                    </p>
                </td>
            </tr>
            <tr>
                <td width="113" rowspan="2">
                    <p align="center">
                        夫妻骨灰櫃位
                    </p>
                </td>
                <td width="123">
                    <p align="center">
                        第一 、二層
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['200']['0']['cost']))
                        {{number_format($new_data['C']['1-2']['200']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>               

                <td width="76">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['500']['0']['cost']))
                        {{number_format($new_data['C']['1-2']['500']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                                

                <td width="76">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['other_total']['0']['other_amount_due']))
                        {{number_format($new_data['C']['1-2']['other_total']['0']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['200']['1']['cost']))
                        {{number_format($new_data['C']['1-2']['200']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['500']['1']['cost']))
                        {{number_format($new_data['C']['1-2']['500']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['other_total']['1']['other_amount_due']) )
                        {{number_format($new_data['C']['1-2']['other_total']['1']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['200']['2']['cost']))
                        {{number_format($new_data['C']['1-2']['200']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['500']['2']['cost']))
                        {{number_format($new_data['C']['1-2']['500']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['1-2']['other_total']['2']['other_amount_due']))
                        {{number_format($new_data['C']['1-2']['other_total']['2']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_year['C']['1-2']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum13=$sum13+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum13}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['C']['1-2']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum14=$sum14+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum14}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['C']['1-2']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum15=$sum15+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum15}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_total['C']['1-2']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum16=$sum16+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum16}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['C']['1-2']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum17=$sum17+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum17}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['C']['1-2']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum18=$sum18+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum18}}
                    </p>
                </td>
            </tr>
            <tr>
                <td width="123">
                    <p align="center">
                        第三 ~ 八層
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['200']['0']['cost']))
                        {{number_format($new_data['C']['3-8']['200']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>               

                <td width="76">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['500']['0']['cost']))
                        {{number_format($new_data['C']['3-8']['500']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                                

                <td width="76">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['other_total']['0']['other_amount_due']))
                        {{number_format($new_data['C']['3-8']['other_total']['0']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['200']['1']['cost']))
                        {{number_format($new_data['C']['3-8']['200']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['500']['1']['cost']))
                        {{number_format($new_data['C']['3-8']['500']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['other_total']['1']['other_amount_due']))
                        {{number_format($new_data['C']['3-8']['other_total']['1']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['200']['2']['cost']))
                        {{number_format($new_data['C']['3-8']['200']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['500']['2']['cost']))
                        {{number_format($new_data['C']['3-8']['500']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['C']['3-8']['other_total']['2']['other_amount_due']))
                        {{number_format($new_data['C']['3-8']['other_total']['2']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_year['C']['3-8']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum19=$sum19+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum19}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['C']['3-8']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum20=$sum20+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum20}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['C']['3-8']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum21=$sum21+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum21}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_total['C']['3-8']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum22=$sum22+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum22}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['C']['3-8']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum23=$sum23+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum23}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['C']['3-8']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum24=$sum24+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum24}}
                    </p>
                </td>
            </tr>
            <tr>
                <td width="113">
                    <p align="center">
                        家族骨灰櫃位
                    </p>
                </td>
                <td width="123">
                    <p align="center">
                        整組
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['200']['0']['cost']))
                        {{number_format($new_data['F']['3-8']['200']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>               

                <td width="76">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['500']['0']['cost']))
                        {{number_format($new_data['F']['3-8']['500']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                                

                <td width="76">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['other_total']['0']['other_amount_due']))
                        {{number_format($new_data['F']['3-8']['other_total']['0']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['200']['1']['cost']))
                        {{number_format($new_data['F']['3-8']['200']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['500']['1']['cost']))
                        {{number_format($new_data['F']['3-8']['500']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['other_total']['1']['other_amount_due']) )
                        {{number_format($new_data['F']['3-8']['other_total']['1']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['200']['2']['cost']))
                        {{number_format($new_data['F']['3-8']['200']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['500']['2']['cost']))
                        {{number_format($new_data['F']['3-8']['500']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data['F']['3-8']['other_total']['2']['other_amount_due']) )
                        {{number_format($new_data['F']['3-8']['other_total']['2']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_year['F']['3-8']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum25=$sum25+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum25}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['F']['3-8']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum26=$sum26+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum26}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_year['F']['3-8']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum27=$sum27+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum27}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data_total['F']['3-8']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum28=$sum28+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum28}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['F']['3-8']['500'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum29=$sum29+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum29}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data_total['F']['3-8']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum30=$sum30+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum30}}
                    </p>
                </td>
            </tr>
            <tr>
                <td width="113">
                    <p align="center">
                        牌位
                    </p>
                </td>
                <td width="123">
                    <p align="center">
                        整付
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['200']['0']['cost']))
                        {{number_format($new_data2['T']['3-8']['200']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>               

                <td width="76">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['500']['0']['cost']))
                        {{number_format($new_data2['T']['3-8']['500']['0']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                                

                <td width="76">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['other_total']['0']['other_amount_due']))
                        {{number_format($new_data2['T']['3-8']['other_total']['0']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['200']['1']['cost']))
                        {{number_format($new_data2['T']['3-8']['200']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['500']['1']['cost']))
                        {{number_format($new_data2['T']['3-8']['500']['1']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['other_total']['1']['other_amount_due']) )
                        {{number_format($new_data2['T']['3-8']['other_total']['1']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['200']['2']['cost']))
                        {{number_format($new_data2['T']['3-8']['200']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['500']['2']['cost']))
                        {{number_format($new_data2['T']['3-8']['500']['2']['cost'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @if(isset($new_data2['T']['3-8']['other_total']['2']['other_amount_due']) )
                        {{number_format($new_data2['T']['3-8']['other_total']['2']['other_amount_due'])}}
                        @else
                        0
                        @endif
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data2_year['T']['3-8']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum31=$sum31+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum31}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        
                        {{$sum32}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data2_year['T']['3-8']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum33=$sum33+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum33}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        @foreach($new_data2_total['T']['3-8']['200'] as $value)
                                @if(isset($value['cost']))
                                    <?php $sum34=$sum34+$value['cost']?>
                                @endif
                        @endforeach
                        {{$sum34}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        {{$sum35}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        @foreach($new_data2_total['T']['3-8']['other_total'] as $value)
                            @if(isset($value['other_amount_due']))
                                <?php $sum36=$sum36+$value['other_amount_due']?>
                            @endif
                        @endforeach
                        {{$sum36}}
                    </p>
                </td>
            </tr>
            <tr>
                <td width="236" colspan="2">
                    <p align="center">
                        小 計
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        <?PHP $A=0?>
                        @if(isset($new_data['S']['1-2']['200']['0']['cost']))
                            <?php $A=$A+$new_data['S']['1-2']['200']['0']['cost']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['200']['0']['cost']))
                            <?php $A=$A+$new_data['S']['3-8']['200']['0']['cost']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['200']['0']['cost']))
                            <?php $A=$A+$new_data['C']['1-2']['200']['0']['cost']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['200']['0']['cost']))
                            <?php $A=$A+$new_data['C']['3-8']['200']['0']['cost']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['200']['0']['cost']))
                            <?php $A=$A+$new_data['F']['3-8']['200']['0']['cost']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['200']['0']['cost']))
                            <?php $A=$A+$new_data2['T']['3-8']['200']['0']['cost']?>
                        @endif
                        {{number_format($A)}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                         <?PHP $B=0?>
                        @if(isset($new_data['S']['1-2']['500']['0']['cost']))
                            <?php $B=$B+$new_data['S']['1-2']['500']['0']['cost']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['500']['0']['cost']))
                            <?php $B=$B+$new_data['S']['3-8']['500']['0']['cost']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['500']['0']['cost']))
                            <?php $B=$B+$new_data['C']['1-2']['500']['0']['cost']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['500']['0']['cost']))
                            <?php $B=$B+$new_data['C']['3-8']['500']['0']['cost']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['500']['0']['cost']))
                            <?php $B=$B+$new_data['F']['3-8']['500']['0']['cost']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['500']['0']['cost']))
                            <?php $B=$B+$new_data2['T']['3-8']['500']['0']['cost']?>
                        @endif
                        {{number_format($B)}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        <?PHP $C=0?>
                        @if(isset($new_data['S']['1-2']['other_total']['0']['other_amount_due']))
                            <?php $C=$C+$new_data['S']['1-2']['other_total']['0']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['other_total']['0']['other_amount_due']))
                            <?php $C=$C+$new_data['S']['3-8']['other_total']['0']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['other_total']['0']['other_amount_due']))
                            <?php $C=$C+$new_data['C']['1-2']['other_total']['0']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['other_total']['0']['other_amount_due']))
                            <?php $C=$C+$new_data['C']['3-8']['other_total']['0']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['other_total']['0']['other_amount_due']))
                            <?php $C=$C+$new_data['F']['3-8']['other_total']['0']['other_amount_due']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['other_total']['0']['other_amount_due']))
                            <?php $C=$C+$new_data2['T']['3-8']['other_total']['0']['other_amount_due']?>
                        @endif
                        {{number_format($C)}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        <?PHP $D=0?>
                        @if(isset($new_data['S']['1-2']['200']['1']['cost']))
                            <?php $D=$D+$new_data['S']['1-2']['200']['1']['cost']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['200']['1']['cost']))
                            <?php $D=$D+$new_data['S']['3-8']['200']['1']['cost']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['200']['1']['cost']))
                            <?php $D=$D+$new_data['C']['1-2']['200']['1']['cost']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['200']['1']['cost']))
                            <?php $D=$D+$new_data['C']['3-8']['200']['1']['cost']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['200']['1']['cost']))
                            <?php $D=$D+$new_data['F']['3-8']['200']['1']['cost']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['200']['1']['cost']))
                            <?php $D=$D+$new_data2['T']['3-8']['200']['1']['cost']?>
                        @endif
                        {{number_format($D)}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        <?PHP $E=0?>
                        @if(isset($new_data['S']['1-2']['500']['1']['cost']))
                            <?php $E=$E+$new_data['S']['1-2']['500']['1']['cost']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['500']['1']['cost']))
                            <?php $E=$E+$new_data['S']['3-8']['500']['1']['cost']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['500']['1']['cost']))
                            <?php $E=$E+$new_data['C']['1-2']['500']['1']['cost']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['500']['1']['cost']))
                            <?php $E=$E+$new_data['C']['3-8']['500']['1']['cost']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['500']['1']['cost']))
                            <?php $E=$E+$new_data['F']['3-8']['500']['1']['cost']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['500']['1']['cost']))
                            <?php $E=$E+$new_data2['T']['3-8']['500']['1']['cost']?>
                        @endif
                        {{number_format($E)}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        <?PHP $F=0?>
                        @if(isset($new_data['S']['1-2']['other_total']['1']['other_amount_due']))
                            <?php $F=$F+$new_data['S']['1-2']['other_total']['1']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['other_total']['1']['other_amount_due']))
                            <?php $F=$F+$new_data['S']['3-8']['other_total']['1']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['other_total']['1']['other_amount_due']))
                            <?php $F=$F+$new_data['C']['1-2']['other_total']['1']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['other_total']['1']['other_amount_due']))
                            <?php $F=$F+$new_data['C']['3-8']['other_total']['1']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['other_total']['1']['other_amount_due']))
                            <?php $F=$F+$new_data['F']['3-8']['other_total']['1']['other_amount_due']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['other_total']['1']['other_amount_due']))
                            <?php $F=$F+$new_data2['T']['3-8']['other_total']['1']['other_amount_due']?>
                        @endif
                        {{number_format($F)}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        <?PHP $G=0?>
                        @if(isset($new_data['S']['1-2']['200']['2']['cost']))
                            <?php $G=$G+$new_data['S']['1-2']['200']['2']['cost']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['200']['2']['cost']))
                            <?php $G=$G+$new_data['S']['3-8']['200']['2']['cost']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['200']['2']['cost']))
                            <?php $G=$G+$new_data['C']['1-2']['200']['2']['cost']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['200']['2']['cost']))
                            <?php $G=$G+$new_data['C']['3-8']['200']['2']['cost']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['200']['2']['cost']))
                            <?php $G=$G+$new_data['F']['3-8']['200']['2']['cost']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['200']['2']['cost']))
                            <?php $G=$G+$new_data2['T']['3-8']['200']['2']['cost']?>
                        @endif
                        {{number_format($G)}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        <?PHP $H=0?>
                        @if(isset($new_data['S']['1-2']['500']['2']['cost']))
                            <?php $H=$H+$new_data['S']['1-2']['500']['2']['cost']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['500']['2']['cost']))
                            <?php $H=$H+$new_data['S']['3-8']['500']['2']['cost']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['500']['2']['cost']))
                            <?php $H=$H+$new_data['C']['1-2']['500']['2']['cost']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['500']['2']['cost']))
                            <?php $H=$H+$new_data['C']['3-8']['500']['2']['cost']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['500']['2']['cost']))
                            <?php $H=$H+$new_data['F']['3-8']['500']['2']['cost']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['500']['2']['cost']))
                            <?php $H=$H+$new_data2['T']['3-8']['500']['2']['cost']?>
                        @endif
                        {{number_format($H)}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        <?PHP $I=0?>
                        @if(isset($new_data['S']['1-2']['other_total']['2']['other_amount_due']))
                            <?php $I=$I+$new_data['S']['1-2']['other_total']['2']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['S']['3-8']['other_total']['2']['other_amount_due']))
                            <?php $I=$I+$new_data['S']['3-8']['other_total']['2']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['C']['1-2']['other_total']['2']['other_amount_due']))
                            <?php $I=$I+$new_data['C']['1-2']['other_total']['2']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['C']['3-8']['other_total']['2']['other_amount_due']))
                            <?php $I=$I+$new_data['C']['3-8']['other_total']['2']['other_amount_due']?>
                        @endif
                        @if(isset($new_data['F']['3-8']['other_total']['2']['other_amount_due']))
                            <?php $I=$I+$new_data['F']['3-8']['other_total']['2']['other_amount_due']?>
                        @endif
                        @if(isset($new_data2['T']['3-8']['other_total']['2']['other_amount_due']))
                            <?php $I=$I+$new_data2['T']['3-8']['other_total']['2']['other_amount_due']?>
                        @endif
                        {{number_format($I)}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        {{number_format($sum1+$sum7+$sum13+$sum19+$sum25+$sum31)}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        {{number_format($sum2+$sum8+$sum14+$sum20+$sum26+$sum32)}}
                    </p>
                </td>
                <td width="76">
                    <p align="right">
                        {{number_format($sum3+$sum9+$sum15+$sum21+$sum27+$sum33)}}
                    </p>
                </td>
                <td width="95">
                    <p align="right">
                        {{number_format($sum4+$sum10+$sum16+$sum22+$sum28+$sum34)}}
                    </p>
                </td>
                <td width="85">
                    <p align="right">
                        {{number_format($sum5+$sum11+$sum17+$sum23+$sum29+$sum35)}}
                    </p>
                </td>
                <td width="78">
                    <p align="right">
                        {{number_format($sum6+$sum12+$sum18+$sum24+$sum30+$sum36)}}
                    </p>
                </td>
            </tr>
            <tr>
                <td width="236" colspan="2">
                    <p align="center">
                        合 計
                    </p>
                </td>
                <td width="737" colspan="9">
                    <p align="center">
                        {{number_format($A+$B+$C+$D+$F+$E+$G+$H+$I)}}
                    </p>
                </td>
                <td width="236" colspan="3">
                    <p align="center">
                       <?php $sum_total=0?>
                       <?php $sum_total=$sum_total+($sum1+$sum7+$sum13+$sum19+$sum25+$sum31)?>
                       <?php $sum_total=$sum_total+($sum2+$sum8+$sum14+$sum20+$sum26+$sum32)?>
                       <?php $sum_total=$sum_total+($sum3+$sum9+$sum15+$sum21+$sum27+$sum33)?>
                       {{number_format($sum_total)}}
                    </p>
                </td>
                <td width="258" colspan="3">
                    <p align="center">
                        <?php $sum_total=0?>
                       <?php $sum_total=$sum_total+($sum4+$sum10+$sum16+$sum22+$sum28+$sum34)?>
                       <?php $sum_total=$sum_total+($sum5+$sum11+$sum17+$sum23+$sum29+$sum35)?>
                       <?php $sum_total=$sum_total+($sum6+$sum12+$sum18+$sum24+$sum30+$sum36)?>
                       {{number_format($sum_total)}}
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</div>