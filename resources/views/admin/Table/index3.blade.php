<style type="text/css">
    body {
        height: 842px;
        width: 595px;
        /* to centre page on screen*/
        margin-left: auto;
        margin-right: auto;
    }
    table {
        border-collapse: collapse;
    }
     
    table, tr, td {
        border: 1px solid black;
    }
    td {
        text-align: center;
        height: 60px; /*这里需要自己调整，根据自己的需求调整高度*/
        position: relative;
        font-size: 10px;
    }
    td[class=first]{
        width: 100px;
    }
    td[class=first]:before {
        content: "";
        position: absolute;
        width: 1px;
        height: 110px;
        top: 0;
        left: 0;
        background-color: #000;
        display: block;
        transform: rotate(-75deg);
        transform-origin: top;
        -ms-transform: rotate(-75deg);
        -ms-transform-origin: top;
    }
    td[class=first]:after {
        content: "";
        position: absolute;
        width: 1px;
        height: 85px;
        top: 0;
        left: 0;
        background-color: #000;
        display: block;
        transform: rotate(-45deg);
        transform-origin: top;
        -ms-transform: rotate(-45deg);
        -ms-transform-origin: top;
    }
    .title1{
        position: absolute;
        top: 0px;
        right:0px;
    }
    .title2{
        position: absolute;
        top: 36px;
        right:0px;
    }
    .title3{
        position: absolute;
        top: 30px;
        left:0px;
    }
</style> 
                                        
<p style="text-align: center;">
    壯圍鄉生命紀念館107年各項退費金額統計
</p>
<p align="center">{{$serach['application_start_date']}}~{{$serach['application_end_date']}}</p>
<p align="right" style="text-align: right">
製表日期：{{tw_date_now()}}
</p>
<table border="1" cellspacing="0" cellpadding="0" width="655">
    <tbody>
        <tr>
            <td width="113">
                <p align="center">
                    櫃位類別
                </p>
            </td>
            <td class="first""> 
              <span class="title1">身份</span><br /> 
              <span class="title2">金額</span><br /> 
              <span class="title3">層組</span> 
            </td>
            <td width="76">
                <p align="center">
                    本鄉鄉民
                </p>
            </td>
            <td width="85">
                <p align="center">
                    本縣縣民
                </p>
            </td>
            <td width="85">
                <p align="center">
                    外縣市民
                </p>
            </td>
            <td width="95">
                <p align="center">
                    本年度累計
                </p>
                <p align="center">
                    金額
                </p>
            </td>
            <td width="94">
                <p align="center">
                    總累計
                </p>
                <p align="center">
                    金額
                </p>
            </td>
        </tr>
        <tr>
            <td width="113" rowspan="2">
                <p align="center">
                    個人骨灰櫃位
                </p>
            </td>
            <td width="107">
                <p align="center">
                    第一 、二層
                </p>
            </td>
            <td width="76">
                <p align="right">
                    <?php $a=0?>
                    @if(isset($new_data['S']['1-2']['800']['0']['cost']))
                    <?php ($new_data['S']['1-2']['800']['0']['cost']) ?>
                    {{number_format($new_data['S']['1-2']['800']['0']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['S']['1-2']['800']['1']['cost']))
                    <?php ($new_data['S']['1-2']['800']['1']['cost'])?>
                    {{number_format($new_data['S']['1-2']['800']['1']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['S']['1-2']['800']['2']['cost']))
                    <?php ($new_data['S']['1-2']['800']['2']['cost'])?>
                    {{number_format($new_data['S']['1-2']['800']['2']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="95">
                <p align="right">
                    @foreach($new_data_year['S']['1-2']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $a=$a+$value2?>
                        @endforeach
                    @endforeach
                    {{$a}}
                </p>
            </td>
            <td width="94">
                <p align="right">
                    <?php $aa=0;?>
                    @foreach($new_data_total['S']['1-2']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $aa=$aa+$value2?>
                        @endforeach
                    @endforeach
                    {{$aa}}
                </p>
            </td>
        </tr>
        <tr>
            <td width="107">
                <p align="center">
                    第三 ~ 八層
                </p>
            </td>
             <td width="76">
                <p align="right">
                    <?php $b=0?>
                    @if(isset($new_data['S']['3-8']['800']['0']['cost']))
                    <?php $b=$b+($new_data['S']['3-8']['800']['0']['cost'])?>
                    {{number_format($new_data['S']['3-8']['800']['0']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['S']['3-8']['800']['1']['cost']))
                    <?php $b=$b+($new_data['S']['3-8']['800']['1']['cost'])?>
                    {{number_format($new_data['S']['3-8']['800']['1']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['S']['3-8']['800']['2']['cost']))
                    <?php $b=$b+($new_data['S']['3-8']['800']['2']['cost'])?>
                    {{number_format($new_data['S']['3-8']['800']['2']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="95">
                <p align="right">
                    @foreach($new_data_year['S']['3-8']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $b=$b+$value2?>
                        @endforeach
                    @endforeach
                    {{$b}}
                </p>
            </td>
            <td width="94">
                <p align="right">
                    <?php $bb=0;?>
                    @foreach($new_data_total['S']['3-8']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $bb=$bb+$value2?>
                        @endforeach
                    @endforeach
                    {{$bb}}
                </p>
            </td>
        </tr>
        <tr>
            <td width="113" rowspan="2">
                <p align="center">
                    夫妻骨灰櫃位
                </p>
            </td>
            <td width="107">
                <p align="center">
                    第一 、二層
                </p>
            </td>
            <td width="76">
                <p align="right">
                    <?php $c=0?>
                    @if(isset($new_data['C']['1-2']['800']['0']['cost']))
                    <?php $c=$c+($new_data['C']['1-2']['800']['0']['cost'])?>
                    {{number_format($new_data['C']['1-2']['800']['0']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['C']['1-2']['800']['1']['cost']))
                    <?php $c=$c+($new_data['C']['1-2']['800']['1']['cost'])?>
                    {{number_format($new_data['C']['1-2']['800']['1']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['C']['1-2']['800']['2']['cost']))
                    <?php $c=$c+($new_data['C']['1-2']['800']['2']['cost'])?>
                    {{number_format($new_data['C']['1-2']['800']['2']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="95">
                <p align="right">
                    @foreach($new_data_year['C']['1-2']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $c=$c+$value2?>
                        @endforeach
                    @endforeach
                    {{$c}}
                </p>
            </td>
            <td width="94">
                <p align="right">
                    <?php $cc=0;?>
                    @foreach($new_data_total['C']['1-2']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $cc=$cc+$value2?>
                        @endforeach
                    @endforeach
                    {{$cc}}
                </p>
            </td>
        </tr>
        <tr>

            <td width="107">
                <p align="center">
                    第三 ~ 八層
                </p>
            </td>
            <td width="76">
                <p align="right">
                    <?php $d=0?>
                    @if(isset($new_data['C']['3-8']['800']['0']['cost']))
                    <?php $d=$d+($new_data['C']['3-8']['800']['0']['cost'])?>
                    {{number_format($new_data['C']['3-8']['800']['0']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['C']['3-8']['800']['1']['cost']))
                    <?php $d=$d+($new_data['C']['3-8']['800']['1']['cost'])?>
                    {{number_format($new_data['C']['3-8']['800']['1']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['C']['3-8']['800']['2']['cost']))
                    <?php $d=$d+($new_data['C']['3-8']['800']['2']['cost'])?>
                    {{number_format($new_data['C']['3-8']['800']['2']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="95">
                <p align="right">
                    @foreach($new_data_year['C']['3-8']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $d=$d+$value2?>
                        @endforeach
                    @endforeach
                    {{$d}}
                </p>
            </td>
            <td width="94">
                <p align="right">
                    <?php $dd=0;?>
                    @foreach($new_data_total['C']['3-8']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $dd=$dd+$value2?>
                        @endforeach
                    @endforeach
                    {{$dd}}
                </p>
            </td>
        </tr>
        <tr>
            <td width="113">
                <p align="center">
                    家族骨灰櫃位
                </p>
            </td>
            <td width="107">
                <p align="center">
                    整組
                </p>
            </td>
            <td width="76">
                <p align="right">
                    <?php $e=0?>
                    @if(isset($new_data['F']['3-8']['800']['0']['cost']))
                    <?php $e=$e+($new_data['F']['3-8']['800']['0']['cost'])?>
                    {{number_format($new_data['F']['3-8']['800']['0']['cost'])}}

                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['F']['3-8']['800']['1']['cost']))
                    <?php $e=$e+($new_data['F']['3-8']['800']['1']['cost'])?>
                    {{number_format($new_data['F']['3-8']['800']['1']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data['F']['3-8']['800']['2']['cost']))
                    <?php $e=$e+($new_data['F']['3-8']['800']['2']['cost'])?>
                    {{number_format($new_data['F']['3-8']['800']['2']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="95">
                <p align="right">
                    @foreach($new_data_year['F']['3-8']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $e=$e+$value2?>
                        @endforeach
                    @endforeach
                    {{$e}}
                </p>
            </td>
            <td width="94">
                <p align="right">
                    <?php $ee=0;?>
                    @foreach($new_data_total['F']['3-8']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $ee=$ee+$value2?>
                        @endforeach
                    @endforeach
                    {{$ee}}
                </p>
            </td>
        </tr>
        <tr>
            <td width="113">
                <p align="center">
                    牌位
                </p>
            </td>
            <td width="107">
                <p align="center">
                    整付
                </p>
            </td>
            <td width="76">
                <p align="right">
                    <?php $f=0 ?>
                    @if(isset($new_data2['T']['3-8']['800']['0']['cost']))
                    <?php $f=$f+($new_data2['T']['3-8']['800']['0']['cost'])?>
                    {{number_format($new_data2['T']['3-8']['800']['0']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data2['T']['3-8']['800']['1']['cost']))
                    <?php $f=$f+($new_data2['T']['3-8']['800']['1']['cost'])?>
                    {{number_format($new_data2['T']['3-8']['800']['1']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="85">
                <p align="right">
                    @if(isset($new_data2['T']['3-8']['800']['2']['cost']))
                    <?php $f=$f+($new_data2['T']['3-8']['800']['2']['cost'])?>
                    {{number_format($new_data2['T']['3-8']['800']['2']['cost'])}}
                    @else
                    0
                    @endif
                </p>
            </td>
            <td width="95">
                <p align="right">
                    @foreach($new_data2_year['T']['3-8']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $f=$f+$value2?>
                        @endforeach
                    @endforeach
                    {{$f}}
                </p>
            </td>
            <td width="94">
                <p align="right">
                    <?php $ff=0;?>
                    @foreach($new_data2_total['T']['3-8']['800'] as $value)
                        @foreach($value as $value2)
                            <?php $ff=$ff+$value2?>
                        @endforeach
                    @endforeach
                    {{$ff}}
                </p>
            </td>
        </tr>
        <tr>
            <td width="220" colspan="2">
                <p align="center">
                    小 計
                </p>
            </td>
            <td width="76">
                <p align="right">
                    <?PHP $A=0?>
                    @if(isset($new_data['S']['1-2']['800']['0']['cost']))
                        <?php $A=$A+$new_data['S']['1-2']['800']['0']['cost']?>
                    @endif
                    @if(isset($new_data['S']['3-8']['800']['0']['cost']))
                        <?php $A=$A+$new_data['S']['3-8']['800']['0']['cost']?>
                    @endif
                    @if(isset($new_data['C']['1-2']['800']['0']['cost']))
                        <?php $A=$A+$new_data['C']['1-2']['800']['0']['cost']?>
                    @endif
                    @if(isset($new_data['C']['3-8']['800']['0']['cost']))
                        <?php $A=$A+$new_data['C']['3-8']['800']['0']['cost']?>
                    @endif
                    @if(isset($new_data['F']['3-8']['800']['0']['cost']))
                        <?php $A=$A+$new_data['F']['3-8']['800']['0']['cost']?>
                    @endif
                    @if(isset($new_data2['T']['3-8']['800']['0']['cost']))
                        <?php $A=$A+$new_data2['T']['3-8']['800']['0']['cost']?>
                    @endif
                    {{number_format($A)}}
                </p>
            </td>
            <td width="85">
                <p align="right">
                    <?PHP $B=0?>
                    @if(isset($new_data['S']['1-2']['800']['1']['cost']))
                        <?php $B=$B+$new_data['S']['1-2']['800']['1']['cost']?>
                    @endif
                    @if(isset($new_data['S']['3-8']['800']['1']['cost']))
                        <?php $B=$B+$new_data['S']['3-8']['800']['1']['cost']?>
                    @endif
                    @if(isset($new_data['C']['1-2']['800']['1']['cost']))
                        <?php $B=$B+$new_data['C']['1-2']['800']['1']['cost']?>
                    @endif
                    @if(isset($new_data['C']['3-8']['800']['1']['cost']))
                        <?php $B=$B+$new_data['C']['3-8']['800']['1']['cost']?>
                    @endif
                    @if(isset($new_data['F']['3-8']['800']['1']['cost']))
                        <?php $B=$B+$new_data['F']['3-8']['800']['1']['cost']?>
                    @endif
                    @if(isset($new_data2['T']['3-8']['800']['1']['cost']))
                        <?php $B=$B+$new_data2['T']['3-8']['800']['1']['cost']?>
                    @endif
                    {{number_format($B)}}
                </p>
            </td>
            <td width="85">
                <p align="right">
                    <?PHP $C=0?>
                    @if(isset($new_data['S']['1-2']['200']['2']['cost']))
                        <?php $C=$C+$new_data['S']['1-2']['200']['2']['cost']?>
                    @endif
                    @if(isset($new_data['S']['3-8']['200']['2']['cost']))
                        <?php $C=$C+$new_data['S']['3-8']['200']['2']['cost']?>
                    @endif
                    @if(isset($new_data['C']['1-2']['200']['2']['cost']))
                        <?php $C=$C+$new_data['C']['1-2']['200']['2']['cost']?>
                    @endif
                    @if(isset($new_data['C']['3-8']['200']['2']['cost']))
                        <?php $C=$C+$new_data['C']['3-8']['200']['2']['cost']?>
                    @endif
                    @if(isset($new_data['F']['3-8']['200']['2']['cost']))
                        <?php $C=$C+$new_data['F']['3-8']['200']['2']['cost']?>
                    @endif
                    @if(isset($new_data2['T']['3-8']['200']['2']['cost']))
                        <?php $C=$C+$new_data2['T']['3-8']['200']['2']['cost']?>
                    @endif
                     {{number_format($C)}}
                </p>
            </td>
            <td width="95">
                <p align="right">
                   {{number_format($a+$b+$c+$d+$e+$f)}}
                </p>
            </td>
            <td width="94">
                <p align="right">
                   {{number_format($aa+$bb+$cc+$dd+$ff)}}
                </p>
            </td>
        </tr>
        <tr>
            <td width="220" colspan="2">
                <p align="center">
                    合 計
                </p>
            </td>
            <td width="246" colspan="3">
                <p align="center">
                    <strong>{{number_format($C+$B+$A)}}</strong>
                </p>
            </td>
            <td width="95">
                <p align="center">
                    <strong>{{number_format($a+$b+$c+$d+$e+$f)}}</strong>
                </p>
            </td>
            <td width="94">
                <p align="center">
                    <strong>{{number_format($aa+$bb+$cc+$dd+$ee+$ff)}}</strong>
                </p>
            </td>
        </tr>
    </tbody>
</table>