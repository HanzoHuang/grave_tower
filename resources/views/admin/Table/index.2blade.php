<table cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td width="272" height="54">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <div>
                                    <p>
                                        製表日期：107/08/07
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<p>
    壯圍鄉生命紀念館107年6月份各類櫃位及牌位使用情形及數量統計
</p>
<p align="center">
    櫃位使用情形及數量
</p>
<div align="center">
    <table border="1" cellspacing="0" cellpadding="0" width="690">
        <tbody>
            <tr>
                <td width="73" rowspan="2">
                    <p align="center">
                        櫃位類別
                    </p>
                </td>
                <td width="101" rowspan="2">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td width="145" height="47">
                                    <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        width="100%"
                                    >
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <p>
                                                            層組
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <img
                        width="101"
                        height="35"
                        src="file:///C:/Users/joe/AppData/Local/Temp/msohtmlclip1/01/clip_image001.png"
                    />
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td width="145" height="48">
                                    <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        width="100%"
                                    >
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <p>
                                                            數量
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <img
                        width="71"
                        height="87"
                        src="file:///C:/Users/joe/AppData/Local/Temp/msohtmlclip1/01/clip_image002.png"
                    />
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td width="145" height="114">
                                    <table
                                        cellpadding="0"
                                        cellspacing="0"
                                        width="100%"
                                    >
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <p>
                                                            狀況
                                                        </p>
                                                        <p>
                                                            區分
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td width="46" rowspan="2">
                    <p align="center">
                        櫃位數量
                    </p>
                </td>
                <td width="46" rowspan="2">
                    <p align="center">
                        使用數量
                    </p>
                </td>
                <td width="46" rowspan="2">
                    <p align="center">
                        剩餘數量
                    </p>
                </td>
                <td width="141" colspan="3">
                    <p align="center">
                        櫃位安置
                    </p>
                </td>
                <td width="142" colspan="3">
                    <p align="center">
                        櫃位異動
                    </p>
                </td>
                <td width="94" colspan="2">
                    <p align="center">
                        櫃位遷出
                    </p>
                </td>
            </tr>
            <tr>
                <td width="46">
                    <p align="center">
                        申請
                    </p>
                </td>
                <td width="47">
                    <p align="center">
                        繳費
                    </p>
                </td>
                <td width="47">
                    <p align="center">
                        進塔
                    </p>
                </td>
                <td width="47">
                    <p align="center">
                        申請
                    </p>
                </td>
                <td width="47">
                    <p align="center">
                        繳費
                    </p>
                </td>
                <td width="47">
                    <p align="center">
                        進塔
                    </p>
                </td>
                <td width="47">
                    <p align="center">
                        申請
                    </p>
                </td>
                <td width="47">
                    <p align="center">
                        遷出
                    </p>
                </td>
            </tr>
            <tr>
                <td width="73" rowspan="2">
                    <p align="center">
                        個人
                    </p>
                </td>
                <td width="101">
                    <p align="center">
                        第一 、二層
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        63
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        10
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        53
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        4
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
            </tr>
            <tr>
                <td width="101">
                    <p align="center">
                        第三 ~ 八層
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        63
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        10
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        53
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        4
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
            </tr>
            <tr>
                <td width="73" rowspan="2">
                    <p align="center">
                        夫妻
                    </p>
                </td>
                <td width="101">
                    <p align="center">
                        第一 、二層
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        63
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        10
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        53
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        4
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
            </tr>
            <tr>
                <td width="101">
                    <p align="center">
                        第三 ~ 八層
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        63
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        10
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        53
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        4
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
            </tr>
            <tr>
                <td width="73">
                    <p align="center">
                        家族
                    </p>
                </td>
                <td width="101">
                    <p align="center">
                        整組
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        63
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        10
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        53
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        4
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        3
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        2
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        1
                    </p>
                </td>
            </tr>
            <tr>
                <td width="174" colspan="2">
                    <p align="center">
                        總 計
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        315
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        50
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        265
                    </p>
                </td>
                <td width="46">
                    <p align="right">
                        10
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        15
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        20
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        15
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        5
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        10
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        5
                    </p>
                </td>
                <td width="47">
                    <p align="right">
                        5
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<p align="center">
    牌位使用情形及數量
</p>
<table border="1" cellspacing="0" cellpadding="0" width="569">
    <tbody>
        <tr>
            <td width="94" rowspan="2">
                <p align="center">
                    牌位區
                </p>
            </td>
            <td width="46" rowspan="2">
                <p align="center">
                    牌位數量
                </p>
            </td>
            <td width="47" rowspan="2">
                <p align="center">
                    使用數量
                </p>
            </td>
            <td width="47" rowspan="2">
                <p align="center">
                    剩餘數量
                </p>
            </td>
            <td width="201" colspan="3">
                <p align="center">
                    牌位安置
                </p>
            </td>
            <td width="132" colspan="2">
                <p align="center">
                    牌位遷出
                </p>
            </td>
        </tr>
        <tr>
            <td width="66">
                <p align="center">
                    申請
                </p>
            </td>
            <td width="66">
                <p align="center">
                    繳費
                </p>
            </td>
            <td width="69">
                <p align="center">
                    進塔
                </p>
            </td>
            <td width="66">
                <p align="center">
                    申請
                </p>
            </td>
            <td width="66">
                <p align="center">
                    遷出
                </p>
            </td>
        </tr>
        <tr>
            <td width="94">
                <p align="center">
                    牌位A區
                </p>
            </td>
            <td width="46" valign="top">
                <p align="right">
                    90
                </p>
            </td>
            <td width="47" valign="top">
                <p align="right">
                    66
                </p>
            </td>
            <td width="47">
                <p align="right">
                    24
                </p>
            </td>
            <td width="66">
                <p align="right">
                    2
                </p>
            </td>
            <td width="66">
                <p align="right">
                    1
                </p>
            </td>
            <td width="69">
                <p align="right">
                    3
                </p>
            </td>
            <td width="66">
                <p align="right">
                    1
                </p>
            </td>
            <td width="66">
                <p align="right">
                    1
                </p>
            </td>
        </tr>
        <tr>
            <td width="94">
                <p align="center">
                    牌位B區
                </p>
            </td>
            <td width="46" valign="top">
                <p align="right">
                    90
                </p>
            </td>
            <td width="47" valign="top">
                <p align="right">
                    88
                </p>
            </td>
            <td width="47">
                <p align="right">
                    2
                </p>
            </td>
            <td width="66">
                <p align="right">
                    2
                </p>
            </td>
            <td width="66">
                <p align="right">
                    1
                </p>
            </td>
            <td width="69">
                <p align="right">
                    3
                </p>
            </td>
            <td width="66">
                <p align="right">
                    1
                </p>
            </td>
            <td width="66">
                <p align="right">
                    1
                </p>
            </td>
        </tr>
        <tr>
            <td width="94">
                <p align="center">
                    總 計
                </p>
            </td>
            <td width="46" valign="top">
                <p align="right">
                    180
                </p>
            </td>
            <td width="47">
                <p align="right">
                    154
                </p>
            </td>
            <td width="47">
                <p align="right">
                    26
                </p>
            </td>
            <td width="66">
                <p align="right">
                    4
                </p>
            </td>
            <td width="66">
                <p align="right">
                    2
                </p>
            </td>
            <td width="69">
                <p align="right">
                    6
                </p>
            </td>
            <td width="66">
                <p align="right">
                    2
                </p>
            </td>
            <td width="66">
                <p align="right">
                    2
                </p>
            </td>
        </tr>
    </tbody>
</table>