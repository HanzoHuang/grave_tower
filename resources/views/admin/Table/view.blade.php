@extends('layouts.admin.master2')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')


@if (isset($DetailName))
    <div class="alert alert-success" style="margin-top: 20px;">
        您現在所在的頁面：{{ $DetailName}}

    </div>
@endif
<form action="{{$Url}}" method="get" >
<label class="">日期區間：</label>
{!!Form::text('application_start_date',Input::get('start_date',''), array('id' => 'application_start_date' ,'class' => 'datepickerTW', 'placeholder' => '', 'required' => 'required'))!!}
     <label class="">-</label>
{!!Form::text('application_end_date',Input::get('end_date',''), array('id' => 'application_end_date' , 'class' => 'datepickerTW', 'placeholder' => '', 'required' => 'required'))!!}

<input type="submit" name="搜尋" value="搜尋">
</form>

@section('script')

<script>

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
})
function change_sys_select(type) {
    area=$('#area').find(':selected').val();
    position=$('#position').find(':selected').val();
    layer=$('#layer').find(':selected').val();
    seat=$('#seat').find(':selected').val();
    if(type=='area'){
        data={area:area,position:position};
    }else if(type=='layer'){
        data={area:area,layer:layer,position:position};
    }else if(type=='seat'){
        data={area:area,layer:layer,position:position,seat:seat};
    }else if(type=='position'){
        data={area:area,layer:layer,position:position};
    }
    $.ajax({
        url: "{{ route('BackTombstone.ChangeSelect') }}",
        data: data,
        dataType:"html",
        type: "get",
        success: function(data){
            $('#sys_select_div').html(data);
        },
        error: function(){
        }
    });
}
</script>
@stop