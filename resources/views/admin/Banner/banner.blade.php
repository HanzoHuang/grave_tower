<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="{{asset('BannerData/css/main.css')}}" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{asset('BannerData/js/jquery-latest.min.js')}}"></script>

<title>宜蘭縣壯圍鄉</title>
<script type="text/javascript">
	$(function(){
		var $block = $('#abgne_fade_pic'), 
			$ad = $block.find('.ad'),
			showIndex = 0,			// 預設要先顯示那一張
			fadeOutSpeed = 2000,	// 淡出的速度
			fadeInSpeed = 3000,		// 淡入的速度
			defaultZ = 10,			// 預設的 z-index
			isHover = false,
			timer, speed = 2000;	// 計時器及輪播切換的速度
		
		// 先把其它圖片的變成透明
		$ad.css({
			opacity: 0,
			zIndex: defaultZ - 1
		}).eq(showIndex).css({
			opacity: 1,
			zIndex: defaultZ
		});
		
		// 組出右下的按鈕
		var str = '';
		for(var i=0;i<$ad.length;i++){
			str += '<a href="#">' + (i + 1) + '</a>';
		}
		var $controlA = $('#abgne_fade_pic').append($('<div class="control">' + str + '</div>').css('zIndex', defaultZ + 1)).find('.control a');

		// 當按鈕被點選時
		// 若要變成滑鼠滑入來切換時, 可以把 click 換成 mouseover
		$controlA.click(function(){
			// 取得目前點擊的號碼
			showIndex = $(this).text() * 1 - 1;
			
			// 顯示相對應的區域並把其它區域變成透明
			$ad.eq(showIndex).stop().fadeTo(fadeInSpeed, 1, function(){
				if(!isHover){
					// 啟動計時器
					timer = setTimeout(autoClick, speed + fadeInSpeed);
				}
			}).css('zIndex', defaultZ).siblings('a').stop().fadeTo(fadeOutSpeed, 0).css('zIndex', defaultZ - 1);
			// 讓 a 加上 .on
			$(this).addClass('on').siblings().removeClass('on');

			return false;
		}).focus(function(){
			$(this).blur();
		}).eq(showIndex).addClass('on');

		$block.hover(function(){
			isHover = true;
			// 停止計時器
			clearTimeout(timer);
		}, function(){
			isHover = false;
			// 啟動計時器
			timer = setTimeout(autoClick, speed);
		})
		
		// 自動點擊下一個
		function autoClick(){
			if(isHover) return;
			showIndex = (showIndex + 1) % $controlA.length;
			$controlA.eq(showIndex).click();
		}
		
		// 啟動計時器
		timer = setTimeout(autoClick, speed);
	});
</script>
</head>
<body>
<div id="wrapper">
<div id="container">
      <div class="banner_bg">
        <div class="banner">
           <div id="abgne_fade_pic">
           	@foreach($banner as $banners)
       			@if($banners->file1<>'' )
	       			<a href="#" class="ad">
	       				<img src="{{asset('Upload/BackBanner/'.$banners->file1)}}" />
	       			</a>
       			@endif
	      	@endforeach
	        @foreach($announcement_information as $announcement_informations)
			    <a href="#" class="ad">
	              <div class="news">
		            <div class="new_area"><h1><strong>{{$announcement_informations->name}}</strong>{{$announcement_informations->title}}</h1><font>{!!$announcement_informations->body!!}</font></div>
		            <div class="clear_line"></div>
		          </div>
					@if($announcement_informations->file1<>'' & is_file(asset('Upload/AnnouncementInformation/'.$announcement_informations->file1)))
		          		<img src="{{asset('Upload/AnnouncementInformation/'.$announcement_informations->file1)}}" />
		          	@else
						<img src="{{asset('Upload/images/nothing.png')}}" />
		          	@endif
		        </a>
            @endforeach
           </div>
        </div>
       </div>
</div>
</div>
</body>
</html>
