<div class="form-group">
    <label for="fname" class="col-sm-3 control-label">櫃位區域</label>
    <div class="col-sm-3">
        <select name="tombstone_class_aera" id="area"  onchange="change_sys_select('area');">
            @foreach($area as $key =>$value)
            <option value="{{$value->id}}"
                @if(isset($request_data['area']))
                    @if($request_data['area']==$value->id)
                        selected
                    @endif
                @endif
                >{{$value->code.' '.$value->name}}
            </option>
            @endforeach
        </select>
    </div>
    <label for="fname" class="col-sm-3 control-label">方位座向</label>
    <div class="col-sm-3">
        <select name="tombstone_class_position" id="position" onchange="change_sys_select('position');">
            @foreach($position as $key =>$value)
            <option value="{{$key}}"  
                @if(isset($request_data['position']))
                    @if($request_data['position']==$key)
                        selected
                    @endif
                @endif
            >{{$value}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="fname" class="col-sm-3 control-label">排</label>
    <div class="col-sm-3">
        <select name="tombstone_class_layer" id="layer" onchange="change_sys_select('layer');">
            @foreach($layer as $key =>$value)
             <option value="{{$value->id}}"
                 @if(isset($request_data['layer']))
                    @if($request_data['layer']==$value->id)
                        selected
                    @endif
                @endif
                >{{$value->code}}</option>
            @endforeach
        </select>
    </div>
    <label for="fname" class="col-sm-3 control-label">位子</label>
    <div class="col-sm-3">
        <select name="tombstone_class_seat" id='seat' onchange="change_sys_select('seat');">
            @foreach($seat as $key =>$value)
                <option value='{{$value->id}}'
                @if(isset($request_data['seat']))
                    @if($request_data['seat']==$value->id)
                            selected
                    @endif
                @endif
                >{{$value->code}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label for="fname" class="col-sm-3 control-label">現有空位</label>
    <div class="col-sm-9">
        <select name="tombstone_code" id='tombstone_code'>
            <option value="">沒有任何位置</option>
        </select>
        <a class="btn btn-success"  href="#pod" onclick="SaveData()">搜尋空位</a>
    </div>
</div>