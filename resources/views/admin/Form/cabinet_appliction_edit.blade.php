@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
<style type="text/css">
    table,td,tr{
        border:solid 1px;
    }
    th{
        border:solid 1px;
        text-align: center;
        width: 15%
    }
    #table_div table{
        width: 100%
    }
</style>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {!! session('status') !!}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    <input class="" name="cabinet_appliction_type" type="hidden" value='1'>
    <input name="_method" type="hidden" value="PATCH">
    <input class="" name="id" type="hidden" value="{{$cabinet_appliction->id}}">
    {{ csrf_field() }}
    <div class="form-group">
        <div class="col-sm-3">
        <h4>申請資訊</h4>
        </div>
        <div class="col-sm-9" style="text-align: right;">
        <a target="black" href="{{asset('BackCabinetApplictionPdf2?no='. $cabinet_appliction->no).'& number='.$cabinet_appliction->applicant_number}}" class="btn btn-info" title="明細" style="margin-right: 10px;">申請單</a>
        @if(Auth::user()->role_id=='2')
        <a target="black" href="{{asset('BackCabinetApplictionPdf4?no='. $cabinet_appliction->no).'& number='.$cabinet_appliction->applicant_number}}" class="btn btn-info" title="明細" style="margin-right: 10px;">繳費單</a>
        @endif
        <a target="black" href="{{asset('BackCabinetApplictionPdf?no='. $cabinet_appliction->no).'& number='.$cabinet_appliction->applicant_number}}" class="btn btn-info" title="明細" style="margin-right: 10px;">收據</a>
        @if(Auth::user()->role_id=='2')
        <a target="black" href="{{asset('BackCabinetApplictionPdf3?no='. $cabinet_appliction->no).'& number='.$cabinet_appliction->applicant_number}}" class="btn btn-info" title="明細" style="margin-right: 10px;">許可證</a>
        @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" id="no" name="no" type="text" readonly="readonly" value="{{$cabinet_appliction->no}}" >
        </div>             
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請狀態</label>
        <div class="col-sm-3">
            <input  type="radio" id="status" name="status" value='100'>已申請
            <input  type="radio" id="status" name="status" value='200' @if($cabinet_appliction->status==100) checked  @else onclick="return false"  @endif>已繳費
            <input  type="radio" id="status" name="status" value='300' @if($cabinet_appliction->status==200) checked @else onclick="return false"  @endif>已進塔
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control " readonly id="application_date" name="application_date"  value="{{$cabinet_appliction->application_date}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" id="applicant" name="applicant" type="text" value="{{$cabinet_appliction->applicant}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" id="applicant_number" name="applicant_number" type="text" value="{{$cabinet_appliction->applicant_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" id="household_registration" name="household_registration" type="text" value="{{$cabinet_appliction->household_registration}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" id="tel" name="tel" type="text" value="{{$cabinet_appliction->tel}}" readonly="" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" id="phone" name="phone" type="text"  value="{{$cabinet_appliction->phone}}" readonly="" placeholder="0912-345678">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <select name="relationship">
                @if($cabinet_appliction->relationship=="祖孫")
                <option value="祖孫">祖孫</option>
                @endif
                @if($cabinet_appliction->relationship=="父子")
                <option value="父子">父子</option>
                @endif
                @if($cabinet_appliction->relationship=="父女")
                <option value="父女">父女</option>
                @endif
                @if($cabinet_appliction->relationship=="母子")
                <option value="母子">母子</option>
                @endif
                @if($cabinet_appliction->relationship=="母女")
                <option value="母女">母女</option>
                @endif
                @if($cabinet_appliction->relationship=="夫妻")
                <option value="夫妻">夫妻</option>
                @endif
                @if($cabinet_appliction->relationship=="祖先")
                <option value="祖先">祖先</option>
                @endif
                @if($cabinet_appliction->relationship=="其他")
                <option value="其他">其他</option>
                @endif
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" id="address" name="address" type="text" value="{{$cabinet_appliction->address}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <h4>委託人資訊</h4>
    </div>
    <input class="" name="cabinet_principal_id" type="hidden" value="{{$cabinet_principal->id}}">
    {{-- cabinet_principal --}}
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" id="client" name="client" type="text" value="{{$cabinet_principal->client}}" readonly="" >
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" id="client_number" name="client_number" type="text" value="{{$cabinet_principal->client_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" id="client_household_registration" name="client_household_registration" value="{{$cabinet_principal->client_household_registration}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" id="client_tel" name="client_tel" type="text" value="{{$cabinet_principal->client_tel}}" readonly="" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" id="client_phone" name="client_phone" type="text" value="{{$cabinet_principal->client_phone}}" readonly="" placeholder="0912-345678">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" id="client_address" name="client_address" type="text" value="{{$cabinet_principal->client_address}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">    
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file1))

            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file1)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file1),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">火化許可證</label>
        <div class="col-sm-6">
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file2))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file2)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file2),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">死亡證明書</label>
        <div class="col-sm-6">
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file3))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file3)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file3),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file4))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file4)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file4),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">亡者除戶謄本</label>
        <div class="col-sm-6">
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file5))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file5)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file5),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">申請單</label>
        <div class="col-sm-6">
            @if($cabinet_appliction->status==100)
            <input  type="file" id="file6" name="file6" >
            @endif
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file6))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file6)}}"></a>
                {{-- {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file6),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">繳費單</label>
        <div class="col-sm-6">
            @if($cabinet_appliction->status==100)
            <input  type="file" id="file7" name="file7" >
            @endif
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file7))
                <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file7)}}"></a>
                {{-- {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file7),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <h4>逝者資訊</h4>
    </div>
    
    @foreach($cabinet_appliction_user as $key =>$value)
    <input class="" name="cabinet_appliction_user_id{{$key+1}}" type="hidden" value="{{$value->id}}">
    <input class="" name="cabinet_type" type="hidden" value="{{$cabinet_appliction_data[0]->cabinet_type}}">
    <table>
        <tr style="background-color: #ecdfc2">
            <th colspan="6" >逝者{{$key+1}}</th>
        </tr>
        <tr >
            <th>逝者</th>
            <th>逝者身分證字號</th>
            <th>逝者戶籍地址</th>
            <th>性別</th>
            <th>身分</th>
            <th>優待</th>
        </tr>
        <tr>
            <th>
                <input class="form-control" id="name" name="name{{$key+1}}" type="text" value="{{$value->name}}" readonly="">
            </th>
            <th>
                <input class="form-control" id="name_number" name="name_number{{$key+1}}" type="text"  value="{{$value->name_number}}" readonly="">
            </th>
            <th>
                <input class="form-control" id="name_household_registration" name="name_household_registration{{$key+1}}" type="text" value="{{$value->address}}" readonly="">
            </th>
            <th>
                <input  type="radio" id="gender" name="gender{{$key+1}}" @if($value->gender==1) checked  @else onclick="return false;" @endif value='1'>男
                <input  type="radio" id="gender" name="gender{{$key+1}}" @if($value->gender==2) checked @else onclick="return false;" @endif value='2'>女</th>
            <th>
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==0) checked  @else onclick="return false;" @endif  value="0" checked>本鄉鄉民
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==1) checked  @else onclick="return false;" @endif value="1">本縣縣民
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==2) checked  @else onclick="return false;" @endif value="2">外縣市民
            </th>
            <th>
                <select name="discount{{$key+1}}">
                    @if($value->discount=='0')
                    <option value='0' selected >無</option>
                    @endif
                    @if($value->discount=='1')
                    <option value='1' selected >復興村13.14鄰 106.8.18以前優待80%</option>
                    @endif
                    @if($value->discount=='2')
                    <option value='2' selected >復興村13.14鄰 106.8.19以後優待50%</option>
                    @endif
                    @if($value->discount=='3')
                    <option value='3'selected >101.9.1~103.11.30入懷恩堂優待10%</option>
                    @endif
                    @if($value->discount=='4')
                    <option value='4' selected >曾設籍本鄉減免5%</option>
                    @endif
                    @if($value->discount=='5')
                    <option value='5' selected >復興村 13.14 鄰戶 106.8.18 前已故優待 20%%</option>
                    @endif
                </select>
            </th>
        </tr>
        <tr>
            <th>死亡日期</th>
            <th>死亡原因</th>
            <th>死亡地點</th>
            <th>出生日期</th>
            <th>預計進塔日期</th>
            <th>預計進塔時間</th>
        </tr>
        <tr>
            <th><input class="form-control " readonly id="dead_date{{$key+1}}" name="dead_date{{$key+1}}" value="{{$value->dead_date}}" readonly=""></th>
            <th>
                <select name="dead_reason{{$key+1}}">
                    <option value="{{$value->dead_reason}}">{{$value->dead_reason}}</option>
                </select>

            </th>
            <th>
                <input class="form-control" id="dead_location{{$key+1}}" name="dead_location{{$key+1}}" type="text" value="{{$value->dead_location}}" readonly="">
            </th>
            <th>
                <input class="form-control "  readonly id="birthday{{$key+1}}" name="birthday{{$key+1}}"  value="{{$value->birthday}}" readonly="">
            </th>
            <th>
                <input class="form-control " readonly id="expected_date{{$key+1}}" name="expected_date{{$key+1}}"  value="{{$value->expected_date}}" readonly="">
            </th>
            <th><input class="form-control " readonly id="expected_date{{$key+1}}" name="expected_date{{$key+1}}"  value="{{$value->expected_time}}" readonly=""></th>
        </tr>
    </table>
    @endforeach 
    <div class="form-group">
        <h4>櫃位資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請類型：</label>
        
        <div class="col-sm-3">
            <select name="application_type">
                @if($cabinet_appliction->application_type=='1')<option value="1" selected  >納骨櫃位</option>@endif
                @if($cabinet_appliction->application_type=='2')<option value="2"  selected >長生櫃位</option>@endif
            </select>
        </div>
    </div>
    <div class="form-group">

        @foreach($cabinet_appliction_data as $data_key =>$data_value)
        <table id="table{{$data_key+1}}" class="cabinet_table">
            <tr style="background-color: #ecdfc2">
                <th colspan="6" >逝者{{$data_key+1}}</th>
            </tr>
            <tr>
                <th>櫃位區域</th>
                <th>排</th>
                <th>層</th>
                <th>位置</th>
                <th>方位座向</th>
            </tr>
            <tr>
                <th>
                    <select name="cabinet_class_aera{{$data_key+1}}" id="area{{$data_key+1}}" onchange='change_sys_select("area","{{$data_key+1}}");' slect-type="area" >

                        @if($data_value->cabinet_class_aera==$area[$data_key]->id)
                        <option value="{{$area[$data_key]->id}}">{{$area[$data_key]->code.' '.$area[$data_key]->name}}</option>
                        @endif
                    </select>
                </th>
                
                 <th>
                    <select name="cabinet_class_layer{{$data_key+1}}" id="layer{{$data_key+1}}"  onchange='change_sys_select("layer","{{$data_key+1}}");' slect-type="layer">
                        
                         <option value="{{$layer[0]->id}}">{{'編號:'.$layer[0]->code.','.$layer[0]->name}}</option>
                        
                    </select>
                </th> 
                 <th>
                    <select name="cabinet_class_row{{$data_key+1}}" id="row{{$data_key+1}}" onchange='change_sys_select("row","{{$data_key+1}}");' slect-type="row">
                        
                        <option value="{{$row[$data_key]->id}}">{{'編號:'.$row[$data_key]->code.','.$row[$data_key]->name.'排'}}</option>
                       
                    </select> 
                </th>

                <th style="width:25%">
                    <select name="cabinet_code{{$data_key+1}}" id='cabinet_code{{$data_key+1}}'>
                        <option value="{{$cabinet_code[$data_key]->code}}">{{$cabinet_code[$data_key]->code}}</option>
                    </select>
                </th> 
                <th>
                     <select name="cabinet_class_position{{$data_key+1}}" id="position{{$data_key+1}}" onchange='change_sys_select("position","{{$data_key+1}}");' slect-type="position">
                        @foreach($position as $position_key =>$position_value)
                        @if($data_value->cabinet_class_position==$position_key)
                        <option value="{{$position_key}}">{{$position_value}}</option>
                        @endif
                        @endforeach
                    </select>
                </th> 
            </tr>
        </table>
        @endforeach
    </div>
{{--  --}}


    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">櫃位費用：</label>

        <div class="col-sm-3">
            <input class="form-control" id="amount_due" name="amount_due" type="number" min='1' value='{{$cabinet_appliction->cost}}' readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">其他費用：</label>
        <div class="col-sm-3">
            <input class="form-control" id="amount_due" name="amount_due" type="number" min='1' value='{{$cabinet_appliction->other_amount_due}}' readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">總費用：</label>
        <div class="col-sm-3">
            <input class="form-control" id="amount_due" name="amount_due" type="number" min='1' value='{{$cabinet_appliction->amount_due}}' readonly="">
        </div>
    </div>

    
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{$users->name}}
        </div>
    </div>
    {{-- 目前 --}}
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">確認繳費人：</label>
        <div class="col-sm-3">
    {{-- 繳費單 --}}
    @if($cabinet_appliction->status==100)
            {{Auth::user()->name}}
    @endif
    {{-- 入塔單 --}}
    @if($cabinet_appliction->status==200)
        @if(isset($confirm_payment_user->name)) {{$confirm_payment_user->name}} @endif
    @endif
        </div>
        <label for="fname" class="col-sm-3 control-label">確認繳費日期：</label>
        <div class="col-sm-3">
    @if($cabinet_appliction->status==100)
            <input class="form-control col-sm-9 datepickerTW" readonly id="payment_date" name="payment_date"  required="" value="{{tw_date_now()}}">
    @endif
    @if($cabinet_appliction->status==200)
            {{$cabinet_appliction->payment_date}}

    @endif
        </div>
    </div>
    @if($cabinet_appliction->status==200)
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">進塔人員：</label>
        <div class="col-sm-3">
            <input class="form-control" id="principal" name="principal" type="text" required="" value="{{ old('principal',Auth::user()->name)}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">進塔日期：</label>
        <div class="col-sm-3">
            <input class="form-control col-sm-9 datepickerTW" readonly id="change_date" name="change_date"  required="" value="{{tw_date_now()}}">
        </div>
    </div>
    @endif
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50" readonly="">{{$cabinet_appliction->ext}}</textarea>
        </div>
    </div>
    @if(empty($show))
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-1">
            <button class="btn btn-success" type="submit">送出</button>
        </div>
        <div class="col-sm-3" style="margin-left: 10px;">
            <a class="btn btn-success " href="{{asset('BackCabinetAppliction')}}">返回</a>
        </div>
    </div>
    @endif
</form>
@stop
@section('script')

@stop
