@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
<style type="text/css">
    table,td,tr{
        border:solid 1px;
    }
    th{
        border:solid 1px;
        text-align: center;
        width: 15%
    }
    #table_div table{
        width: 100%
    }
</style>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}
<form method="GET" action="{{asset($url).'/create'}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" >
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請者身份證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="select_applicant_number" name="select_applicant_number" type="text" @if(Input::has('select_applicant_number')) value="{{Input::get('select_applicant_number')}}" @endif >
            <a class="btn" id="select_no_a">查詢單號</a>
        </div>
        <div class="col-sm-3">
            <select  class="form-control" name="select_no" id="select_no">

            </select>
        </div>
        <div class="col-sm-3">
            <input type="submit" class="form-control" placeholder=""  type="text" >
        </div>
    </div>
</form>
@if(isset($cabinet_appliction))
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="form">
    {{ csrf_field() }}
    <input type="hidden" name="cabinet_appliction_id" value="{{$cabinet_appliction->id}}">
    <input type="hidden" name="cabinet_appliction_type" value="3">
    <input type="hidden" name="cabinet_type" id="cabinet_type" value="{{$type}}">
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="no" name="no" type="text" readonly="readonly" value="{{$no}}" >
        </div>
        <label for="fname" class="col-sm-3 control-label">申請類型：</label>
        <div class="col-sm-3">
            <select name="application_type" >
                @if($cabinet_appliction->application_type=='1')<option value="1"  selected >納骨櫃位</option> @endif
                @if($cabinet_appliction->application_type=='2')<option value="2"  selected >長生櫃位</option>@endif
            </select>
        </div>
        
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請狀態</label>
        <div class="col-sm-3">
            <input  type="radio" id="status" name="status" value='700' checked>申請遷出
            <input  type="radio" id="status" name="status" value='800' onclick="return false;">已遷出
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="application_date" name="application_date"  value="{{tw_date_now()}}" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant" name="applicant" type="text" value="{{$cabinet_appliction->applicant}}" required="">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant_number" name="applicant_number" type="text" value="{{$cabinet_appliction->applicant_number}}" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="household_registration" name="household_registration" type="text" value="{{$cabinet_appliction->household_registration}}" required="" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="tel" name="tel" type="text" value="{{$cabinet_appliction->tel}}" required="" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="phone" name="phone" type="text"  value="{{$cabinet_appliction->phone}}" placeholder="0912-933012">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <select name="relationship">
                <option value="祖孫">祖孫</option>
                <option value="父子">父子</option>
                <option value="父女">父女</option>
                <option value="母子">母子</option>
                <option value="母女">母女</option>
                <option value="夫妻">夫妻</option>
                <option value="祖先">祖先</option>
                <option value="其他">其他</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$cabinet_appliction->address}}" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
            <input  type="checkbox" id="same_applicant" name="same_applicant">同戶籍地址
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">
            <input  type="file" id="file1" name="file1" >
            
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            <input  type="file" id="file8" name="file8" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">申請書</label>
        <div class="col-sm-6">
            <input  type="file" id="file9" name="file9" >
            
        </div>
    </div>
    <div class="form-group">
        <h4>逝者資訊</h4>
    </div>
    <div class="form-group">
        @foreach($cabinet_appliction_user as $key =>$value)
        <table>
        <tr style="background-color: #ecdfc2">
            <th colspan="6" >逝者{{$key+1}}</th>
        </tr>
        <tr >
            <th>逝者</th>
            <th>逝者身分證字號</th>
            <th>逝者戶籍地址</th>
            <th>性別</th>
            <th>身分</th>
            <th>優待</th>
        </tr>
        <tr>
            <th>
                <input class="form-control" placeholder="" id="name" name="name{{$key+1}}" type="text" value="{{$value->name}}" readonly="">
            </th>
            <th>
                <input class="form-control" placeholder="" id="name_number" name="name_number{{$key+1}}" type="text"  value="{{$value->name_number}}" readonly="">
            </th>
            <th>
                <input class="form-control" placeholder="" id="name_household_registration" name="name_household_registration{{$key+1}}" type="text" value="{{$value->address}}" readonly="">
            </th>
            <th>
                <input  type="radio" id="gender" name="gender{{$key+1}}" @if($value->gender==1) checked @else onclick="return false;" @endif value='1' >男
                <input  type="radio" id="gender" name="gender{{$key+1}}" @if($value->gender==2) checked @else onclick="return false;" @endif value='2'>女</th>
            <th>
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==0) checked @else onclick="return false;" @endif  value="0" checked>本鄉鄉民
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==1) checked @else onclick="return false;" @endif value="1">本縣縣民
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==2) checked @else onclick="return false;" @endif value="2">外縣市民
            </th>
            <th>
                <select name="discount{{$key+1}}">
                    @if($value->discount==0)
                    <option value='0'>無</option>
                    @endif
                    @if($value->discount==1)
                    <option value='1'>復興村13.14鄰 106.8.18以前優待80%</option>
                    @elseif($value->discount==2)
                    <option value='2'>復興村13.14鄰 106.8.19以後優待50%</option>
                    @elseif($value->discount==3)
                    <option value='3'>101.9.1~103.11.30入懷恩堂優待10%</option>
                    @elseif($value->discount==4)
                    <option value='4'>曾設籍本鄉減免5%</option>
                    @endif
                </select>
            </th>
        </tr>
        <tr>
            <th>死亡日期</th>
            <th>死亡原因</th>
            <th>死亡地點</th>
            <th>出生日期</th>
            <th>預計遷出日期</th>
            <th>預計遷出時間</th>
        </tr>
        <tr>
            <th><input class="form-control " readonly  id="dead_date{{$key+1}}" name="dead_date{{$key+1}}" value="{{$value->dead_date}}" readonly=""></th>
            <th>
                <select name="dead_reason{{$key+1}}">
                    <option value="{{$value->dead_reason}}">{{$value->dead_reason}}</option>
                </select>
            </th>
            <th>
                <input class="form-control " readonly placeholder="" id="dead_location{{$key+1}}" name="dead_location{{$key+1}}" type="text" value="{{$value->dead_location}}" readonly="">
            </th>
            <th>
                <input class="form-control " readonly placeholder="" id="birthday{{$key+1}}" name="birthday{{$key+1}}"  value="{{$value->birthday}}" readonly="">
            </th>
            <th>
                <input class="form-control datepickerTW" readonly placeholder="" id="expected_date{{$key+1}}" name="expected_date{{$key+1}}"  value="{{$value->expected_date}}" required="">
            </th>
            <th>
                <input class="form-control timepicker"  placeholder="" id="expected_time{{$key+1}}" name="expected_time{{$key+1}}"  value="{{$value->expected_time}}" required="">
            </th>
        </tr>
    </table>
    @endforeach
    </div>
    <div class="form-group">
        <h4>原櫃位資訊</h4>
    </div>
    <div class="form-group">
        @foreach($cabinet_appliction_data as $data_key =>$data_value)
        {{-- <input name="old_cabinet_class_data{{$data_key+1}}" type="hidden" value="{{$data_value->id}}" > --}}
        <table  class="cabinet_table">
            <tr style="background-color: #ecdfc2">
                <th colspan="6" >逝者{{$data_key+1}}</th>
            </tr>
            <tr>
                <th>櫃位區域</th>
                <th>排</th>
                <th>層</th>
                <th>位置</th>
                <th>方位座向</th>
            </tr>
            <tr>
                <th>
                    <select name="cabinet_class_aera{{$data_key+1}}">
                        @if($data_value->cabinet_class_aera==$area[$data_key]->id)
                        <option value="{{$area[$data_key]->id}}">{{$area[$data_key]->code.' '.$area[$data_key]->name}}</option>
                        @endif
                    </select>
                </th>
                
                 <th>
                    <select name="cabinet_class_layer{{$data_key+1}}">
                         <option value="{{$layer[0]->id}}">{{'編號:'.$layer[0]->code.','.$layer[0]->name}}</option>
                        
                    </select>
                </th> 
                 <th>
                    <select name="cabinet_class_row{{$data_key+1}}" >
                        <option value="{{$row[$data_key]->id}}">{{'編號:'.$row[$data_key]->code.','.$row[$data_key]->name.'排'}}</option>
                    </select> 
                </th> 
                <input type="hidden" name="cabinet_class_seat{{$data_key+1}}" value="{{$data_value->cabinet_class_seat}}">
                 <th style="width:25%">
                    <select name="cabinet_code{{$data_key+1}}">
                        <option value="{{$cabinet_code[$data_key]->code}}">{{$cabinet_code[$data_key]->code}}</option>
                    </select>
                </th> 
                <th>
                     <select name="cabinet_class_position{{$data_key+1}}">
                        @foreach($position as $position_key =>$position_value)
                        @if($data_value->cabinet_class_position==$position_key)
                        <option value="{{$position_key}}">{{$position_value}}</option>
                        @endif
                        @endforeach
                    </select>
                </th> 
            </tr>
        </table>
        @endforeach
    </div>

    <div class="form-group">
        <h4>委託人資訊</h4>
    </div>
    <input class="" placeholder="" name="cabinet_principal_id" type="hidden" value="{{$cabinet_principal->id}}">
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client" name="client" type="text" value="{{$cabinet_principal->client}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_number" name="client_number" type="text" value="{{$cabinet_principal->client_number}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_household_registration" name="client_household_registration" value="{{$cabinet_principal->client_household_registration}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_tel" name="client_tel" type="text" value="{{$cabinet_principal->client_household_registration}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_phone" name="client_phone" type="text" value="{{$cabinet_principal->client_phone}}">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_address" name="client_address" type="text" value="{{$cabinet_principal->client_address}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{$users->name}}
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">退費金額：</label>
        <div class="col-sm-9">
            <input type="number" class="form-control" placeholder="" id="cost" name="cost" type="text" min="0" value="0" required="">
        </div>
    </div>
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50">{{$cabinet_appliction->ext}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-1">
            <button class="btn btn-success d_s" type="submit">送出</button>
        </div>
        <div class="col-sm-3" style="margin-left: 10px;">
            <a class="btn btn-success " href="{{asset('BackCabinetOutAppliction')}}">返回</a>
        </div>
    </div>
</form> 
@endif
<script type="text/javascript">
    var aa=true;
    var b=true;
    var c=true;
    $('#form').submit(function(){
        $('.name_number').each(function(){
            val=$(this).val();
            if(val!=''){
                if(checkTwID(val)==false){
                    aa=checkTwID(val);
                    return false;
                }
            }
        });
        applicant_number=$('#applicant_number').val();
        if(applicant_number!=''){
            b=checkTwID(applicant_number);
        }
        client_number=$('#client_number').val();
        if(client_number!=''){
            c=checkTwID(client_number);
        }
        if(aa&b&c){
            $('.d_s').attr('disabled', true);
            return true;
        }else{
            return false;
        }
    });
    function checkTwID(id){

    //建立字母分數陣列(A~Z)
    a=id;
    var city = new Array(1,10,19,28,37,46,55,64,39,73,82, 2,11,20,48,29,38,47,56,65,74,83,21, 3,12,30);

    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式

        if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {

            alert(a+',身分證不符');

            return false;

        } else {
            //將字串分割為陣列(IE必需這麼做才不會出錯)

            id = id.split('');

            //計算總分

            var total = city[id[0].charCodeAt(0)-65];

            for(var i=1; i<=8; i++){

                total += eval(id[i]) * (9 - i);

            }
            //補上檢查碼(最後一碼)

            total += eval(id[9]);

            //檢查比對碼(餘數應為0);
            if((total%10 == 0)){
                return true;
            }else{
                alert(a+',身分證不符');
                return false;
            }
        }
    }
   function SaveData(qty_id){
        $.ajax({
            url: "{{ asset('BackCabinetApplictionApi') }}",
            data: { 
                "class": $("#area"+qty_id).find("option:selected").val(),
                "layer": $("#layer"+qty_id).find("option:selected").val(),
                "row": $("#row"+qty_id).find("option:selected").val(),
                "seat": $("#seat"+qty_id).find("option:selected").val(),
                // "position": $("#position").find("option:selected").val(),
             },
            dataType:"html",
            type: "get",
            success: function(data){
                $("#cabinet_code"+qty_id+" option").remove();
                $("#cabinet_code"+qty_id).append(data);
            },
            error: function(){
            }
        });
    }
    
   function change_sys_select(type,qty_id) {

        cabinet_type=$('#cabinet_type').val();

        area=$('#area'+qty_id).find(':selected').val();
        row=$('#row'+qty_id).find(':selected').val();
        layer=$('#layer'+qty_id).find(':selected').val();
        position=$('#position'+qty_id).find(':selected').val();
        seat=$('#seat'+qty_id).find(':selected').val();

        if(type=='area'){
            data={cabinet_type:cabinet_type,area:area,position:position,qty_id:qty_id};
        }else if(type=='layer'){
            data={cabinet_type:cabinet_type,area:area,layer:layer,position:position,qty_id:qty_id};
        }else if(type=='row'){
            data={cabinet_type:cabinet_type,area:area,layer:layer,row:row,position:position,qty_id:qty_id};
        }else if(type=='position'){
            data={cabinet_type:cabinet_type,area:area,row:row,layer:layer,position:position,qty_id:qty_id};
        }else if(type=='seat'){
            data={cabinet_type:cabinet_type,area:area,row:row,layer:layer,position:position,seat:seat,qty_id:qty_id};
        }

        $.ajax({
            url: "{{ asset('BackCabinetApplictionGetArea') }}",
            data: data,
            dataType:"html",
            // type: "get",
            success: function(data){
                $('#table'+qty_id).html(data);
            },
            error: function(){
            }
        });
    }
    $('#select_no_a').click(function(){
        select_applicant_number=$('#select_applicant_number').val();
        $.ajax({
            url: "{{ asset('BackCabinetOutGetNo') }}",
            data: {select_applicant_number:select_applicant_number},
            dataType:"json",
            type: "get",
            success: function(data){
                option='';
                $.each(data,function(a){
                    option+="<option value='"+data[a]['id']+"'>"+data[a]['no']+"</option>";
                });
                $('#select_no').html(option);
            },
            error: function(){
            }
        });
    });
    $('#same_applicant').click(function(){
        $('#address').val($('#household_registration').val());
    });
    $('#same_client_applicant').click(function(){
        $('#client_address').val($('#client_household_registration').val());
    });
    $('#applicant_number').blur(function(){
        select_data('applicant_number');
    });
    $('#client_number').blur(function(){
        select_data('client_number');
    });

    function select_data(id) {
        if(id=='applicant_number'){
            type=1;
            number=$('#applicant_number').val();
        }else{
            type=2;
            number=$('#client_number').val();
        }
        if(number.length==10){
             $.ajax({
                url: "{{ asset('BackGetData') }}",
                data: {type:type,number:number},
                dataType:"json",
                type: "get",
                success: function(data){
                    if(data[0]!=0){
                        $.each(data,function(i){
                            $('#'+i).val(data[i]);
                        });
                    }
                },
                error: function(){
                }
            });
        }   
    }
</script>
@stop
