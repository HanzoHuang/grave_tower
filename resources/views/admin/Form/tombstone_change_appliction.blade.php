@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!! Html::script(asset('js/ckeditor/ckeditor.js')) !!}
{!! Html::style(asset('backend/plugins/bootstrap-datepicker/css/datepicker3.css'))  !!}
{!! Html::script(asset('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'))!!}
{!! Html::script(asset('js/bootstrap-datetimepicker.js'))!!}
<link rel="stylesheet" href="{{asset('css/lightbox.min.css')}}">
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}
 <?php $tombstoneSys=CabinetSys(); ?>
<form method="GET" action="{{asset($url).'/create'}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" >
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">逝者身份證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="deceased_number" name="deceased_number" type="text" @if(Input::has('deceased_number')) value="{{Input::get('deceased_number')}}" @endif >
        </div>
        <div class="col-sm-3">
            <input type="submit" class="form-control" placeholder=""  type="text" >
        </div>
    </div>
</form>
@if(isset($tombstone_appliction))
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="tombstone_appliction_type" value="2">
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="no" name="no" type="text" readonly="readonly" value="{{date('Ymd').rand('1111','9999')}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請狀態</label>
        <div class="col-sm-3">
            <input  type="radio" id="status" name="status" value='400' checked>申請異動
            <input  type="radio" id="status" name="status" value='500' onclick="return false;">已繳費
            <input  type="radio" id="status" name="status" value='600' onclick="return false;">已異動
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="application_date" name="application_date" type="date" value="" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant" name="applicant" type="text" value="{{$tombstone_appliction->applicant}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant_number" name="applicant_number" type="text" value="{{$tombstone_appliction->applicant_number}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="household_registration" name="household_registration" type="text" value="{{$tombstone_appliction->household_registration}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="tel" name="tel" type="text" value="{{$tombstone_appliction->tel}}" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="phone" name="phone" type="text"  value="{{$tombstone_appliction->phone}}" placeholder="0912-383012">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="relationship" name="relationship" type="text" value="{{$tombstone_appliction->relationship}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction->address}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
            <input  type="checkbox" id="same_applicant" name="same_applicant">同戶籍地址
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">
            <input  type="file" id="file1" name="file1">
            
        </div>
    </div>

    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">相片</label>
        <div class="col-sm-6">
            <input  type="file" id="file2" name="file2">
            
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            <input  type="file" id="file3" name="file3">
            
        </div>
    </div>
    

    <div class="form-group">
        <h4>牌位者資訊</h4>
    </div>
    <input class="" placeholder="" name=tombstone_appliction_user_id" type="hidden" value="{{$tombstone_appliction_user->id}}">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者：</label>
        <div class="col-sm-3">
            
            <input class="form-control" placeholder="" id="name" name="name" type="text" value="{{$tombstone_appliction_user->name}}" >
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位者身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="name_number" name="name_number" type="text"  value="{{$tombstone_appliction_user->name_number}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者戶籍地址：</label>
        <div class="col-sm-9">

            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction_user->address}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">性別</label>
        <div class="col-sm-9">
            <input  type="radio" id="gender" name="gender" value='1' @if($tombstone_appliction_user->gender=='1') checked @endif>男
            <input  type="radio" id="gender" name="gender" value='2' @if($tombstone_appliction_user->gender=='2') checked @endif>女
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">身分</label>
        <div class="col-sm-9">
            <input  type="radio" name="identity" value="0" @if($tombstone_appliction_user->identity=='0') checked @endif>本鄉鄉民
            <input  type="radio" name="identity" value="1" @if($tombstone_appliction_user->identity=='1') checked @endif>本縣縣民
            <input  type="radio" name="identity" value="2" @if($tombstone_appliction_user->identity=='2') checked @endif>外縣市民
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">優待</label>
        <div class="col-sm-9">
            <select name="discount">
                <option value='1' @if($tombstone_appliction_user->discount=='1') selected @endif>復興村13.14鄰 106.8.18以前優待80%</option>
                <option value='2' @if($tombstone_appliction_user->discount=='2') selected @endif>復興村13.14鄰 106.8.19以後優待50%</option>
                <option value='3' @if($tombstone_appliction_user->discount=='3') selected @endif>101.9.1~103.11.30入懷恩堂優待10%</option>
                <option value='4' @if($tombstone_appliction_user->discount=='4') selected @endif>曾設籍本鄉減免5%</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">出生日期：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="birthday" name="birthday" type="date" value="{{$tombstone_appliction_user->birthday}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">進塔日期：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="change_date" name="change_date" type="date" value="{{$tombstone_appliction->change_date}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">預計換位日期日期：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="expected_date" name="expected_date" type="date">
        </div> 
    </div>
    <div class="form-group">
        <h4>原牌位資訊</h4>
    </div>
    <input class="" placeholder="" name="old_tombstone_appliction_data_id" type="hidden" value="{{$tombstone_appliction_data->id}}">
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位類型</label>
        <div class="col-sm-9">
            <input  type="radio" name=tombstone_type" value="L">長生
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">方位座向</label>
        <div class="col-sm-3">
            <select name=tombstone_class_seat">
                @foreach($tombstoneSys['position'] as $key =>$value)
                <option value="{{$key}}" @if($tombstone_appliction_data->tombstone_class_seat==$key) selected @endif>{{$value}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位樓層</label>
        <div class="col-sm-3">
            <select name=tombstone_class_floor">
                @foreach($tombstoneSys['floor'] as $key =>$value)
                <option value="{{$key}}"  @if($tombstone_appliction_data->tombstone_class_floor==$key) selected @endif>{{$value}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位區域</label>
        <div class="col-sm-3">
            <select name=tombstone_class_aera">
                @foreach($tombstoneSys['class'] as $key =>$value)
                <option value="{{$key}}" @if($tombstone_appliction_data->tombstone_class_aera==$key) selected @endif>{{$value}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位排</label>
        <div class="col-sm-3">
            <select name=tombstone_class_row">
                @foreach($tombstoneSys['row'] as $key =>$value)
                <option value="{{$key}}" @if($tombstone_appliction_data->tombstone_class_row==$key) selected @endif>{{$value}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位層</label>
        <div class="col-sm-3">
            <select name=tombstone_class_layer">
                @foreach($tombstoneSys['layer'] as $key =>$value)
                <option value="{{$key}}" @if($tombstone_appliction_data->tombstone_class_layer==$key) selected @endif>{{$value}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位位子</label>
        <div class="col-sm-3">
            <select name=tombstone_class_seat">
                @foreach($tombstone_sys as $key =>$value)
                <option value="{{$value->id}}" @if($tombstone_appliction_data->tombstone_class_seat==$key) selected @endif>{{$value->floor.'-'.$value->class.'-'.$value->row.'-'.$value->layer.'-'.$value->code}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <h4>新牌位資訊</h4>
    </div>
    <input class="" placeholder="" name=tombstone_appliction_data_id" type="hidden" value="{{$tombstone_appliction_data->id}}">
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位類型</label>
        <div class="col-sm-9">
            <input  type="radio" name=new_tombstone_type" value="L" checked>長生
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">方位座向</label>
        <div class="col-sm-3">
            <select name="new_tombstone_class_seat">
                @foreach($tombstoneSys['position'] as $key =>$value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位樓層</label>
        <div class="col-sm-3">
            <select name="new_tombstone_class_floor">
                @foreach($tombstoneSys['floor'] as $key =>$value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位區域</label>
        <div class="col-sm-3">
            <select name="new_tombstone_class_aera">
                @foreach($tombstoneSys['class'] as $key =>$value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位排</label>
        <div class="col-sm-3">
            <select name="new_tombstone_class_row">
                @foreach($tombstoneSys['row'] as $key =>$value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位層</label>
        <div class="col-sm-3">
            <select name="new_tombstone_class_layer">
                @foreach($tombstoneSys['layer'] as $key =>$value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位位子</label>
        <div class="col-sm-3">
            <select name="new_tombstone_class_seat">
                @foreach($tombstone_sys as $key =>$value)
                <option value="{{$value->id}}">{{$value->floor.'-'.$value->class.'-'.$value->row.'-'.$value->layer.'-'.$value->code}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">換位費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="cost" name="cost" type="number" min='1' value=''>
        </div>
        <label for="fname" class="col-sm-3 control-label">其他費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="other_cost" name="other_cost" type="number" min='1' value=''>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">應繳費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="amount_due" name="amount_due" type="number" min='1' value=''>
        </div>
    </div>

    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <input class="" placeholder="" name=tombstone_principal_id" type="hidden" value="{{$tombstone_principal->id}}">
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client" name="client" type="text" value="{{$tombstone_principal->client}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_number" name="client_number" type="text" value="{{$tombstone_principal->client_number}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_household_registration" name="client_household_registration" value="{{$tombstone_principal->client_household_registration}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_tel" name="client_tel" type="text" value="{{$tombstone_principal->client_tel}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_phone" name="client_phone" type="text" value="{{$tombstone_principal->client_phone}}">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_address" name="client_address" type="text" value="{{$tombstone_principal->client_address}}">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{$users->name}}
        </div>
    </div>
    
    @if($tombstone_appliction->status==400)
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">確認繳費人：</label>
        <div class="col-sm-3">
            {{Auth::user()->name}}
        </div>
        <label for="fname" class="col-sm-3 control-label">確認繳費日期：</label>
        <div class="col-sm-3">
            <input class="form-control col-sm-9" placeholder="" id="payment_date" name="payment_date" type="date" >
        </div>
    </div>
    @endif
    @if($tombstone_appliction->status==500)
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">確認繳費人：</label>
        <div class="col-sm-3">
            @if($confirm_payment_user->name) {{$confirm_payment_user->name}} @endif
        </div>
        <label for="fname" class="col-sm-3 control-label">確認繳費日期：</label>
        <div class="col-sm-3">
            {{$tombstone_appliction->payment_date}}
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">進塔人員：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="principal" name="principal" type="text">
        </div>
        <label for="fname" class="col-sm-3 control-label">進塔日期：</label>
        <div class="col-sm-3">
            <input class="form-control col-sm-9" placeholder="" id="change_date" name="change_date" type="date" >
        </div>
    </div>
    @endif
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50">{{$tombstone_appliction->ext}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-9">
            <button class="btn btn-success" type="submit">送出</button>
        </div>
    </div>
</form> 
@endif
@stop
