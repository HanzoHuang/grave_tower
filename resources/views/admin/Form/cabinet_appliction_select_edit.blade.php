@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
<style type="text/css">
    table,td,tr{
        border:solid 1px;
    }
    th{
        border:solid 1px;
        text-align: center;
        width: 15%
    }
    #table_div table{
        width: 100%
    }
</style>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {!! session('status') !!}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$LayoutTitle!!}
 <?php $CabinetSys=CabinetSys(); ?>
<div class="container-fluid container-fixed-lg"> 
<div class="panel panel-transparent"> 
<div class="panel-body"> 
<div class="row"> 
<div class="col-sm-10"> 
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="form">
    <input class="" placeholder="" name="cabinet_appliction_type" type="hidden" value='1'>
    <input name="_method" type="hidden" value="PATCH">
    <input class="" placeholder="" name="id" type="hidden" value="{{$cabinet_appliction->id}}">
    <input class="" placeholder="" name="cabinet_type" type="hidden" value="{{$cabinet_appliction_data[0]->cabinet_type}}">
    {{ csrf_field() }}
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="no" name="no" type="text" readonly="readonly" value="{{$cabinet_appliction->no}}" >
        </div>
        <label for="fname" class="col-sm-3 control-label">申請類型：</label>
        <div class="col-sm-3">
            <select name="application_type">
                @if($cabinet_appliction->application_type=='1')<option value="1" selected  >納骨櫃位</option>@endif
                @if($cabinet_appliction->application_type=='2')<option value="2"  selected >長生櫃位</option> @endif
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請狀態</label>
        <div class="col-sm-3">
            <input  type="radio" id="status" name="status" value='100'@if($cabinet_appliction->status==100) checked @else onclick="return false;" @endif>已申請
            <input  type="radio" id="status" name="status" value='200' @if($cabinet_appliction->status==200) checked @else onclick="return false;" @endif>已繳費
            <input  type="radio" id="status" name="status" value='300' @if($cabinet_appliction->status==300) checked @else onclick="return false;" @endif>已進塔
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="application_date" name="application_date" value="{{$cabinet_appliction->application_date}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant" name="applicant" type="text" value="{{$cabinet_appliction->applicant}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant_number" name="applicant_number" type="text" value="{{$cabinet_appliction->applicant_number}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="household_registration" name="household_registration" type="text" value="{{$cabinet_appliction->household_registration}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="tel" name="tel" type="text" value="{{$cabinet_appliction->tel}}" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="phone" name="phone" type="text"  value="{{$cabinet_appliction->phone}}" placeholder="0912-345678">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="relationship" name="relationship" type="text" value="{{$cabinet_appliction->relationship}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$cabinet_appliction->address}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">     
            <input  type="file" id="file1" name="file1">   
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file1))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file1)}}">檔案下載</a>
                {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file1),50) !!}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">火化許可證</label>
        <div class="col-sm-6">
            <input  type="file" id="file2" name="file2">
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file2))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file2)}}">檔案下載</a>
                {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file2),50) !!}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">死亡證明書</label>
        <div class="col-sm-6">
            <input  type="file" id="file3" name="file3">
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file3))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file3)}}">檔案下載</a>
                {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file3),50) !!}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            <input  type="file" id="file4" name="file4">
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file4))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file4)}}">檔案下載</a>
                {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file4),50) !!}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">亡者除戶謄本</label>
        <div class="col-sm-6">
            <input  type="file" id="file5" name="file5">
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file5))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file5)}}">檔案下載</a>
                {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file5),50) !!}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">申請單</label>
        <div class="col-sm-6">
            
            <input  type="file" id="file6" name="file6">
            
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file6))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file6)}}">檔案下載</a>
                {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file6),50) !!}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">繳費單</label>
        <div class="col-sm-6">
           
            <input  type="file" id="file7" name="file7">
            
            @if(is_file('Upload/CabinetAppliction/'.$cabinet_appliction->file7))
            <a href="{{asset('Upload/CabinetAppliction/'.$cabinet_appliction->file7)}}">檔案下載</a>
                {!! getImagerLightbox(asset('Upload/CabinetAppliction/'.$cabinet_appliction->file7),50) !!}
            @endif
        </div>
    </div>
    <div class="form-group">
        <h4>逝者資訊</h4>
    </div>
    @foreach($cabinet_appliction_user as $key =>$value)
    <input class="" placeholder="" name="cabinet_appliction_user_id{{$key+1}}" type="hidden" value="{{$value->id}}">
    <table>
        <tr style="background-color: #ecdfc2">
            <th colspan="6" >逝者{{$key+1}}</th>
        </tr>
        <tr >
            <th>逝者</th>
            <th>逝者身分證字號</th>
            <th>逝者戶籍地址</th>
            <th>性別</th>
            <th>身分</th>
            <th>優待</th>
        </tr>
        <tr>
            <th>
                <input class="form-control" placeholder="" id="name" name="name{{$key+1}}" type="text" value="{{$value->name}}" >
            </th>
            <th>
                <input class="form-control name_number" placeholder="" id="name_number" name="name_number{{$key+1}}" type="text"  value="{{$value->name_number}}">
            </th>
            <th>
                <input class="form-control" placeholder="" id="name_household_registration" name="name_household_registration{{$key+1}}"  maxlength="7"  type="text" value="{{$value->address}}">
            </th>
            <th>
                <input  type="radio" id="gender" name="gender{{$key+1}}" @if($value->gender==1) checked  @endif value='1' >男
                <input  type="radio" id="gender" name="gender{{$key+1}}" @if($value->gender==2) checked  @endif value='2'>女</th>
            <th>
                @if($value->identity==0)
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==0) checked  @endif  value="0" checked>本鄉鄉民
                @endif
                @if($value->identity==1)
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==1) checked  @endif value="1">本縣縣民
                @endif
                @if($value->identity==2)
                <input  type="radio" name="identity{{$key+1}}" @if($value->identity==2) checked  @endif value="2">外縣市民
                @endif
            </th>
            <th>
                <select name="discount{{$key+1}}">
                    @if($value->discount==1)
                    <option value='1' @if($value->discount==1) selected @endif>復興村13.14鄰 106.8.18以前優待80%</option>
                    @endif
                    @if($value->discount==2)

                    <option value='2' @if($value->discount==2) selected @endif>復興村13.14鄰 106.8.19以後優待50%</option>
                    @endif
                    
                    @if($value->discount==3)
                    <option value='3' @if($value->discount==3) selected @endif>101.9.1~103.11.30入館優待10%</option>
                    @endif
                    
                    @if($value->discount==4)

                    <option value='4' @if($value->discount==4) selected @endif>曾設籍本鄉減免5%</option>
                     @endif
                </select>
            </th>
        </tr>
        <tr>
            <th>死亡日期</th>
            <th>死亡原因</th>
            <th>死亡地點</th>
            <th>出生日期</th>
            <th>預計進塔日期</th>
            <th>預計進塔時間</th>
        </tr>
        <tr>
            <th><input class="form-control datepickerTW" readonly id="dead_date{{$key+1}}" name="dead_date{{$key+1}}" value="{{$value->dead_date}}"></th>
            <th>
                <select name="dead_reason{{$key+1}}">
                    <option value="自然死亡" 
                    @if ($value->dead_reason=='自然死亡')
                    selected=""
                    @endif
                    >
                    自然死亡
                    </option>
                    <option value="其他" 
                    @if ($value->dead_reason=='其他')
                    selected=""
                    @endif
                    >
                    其他
                    </option>
                </select>
            </th>
            <th>
                <input class="form-control" placeholder="" id="dead_location{{$key+1}}" name="dead_location{{$key+1}}" type="text" value="{{$value->dead_location}}">
            </th>
            <th>
                <input class="form-control datepicker" readonly placeholder="" id="birthday{{$key+1}}" name="birthday{{$key+1}}" value="{{$value->birthday}}">
            </th>
            <th>
                <input class="form-control datepicker" readonly placeholder="" id="expected_date{{$key+1}}" name="expected_date{{$key+1}}"  value="{{$value->expected_date}}">
            </th>
            <th>
                <input class="form-control datepickerTW" readonly placeholder="" id="expected_time{{$key+1}}" name="expected_time{{$key+1}}"  value="{{$value->expected_time}}">
            </th>
        </tr>
    </table>
    @endforeach 
    <div class="form-group">
        <h4>櫃位資訊</h4>
    </div>

    <div class="form-group">
        @foreach($cabinet_appliction_data as $data_key =>$data_value)
        <table id="table{{$data_key+1}}" class="cabinet_table">
            <tr style="background-color: #ecdfc2">
                <th colspan="6" >逝者{{$data_key+1}}</th>
            </tr>
            <tr>
                <th>櫃位區域</th>
                <th>方位座向</th>
                <th>排</th>
                <th>層</th>
                <th>位置</th>
            </tr>
            <tr>
                <th>
                    <select name="cabinet_class_aera{{$data_key+1}}" id="area{{$data_key+1}}" onchange='change_sys_select("area","{{$data_key+1}}");' slect-type="area" >

                        @if($data_value->cabinet_class_aera==$area[$data_key]->id)
                        <option value="{{$area[$data_key]->id}}">{{$area[$data_key]->code.' '.$area[$data_key]->name}}</option>
                        @endif
                    </select>
                </th>
                 <th>
                     <select name="cabinet_class_position{{$data_key+1}}" id="position{{$data_key+1}}" onchange='change_sys_select("position","{{$data_key+1}}");' slect-type="position">
                        @foreach($position as $position_key =>$position_value)
                        @if($data_value->cabinet_class_position==$position_key)
                        <option value="{{$position_key}}">{{$position_value}}</option>
                        @endif
                        @endforeach
                    </select>
                </th> 
                 <th>
                    <select name="cabinet_class_layer{{$data_key+1}}" id="layer{{$data_key+1}}"  onchange='change_sys_select("layer","{{$data_key+1}}");' slect-type="layer">
                        
                         <option value="{{$layer[0]->id}}">{{'編號:'.$layer[0]->code.','.$layer[0]->name}}</option>
                        
                    </select>
                </th> 
                 <th>
                    <select name="cabinet_class_row{{$data_key+1}}" id="row{{$data_key+1}}" onchange='change_sys_select("row","{{$data_key+1}}");' slect-type="row">
                        
                        <option value="{{$row[$data_key]->id}}">{{'編號:'.$row[$data_key]->code.','.$row[$data_key]->name.'排'}}</option>
                       
                    </select> 
                 </th>
                 <th style="width:25%">
                    <select name="cabinet_code{{$data_key+1}}" id='cabinet_code{{$data_key+1}}'>
                        <option value="{{$cabinet_code[$data_key]->code}}">{{$cabinet_code[$data_key]->code}}</option>
                    </select>
                </th> 
            </tr>
        </table>
        @endforeach
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">櫃位費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="cost" name="cost" type="number" min='0' value='{{$cost_total}}' readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">其他費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="other_amount_due" name="other_amount_due" id="other_amount_due" type="number" min='0' value='{{$other_total}}'>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">總費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="amount_due" name="amount_due" type="number" min='0' value='{{$total}}' readonly="">
        </div>
    </div>

    <div class="form-group">
        <h4>委託人資訊</h4>
    </div>
    <input class="" placeholder="" name="cabinet_principal_id" type="hidden" value="{{$cabinet_principal->id}}">
    {{-- cabinet_principal --}}
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client" name="client" type="text" value="{{$cabinet_principal->client}}" >
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_number" name="client_number" type="text" value="{{$cabinet_principal->client_number}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_household_registration" name="client_household_registration" value="{{$cabinet_principal->client_household_registration}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_tel" name="client_tel" type="text" value="{{$cabinet_principal->client_tel}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_phone" name="client_phone" type="text" value="{{$cabinet_principal->client_phone}}">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_address" name="client_address" type="text" value="{{$cabinet_principal->client_address}}">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{$users->name}}
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">確認繳費人：</label>
        <div class="col-sm-3">
        @if($confirm_payment_user) {{$confirm_payment_user->name}} @endif
        </div>
        <label for="fname" class="col-sm-3 control-label">確認繳費日期：</label>
        <div class="col-sm-3">
            <input class="form-control col-sm-9 datepickerTW" readonly placeholder="" id="payment_date" name="payment_date"  value="{{$cabinet_appliction->payment_date}}">
    
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">進塔人員：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="principal" name="principal" type="text" value="{{$cabinet_appliction->principal}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">進塔日期：</label>
        <div class="col-sm-3">
            <input class="form-control col-sm-9 datepickerTW" readonly placeholder="" id="change_date" name="change_date"  value="{{$cabinet_appliction->change_date}}">
        </div>
    </div>
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50" >{{$cabinet_appliction->ext}}</textarea>
        </div>
    </div>   
    <div class="form-group">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-1">
            <button class="btn btn-success" type="submit">送出</button>
        </div>
        <div class="col-sm-3" style="margin-left: 10px;">
            <a class="btn btn-success " href="{{asset('BackCabinet')}}">返回</a>
        </div>
    </div>
</form>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
    function SaveData(){
        $.ajax({

                url: "{{ asset('BackCabinetApplictionApi') }}",
                data: { 
                    "position": $("#position").find("option:selected").val(),
                    "floor": $("#floor").find("option:selected").val(),
                    "class": $("#class").find("option:selected").val(),
                    "row": $("#row").find("option:selected").val(),
                 },
                dataType:"html",
                type: "get",
                success: function(data){
                    $("#cabinet_code option").remove();
                    $("#cabinet_code" ).append(data);
                },
                error: function(){
                }
            });
    }
    $('#form').submit(function(){
        var aa=true;
        var b=true;
        var c=true;
        if($('#cost').val()==0){
            alert('費用不得為0');
            return false;
        }
        $('.name_number').each(function(){
            val=$(this).val();
            if(val!=''){
                if(checkTwID(val)==false){
                    aa=checkTwID(val);
                    return false;
                }
            }
        });
        applicant_number=$('#applicant_number').val();
        if(applicant_number!=''){
            b=checkTwID(applicant_number);
        }
        client_number=$('#client_number').val();
        if(client_number!=''){
            c=checkTwID(client_number);
        }
        if(aa&b&c){
            $('.d_s').attr('disabled', true);
            return true;
        }else{
            return false;
        }
    });
    function checkTwID(id){

    //建立字母分數陣列(A~Z)
    a=id;
    var city = new Array(1,10,19,28,37,46,55,64,39,73,82, 2,11,20,48,29,38,47,56,65,74,83,21, 3,12,30);

    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式

        if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {

            alert(a+',身分證不符');

            return false;

        } else {
            //將字串分割為陣列(IE必需這麼做才不會出錯)

            id = id.split('');

            //計算總分

            var total = city[id[0].charCodeAt(0)-65];

            for(var i=1; i<=8; i++){

                total += eval(id[i]) * (9 - i);

            }
            //補上檢查碼(最後一碼)

            total += eval(id[9]);

            //檢查比對碼(餘數應為0);
            if((total%10 == 0)){
                return true;
            }else{
                alert(a+',身分證不符');
                return false;
            }
        }
    }
    $("#other_amount_due").change(function(){
        $("#amount_due").val(parseInt($("#other_amount_due").val())+parseInt($("#cost").val()));
    });
</script>
@stop
