<label>樓層：</label>
<select name="class_floor">
	<option value="1">1樓</option>
</select>	
<label>區域：</label>
<select name="class_area" id="area" onchange="change_sys_select('area')">
	<option value="0">無</option>
	@foreach($sys['area'] as $val)
	<option value="{{$val->id}}"
		@if(isset($request_data['area']))
			@if($request_data['area']==$val->id)
			selected 
			@endif
		@endif
		>{{$val->code.' '.$val->name}}</option>
	@endforeach
</select>

<label>排：</label>
<select name="class_layer" id="layer" onchange="change_sys_select('layer')">
	<option value="0">無</option>
	@foreach($sys['layer'] as $val)
	<option value="{{$val->id}}"
		@if(isset($request_data['layer']))
			@if($request_data['layer']==$val->id)
			selected 
			@endif
		@endif
		>{{$val->code.'排 '}}</option>
	@endforeach
</select>
<label>方位：</label>
<select name="class_position" id="position" onchange="change_sys_select('position')">
	@foreach($sys['position'] as $key=> $val)
	<option value="{{$key}}"
	@if(isset($request_data['position']))
		@if($request_data['position']==$key)
		selected 
		@endif
	@endif
	>{{$val}}</option>
	@endforeach
</select>



