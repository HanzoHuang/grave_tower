@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}
<form method="GET" action="{{asset($url).'/create'}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" >
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請者身份證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="select_number" name="select_number" type="text" @if(Input::has('select_number')) value="{{Input::get('select_number')}}" @endif >
            <a class="btn" id="select_no_a">查詢單號</a>
        </div>
        <div class="col-sm-3">
            <select  class="form-control" name="select_no" id="select_no">

            </select>
        </div>
        <div class="col-sm-3">
            <input type="submit" class="form-control" placeholder=""  type="text" >
        </div>
    </div>
</form>

@if(isset($tombstone_appliction))
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="tombstone_appliction_type" value="2">
    <input type="hidden" name="old_tombstone_appliction_id" value="{{$tombstone_appliction->id}}">
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="no" name="no" type="text" readonly="readonly" value="{{$no}}" >
        </div>
        
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請狀態</label>
        <div class="col-sm-3">
            <input  type="radio" id="status" name="status" value='700' checked>已申請
            <input  type="radio" id="status" name="status" value='800' onclick="return false;">已出塔
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="application_date" name="application_date" value="{{tw_date_now()}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant" name="applicant" type="text" value="{{$tombstone_appliction->applicant}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant_number" name="applicant_number" type="text" value="{{$tombstone_appliction->applicant_number}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="household_registration" name="household_registration" type="text" value="{{$tombstone_appliction->household_registration}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="tel" name="tel" type="text" value="{{$tombstone_appliction->tel}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="phone" name="phone" type="text"  value="{{$tombstone_appliction->phone}}">
        </div>
    </div>
     <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <select name="relationship">
                <option value="祖孫">祖孫</option>
                <option value="父子">父子</option>
                <option value="父女">父女</option>
                <option value="母子">母子</option>
                <option value="母女">母女</option>
                <option value="夫妻">夫妻</option>
                <option value="祖先">祖先</option>
                <option value="其他">其他</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction->address}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
            <input  type="checkbox" id="same_applicant" name="same_applicant">同戶籍地址
        </div>
    </div>
    <div class="form-group">
        <h4>委託人資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_principal_id" type="hidden" value="{{$tombstone_principal->id}}">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client" name="client" type="text" value="{{$tombstone_principal->client}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_number" name="client_number" type="text" value="{{$tombstone_principal->client_number}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_household_registration" name="client_household_registration" value="{{$tombstone_principal->client_household_registration}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_tel" name="client_tel" type="text" value="{{$tombstone_principal->client_tel}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_phone" name="client_phone" type="text" value="{{$tombstone_principal->client_phone}}">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_address" name="client_address" type="text" value="{{$tombstone_principal->client_address}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">
            <input  type="file" id="file1" name="file1">
            
        </div>

    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            <input  type="file" id="file2" name="file2">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">申請書</label>
        <div class="col-sm-6">
            <input  type="file" id="file3" name="file3">
            
        </div>
    </div>

    
    <div class="form-group">
        <h4>牌位資訊</h4>
    </div>
    <input class="" placeholder="" name="old_tombstone_appliction_data_id" type="hidden" value="{{$tombstone_appliction_data->id}}">
    <input class="" placeholder="" name="tombstone_appliction_data_id" type="hidden" value="{{$tombstone_appliction_data->id}}">
    
   <div id="sys_select_div">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">櫃位區域</label>
        <div class="col-sm-3">
            <select name="tombstone_class_aera" id="area" onchange="change_sys_select('area')">
                @foreach($area as $key =>$value)
                <option value="{{$value->id}}">{{$value->code.' '.$value->name}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">方位座向</label>
        <div class="col-sm-3">
            <select name="tombstone_class_position" id="position" onchange="change_sys_select('position')">
                @foreach($position as $key =>$value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">排</label>
        <div class="col-sm-3">
            <select name="tombstone_class_layer" id="layer" onchange="change_sys_select('layer')">
                @foreach($layer as $key =>$value)
                <option value="{{$value->id}}">{{$value->code}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">位子</label>
        <div class="col-sm-3">
            <select name="tombstone_class_seat" id='seat' onchange="change_sys_select('seat')">
                @foreach($seat as $key =>$value)
                <option value="{{$value->id}}">{{$value->code}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group"  >
        <label for="fname" class="col-sm-3 control-label">櫃位位子</label>
        <div class="col-sm-9">
            <select name="tombstone_code" id='tombstone_code'>
                @foreach($sys as $key =>$value)
                    <option value="{{$value->code}}">{{$value->code}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
    <div class="form-group">
        <h4>逝者資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_appliction_user_id" type="hidden" value="{{$tombstone_appliction_user->id}}">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">逝者：</label>
        <div class="col-sm-3">
            
            <input class="form-control" placeholder="" id="name" name="name" type="text" value="{{$tombstone_appliction_user->name}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">逝者身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="name_number" name="name_number" type="text"  value="{{$tombstone_appliction_user->name_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">逝者戶籍地：</label>
        <div class="col-sm-9">

            <input class="form-control" placeholder="" id="name_household_registration" name="name_household_registration" type="text" value="{{$tombstone_appliction_user->address}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">性別</label>
        <div class="col-sm-9">
            <input  type="radio" id="gender" name="gender" value='1' @if($tombstone_appliction_user->gender=='1') checked @else onclick="return false;" @endif>男
            <input  type="radio" id="gender" name="gender" value='2' @if($tombstone_appliction_user->gender=='2') checked @else onclick="return false;" @endif>女
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">身分</label>
        <div class="col-sm-9">
            <input  type="radio" name="identity" value="0" @if($tombstone_appliction_user->identity=='0') checked @else onclick="return false;" @endif>本鄉鄉民
            <input  type="radio" name="identity" value="1" @if($tombstone_appliction_user->identity=='1') checked @else onclick="return false;" @endif>本縣縣民
            <input  type="radio" name="identity" value="2" @if($tombstone_appliction_user->identity=='2') checked @else onclick="return false;" @endif>外縣市民
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">出生日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="birthday" name="birthday" value="{{$tombstone_appliction_user->birthday}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">預計遷出日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="expected_date" name="expected_date"   required="" value="{{tw_date_now()}}">
        </div>
        
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{$users->name}}
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">退費金額：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="cost" name="cost" type="number" min='0' value='{{old('cost',0)}}' >
        </div>
    </div>
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50">{{$tombstone_appliction->ext}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-1">
            <button class="btn btn-success d_s" type="submit">送出</button>
        </div>
        <div class="col-sm-3" style="margin-left: 10px;">
            <a class="btn btn-success " href="{{asset('BackTombstoneOutAppliction')}}">返回</a>
        </div>
    </div>
</form> 
@endif
<script type="text/javascript">


    $("#position option").remove();
      if($("#area").val()==1){
         $("#position").append('<option value="2">座南朝北</option>');
      }
      else if($("#area").val()==2){
        $("#position").append('<option value="1">座北朝南</option>');
    }

    var aa=true;
    var b=true;
    var c=true;
    $('#form').submit(function(){
        $('.name_number').each(function(){
            val=$(this).val();
            if(val!=''){
                if(checkTwID(val)==false){
                    aa=checkTwID(val);
                    return false;
                }
            }
        });
        applicant_number=$('#applicant_number').val();
        if(applicant_number!=''){
            b=checkTwID(applicant_number);
        }
        client_number=$('#client_number').val();
        if(client_number!=''){
            c=checkTwID(client_number);
        }
        if(aa&b&c){
            $('.d_s').attr('disabled', true);
            return true;
        }else{
            return false;
        }
    });
    function checkTwID(id){

    //建立字母分數陣列(A~Z)
    a=id;
    var city = new Array(1,10,19,28,37,46,55,64,39,73,82, 2,11,20,48,29,38,47,56,65,74,83,21, 3,12,30);

    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式

        if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {

            alert(a+',身分證不符');

            return false;

        } else {
            //將字串分割為陣列(IE必需這麼做才不會出錯)

            id = id.split('');

            //計算總分

            var total = city[id[0].charCodeAt(0)-65];

            for(var i=1; i<=8; i++){

                total += eval(id[i]) * (9 - i);

            }
            //補上檢查碼(最後一碼)

            total += eval(id[9]);

            //檢查比對碼(餘數應為0);
            if((total%10 == 0)){
                return true;
            }else{
                alert(a+',身分證不符');
                return false;
            }
        }
    }
    
    function SaveData(){
        $.ajax({
            url: "{{ asset('BackTombstoneSysApi') }}",
            data: { 
                "position": $("#position").find("option:selected").val(),
                "floor": $("#floor").find("option:selected").val(),
                "class": $("#class").find("option:selected").val(),
                "row": $("#row").find("option:selected").val(),
             },
            dataType:"html",
            type: "get",
            success: function(data){
                $("#tombstone_code option").remove();
                $("#tombstone_code" ).append(data);
            },
            error: function(){
            }
        });
    }
    $('#select_no_a').click(function(){

        select_number=$('#select_number').val();
        $.ajax({
            url: "{{ asset('BackTombstoneOutGetNo') }}",
            data: {select_number:select_number},
            dataType:"json",
            type: "get",
            success: function(data){
                option='';
                $.each(data,function(a){
                    option+="<option value='"+data[a]['id']+"'>"+data[a]['no']+"</option>";
                });
                $('#select_no').html(option);
            },
            error: function(){
            }
        });
    });
</script>
@stop
