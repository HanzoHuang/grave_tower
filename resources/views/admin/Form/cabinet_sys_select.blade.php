
<label>樓層：</label>
<select name="class_floor">
	<option value="1">01 1樓</option>
</select>	
<label>區域：</label>
<select name="class_area" id="area" onchange="change_sys_select('area')">
	@foreach($sys['area'] as $val)
	<option value="{{$val->id}}"
		@if(isset($request_data['area']))
			@if($request_data['area']==$val->id)
			selected 
			@endif
		@endif
		>{{$val->code.' '.$val->name}}</option>
	@endforeach
<option value="0">全部</option>
</select>

<label>排：</label>
<select name="class_layer" id="layer" onchange="change_sys_select('layer')">
	@foreach($sys['layer'] as $val)
	<option value="{{$val->id}}"
		@if(isset($request_data['layer']))
			@if($request_data['layer']==$val->id)
			selected 
			@endif
		@endif
		>{{$val->code.'排 '}}</option>
	@endforeach
<option value="0">全部</option>
</select>
<label>層：</label>
<select name="class_row" id="row" onchange="change_sys_select('row')">
	@foreach($sys['row'] as $val)
	<option value="{{$val->id}}"
		@if(isset($request_data['row']))
			@if($request_data['row']==$val->id)
			selected 
			@endif
		@endif
		>{{$val->code.'層 '}}</option>
	@endforeach
<option value="0">全部</option>
</select>
<label>方位：</label>
<select name="class_position" id="position" onchange="change_sys_select('position')">
	@foreach($sys['position'] as $key=> $val)
	<option value="{{$key}}"
	@if(isset($request_data['position']))
		@if($request_data['position']==$key)
		selected 
		@endif
	@endif
	>{{$val}}</option>
	@endforeach
<option value="0">全部</option>
</select>
