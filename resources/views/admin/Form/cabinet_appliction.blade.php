@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
<style type="text/css">
    table,td,tr{
        border:solid 1px;
    }
    th{
        border:solid 1px;
        text-align: center;
        width: 15%;
    }
    #table_div table{
        width: 100%
    }
    #table_div table{
        width: 100%
    }
</style>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile!!}
<form method="GET" action="{{asset($url).'/create'}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" >
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">類型</label>
        <div class="col-sm-3">
            <select class="form-control"  name="cabinet_type" type="text">
                <option value="個人" @if(Input::get('cabinet_type')=='個人') selected @endif>個人</option>
                <option value="夫妻" @if(Input::get('cabinet_type')=='夫妻') selected @endif>夫妻</option>
                <option value="家族" @if(Input::get('cabinet_type')=='家族') selected @endif>家族</option>
            </select>
        </div>
        <div class="col-sm-3">
            <input type="submit" class="form-control"  type="text"  >
        </div>
    </div>
</form>
@if(Input::has('cabinet_type'))
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="form">
    <input class=""  name="id" id="id" type="hidden">
    <input class=""  name="status" id="status" type="hidden" value='100'>
    <input class=""  name="cabinet_appliction_type" type="hidden" value='1'><div class="form-group">
    <input type="hidden" name="cabinet_type" id="cabinet_type" value="{{Input::get('cabinet_type')}}">
    {{ csrf_field() }}
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control"  id="no" name="no" type="text" readonly="readonly" value="{{$no}}" >
        </div>
        
    </div>
    <div class="form-group">
        
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly id="application_date" name="application_date"  required="" value="{{tw_date_now()}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control"  value="{{ old('applicant') }}" id="applicant" name="applicant" type="text" required="">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control"  id="applicant_number" name="applicant_number" type="text" required="" value="{{ old('applicant_number') }}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control"  id="household_registration" name="household_registration" type="text" value="宜蘭縣壯圍鄉" required=""  >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control"  id="tel" name="tel" type="text" required="" placeholder="03-9383012" value="{{ old('tel') }}">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control"  id="phone" name="phone" type="text" placeholder="0912-345678" value="{{ old('phone') }}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <select name="relationship">
                <option value="祖孫">祖孫</option>
                <option value="父子">父子</option>
                <option value="父女">父女</option>
                <option value="母子">母子</option>
                <option value="母女">母女</option>
                <option value="夫妻">夫妻</option>
                <option value="祖先">祖先</option>
                <option value="其他">其他</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control"  id="address" name="address" type="text" required="" value="{{ old('address') }}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
            <input  type="checkbox" id="same_applicant" name="same_applicant" value="{{ old('same_applicant') }}">同戶籍地址
        </div>
    </div>
    <div class="form-group">
        <h4>委託人資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" value="{{ old('client') }}"   id="client" name="client" type="text">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" value="{{ old('client_number') }}"  id="client_number" name="client_number" type="text" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control"  id="client_household_registration" name="client_household_registration" type="text" value="{{ old('client_household_registration') }}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control"  value="{{ old('client_tel') }}"  id="client_tel" name="client_tel" type="text" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control"  value="{{ old('client_phone') }}"   id="client_phone" name="client_phone" type="text" placeholder="0912-345678">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control"  value="{{ old('client_address') }}" id="client_address" name="client_address" type="text">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
            <input  type="checkbox" value="{{ old('same_client_applicant') }}" id="same_client_applicant" name="same_applicant">同申請人通訊地址
        </div>
    </div>
    
    <div class="form-group">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">
            <input  type="file" id="file1" name="file1" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">火化許可證</label>
        <div class="col-sm-6">
            <input  type="file" id="file2" name="file2" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">死亡證明書</label>
        <div class="col-sm-6">
            <input  type="file" id="file3" name="file3" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            <input  type="file" id="file4" name="file4" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">亡者除戶謄本</label>
        <div class="col-sm-6">
            <input  type="file" id="file5" name="file5" >
        </div>
    </div>
    
    <div class="form-group" id="pod">
        <h4>櫃位資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請類型：</label>
        <div class="col-sm-3">
            <select name="application_type" id="application_type">
                <option value="1">納骨櫃位</option>
                <option value="2">長生櫃位</option>
            </select>
        </div>
    </div>
    <div class="form-group" id="table_div">
        @include('admin.Form.sys')
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">櫃位費用：</label>

        <div class="col-sm-3">
            <input class="form-control"  id="cost" name="cost" type="number" min='1' value='0' required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">其他費用：</label>
        <div class="col-sm-3">
            <input class="form-control"  id="other_amount_due" name="other_amount_due" type="number" min='0' value='{{$other_cost}}'  required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">總費用：</label>
        <div class="col-sm-3">
            <input class="form-control"  id="amount_due" name="amount_due" type="number" min='1' value='{{$other_cost}}' >
        </div>
    </div>
    <div class="form-group">
        <h4>逝者資訊</h4>
    </div>
    <table style="width:100%" >
        @for($i=1;$i<=$qty;$i++)
        <tr style="background-color: #ecdfc2">
            <th colspan="6" >逝者{{$i}}</th>
        </tr>
        <tr >
            <th>逝者</th>
            <th>逝者身分證字號</th>
            <th>逝者戶籍地址</th>
            <th>性別</th>
            <th>身分</th>
            <th>優待</th>
        </tr>
        <tr>
            <th>
                <input class="form-control"  id="name" name="name{{$i}}" type="text" value="{{ old('name'.$i) }}">
            </th>
            <th>
                <input class="form-control name_number"  id="name_number" name="name_number{{$i}}" type="text" value="{{ old('name_number'.$i) }}" >
            </th>
            <th>
                <input class="form-control" id="name_household_registration" name="name_household_registration{{$i}}" type="text"  value="{{ old('name_household_registration'.$i) }}">
            </th>
            <th>
                <input  type="radio" id="gender{{$i}}" name="gender{{$i}}" value='1' checked>男
                <input  type="radio" id="gender{{$i}}" name="gender{{$i}}" value='2'>女
            </th>
            <th>
            @if(Input::get('cabinet_type')=='家族')
                @if($i==1)
                <input  type="radio" name="identity{{$i}}" id="identity{{$i}}" value="0" checked onclick="calculate();">本鄉鄉民
                <input  type="radio" name="identity{{$i}}" id="identity{{$i}}" value="1" onclick="calculate();">本縣縣民
                <input  type="radio" name="identity{{$i}}" id="identity{{$i}}" value="2" onclick="calculate();">外縣市民
                @endif
            @else
                <input  type="radio" name="identity{{$i}}" id="identity{{$i}}" value="0" checked onclick="calculate();">本鄉鄉民
                <input  type="radio" name="identity{{$i}}" id="identity{{$i}}" value="1" onclick="calculate();">本縣縣民
                <input  type="radio" name="identity{{$i}}" id="identity{{$i}}" value="2" onclick="calculate();">外縣市民
            @endif
            </th>
            <th>
                @if(Input::get('cabinet_type')=='家族')
                    @if($i==1)
                        <select name="discount{{$i}}"  id="discount{{$i}}" onchange="calculate();">
                            <option value='0'>無</option>
                        </select>
                    @endif
                @else
                    <select name="discount{{$i}}"  id="discount{{$i}}" onchange="calculate();">
                        <option value='0'>無</option>
                        <option value='1'>復興村13.14鄰 106.8.18以前優待80%</option>
                        <option value='2'>復興村13.14鄰 106.8.19以後優待50%</option>
                        <option value='3'>101.9.1~103.11.30入懷恩堂優待10%</option>
                        <option value='4'>曾設籍本鄉減免5%</option>
                        <option value='5'>復興村 13.14 鄰戶 106.8.18 前已故優待 20%</option>
                    </select>
                @endif

                
            </th>
        </tr>
        <tr>
            <th>死亡日期</th>
            <th>死亡原因</th>
            <th>死亡地點</th>
            <th>出生日期</th>
            <th>預計進塔日期</th>
            <th>預計進塔時間</th>
        </tr>
        <tr>
            <th><input readonly class="form-control datepicker"  id="dead_date{{$i}}" name="dead_date{{$i}}"  value="{{ old('dead_date'.$i) }}"></th>
            <th>
                <select name="dead_reason{{$i}}">
                    <option value="自然死亡">自然死亡</option>
                    <option value="其他">其他</option>
                </select>
            </th>
            <th>
                <input  class="form-control" value="{{ old('dead_location'.$i,'同除戶地') }}"  id="dead_location{{$i}}" name="dead_location{{$i}}"  type="text">
            </th>
            <th>
                <input  class="form-control datepicker"  value="{{ old('birthday'.$i) }}" id="birthday{{$i}}" name="birthday{{$i}}" >
            </th>
            <th>
                <input  class="form-control datepickerTW"   value="{{ old('expected_date'.$i) }}" id="expected_date{{$i}}" name="expected_date{{$i}}" >
            </th>
            <th>
                <input  class="form-control timepicker" value="{{ old('expected_time'.$i) }}" id="expected_time{{$i}}" name="expected_time{{$i}}" >
            </th>
        </tr>
        @endfor
    </table>
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{Auth::user()->name}}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-1">
            <button class="btn btn-success d_s" type="submit">儲存</button>
        </div>
        <div class="col-sm-3" style="margin-left: 10px;">
            <a class="btn btn-success " href="{{asset('BackCabinetAppliction')}}">返回</a>
        </div>
    </div>
</form>
@endif
@stop
@section('script')
<script type="text/javascript">
    $('.datepicker').datepicker({format: 'yy/mm/dd'});
    $('.timepicker').timepicker();
    $('.ta').hide();
    var aa=true;
    var b=true;
    var c=true;
    $('#form').submit(function(){
        if($('#cost').val()==0){
            alert('費用不得為0');
            return false;
        }
        $('.name_number').each(function(){
            val=$(this).val();
            if(val!=''){
                if(checkTwID(val)==false){
                    aa=checkTwID(val);
                    return false;
                }
            }
        });
        applicant_number=$('#applicant_number').val();
        if(applicant_number!=''){
            b=checkTwID(applicant_number);
        }
        client_number=$('#client_number').val();
        if(client_number!=''){
            c=checkTwID(client_number);
        }
        if(aa&b&c){
            $('.d_s').attr('disabled', true);
            return true;
        }else{
            return false;
        }
    });
    // $(".ta").hide();
    function change_sys_select(type,qty_id) {
        cabinet_type="{{Input::get('cabinet_type')}}";
        area=$('#area'+qty_id).find(':selected').val();
        row=$('#row'+qty_id).find(':selected').val();
        layer=$('#layer'+qty_id).find(':selected').val();
        position=$('#position'+qty_id).find(':selected').val();
        seat=$('#seat'+qty_id).find(':selected').val();
        cabinet_code=$('#cabinet_code1').find(':selected').val();
        if(type=='area'){
            data={type:type,cabinet_type:cabinet_type,area:area,qty_id:qty_id};
        }else if(type=='layer'){
            data={type:type,cabinet_type:cabinet_type,area:area,layer:layer,qty_id:qty_id};
        }else if(type=='row'){
            data={type:type,cabinet_type:cabinet_type,area:area,layer:layer,row:row,qty_id:qty_id};
        }else if(type=='position'){
            data={type:type,cabinet_type:cabinet_type,area:area,row:row,layer:layer,position:position,qty_id:qty_id};
        }else if(type=='seat'){
            data={type:type,cabinet_type:cabinet_type,area:area,row:row,layer:layer,seat:seat,qty_id:qty_id};
        }else if(type=='code'){
            data={type:type,cabinet_type:cabinet_type,area:area,row:row,layer:layer,seat:seat,qty_id:qty_id,cabinet_code:cabinet_code};
        }
        if(cabinet_type=='個人'){
            qty=1;
        }else if(cabinet_type=='夫妻'){
            qty=2;
        }else if(cabinet_type=='家族'){
            qty=12;
        }
        $.ajax({
            url: "{{ asset('BackCabinetApplictionGetArea') }}",
            data: data,
            dataType:"json",
            type: "get",
            success: function(data){
                if(data[0]==0){
                    if(type=='area'){
                        $('#layer1').html('<option value="0">請選擇</option>');
                        $('#row1').html('<option value="0">請選擇</option>');
                        for(i=1;i<=qty;i++){
                            $('#cabinet_code'+i).html('<option value="0">請選擇</option>');
                        }
                    }else if(type=='layer'){
                        $('#row1').html('<option value="0">請選擇</option>');
                    }
                    alert('請選擇其他樓/層/排');
                    return '';
                }
                if(type=='area'){

                    $('#layer1').html('<option value="0">請選擇</option>');
                    $('#row1').html('<option value="0">請選擇</option>');
                    for(i=1;i<=qty;i++){
                        $('#cabinet_code'+i).html('<option value="0">請選擇</option>');
                    }
                    $('#position1').html('<option value="0">請選擇</option>');
                    $.each(data,function(i){
                        option="<option value='" +data[i]['id']+ "'>" + data[i]['code'] +"</option>";
                        $('#layer1').append(option);
                    });
                }else if(type=="layer"){
                    $('#row1').html('<option value="0">請選擇</option>');
                    for(i=1;i<=qty;i++){
                        $('#cabinet_code'+i).html('<option value="0">請選擇</option>');
                    }
                    $('#position1').html('');
                    $.each(data,function(i){
                        option="<option value='" +data[i]['id']+ "'>" + data[i]['code'] +"</option>";
                        $('#row1').append(option);
                    });
                }else if(type=='row'){
                    for(i=1;i<=qty;i++){
                        $('#cabinet_code'+i).html('<option value="0">請選擇</option>');
                    }
                    $('#position1').html('<option value="0">請選擇</option>');
                    option='';
                    $.each(data['sys'],function(i){
                        if(qty>1){
                            option="<option value='" +data['sys'][i]['code']+ "'>" + data['sys'][i]['code'] +"</option>";
                        }else{
                            option+="<option value='" +data['sys'][i]['code']+ "'>" + data['sys'][i]['code'] +"</option>";
                        }
                        a=i+1;
                        if(qty>1){
                            $('#cabinet_code'+a).html(option);
                        }
                    });
                    if(qty==1){
                        $('#cabinet_code1').append(option);
                    }
                    $('#position1').html('');
                    $.each(data['position'],function(i){
                        option="<option value='" +i+ "'>" + data['position'][i] +"</option>";
                        $('#position1').append(option);
                    });
                    //計算金額
                }
                    setTimeout("calculate()",1000);
            },
            error: function(){

            }
        });
    }
    $('#same_applicant').click(function(){
        $('#address').val($('#household_registration').val());
    });
    $('#same_client_applicant').click(function(){
        $('#client_address').val($('#address').val());
    });
    //*123*//
    function show_or_hide(qty){
        for(a=1;a<=qty;a++){
            $("#layer"+a).hide();
            $("#row"+a).hide();
            $("#cabinet_code"+a).hide();
            $("#position"+a).hide();
            iii=0;
            if($("#area"+a).val()!=0){
                iii=1;
            }
            if($("#area"+a).val()!=0 & $("#layer"+a).val()!=0){
                iii=2;
            }
            if($("#area"+a).val()!=0 & $("#layer"+a).val()!=0 & $("#row"+a).val()!=0){
                iii=3;
            }
            if($("#area"+a).val()!=0 & $("#layer"+a).val()!=0 & $("#row"+a).val()!=0 & $("#cabinet_code"+a).val()!=0){
                iii=4;
            } 
            switch(iii) {
                case 0:
                    $("#area "+a).show();
                    break;
                case 1:
                    $("#layer"+a).show();
                    break;
                case 2:
                    $("#layer"+a).show();
                    $("#row"+a).show();
                    break;
                case 3:
                    $("#layer"+a).show();
                    $("#row"+a).show();
                    $("#cabinet_code"+a).show();
                    break;
                case 4:
                    $("#layer"+a).show();
                    $("#row"+a).show();
                    $("#cabinet_code"+a).show();
                    $("#position"+a).show();
                    break;
                default:
                    $("#area"+a).val(0);
                    $("#layer"+a).val(0);
                    $("#row"+a).val(0);
                    $("#cabinet_code"+a).val(0);
                    $("#position"+a).val(0);
            }
            
        } 
    }
    function calculate() {
        type=$('#cabinet_type').val();
        qty=0;
        if(type=='個人'){
            qty=1;
        }else if(type=='夫妻'){
            qty=2;

        }else if(type=='家族'){
            qty=12;
        }
        //*123*//
        show_or_hide(qty);
        total=0;
        // data=new Object;
        var a="?qty="+qty+'&';
        for (i = 1; i <= qty ; i++){
            if($('#cabinet_code'+i).find(':selected').val()==0){
                $('#cost').val(0);
                $('#amount_due').val($('#other_amount_due').val());
                return '';
            }
            identity=$('#identity1:checked').val();
            discount=$('#discount1').find(':selected').val();
            cabinet_code=$('#cabinet_code'+i).find(':selected').val();
            a+='identity'+i+'='+identity+'&';
            a+='discount'+i+'='+discount+'&';
            a+='cabinet_code'+i+'='+cabinet_code+'&';
        }
        var total=0;
        $.ajax({
            url: "{{ asset('BackCabinetCalculate') }}"+a,
            // data: adata,
            dataType:"json",
            type: "get",
            success: function(data){
                console.log(data);
                $.each(data,function(i){
                    if(data[i].price==0){
                        alert('未設定費用，請勿送出');
                    }
                    total+=data[i].price;

                });
                $('#cost').val(total);
                $('#amount_due').val(total+parseInt($('#other_amount_due').val()));
            },
            error: function(){
            }
        });
    }
    $('#applicant_number').blur(function(){
        select_data('applicant_number');
    });
    $('#client_number').blur(function(){
        select_data('client_number');
    });

    function select_data(id) {
        if(id=='applicant_number'){
            type=1;
            number=$('#applicant_number').val();
        }else{
            type=2;
            number=$('#client_number').val();
        }
        if(number.length==10){
             $.ajax({
                url: "{{ asset('BackGetData') }}",
                data: {type:type,number:number},
                dataType:"json",
                type: "get",
                success: function(data){
                    if(data[0]!=0){
                        $.each(data,function(i){
                            $('#'+i).val(data[i]);
                        });
                    }
                },
                error: function(){
                }
            });
        }   
    }
    $('#other_amount_due').on('input propertychange', function() {
        cost=parseInt($('#cost').val());
        other_amount_due=parseInt($('#other_amount_due').val());
         $('#amount_due').val(cost+other_amount_due);
    });

    function checkTwID(id){

    //建立字母分數陣列(A~Z)
    a=id;
    var city = new Array(1,10,19,28,37,46,55,64,39,73,82, 2,11,20,48,29,38,47,56,65,74,83,21, 3,12,30);

    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式

        if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {

            alert(a+',身分證不符');

            return false;

        } else {
            //將字串分割為陣列(IE必需這麼做才不會出錯)

            id = id.split('');

            //計算總分

            var total = city[id[0].charCodeAt(0)-65];

            for(var i=1; i<=8; i++){

                total += eval(id[i]) * (9 - i);

            }
            //補上檢查碼(最後一碼)

            total += eval(id[9]);

            //檢查比對碼(餘數應為0);
            if((total%10 == 0)){
                return true;
            }else{
                alert(a+',身分證不符');
                return false;
            }
        }
    }
    $("#other_amount_due").change(function(){
        $("#amount_due").val(parseInt($("#other_amount_due").val())+parseInt($("#cost").val()));
    });
</script>
@stop
