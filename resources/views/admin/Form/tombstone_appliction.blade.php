@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}
 <?php $CabinetSys=CabinetSys(); ?>

<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" id="form" class="form-horizontal" enctype="multipart/form-data">
    <input class="" placeholder="" name="id" type="hidden"><div class="form-group">
    <input class="" placeholder="" name="tombstone_appliction_type" type="hidden" value='1'>
    <input class="" placeholder="" name="status" type="hidden" value='100'><div class="form-group">
    {{ csrf_field() }}
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="no" name="no" type="text" readonly="readonly" value="{{$no}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <div class="col-sm-3">
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="application_date" name="application_date"  required="" value="{{tw_date_now()}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant" name="applicant" type="text" required="">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant_number" name="applicant_number" type="text" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="household_registration" name="household_registration" type="text" value="宜蘭縣壯圍鄉" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="tel" name="tel" type="text" required="" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="phone" name="phone" type="text" placeholder="0912-383012">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <select name="relationship">
                <option value="祖孫">祖孫</option>
                <option value="父子">父子</option>
                <option value="父女">父女</option>
                <option value="母子">母子</option>
                <option value="母女">母女</option>
                <option value="夫妻">夫妻</option>
                <option value="祖先">祖先</option>
                <option value="其他">其他</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="address" name="address" type="text" value="" required="">
        </div>
    </div>
    <div class="form-group">
        <h4>委託人資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client" name="client" type="text">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_number" name="client_number" type="text" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_household_registration" name="client_household_registration" type="text" value="宜蘭縣壯圍鄉">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_tel" name="client_tel" type="text" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_phone" name="client_phone" type="text" placeholder="0912-983012" >
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_address" name="client_address" type="text">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
            <input  type="checkbox" id="same_client_applicant" name="same_applicant">同申請人通訊地址
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">
            <input  type="file" id="file1" name="file1">
        </div>
    </div>

    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">相片</label>
        <div class="col-sm-6">
            <input  type="file" id="file2" name="file2">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            <input  type="file" id="file3" name="file3">
        </div>
    </div>
    
    <div class="form-group">
        <h4>牌位資訊</h4>
    </div>
<div id="sys_select_div">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">櫃位區域</label>
        <div class="col-sm-3">
            <select name="tombstone_class_aera" id="area" onchange="change_sys_select('area')">
                @foreach($area as $key =>$value)
                <option value="{{$value->id}}">{{$value->code.' '.$value->name}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">方位座向</label>
        <div class="col-sm-3">
            <select name="tombstone_class_position" id="position" onchange="change_sys_select('position')">
                @foreach($position as $key =>$value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">排</label>
        <div class="col-sm-3">
            <select name="tombstone_class_layer" id="layer" onchange="change_sys_select('layer')">
                @foreach($layer as $key =>$value)
                <option value="{{$value->id}}">{{$value->code}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">位置</label>
        <div class="col-sm-3">
            <select name="tombstone_class_seat" id='seat' onchange="change_sys_select('seat')">
                @foreach($seat as $key =>$value)
                <option value="{{$value->id}}">{{$value->code}}</option>
                @endforeach
            </select>
        </div>
    </div>
    
    <div class="form-group"  >
        <label for="fname" class="col-sm-3 control-label">櫃位位子</label>
        <div class="col-sm-9">
            <select name="tombstone_code" id='tombstone_code'>
                <option value="">沒有任何位置</option>
            </select>
            <a class="btn btn-success"  href="#pod" onclick="SaveData()">搜尋空位</a>
        </div>
    </div>
</div>
    <div class="form-group"  >
        <label for="fname" class="col-sm-3 control-label">選位類型</label>
        <div class="col-sm-9">
            <select name="select_type" id='select_type'>
                <option value="1">不選</option>
                <option value="2">自選+10%</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">櫃位費用：</label>

        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="cost" name="cost" type="number" min='1' value='0' >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">其他費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="other_amount_due" name="other_amount_due" type="number" min='0' value='0' >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">總費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="amount_due" name="amount_due" type="number" min='1' value='0'>
        </div>
    </div>
    <div class="form-group">
        <h4>牌位者資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="name" name="name" type="text" required="">
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位者身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="name_number" name="name_number" type="text" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="name_household_registration" name="name_household_registration" type="text" required="" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <div class="col-sm-9">
            <input  type="checkbox" id="name_same_applicant" name="name_same_applicant">同申請者戶籍地
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">性別</label>
        <div class="col-sm-9">
            <input  type="radio" id="gender" name="gender" value='1' checked="">男
            <input  type="radio" id="gender" name="gender" value='2'>女
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">身分</label>
        <div class="col-sm-9">
            <input  type="radio" name="identity" id="identity" value="0" checked="" onclick="calculate();">本鄉鄉民
            <input  type="radio" name="identity" id="identity" value="1" onclick="calculate();">本縣縣民
            <input  type="radio" name="identity" id="identity" value="2" onclick="calculate();">外縣市民
        </div>
    </div>

    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">出生日期：</label>
        <div class="col-sm-3">

            <input class="form-control datepicker" readonly placeholder="" id="birthday" name="birthday" required="" required="">
        </div>
        <label for="fname" class="col-sm-3 control-label">預計進塔日期：<br>預計進塔時間：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="expected_date" name="expected_date"  value="" required="">
            <input class="form-control timepicker"  placeholder="" id="expected_time" name="expected_time"  value="" required="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{Auth::user()->name}}
        </div>
    </div>
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-1">
            <button class="btn btn-success d_s" type="submit">送出</button>
        </div>
        <div class="col-sm-3" style="margin-left: 10px;">
            <a class="btn btn-success " href="{{asset('BackTombstoneAppliction')}}">返回</a>
        </div>
    </div>
</form>
    

<script type="text/javascript">
    @if(Session::has('_old_input'))
        @foreach(Session::get('_old_input') as $key =>$value)
            $('#{{$key}}').val('{{$value}}');
        @endforeach
    @endif
    $('.datepicker').datepicker();
    change_type();
    function SaveData(){
        $.ajax({
            url: "{{ asset('BackTombstoneSysApi') }}",
            data: { 
                "area": $("#area").find("option:selected").val(),
                "layer": $("#layer").find("option:selected").val(),
                "seat": $("#seat").find("option:selected").val(),
                "position": $("#position").find("option:selected").val()
             },
            dataType:"json",
            type: "get",
            success: function(data){
                $("#tombstone_code option").remove();
                if(data[0]==0){
                    $("#tombstone_code").append('<option>目前沒有位置</option>');

                }else{
                    a='';
                    $.each(data['data'],function(i){
                        a="<option value='"+data['data'][i]['code']+"'>"+data['data'][i]['code']+"</option>";
                    });
                    $("#tombstone_code").append(a);
                    calculate();
                    $('#other_amount_due').val(data['other_cost']);

                }
            },
            error: function(){
            }
        });
        
    }

    
    $("#other_amount_due").change(function(){
        $("#amount_due").val(parseInt($("#other_amount_due").val())+parseInt($("#cost").val()));
    });
    $("#select_type").change(function(){
        calculate();
    });
    
    function change_type(){
        $("#position option").remove();
          if($("#area").val()==1){
             $("#position").append('<option value="2">座南朝北</option>');
          }
          else if($("#area").val()==2){
            $("#position").append('<option value="1">座北朝南</option>');
          }
    }
    function change_sys_select(type) {

        area=$('#area').find(':selected').val();
        position=$('#position').find(':selected').val();
        layer=$('#layer').find(':selected').val();
        seat=$('#seat').find(':selected').val();
        if(type=='area'){
            data={area:area,position:position};
        }else if(type=='layer'){
            data={area:area,layer:layer,position:position};
        }else if(type=='seat'){
            data={area:area,layer:layer,position:position,seat:seat};
        }else if(type=='position'){
            data={area:area,layer:layer,position:position};
        }
        $.ajax({
            url: "{{ asset('BackTombstoneApplictionGetArea') }}",
            data: data,
            dataType:"html",
            type: "get",
            success: function(data){
                $('#sys_select_div').html(data);
                change_type();
            },
            error: function(){
            }
        });
    }
    function calculate() {
            select_type=$("#select_type").find("option:selected").val(),
            adata={
                identity:$('#identity:checked').val(),
                // discount:$('#discount').find(':selected').val(),
                tombstone_code:$('#tombstone_code').find(':selected').val(),
            };
            $.ajax({
                url: "{{ asset('BackTombstoneApplictionCalculate') }}",
                data: adata,
                dataType:"json",
                type: "get",
                success: function(data){
                    other_amount_due=parseInt($('#other_amount_due').val());
                    if(select_type==2){
                        data[0]=parseInt(data[0]*1.1);
                    }
                    $('#amount_due').val(data[0]+other_amount_due);
                    $('#cost').val(data[0]);
                },
                error: function(){
                }
            });
        
    }
    $('#same_applicant').click(function(){

        $('#address').val($('#household_registration').val());
    });$('#name_same_applicant').click(function(){
        $('#name_household_registration').val($('#household_registration').val());
    });

    $('#same_client_applicant').click(function(){
        $('#client_address').val($('#address').val());
    });
    $('#applicant_number').blur(function(){
        select_data('applicant_number');
    });
    $('#client_number').blur(function(){
        select_data('client_number');
    });
    function select_data(id) {
        if(id=='applicant_number'){
            type=1;
            number=$('#applicant_number').val();
        }else{
            type=2;
            number=$('#client_number').val();
        }
        if(number.length==10){
             $.ajax({
                url: "{{ asset('BackGetDataT') }}",
                data: {type:type,number:number},
                dataType:"json",
                type: "get",
                success: function(data){
                    if(data[0]!=0){
                        $.each(data,function(i){
                            $('#'+i).val(data[i]);
                        });
                    }
                },
                error: function(){
                }
            });
        }
          
    }

    $('#form').submit(function(){
        if($('#cost').val()==0){
            alert('費用不得為0');
            return false;
        }
        var aa=true;
        var b=true;
        var c=true;
        applicant_number=$('#applicant_number').val();
        if(applicant_number!=''){
            aa= checkTwID(applicant_number);
        }
        client_number=$('#client_number').val();
        if(client_number!=''){
            b= checkTwID(client_number);
        }
        name_number=$('#name_number').val();
        if(name_number!=''){
            c= checkTwID(name_number);
        }

        if(aa&b&c){
            $('.d_s').attr('disabled', true);
            return true;
        }else{
            return false;
        }
    });

    function checkTwID(id){

    //建立字母分數陣列(A~Z)
    a=id;
    var city = new Array(1,10,19,28,37,46,55,64,39,73,82, 2,11,20,48,29,38,47,56,65,74,83,21, 3,12,30);

    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式

        if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {

            alert(a+',身分證不符');

            return false;

        } else {
            //將字串分割為陣列(IE必需這麼做才不會出錯)

            id = id.split('');

            //計算總分

            var total = city[id[0].charCodeAt(0)-65];

            for(var i=1; i<=8; i++){

                total += eval(id[i]) * (9 - i);

            }
            //補上檢查碼(最後一碼)

            total += eval(id[9]);

            //檢查比對碼(餘數應為0);
            if((total%10 == 0)){
                return true;
            }else{
                alert(a+',身分證不符');
                return false;
            }
        }
    }
</script>
@stop
