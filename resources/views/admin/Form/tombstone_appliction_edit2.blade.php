@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else`
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}
<?php $tombstoneSys=CabinetSys(); ?>
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="form">
    <input name="_method" type="hidden" value="PATCH">
    <input class="" placeholder="" name="tombstone_appliction_type" type="hidden" value='1'>
    <input class="" placeholder="" name="id" type="hidden" value="{{$tombstone_appliction->id}}">
    {{ csrf_field() }}
    <div class="form-group">
        <h4>申請資訊</h4>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="no" name="no" type="text" readonly="readonly" value="{{$tombstone_appliction->no}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請狀態</label>
        <div class="col-sm-3">
            <input  type="radio" id="status" name="status" value='100' @if($tombstone_appliction->status==100) checked @else onclick="return false;" @endif>已申請
            <input  type="radio" id="status" name="status" value='200' @if($tombstone_appliction->status==200) checked @else onclick="return false;" @endif>已繳費
            <input  type="radio" id="status" name="status" value='300' @if($tombstone_appliction->status==300) checked @else onclick="return false;" @endif>已進塔
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="application_date" name="application_date"  value="{{$tombstone_appliction->application_date}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant" name="applicant" type="text" value="{{$tombstone_appliction->applicant}}" >
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant_number" name="applicant_number" type="text" value="{{$tombstone_appliction->applicant_number}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="household_registration" name="household_registration" type="text" value="{{$tombstone_appliction->household_registration}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="tel" name="tel" type="text" value="{{$tombstone_appliction->tel}}" placeholder="03-9383012">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="phone" name="phone" type="text"  value="{{$tombstone_appliction->phone}}" placeholder="0912-383012">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="relationship" name="relationship" type="text" value="{{$tombstone_appliction->relationship}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction->address}}" >
        </div>
    </div>
    <div class="form-group">
        <h4>委託人資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_principal_id" type="hidden" value="{{$tombstone_principal->id}}">
    {{-- tombstone_principal --}}
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client" name="client" type="text" value="{{$tombstone_principal->client}}" >
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_number" name="client_number" type="text" value="{{$tombstone_principal->client_number}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_household_registration" name="client_household_registration" value="{{$tombstone_principal->client_household_registration}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_tel" name="client_tel" type="text" value="{{$tombstone_principal->client_tel}}" >
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_phone" name="client_phone" type="text" value="{{$tombstone_principal->client_phone}}" >
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_address" name="client_address" type="text" value="{{$tombstone_principal->client_address}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">
            @if(is_file('Upload/TombstoneAppliction/'.$tombstone_appliction->file1))
            <a href="{{asset('Upload/TombstoneAppliction/'.$tombstone_appliction->file1)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file1),50) !!} --}}
            @endif
        </div>

    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">相片</label>
        <div class="col-sm-6">
            @if(is_file('Upload/TombstoneAppliction/'.$tombstone_appliction->file2))
            <a href="{{asset('Upload/TombstoneAppliction/'.$tombstone_appliction->file2)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file2),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            @if(is_file('Upload/TombstoneAppliction/'.$tombstone_appliction->file3))
            <a href="{{asset('Upload/TombstoneAppliction/'.$tombstone_appliction->file3)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file3),50) !!} --}}
            @endif
        </div>
    </div>
    
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="cost" name="cost" type="number" min='1' value='{{$tombstone_appliction->cost}}' required="" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">其他費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="other_amount_due" name="other_amount_due" type="number" min='0' value='{{$tombstone_appliction->other_amount_due}}' required="" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">總費用：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="amount_due" name="amount_due" type="number" min='1' value='{{$tombstone_appliction->cost+$tombstone_appliction->other_amount_due}}' required="" readonly="">
        </div>
    </div>

    <div class="form-group">
        <h4>牌位資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_appliction_data_id" type="hidden" value="{{$tombstone_appliction_data->id}}">
    {{-- tombstone_appliction_data --}}

    <div id="sys_select_div">
        <div class="form-group">
            <label for="fname" class="col-sm-3 control-label">區域</label>
            <div class="col-sm-3">
                --
            </div>
            <label for="fname" class="col-sm-3 control-label">方位座向</label>
            <div class="col-sm-3">
                --
            </div>
        </div>
        
        <div class="form-group">
            <label for="fname" class="col-sm-3 control-label">排</label>
            <div class="col-sm-3">
               --
                            </div>
            <label for="fname" class="col-sm-3 control-label">位子</label>
            <div class="col-sm-3">
               --
            </div>
        </div>
        <div class="form-group"  >
            <label for="fname" class="col-sm-3 control-label">櫃位位子</label>
            <div class="col-sm-9">
                <input type="hidden" name="tombstone_code" value="{{$tombstone_appliction_data->tombstone_code}}">
                {{$tombstone_appliction_data->tombstone_code}}
            </div>
        </div>
    </div>
    <div class="form-group">
        <h4>牌位者資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_appliction_user_id" type="hidden" value="{{$tombstone_appliction_user->id}}">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者：</label>
        <div class="col-sm-3">
            {{-- tombstone_appliction_user --}}
            <input class="form-control" placeholder="" id="name" name="name" type="text" value="{{$tombstone_appliction_user->name}}" r>
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位者身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control name_number" placeholder="" id="name_number" name="name_number" type="text"  value="{{$tombstone_appliction_user->name_number}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者戶籍地址：</label>
        <div class="col-sm-9">

            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction_user->address}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">身分</label>
        <div class="col-sm-9">
            @if($tombstone_appliction_user->identity=='0')
            <input  type="radio" name="identity" value="0" @if($tombstone_appliction_user->identity=='0') checked @else  @endif>本鄉鄉民
            @endif
            @if($tombstone_appliction_user->identity=='1')
            <input  type="radio" name="identity" value="1" @if($tombstone_appliction_user->identity=='1') checked @else  @endif>本縣縣民
            @endif
            @if($tombstone_appliction_user->identity=='2')
            <input  type="radio" name="identity" value="2" @if($tombstone_appliction_user->identity=='2') checked @else  @endif>外縣市民
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">性別</label>
        <div class="col-sm-9">
            <input  type="radio" id="gender" name="gender" value='1' @if($tombstone_appliction_user->gender=='1') checked @else  @endif>男
            <input  type="radio" id="gender" name="gender" value='2' @if($tombstone_appliction_user->gender=='2') checked @else  @endif>女
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">出生日期：</label>
        <div class="col-sm-3">
            <input class="form-control datepicker" readonly placeholder="" id="birthday" name="birthday" value="{{$tombstone_appliction_user->birthday}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">預計進塔日期：<br>預計進塔時間：</label>
        <div class="col-sm-3">
            <input class="form-control datepickerTW" readonly placeholder="" id="expected_date" name="expected_date"  value="{{$tombstone_appliction_user->expected_date}}" >
            <input class="form-control timepicker"  placeholder="" id="expected_time" name="expected_time"  value="{{$tombstone_appliction_user->expected_time}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{$users->name}}
        </div>
    </div>   
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">確認繳費人：</label>
        <div class="col-sm-3">
            @if($confirm_payment_user) {{$confirm_payment_user->name}} @endif
        </div>
        <label for="fname" class="col-sm-3 control-label">確認繳費日期：</label>
        <div class="col-sm-3">
            {{$tombstone_appliction->payment_date}}
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">進塔人員：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" value="{{$tombstone_appliction->principal}}"  id="principal" name="principal" type="text">
        </div>
        <label for="fname" class="col-sm-3 control-label">進塔日期：</label>
        <div class="col-sm-3">
            <input class="form-control col-sm-9 datepickerTW" readonly placeholder="" value="{{$tombstone_appliction->change_date}}" id="change_date" name="change_date"  >
        </div>
    </div>
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50">{{$tombstone_appliction->ext}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        @if(isset($show))
        <div class="col-sm-1">
            <button class="btn btn-success" type="submit">確認修改</button>
        </div>
        <div class="col-sm-3" style="margin-left: 10px;">
            <a class="btn btn-success " href="{{asset('BackTombstoneSelect')}}">返回</a>
        </div>
        @endif
    </div>
</form>
<script type="text/javascript">
    
    $('#form').submit(function(){
        var aa=true;
    var b=true;
    var c=true;
        if($('#cost').val()==0){
            alert('費用不得為0');
            return false;
        }
        $('.name_number').each(function(){
            val=$(this).val();
            if(val!=''){
                if(checkTwID(val)==false){
                    aa=checkTwID(val);
                    return false;
                }
            }
        });
        applicant_number=$('#applicant_number').val();
        if(applicant_number!=''){
            b=checkTwID(applicant_number);
        }
        client_number=$('#client_number').val();
        if(client_number!=''){
            c=checkTwID(client_number);
        }
        if(aa&b&c){
            $('.d_s').attr('disabled', true);
            return true;
        }else{
            return false;
        }
    });

    function checkTwID(id){

    //建立字母分數陣列(A~Z)
    a=id;
    var city = new Array(1,10,19,28,37,46,55,64,39,73,82, 2,11,20,48,29,38,47,56,65,74,83,21, 3,12,30);

    id = id.toUpperCase();

    // 使用「正規表達式」檢驗格式

        if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {

            alert(a+',身分證不符');

            return false;

        } else {
            //將字串分割為陣列(IE必需這麼做才不會出錯)

            id = id.split('');

            //計算總分

            var total = city[id[0].charCodeAt(0)-65];

            for(var i=1; i<=8; i++){

                total += eval(id[i]) * (9 - i);

            }
            //補上檢查碼(最後一碼)

            total += eval(id[9]);

            //檢查比對碼(餘數應為0);
            if((total%10 == 0)){
                return true;
            }else{
                alert(a+',身分證不符');
                return false;
            }
        }
    }
    $("#other_amount_due").change(function(){
        $("#amount_due").val(parseInt($("#other_amount_due").val())+parseInt($("#cost").val()));
    });
</script>
@stop
