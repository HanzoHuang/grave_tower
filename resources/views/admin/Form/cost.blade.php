@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
{!! Html::script(asset('js/ckeditor/ckeditor.js')) !!}
{!! Html::style(asset('backend/plugins/bootstrap-datepicker/css/datepicker3.css'))  !!}
{!! Html::script(asset('backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'))!!}
{!! Html::script(asset('js/bootstrap-datetimepicker.js'))!!}
<style type="text/css">
ul, li {
    margin: 0;
    padding: 0;
    list-style: none;
}
.abgne_tab {
    clear: left;
    margin: 10px 0;
}
ul.tabs {
    width: 100%;
    height: 32px;
    border-bottom: 1px solid #999;
    border-left: 1px solid #999;
}
ul.tabs li {
    float: left;
    height: 31px;
    line-height: 31px;
    overflow: hidden;
    position: relative;
    margin-bottom: -1px;    /* 讓 li 往下移來遮住 ul 的部份 border-bottom */
    border: 1px solid #999;
    border-left: none;
    background: #e1e1e1;
}
ul.tabs li a {
    display: block;
    padding: 0 20px;
    color: #000;
    border: 1px solid #fff;
    text-decoration: none;
}
ul.tabs li a:hover {
    background: #ccc;
}
ul.tabs li.active  {
    background: #fff;
    border-bottom: 1px solid#fff;
}
ul.tabs li.active a:hover {
    background: #fff;
}
div.tab_container {
    clear: left;
    width: 100%;
    border: 1px solid #999;
    border-top: none;
    background: #fff;
}
div.tab_container .tab_content {
    padding: 20px;
}
div.tab_container .tab_content h2 {
    margin: 0 0 20px;
}


</style>
<div class="abgne_tab">
    <form action="{{route('BackCabinetCostDiscount.store')}}" method="POST" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <ul class="tabs">
            <li id="li_tab1" ><a href="#tab1" onclick="$('#type').val(0);">本鄉鄉民</a></li>
            <li id="li_tab2" ><a href="#tab2" onclick="$('#type').val(1);">本縣縣民</a></li>
            <li id="li_tab3" ><a href="#tab3" onclick="$('#type').val(2);">外縣市民</a></li>
        </ul>
        <input type="hidden" name="type" id="type" value="{{Input::get('type',0)}}">
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <label for="fname" >區域類型：</label>
                <select name="area_0" id="area_0" class="area" no="0">
                    @foreach($area as $key =>$value)
                        <option value="{{$value->code}}">{{$value->code.' '.$value->name}}</option>
                    @endforeach
                </select>
                <label for="fname" class="">層數：</label>
                <select name="row_0" id="row_0" class="row" no="0">
                    @foreach($row as $key =>$value)
                        <option value="{{$value->code}}">{{$value->code}}</option>
                    @endforeach
                </select>
                <br>
                <label for="fname" class="">原價：</label>
                
                <input class="" width="100px" placeholder="" id="price_0" name="price_0" type="number" value="@if(isset($cost[0])){{$cost[0]->price}}@endif">
                
                <br>
                <br>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村13.14鄰 106.8.18以前優待80%：</label>
                    <input type="radio"  name="discount1_0" id="discount1_0" value="1"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount1==1)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount1_0" id="discount1_0" value="2"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount1==2)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村13.14鄰 106.8.19以後優待50%：</label>
                    <input type="radio"  name="discount2_0" id="discount2_0"   value="1"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount2==1)
                         checked 
                        @endif
                    @endif  
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount2_0" id="discount2_0" value="2"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount2==2)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">101.9.1~103.11.30入懷恩堂優待10%    ：</label>
                    <input type="radio"  name="discount3_0" id="discount3_0"  value="1"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount3==1)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount3_0" id="discount3_0" value="2"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount3==2)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">曾設籍本鄉減免5%：</label>
                    <input type="radio"  name="discount4_0" id="discount4_0"  value="1"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount4==1)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount4_0" id="discount4_0" value="2"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount4==2)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村 13.14 鄰戶 106.8.18 前已故優待 20%”：</label>
                    <input type="radio"  name="discount5_0" id="discount5_0"  value="1" 
                    @if(isset($cost[0]))
                        @if($cost[0]->discount5==1)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount5_0" id="discount5_0" value="2"
                    @if(isset($cost[0]))
                        @if($cost[0]->discount5==2)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="dewey">關</label>
                </div>
            </div>
            <div id="tab2" class="tab_content">
                <label for="fname" class="">區域類型：</label>
                <select name="area_1" id="area_1" class="area" no="1">
                      @foreach($area as $key =>$value)
                        <option value="{{$value->code}}">{{$value->code.' '.$value->name}}</option>
                    @endforeach
                </select>
                <label for="fname" class="">層數：</label>
                <select name="row_1" id="row_1" class="row" no="1">
                    @foreach($row as $key =>$value)
                        <option value="{{$value->code}}">{{$value->code}}</option>
                    @endforeach
                </select>
                <br>
                <label for="fname" class="">原價：</label>
                <input class="" width="100px" placeholder="" id="price_1" name="price_1" type="number" value="@if(isset($cost[1])){{$cost[1]->price}}@endif">
                <br>
                <br>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村13.14鄰 106.8.18以前優待80%：</label>
                    <input type="radio"  name="discount1_1" id="discount1_1" value="1"
                    @if(isset($cost[1]))
                        @if($cost[1]->discount1==1)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount1_1" id="discount1_1" value="2"
                    @if(isset($cost[1]))
                        @if($cost[1]->discount1==2)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村13.14鄰 106.8.19以後優待50：</label>
                    <input type="radio"  name="discount2_1" id="discount2_1" value="1" 
                    @if(isset($cost[1]))
                        @if($cost[1]->discount2==1)
                         checked 
                        @endif
                    @endif 
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount2_1" id="discount2_1" value="2" 
                    @if(isset($cost[1]))
                        @if($cost[1]->discount2==2)
                         checked 
                        @endif
                    @endif 
                    />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">101.9.1~103.11.30入懷恩堂優待10%    ：</label>
                    <input type="radio"  name="discount3_1" id="discount3_1"  value="1" 
                    @if(isset($cost[1]))
                        @if($cost[1]->discount3==1)
                         checked 
                        @endif
                    @endif 
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount3_1"  id="discount3_1" value="2" 
                    @if(isset($cost[1]))
                        @if($cost[1]->discount3==2)
                         checked 
                        @endif
                    @endif 
                    />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">曾設籍本鄉減免5%：</label>
                    <input type="radio"  name="discount4_1" id="discount4_1"  value="1" 
                    @if(isset($cost[1]))
                        @if($cost[1]->discount4==1)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount4_1" id="discount4_1" value="2"
                    @if(isset($cost[1]))
                        @if($cost[1]->discount4==2)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村 13.14 鄰戶 106.8.18 前已故優待 20%”：</label>
                    <input type="radio"  name="discount5_1" id="discount5_1"  value="1" 
                    @if(isset($cost[1]))
                        @if($cost[1]->discount5==1)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount5_1" id="discount5_1" value="2"
                    @if(isset($cost[1]))
                        @if($cost[1]->discount5==2)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="dewey">關</label>
                </div>
            </div>
            <div id="tab3" class="tab_content">
                <label for="fname" class="">區域類型：</label>
                <select name="area_2" id="area_2" class="area" no="2">
                    @foreach($area as $key =>$value)
                        <option value="{{$value->code}}">{{$value->code.' '.$value->name}}</option>
                    @endforeach
                </select>
                <label for="fname" class="">層數：</label>
                <select name="row_2" id="row_2" class="row" no="2">
                    @foreach($row as $key =>$value)
                        <option value="{{$value->code}}">{{$value->code}}</option>
                    @endforeach
                </select>
                <br>
                <label for="fname" class="">原價：</label>
                <input class="" width="100px" placeholder="" id="price_2" name="price_2" type="number" value="@if(isset($cost[2])){{$cost[2]->price}}@endif">
                <br>
                <br>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村13.14鄰 106.8.18以前優待80%：</label>
                    <input type="radio"  name="discount1_2" id="discount1_2"  value="1"
                    @if(isset($cost[2]))
                        @if($cost[2]->discount1==1)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount1_2" id="discount1_2" value="2" 
                    @if(isset($cost[2]))
                        @if($cost[2]->discount1==2)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村13.14鄰 106.8.19以後優待50：</label>
                    <input type="radio"  name="discount2_2" id="discount2_2"  value="1" 
                    @if(isset($cost[2]))
                        @if($cost[2]->discount2==1)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount2_2" id="discount2_2" value="2"
                    @if(isset($cost[2]))
                        @if($cost[2]->discount2==2)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">101.9.1~103.11.30入懷恩堂優待10%    ：</label>
                    <input type="radio"  name="discount3_2" id="discount3_2"  value="1"
                    @if(isset($cost[2]))
                        @if($cost[2]->discount3==1)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount3_2" id="discount3_2" value="2" 
                    @if(isset($cost[2]))
                        @if($cost[2]->discount3==2)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">曾設籍本鄉減免5%：</label>
                    <input type="radio"  name="discount4_2" id="discount4_2"  value="1" 
                    @if(isset($cost[2]))
                        @if($cost[2]->discount4==1)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount4_2" id="discount4_2" value="2"
                    @if(isset($cost[2]))
                        @if($cost[2]->discount4==2)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="dewey">關</label>
                </div>
                <div>
                    <label for="fname" class="col-sm-4 control-label">復興村 13.14 鄰戶 106.8.18 前已故優待 20%”：</label>
                    <input type="radio"  name="discount5_2" id="discount5_2"  value="1" 
                    @if(isset($cost[2]))
                        @if($cost[2]->discount5==1)
                         checked 
                        @endif
                    @endif
                    />
                    <label for="huey">開</label>
                    <input type="radio"  name="discount5_2" id="discount5_2" value="2"
                    @if(isset($cost[2]))
                        @if($cost[2]->discount5==2)
                         checked 
                        @endif
                    @endif
                     />
                    <label for="dewey">關</label>
                </div>
            </div>
            
        </div>
        
    </div>
    <div class="col-sm-12">
        <button class="btn btn-success" style="width: 100%" type="submit">儲存</button>
    </div>
    </form>
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <br>
    <br>
    <br>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
@stop

@section('script')

<script type="text/javascript">
    $('.area').on('change',function(){
        no=$(this).attr('no');
        area=$('#area_'+no).find(':selected').val();
        type=$('#type').val();
        $.ajax({
            url: "{{ route('BackCabinetCostDiscount.GetRow') }}",
            data: { 
                "area": area,
                'type':type
             },
            dataType:"json",
            type: "get",
            success: function(data){
                if(data['row']!=0){
                    $('#row_'+no).html('');
                    $.each(data['row'],function(i){
                        $('#row_'+no).append('<option value="'+data['row'][i]['code']+'">'+data['row'][i]['code']+'</option>');
                    });
                }
                if(data['discount']!=0){
                    $('#price_'+no).val(data['discount']['price']);
                    $('#discount1_'+no+'[value="'+data['discount']['discount1']+'"]').attr('checked',true);
                    $('#discount2_'+no+'[value="'+data['discount']['discount2']+'"]').attr('checked',true);
                    $('#discount3_'+no+'[value="'+data['discount']['discount3']+'"]').attr('checked',true);
                    $('#discount4_'+no+'[value="'+data['discount']['discount4']+'"]').attr('checked',true);
                    $('#discount5_'+no+'[value="'+data['discount']['discount5']+'"]').attr('checked',true);
                }else{
                    $('#price_'+no).val(0);

                }
            },
            error: function(){
            }
        });
    });
    $('.row').on('change',function(){
        no=$(this).attr('no');
        area=$('#area_'+no).find(':selected').val();
        row=$(this).find(':selected').val();
        $.ajax({
            url: "{{ route('BackCabinetCostDiscount.GetCost') }}",
            data: { 
                no:no,
                area: area,
                row: row,
             },
            dataType:"json",
            type: "get",
            success: function(data){
                if(data!=null){
                    $('#price_'+no).val(data['price']);
                    $('#discount1_'+no+'[value="'+data['discount1']+'"]').attr('checked',true);
                    $('#discount2_'+no+'[value="'+data['discount2']+'"]').attr('checked',true);
                    $('#discount3_'+no+'[value="'+data['discount3']+'"]').attr('checked',true);
                    $('#discount4_'+no+'[value="'+data['discount4']+'"]').attr('checked',true);
                    $('#discount5_'+no+'[value="'+data['discount5']+'"]').attr('checked',true);
                }else{
                    $('#discount1_'+no+'[value="1"]').attr('checked',true);
                    $('#discount2_'+no+'[value="1"]').attr('checked',true);
                    $('#discount3_'+no+'[value="1"]').attr('checked',true);
                    $('#discount4_'+no+'[value="1"]').attr('checked',true);
                    $('#discount5_'+no+'[value="1"]').attr('checked',true);
                    $('#price_'+no).val(0);
                }
            },
            error: function(){
            }
        });
    });
 $(function(){
    // 預設顯示第一個 Tab
    var _showTab = {{Input::get('type',0)}};
    var $defaultLi = $('ul.tabs li').eq(_showTab).addClass('active');
    $($defaultLi.find('a').attr('href')).siblings().hide();
 
    // 當 li 頁籤被點擊時...
    // 若要改成滑鼠移到 li 頁籤就切換時, 把 click 改成 mouseover
    $('ul.tabs li').click(function() {
        // 找出 li 中的超連結 href(#id)
        var $this = $(this),
            _clickTab = $this.find('a').attr('href');
        // 把目前點擊到的 li 頁籤加上 .active
        // 並把兄弟元素中有 .active 的都移除 class
        $this.addClass('active').siblings('.active').removeClass('active');
        // 淡入相對應的內容並隱藏兄弟元素
        $(_clickTab).stop(false, true).fadeIn().siblings().hide();
 
        return false;
    }).find('a').focus(function(){
        this.blur();
    });
});
</script>
@stop
