@for($i=1;$i<=$qty;$i++)
<table id="table{{$i}}" class="cabinet_table">
    <tr style="background-color: #ecdfc2">
        <th colspan="6" >逝者{{$i}}</th>
    </tr>
    <tr>
        <th>櫃位區域</th>
        <th>排</th>
        <th>層/棟</th>
        <th>位置</th>
        <th>方位座向</th>
    </tr>
    <tr>
        <th style="width: 15%">
        @if($i==1)
            <select name="cabinet_class_aera{{$i}}" id="area{{$i}}" onchange='change_sys_select("area","{{$i}}");' slect-type="area" >
                <option value='0'>請選擇</option>
                @foreach($area as $key =>$value)
                <option value="{{$value->id}}"
                    @if(isset($request_data['area']))
                        @if($request_data['area']==$value->id)
                            selected
                        @endif
                    @endif
                    >{{$value->code.' '.$value->name}}</option>
                @endforeach
            </select>
        @endif
        </th>
       
        <th style="width: 15%">
        @if($i==1)
            <select class="ta" name="cabinet_class_layer{{$i}}" id="layer{{$i}}"  onchange='change_sys_select("layer","{{$i}}");' slect-type="layer">
                <option value=''>請選擇</option>
                @foreach($layer as $key =>$value)
                    <option value="{{$value->id}}"
                    @if(isset($request_data['layer']))
                        @if($request_data['layer']==$value->id)
                            selected
                        @endif
                    @endif
                    >{{'編號:'.$value->code.','.$value->name}}</option>
                @endforeach
            </select>
        @endif
        </th>
        <th style="width: 15%">
        @if($i==1)
            <select class="ta" name="cabinet_class_row{{$i}}" id="row{{$i}}" onchange='change_sys_select("row","{{$i}}");' slect-type="row">
                <option value='0'>請選擇</option>
                @foreach($row as $key =>$value)
                <option value="{{$value->id}}"
                @if(isset($request_data['row']))
                    @if($request_data['row']==$value->id)
                        selected
                    @endif
                @endif       
                >{{'編號:'.$value->code.','.$value->name}}</option>
                @endforeach
            </select>
        @endif
        </th>
        @if(isset($sys[$i-1]))
        <input type="hidden" name="cabinet_class_seat{{$i}}" value="{{$sys[$i-1]->seat}}">
        @endif
        <th style="width: 15%">
            <select class="ta"  name="cabinet_code{{$i}}" id='cabinet_code{{$i}}' onchange='change_sys_select("code","{{$i}}");calculate();'>
                <option value='0'>請選擇</option>
                 @foreach($sys as $key =>$value)
                    @if($qty>=2)
                    <option value='{{$sys[$i-1]->code}}'>{{$sys[$i-1]->code}}</option>
                    <?php break; ?>
                    @else
                    <option value='{{$value->code}}'>{{$value->code}}</option>
                    @endif
                @endforeach
            </select>
            
        </th>
        <th style="width: 15%">
        @if($i==1)
            <select class="ta" name="cabinet_class_position{{$i}}" id="position{{$i}}" slect-type="position" onchange='change_sys_select("position","{{$i}}");'>
                @foreach($position as $key =>$value)
                <option value="{{$key}}"
                @if(isset($request_data['position']))
                    @if($request_data['position']==$key)
                        selected
                    @endif
                @endif
                >{{$value}}</option>
                @endforeach
            </select>
        @endif
        </th>
       {{--  <th style="width: 25%">
            <select name="cabinet_code{{$i}}" id='cabinet_code{{$i}}'>
                <option value="">沒有任何位置</option>
            </select>
            <a class="btn btn-success"  href="#pod" onclick="SaveData('{{$i}}')">搜尋空位</a>
        </th> --}}
    </tr>
</table>
@endfor
