@extends('layouts.admin.master')
@section('title','後臺管理')
@if(isset($id))
@section('Level',Breadcrumbs::render(Route::currentRouteName(),$id))
@else
@section('Level',Breadcrumbs::render(Route::currentRouteName()))
@endif
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if (isset($DetailName))
    <div class="alert alert-success">
        項目：{{ $DetailName}}
    </div>
@endif
{!!$FormTtile;!!}


 <?php $tombstoneSys=CabinetSys(); ?>
<form method="POST" action="{{asset($url)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
    <input class="" placeholder="" name="tombstone_appliction_type" type="hidden" value='1'>
    <input name="_method" type="hidden" value="PATCH">
    <input class="" placeholder="" name="id" type="hidden" value="{{$tombstone_appliction->id}}">
    {{ csrf_field() }}
    <div class="form-group">
        <div class="col-sm-3">
        <h4>申請資訊</h4>
        </div>
        <div class="col-sm-9" style="text-align: right;">
        <a target="black" href="{{asset('BackTombstoneApplictionPdf2?no='. $tombstone_appliction->no).'& number='.$tombstone_appliction->applicant_number}}" class="btn btn-info" title="明細" style="margin-right: 10px;">申請單</a>
        <a target="black" href="{{asset('BackTombstoneApplictionPdf4?no='. $tombstone_appliction->no).'& number='.$tombstone_appliction->applicant_number}}" class="btn btn-info" title="明細" style="margin-right: 10px;">繳費單</a>
        <a target="black" href="{{asset('BackTombstoneApplictionPdf?no='. $tombstone_appliction->no).'& number='.$tombstone_appliction->applicant_number}}" class="btn btn-info" title="明細" style="margin-right: 10px;">收據</a>
        <a target="black" href="{{asset('BackTombstoneApplictionPdf3?no='. $tombstone_appliction->no).'& number='.$tombstone_appliction->applicant_number}}" class="btn btn-info" title="明細" style="margin-right: 10px;">許可證</a>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請編號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="no" name="no" type="text" readonly="readonly" value="{{$tombstone_appliction->no}}" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請狀態</label>
        <div class="col-sm-3">
            <input  type="radio" id="status" name="status" value='700' onclick="return false;">已申請
            <input  type="radio" id="status" name="status" value='800' @if($tombstone_appliction->status==700) checked @else onclick="return false;" @endif>已出塔
        </div>
        <label for="fname" class="col-sm-3 control-label">申請日期：</label>
        <div class="col-sm-3">
            <input class="form-control " readonly placeholder="" id="application_date" name="application_date" value="{{$tombstone_appliction->application_date}}" readonly="" >
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant" name="applicant" type="text" value="{{$tombstone_appliction->applicant}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="applicant_number" name="applicant_number" type="text" value="{{$tombstone_appliction->applicant_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請人戶籍地：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="household_registration" name="household_registration" type="text" value="{{$tombstone_appliction->household_registration}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="tel" name="tel" type="text" value="{{$tombstone_appliction->tel}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="phone" name="phone" type="text"  value="{{$tombstone_appliction->phone}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">關係：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="relationship" name="relationship" type="text" value="{{$tombstone_appliction->relationship}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction->address}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <h4>>委託人資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_principal_id" type="hidden" value="{{$tombstone_principal->id}}">
    {{-- tombstone_principal --}}
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client" name="client" type="text" value="{{$tombstone_principal->client}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_number" name="client_number" type="text" value="{{$tombstone_principal->client_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">委託人戶籍地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_household_registration" name="client_household_registration" value="{{$tombstone_principal->client_household_registration}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_tel" name="client_tel" type="text" value="{{$tombstone_principal->client_tel}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">行動電話：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="client_phone" name="client_phone" type="text" value="{{$tombstone_principal->client_phone}}" readonly="">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">通訊地址：</label>
        <div class="col-sm-9">
            <input class="form-control" placeholder="" id="client_address" name="client_address" type="text" value="{{$tombstone_principal->client_address}}" readonly="">
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">申請文件</label>
        <label for="fname" class="col-sm-3 control-label">申請人身分證</label>
        <div class="col-sm-6">
            @if(is_file('Upload/TombstoneAppliction/'.$tombstone_appliction->file1))
            <a href="{{asset('Upload/TombstoneAppliction/'.$tombstone_appliction->file1)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file1),50) !!} --}}
            @endif
        </div>

    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">相片</label>
        <div class="col-sm-6">
            @if(is_file('Upload/TombstoneAppliction/'.$tombstone_appliction->file2))
            <a href="{{asset('Upload/TombstoneAppliction/'.$tombstone_appliction->file2)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file2),50) !!} --}}
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label"></label>
        <label for="fname" class="col-sm-3 control-label">戶籍謄本</label>
        <div class="col-sm-6">
            @if(is_file('Upload/TombstoneAppliction/'.$tombstone_appliction->file3))
            <a href="{{asset('Upload/TombstoneAppliction/'.$tombstone_appliction->file3)}}">檔案下載</a>
                {{-- {!! getImagerLightbox(asset('Upload/tombstoneAppliction/'.$tombstone_appliction->file3),50) !!} --}}
            @endif
        </div>
    </div>
    
    


    <div class="form-group">
        <h4>牌位資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_appliction_data_id" type="hidden" value="{{$tombstone_appliction_data->id}}">
    {{-- tombstone_appliction_data --}}
       <div id="sys_select_div">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">櫃位區域</label>
        <div class="col-sm-3">
            <select name="tombstone_class_aera" id="area" onchange="change_sys_select('area')">
                @foreach($area as $key =>$value)
                <option value="{{$value->id}}">{{$value->code.' '.$value->name}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">方位座向</label>
        <div class="col-sm-3">
            <select name="tombstone_class_position" id="position" onchange="change_sys_select('position')">
                @foreach($position as $key =>$value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">排</label>
        <div class="col-sm-3">
            <select name="tombstone_class_layer" id="layer" onchange="change_sys_select('layer')">
                @foreach($layer as $key =>$value)
                <option value="{{$value->id}}">{{$value->code}}</option>
                @endforeach
            </select>
        </div>
        <label for="fname" class="col-sm-3 control-label">位子</label>
        <div class="col-sm-3">
            <select name="tombstone_class_seat" id='seat' onchange="change_sys_select('seat')">
                @foreach($seat as $key =>$value)
                <option value="{{$value->id}}">{{$value->code}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group"  >
        <label for="fname" class="col-sm-3 control-label">櫃位位子</label>
        <div class="col-sm-9">
            <select name="tombstone_code" id='tombstone_code'>
                @foreach($sys as $key =>$value)
                    <option value="{{$value->code}}">{{$value->code}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
    <div class="form-group">
        <h4>牌位者資訊</h4>
    </div>
    <input class="" placeholder="" name="tombstone_appliction_user_id" type="hidden" value="{{$tombstone_appliction_user->id}}">
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者：</label>
        <div class="col-sm-3">
            {{-- tombstone_appliction_user --}}
            <input class="form-control" placeholder="" id="name" name="name" type="text" value="{{$tombstone_appliction_user->name}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">牌位者身分證字號：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="name_number" name="name_number" type="text"  value="{{$tombstone_appliction_user->name_number}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">牌位者戶籍地：</label>
        <div class="col-sm-9">

            <input class="form-control" placeholder="" id="address" name="address" type="text" value="{{$tombstone_appliction_user->address}}" readonly="">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">性別</label>
        <div class="col-sm-9">
            <input  type="radio" id="gender" name="gender" value='1' @if($tombstone_appliction_user->gender=='1') checked @else onclick="return false;" @endif>男
            <input  type="radio" id="gender" name="gender" value='2' @if($tombstone_appliction_user->gender=='2') checked @else onclick="return false;" @endif>女
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">身分</label>
        <div class="col-sm-9">
            <input  type="radio" name="identity" value="0" @if($tombstone_appliction_user->identity=='0') checked @else onclick="return false;" @endif>本鄉鄉民
            <input  type="radio" name="identity" value="1" @if($tombstone_appliction_user->identity=='1') checked @else onclick="return false;" @endif>本縣縣民
            <input  type="radio" name="identity" value="2" @if($tombstone_appliction_user->identity=='2') checked @else onclick="return false;" @endif>外縣市民
        </div>
    </div>
    
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">出生日期：</label>
        <div class="col-sm-3">
            <input class="form-control " readonly placeholder="" id="birthday" name="birthday"  value="{{$tombstone_appliction_user->birthday}}" readonly="">
        </div>
        <label for="fname" class="col-sm-3 control-label">預計遷出日期：</label>
        <div class="col-sm-3">
            <input class="form-control " readonly placeholder="" id="expected_date" name="expected_date" value="{{$tombstone_appliction_user->expected_date}}" readonly="">
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">受理申請人：</label>
        <div class="col-sm-3">
            {{$users->name}}
        </div>
    </div>

    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">執行人員：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="principal" name="principal" type="text" required="" value="{{old('principal',Auth::user()->name)}}">
        </div>
        <label for="fname" class="col-sm-3 control-label">遷出日期：</label>
        <div class="col-sm-3">
            <input class="form-control col-sm-9 datepickerTW" readonly placeholder="" id="change_date" name="change_date"  required="" value="{{tw_date_now()}}">
        </div>
    </div>
    <div class="form-group">
        <label for="fname" class="col-sm-3 control-label">退費金額：</label>
        <div class="col-sm-3">
            <input class="form-control" placeholder="" id="cost" name="cost" type="number" min='0' value='{{$tombstone_appliction->cost}}'  readonly="">
        </div>
    </div>
    <div class="form-group">
    <label for="fname" class="col-sm-3 control-label">備註：</label>
        <div class="col-sm-9">
            <textarea name='ext' rows="10" cols="50">{{$tombstone_appliction->ext}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <p>送出前，請詳細檢查是否有遺漏的資料</p>
        </div>
        <div class="col-sm-1">
            <button class="btn btn-success" type="submit">送出</button>
        </div>
        <div class="col-sm-3" style="margin-left: 10px;">
            <a class="btn btn-success " href="{{asset('BackTombstoneOutAppliction')}}">返回</a>
        </div>
    </div>
</form>
<script type="text/javascript">
    function SaveData(){
        $.ajax({
            url: "{{ asset('BackTombstoneSysApi') }}",
            data: { 
                "position": $("#position").find("option:selected").val(),
                "floor": $("#floor").find("option:selected").val(),
                "class": $("#class").find("option:selected").val(),
                "row": $("#row").find("option:selected").val(),
             },
            dataType:"html",
            type: "get",
            success: function(data){
                $("#cabinet_code option").remove();
                $("#cabinet_code" ).append(data);
            },
            error: function(){
            }
        });
    }
    $("#position option").remove();
      if($("#area").val()==1){
         $("#position").append('<option value="2">座南朝北</option>');
      }
      else if($("#area").val()==2){
        $("#position").append('<option value="1">座北朝南</option>');
      }
</script>
@stop
