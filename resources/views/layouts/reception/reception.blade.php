<?php $mothed=getCurrentMethodName(); ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Mr.Free自由先生</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="{{asset('assets/plugins/flat-UI/css/flat-ui.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/MrFree-icon/style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/bootstrap/css/datepicker.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/bootstrap/css/jquery-popover-0.0.3.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/bootstrap/css/sb-admin-2.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/swiper/css/swiper.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" />

    {{-- <link href="{{asset('path/to/your/avgrund.css')}}" rel="stylesheet"> --}}
   
    <link href='{{asset('css/dice-menu.min.css')}}' rel='stylesheet' />
    <link href='{{asset('css/fullcalendar.css')}}' rel='stylesheet' />
    <link href='{{asset('css/fullcalendar.print.css')}}' rel='stylesheet' media='print' />
    <link href="{{asset('css/tab.css')}}" rel="stylesheet">
    <style >
    .free-menu{
        z-index: 100;
    }
    </style>
    @yield('style')
    <!--[if lt IE 9]>
       <script src="assets/js//html5shiv.min.js"></script>
       <script src="assets/js/respond.min.js"></script>
     <![endif]-->
    
    
</head>
<body>
    <div id="wrapper">
        @include('layouts.partials.ReceptionTop')
        <!--Header end-->
        @yield('content')
        @include('layouts.partials.ReceptionFooter')
    </div>
    
    <script>
        $(window).scroll(function() {
            var top=$(this).scrollTop();
            if(top<10){
                $('header').removeClass('header_fixed');
            }else{
                $('header').addClass('header_fixed');
            }
        });
    </script>
    <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/jquery.modal.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/metisMenu/metisMenu.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/sb-admin-2.js')}}"></script>
    <script src="{{asset('assets/plugins/swiper/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/plugins/masonry/js/masonry.pkgd.js')}}"></script>
    <script src="{{asset('assets/plugins/fancybox/js/jquery.fancybox.js')}}"></script>
    <script src="{{asset('assets/js/webflow.js')}}"></script>
    {{-- <script src="{{asset('assets/js/main.js')}}"></script> --}}
    <script src='{{asset('js/moment/moment.min.js')}}'></script>
    <script src='{{asset('js/fullcalendar.min.js')}}'></script>
    <script src="{{asset('js/dice-menu.min.js')}}"></script>
 
    @yield('script')
</body>
    {{-- <script src="assets/js/main.js"></script> --}}
</html>
