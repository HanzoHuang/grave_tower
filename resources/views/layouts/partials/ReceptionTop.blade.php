<header>
    <div class="header_top">
        <div class="container logo">
            <a href="{{route('Reception.Index')}}">
                <img src="{{asset('assets/plugins/img/logo.png')}}" />
            </a>
        </div>
    </div>
    <nav class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse collapse">
                {{-- <ul class="nav trvel_class">
                @foreach($menu_trip_list as $key =>$value)
                    <li style="text-align: center">
                        <a style="color:{{$value->color}}" href="{{route('Reception.CombinationList',['id'=>$value->id])}}" >
                        <img src="{{asset('Upload/MenuTripList/'.$value->img)}}" />
                            <h5 class="name">{{$value->title}}</h5>
                        </a>
                    </li>
                @endforeach   
                </ul> --}}
                <ul class="nav main" id="side-menu">
                    <li>
                        <a href="{{route('Reception.About')}}">關於我們</a>
                    </li>
                    <li>
                        <a href="{{route('Reception.News')}}">最新消息</a>
                    </li>
                    <li>
                        <a href="{{route('Reception.Contact')}}">聯絡我們</a>
                    </li>
                    <li>
                        <a href="{{route('Reception.Combination')}}">行程列表</a>
                    </li>
                    <li>
                        <a href="{{route('Reception.SingleItems')}}">單購項目</a>
                    </li>
                    <li>
                        <a href="{{route('Reception.BuyProcess')}}">消費流程</a>
                    </li>
                </ul>
                <ul class="nav member">
                    <li><a href="{{route('Reception.RLogin')}}">@if(Auth::check()){{Auth::user()->name}}您好  @endif<i class="fa fa-user-circle-o"></i>會員中心</a></li>
                </ul>
            </div>
        </div>
    </nav>
    
</header>