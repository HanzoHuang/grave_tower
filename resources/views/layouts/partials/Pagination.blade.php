<div class="row">
    <nav class="text-center">
        <ul class="pagination">
            <li>
                @if ($paginator->currentPage() != 1)
                    <a href="{{ $paginator->url($paginator->currentPage()-1) }}"><span aria-hidden="true">&#8249;</span><span class="sr-only">Previous</span></a></li>
                @else
                    <span aria-hidden="true">&#8249;</span>
                @endif
            </li>
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
            <li>
                @if ($paginator->currentPage() != $paginator->lastPage())
                    <a href="{{ $paginator->url($paginator->currentPage()+1) }}"><span aria-hidden="true">&#8250;</span><span class="sr-only">Next</span></a>
                @else
                    <span aria-hidden="true">&#8250;</span>
                @endif
            </li>
        </ul>
    </nav>
</div>