<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8" />
		<title>@yield('title')</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="apple-touch-icon" href="webroot/pages/ico/60.png">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('backend/ico/76.png')}}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('backend/ico/120.png')}}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('backend/ico/152.png')}}">
		{!!	Html::style('backend/css/pages-icons.css')  !!}
		{!!	Html::style('backend/css/pages.css')  !!}
	</head>
	<body class="fixed-header">
		@yield('content')
	</body>
</html>
