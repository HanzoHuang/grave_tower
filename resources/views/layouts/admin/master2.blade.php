<!DOCTYPE html>
<html>
	<head>
		  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	    <meta charset="utf-8" />
      <meta name="csrf-token" content="{{ csrf_token() }}" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      {!! Html::style('backend/css/bootstrap.min.css')  !!}
      {!! Html::style('backend/css/jquery-ui.css')  !!}
      {!! Html::style('backend/plugins/pace/pace-theme-flash.css')  !!}
      {!! Html::style('backend/plugins/font-awesome/css/font-awesome.css')  !!}
      {!! Html::style('backend/plugins/jquery-scrollbar/jquery.scrollbar.css')  !!}
      {!! Html::style('backend/plugins/bootstrap-select2/select2.css')  !!}
      {!! Html::script('backend/js/jquery-1.12.4.min.js')  !!}
      {!! Html::script(asset('backend/js/bootstrap.min.js'))!!}
      {!! Html::script(asset('backend/js/jquery-ui.min.js'))!!}
  	  <meta name="apple-mobile-web-app-capable" content="yes">
	    <meta name="apple-touch-fullscreen" content="yes">
	    <meta name="apple-mobile-web-app-status-bar-style" content="default">
	    <meta content="" name="description" />
	    <meta content="" name="author" />
    <title>@yield('title')</title>
    <style type="text/css">
      .hideAll  {
        visibility:hidden;
      }
    </style>

	</head>

	 <body class="fixed-header  pace-done sidebar-visible menu-pin hideAll" id="layout_body">
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <div class="sidebar-header">
        <a  target="black" href="{{asset('Banner')}}">
              {{-- <img src="{{asset('backend/img/logo_white.png')}}" alt="logo" class="brand" data-src="backend/img/logo_white.png" data-src-retina="assets/img/logo_white_2x.png" width="78" height="22"> --}}
        </a>
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </nav>
    <div class="page-container">
      <!-- START HEADER -->
      <div class="header ">
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
              <span class="icon-set menu-hambuger-plus"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table">
          <div class="header-inner">
            <div class="brand inline">
            </div>
            </div>
        </div>
        <div class=" pull-right">
          <div class="header-inner">
          </div>
        </div>
      </div>
      <!-- END HEADER -->
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">

          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">
            @yield('Level')
            <!-- START ROW -->
                <div style="width:100%;height:100%;overflow:auto;">
                    @yield('content')
                </div>
            <!-- END STATR ROW -->
          </div>
        </div>
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    <!--START QUICKVIEW -->
    <div id="quickview" class="quickview-wrapper" data-pages="quickview">
      <!-- Nav tabs -->
      <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
      <!-- Tab panes -->

    </div>
    @include('layouts.partials.DeleteModals')
    @include('layouts.partials.DeleteModalUp')
    @include('layouts.partials.DeleteModalDown')

	</body>
</html>
{!! Html::script('js/lightbox/js/lightbox.js')  !!}
{!! Html::script('backend/plugins/pace/pace.min.js')  !!}
{!! Html::script('backend/plugins/modernizr.custom.js')  !!}
{!! Html::script('backend/plugins/jquery-ui/jquery-ui.min.js')  !!}
{!! Html::script('backend/plugins/jquery-scrollbar/jquery.scrollbar.min.js')  !!}
{!! Html::script('backend/plugins/bootstrap-select2/select2.min.js')  !!}
{!! Html::script('backend/js/pages.min.js')  !!}
{!! Html::script('js/jquery.timepicker.js')!!}
<script type="text/javascript">
  $(window).load(function () {
    $("#layout_body").removeClass("hideAll");
        lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    });
  });
  (function(){

    var yearTextSelector = '.ui-datepicker-year';

    var dateNative = new Date(),
        dateTW = new Date(
            dateNative.getFullYear() - 1911,
            dateNative.getMonth(),
            dateNative.getDate()
        );

    function leftPad(val, length) {
        var str = '' + val;
        while (str.length < length) {
            str = '0' + str;
        }
        return str;
    }

    // 應該有更好的做法
    var funcColle = {
        onSelect: {
            basic: function(dateText, inst){
                /*
                var yearNative = inst.selectedYear < 1911
                    ? inst.selectedYear + 1911 : inst.selectedYear;*/
                dateNative = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);

                // 年分小於100會被補成19**, 要做例外處理
                var yearTW = inst.selectedYear > 1911
                    ? leftPad(inst.selectedYear - 1911, 4)
                    : inst.selectedYear;
                var monthTW = leftPad(inst.selectedMonth + 1, 2);
                var dayTW = leftPad(inst.selectedDay, 2);
                console.log(monthTW);
                dateTW = new Date(
                    yearTW + '-' +
                    monthTW + '-' +
                    dayTW + 'T00:00:00.000Z'
                );
                console.log(dateTW);
                return $.datepicker.formatDate(twSettings.dateFormat, dateTW);
            }
        }
    };

    var twSettings = {
        closeText: '關閉',
        prevText: '上個月',
        nextText: '下個月',
        currentText: '今天',
        monthNames: ['一月','二月','三月','四月','五月','六月',
            '七月','八月','九月','十月','十一月','十二月'],
        monthNamesShort: ['一月','二月','三月','四月','五月','六月',
            '七月','八月','九月','十月','十一月','十二月'],
        dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
        dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
        dayNamesMin: ['日','一','二','三','四','五','六'],
        weekHeader: '周',
        dateFormat: 'yy/mm/dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年',
        onSelect: function(dateText, inst){
            $(this).val(funcColle.onSelect.basic(dateText, inst));
            if(typeof funcColle.onSelect.newFunc === 'function'){
                funcColle.onSelect.newFunc(dateText, inst);
            }
        }
    };

    // 把yearText換成民國
    var replaceYearText = function(){
        var $yearText = $('.ui-datepicker-year');

        if(twSettings.changeYear !== true){
            $yearText.text('民國' + dateTW.getFullYear());
        }else{
            // 下拉選單
            if($yearText.prev('span.datepickerTW-yearPrefix').length === 0){
                $yearText.before("<span class='datepickerTW-yearPrefix'>民國</span>");
            }
            $yearText.children().each(function(){
                if(parseInt($(this).text()) > 1911){
                    $(this).text(parseInt($(this).text()) - 1911);
                }
            });
        }
    };

    $.fn.datepickerTW = function(options){

        // setting on init,
        if(typeof options === 'object'){
            //onSelect例外處理, 避免覆蓋
            if(typeof options.onSelect === 'function'){
                funcColle.onSelect.newFunc = options.onSelect;
                options.onSelect = twSettings.onSelect;
            }
            // year range正規化成西元, 小於1911的數字都會被當成民國年
            if(options.yearRange){
                var temp = options.yearRange.split(':');
                for(var i = 0; i < temp.length; i += 1){
                    //民國前處理
                    if(parseInt(temp[i]) < 1 ){
                        temp[i] = parseInt(temp[i]) + 1911;
                    }else{
                        temp[i] = parseInt(temp[i]) < 1911
                            ? parseInt(temp[i]) + 1911
                            : temp[i];
                    }
                }
                options.yearRange = temp[0] + ':' + temp[1];
            }
            // if input val not empty
            if($(this).val() !== ''){
                options.defaultDate = $(this).val();
            }
        }

        // setting after init
        if(arguments.length > 1){
            // 目前還沒想到正常的解法, 先用轉換成init setting obj的形式
            if(arguments[0] === 'option'){
                options = {};
                options[arguments[1]] = arguments[2];
            }
        }

        // override settings
        $.extend(twSettings, options);

        // init
        $(this).datepicker(twSettings);

        // beforeRender
        $(this).click(function(){
            var isFirstTime = ($(this).val() === '');

            // year range and default date

            if((twSettings.defaultDate || twSettings.yearRange) && isFirstTime){

                if((twSettings.defaultDate || twSettings.yearRange) && isFirstTime){
                if(twSettings.defaultDate){
                    $(this).datepicker('setDate','{{date('Y-m-d')}}');
                }

                // 當有year range時, select初始化設成range的最末年
                if(twSettings.yearRange){
                   
                }
            } else {
                $(this).datepicker('setDate', dateNative);
            }
            } else {
                $(this).datepicker('setDate', dateNative);
            }

            $(this).val($.datepicker.formatDate(twSettings.dateFormat, dateTW));

            replaceYearText();

            if(isFirstTime){
                $(this).val('');
            }
        });

        // afterRender
        $(this).focus(function(){
            replaceYearText();
        });

        return this;
    };

})();
        $('.datepickerTW').datepickerTW({
            changeYear: true,
            changeMonth: true,
            yearRange: '1:2050',
            defaultDate: '{{date('Y-m-d')}}',
            dateFormat: 'yy-mm-dd'

        });
        $('.datepicker').datepicker({
            changeYear: true,
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            yearRange: '1911:2018',
            defaultDate: '2018-12-01'
        });
  $('#OrientationConfig').change(function(){
      id=$('#OrientationConfig').val();
      $.ajax({
          url: '{{route('Admin.OrientationData')}}',
          data:'id='+id,
          type:"get",
          dataType:'json',
          success: function(data){
              $('[name="content1"]').val(data.content1);
              $('[name="content2"]').val(data.content2);
              $('[name="hotel_content"]').val(data.content3);
              $('[name="transportation_content"]').val(data.content4);
              $('[name="activity_content"]').val(data.content5);
          },
          beforeSend:function(){
              
          },
          complete:function(){
              
          },
          error:function(xhr, ajaxOptions, thrownError){ 
              alert('錯誤');
           }
      });
  });
  </script>
@yield('script')