-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2018-02-26 07:20:05
-- 伺服器版本: 10.1.10-MariaDB
-- PHP 版本： 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `free`
--

-- --------------------------------------------------------

--
-- 資料表結構 `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `body2` text COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `principal` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '負責人',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_establishment` date NOT NULL COMMENT '成立日期',
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uniform_numbers` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '統一編號',
  `capital` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '資本額',
  `contract_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '契約格式',
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `margin` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '保證金',
  `company_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '公司編號',
  `compliance_insurance` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '履約保險',
  `quality_assurance_association` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '品保協會會員',
  `company_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `about`
--

INSERT INTO `about` (`id`, `body`, `body2`, `company_name`, `principal`, `address`, `date_of_establishment`, `fax`, `uniform_numbers`, `capital`, `contract_format`, `tel`, `margin`, `company_id`, `compliance_insurance`, `quality_assurance_association`, `company_class`, `link1`, `link2`, `file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '<h3 style="text-align:center"><strong><span style="color:#0033cc"><span style="font-size:20px">關於自由先生</span></span></strong></h3>\r\n\r\n<p style="text-align:center"><span style="font-size:16px">我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！&nbsp;<br />\r\n澎湖旅遊.澎湖自由行.迷你小團.蘭嶼東京.大阪.沖繩.自由行. 國鼎旅行社有限公司&nbsp;<br />\r\n我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！&nbsp;<br />\r\n澎湖旅遊.澎湖自由行.迷你小團.蘭嶼東京.大阪.沖繩.自由行</span></p>\r\n', '<h3 style="text-align:center"><strong><span style="color:#0033ff"><span style="font-size:20px">公司概覽</span></span></strong></h3>\r\n\r\n<p style="text-align:center"><span style="font-size:16px">國鼎旅行社有限公司&nbsp;<br />\r\n成立於1986年，屬於甲種旅行社，經營項目包含國際機票、各國簽證、各地自由行、員工旅遊、學生畢旅、會議假期、獎勵旅遊&nbsp;<br />\r\n&nbsp;緣起：國鼎旅行社成立已逾20年，20年來歷經旅客消費習慣的改變，從早年難得出國，一次就要走很多國家，泰港新馬8日遊，瑞士、義大利、法國17日，這種大堆頭的走馬看花行程，演變成單島、單點的定點休閒行程，例如：北海道五日、峇里島五日、長灘島、韓國、濟州、九州、東京、大阪，這種深度旅遊。&nbsp;<br />\r\n近年來，由於網路資訊的發達，商務往返的頻繁，旅遊節目的盛行，旅客對於自由行越來越有自信，越來越喜歡自己安排自己想去的地方！&nbsp;<br />\r\n&nbsp;想要的休閒方式，純粹旅遊或出差兼旅遊的需求越來越多，國鼎旅行社以Mr.Free自由先生的假期名稱，代表著自由自在，無拘無束的概念，加強在旅客自由行的部份，單訂機票、單訂飯店、機票加飯店、機票加飯店加活動、、、任何可能性，讓您自由自在的安排您的國內或國外假期！</span></p>\r\n', '國鼎旅行社有限公司', '董事長 陳善國', '104 台北市中山區林森北路100號9樓之1', '1988-02-16', '02-25230946   02-25319832', '23151866', 'NT$6,000,000元', '乙式', '02-25234101 02-25231052', 'NT$1,500,000', '063700', 'NT$5,000,000', '北123', '甲種旅行業', 'http://www.yahoo.com.tw', 'http://www.yahoo.com.tw', '', '2018-02-05 02:14:19', '2018-02-05 02:14:19', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `trip_location_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adult_price` int(11) NOT NULL,
  `child_price` int(11) NOT NULL,
  `explanation` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ext` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `activity`
--

INSERT INTO `activity` (`id`, `class_id`, `trip_location_id`, `name`, `adult_price`, `child_price`, `explanation`, `img`, `ext`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 9, '吉貝島水上活動無限暢玩', 8000, 1000, '<p>包含水上機車、香蕉船、飛行沙發、鴛鴦飛艇、拖曳甜甜圈、海上飛碟、魔法飛毯、海上曼波、浮潛等不限次數(含來回船票+島上機車2人一部)</p>\r\n', '', '', '2017-02-24 15:33:33', '2018-02-02 18:36:34', NULL),
(2, 2, 8, '南海-七美島.望安島跳島全日遊-不玩水', 500, 20, '<p>望安嶼－綠蠵龜觀光保育中心.望安網垵口沙灘.大草原天台勝境、呂洞濱仙人腳印、中社村古厝<br />\r\n七美祕境&mdash;前往七美人塚.睡美人.大獅風景區.月世界等知名景點及感人肺腑的望夫石傳說；鬼斧神工的巨獅、龍埕美景．寸草不生的月世界洪荒；唯妙唯肖的小台灣岩脈、媒體寵兒的雙心石滬美景讓您仔細觀賞先人的生活智慧,感受那歲月的痕跡！</p>\r\n\r\n<p>＊此行程可加價500元/人，改為七美藍洞全日遊（含七美機車）或是七美島全日遊（含浮潛及島上機車）</p>\r\n\r\n<p>＊此行程離島以機車為主，可加價150元/人，改為七美望安島搭乘環島導覽車。</p>\r\n', '', '', '2017-06-05 19:53:13', '2018-01-16 14:04:30', NULL),
(3, 1, 8, '金沙灘水上活動不限次數', 50, 100, '<p>七合一水上活動：飛毯衝浪、水上摩托車、香蕉船、搖擺快艇、拖曳圈、天旋地轉、鴛鴦飛船、浮潛一票到底不限次數</p>\r\n', '', '', '2017-06-06 01:22:21', '2018-01-16 14:04:24', NULL),
(4, 1, 8, '東海水上活動全日遊', 30, 200, '<p>東海水上活動全日遊</p>\r\n', '17101311145554337.jpg', '<p>xxxx</p>\r\n', '2017-06-06 01:23:14', '2018-01-30 15:56:10', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword` text COLLATE utf8_unicode_ci,
  `alt` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `alt_title` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `banner`
--

INSERT INTO `banner` (`id`, `title`, `file1`, `keyword`, `alt`, `created_at`, `updated_at`, `deleted_at`, `alt_title`) VALUES
(1, 'Line', '16100521522485999.jpg', 'https://www.facebook.com/', 'TEST', '2016-10-05 13:52:24', '2016-10-05 13:52:24', NULL, 'tesT'),
(2, 'IG', '16100606334932872.jpg', 'https://www.facebook.com/', 'test', '2016-10-05 22:33:49', '2016-10-05 22:33:49', NULL, 'test'),
(3, '會員招募', '16100606335825973.jpg', 'https://www.facebook.com/', 'test', '2016-10-05 22:33:58', '2016-10-05 22:33:58', NULL, 'test');

-- --------------------------------------------------------

--
-- 資料表結構 `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `contact_class_id` int(11) NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `contact`
--

INSERT INTO `contact` (`id`, `contact_class_id`, `tel`, `email`, `name`, `body`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '0978797374', 'ab7895123@yahoo.com.tw', 'hanzo', '購物詢問購物詢問購物詢問購物詢問購物詢問購物詢問購物詢問', '2016-10-26 02:38:03', '2016-10-25 16:00:00', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `email`
--

INSERT INTO `email` (`id`, `email`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'hanzo@sunstar-dt.com', '2016-10-17 01:56:12', '2016-10-17 01:56:12', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `free_exercise`
--

CREATE TABLE `free_exercise` (
  `id` int(11) NOT NULL,
  `location` int(10) NOT NULL,
  `type` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transportation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ticket` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `insurance` int(11) NOT NULL,
  `insurance_price` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `days` int(12) NOT NULL,
  `features` text COLLATE utf8_unicode_ci NOT NULL,
  `contain` text COLLATE utf8_unicode_ci NOT NULL,
  `not_contain` text COLLATE utf8_unicode_ci NOT NULL,
  `show_hotel` int(10) NOT NULL DEFAULT '0',
  `show_shuttle_bus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `features_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `free_exercise`
--

INSERT INTO `free_exercise` (`id`, `location`, `type`, `price`, `start_date`, `end_date`, `name`, `activity`, `transportation`, `hotel`, `ticket`, `insurance`, `insurance_price`, `file`, `description`, `days`, `features`, `contain`, `not_contain`, `show_hotel`, `show_shuttle_bus`, `created_at`, `updated_at`, `deleted_at`, `features_file`) VALUES
(1, 8, 0, 0, '2017-03-01', '2017-03-31', '澎湖自由行3天(船)', '1', '1,2', '1', '3,4', 0, 0, '17042416000760568.jpg', '<p>最好玩</p>\r\n', 3, '<p>行程特色</p>\r\n', '<p>費用包含</p>\r\n', '<p>未包含</p>\r\n', 1, '1', '2017-03-02 13:20:42', '2017-09-21 12:35:22', NULL, ''),
(2, 9, 1, 0, '0000-00-00', '0000-00-00', '蘭嶼自由行', '1', '1,2,3', '1', '5', 0, 0, '17030216122332313.jpg', '<p>☆行程注意事項<br />\r\n１.蘭嶼、離島地區，住宿等級無法與本島相比，若無法接受者請勿報名，敬請見諒<br />\r\n２.此自由行行程，蘭嶼、皆要自行騎機車，無法提供其他交通工具<br />\r\n3.若要加購自費行程，請於報名時一併告知客服人員，以利作業<br />\r\n<br />\r\n☆蘭嶼小叮嚀：</p>\r\n\r\n<p>１.蘭嶼為原住民達悟族居住地，島上請勿直接拿著相機對著人們拍照，容易引起糾紛。<br />\r\n2.拼板舟為達悟族的生財器具，請勿隨意觸碰或過於靠近。<br />\r\n3.蘭嶼全島只有一間加油站，抵達時請先前往加油。</p>\r\n', 0, '<p><strong>蘭嶼可說是台灣離島最特殊的島嶼，整個島的達悟族文化保留完整，也不商業化，特別海景與海底景緻<br />\r\n更是優美，山區間的珠光鳳蝶與蘭嶼角鴞是蘭嶼特有種，不管文化、生態與景緻都是台灣極為特別的一<br />\r\n個地方～</strong></p>\r\n\r\n<p><strong>開元港的海水，清澈見底，藍寶石般的顏色映著陽光閃閃發亮，帶著愉快的心情，展開蘭嶼的旅程吧</strong></p>\r\n\r\n<p><strong>蘭嶼處處美景　騎車環島時要小心，路上隨時會有準備過馬路的山羊、逛大街的豬仔、寄居蟹<br />\r\n可別光顧著賞景　讓動物受傷喔</strong></p>\r\n', '<p>☆台北／台東／火車費用、台東／台北機票費用。<br />\r\n☆台東火車站／富岡碼頭接送服務。<br />\r\n☆台東富岡碼頭&rarr;蘭嶼、蘭嶼&rarr;台東富岡碼頭　<br />\r\n☆蘭嶼住宿兩晚費用。<br />\r\n<strong>（蘭嶼為離島中的離島，住宿環境不比本島，無法接受請勿報名）</strong><br />\r\n☆蘭嶼機車費用兩人一部<br />\r\n☆含飯店早餐２次(視住宿民宿提供，若無提供會退早餐餐費)。<br />\r\n<strong>因提供的機車為１２５ＣＣ所以須持有重型機車駕照，外籍人士也須備有國際駕照，<br />\r\n以免到當地無法使用交通工具。若因無攜帶駕照而導致無法使用機車代步，費用恕不退費。</strong><br />\r\n☆贈送蘭嶼浮潛１次</p>\r\n', '<p>☆個人旅行平安保險。<br />\r\n☆其他個人額外支出。</p>\r\n', 1, '', '2017-03-02 13:25:56', '2017-06-07 15:02:47', NULL, ''),
(3, 10, 2, 0, '0000-00-00', '0000-00-00', '沖繩自由行-四天三夜', '', '2', '', '7', 0, 0, '17030216122332313.jpg', '<ol>\r\n	<li>\r\n	<p><strong>廉價航空非一般傳統航空，機票使用規定較為特殊且嚴苛，請同意航空公司相關規定後再行訂購報名。</strong></p>\r\n\r\n	<p>1、此機票為廉價航空~樂桃航空經濟艙四天團體票，須配合團體機位去、回程日期恕無法更改。<br />\r\n	2、機位一經確認OK即需繳付全額機票款訂金，無法更改日期、取消並無法退費，並請提供正確護照英文名單以利訂房作業，待飯店OK另行通知，敬請注意。訂金：平日每人NT8,000、卡連續假日每人NT12,000。<br />\r\n	一、飯店訂房需要保證入住，請付訂金並提供護照影本方可為您處理後續作業，3~5個工作日內回覆您飯店預訂狀況。若因天災等不可抗力之因素致班機延誤，樂桃航空不提供旅客轉乘其他航空公司班機之服務、亦不提供安排旅客住宿之服務，且不負任何行程取消延誤責任，敬請留意。<br />\r\n	★貼心提醒：SEMI-DBL(S/D)意指較小房型、一張雙人床(床寬約120 CM~140 CM，視各飯店狀況而定) ，恕無法接受嬰兒或小孩不佔床之需求，您可加價選購住宿標準雙人房型，敬請注意。<br />\r\n	★貼心提醒：日本飯店訂房規定，六歲以上小孩皆需佔床，恕無法接受不佔床需求，敬請注意。<br />\r\n	二、機票開票，一經開票不可更改姓名亦無退票價值。前列事項恕無法取消，若取消需收全額費用，敬請注意。機位及飯店一經確認即不可取消或變更；此商品若取消，將收取全額機票費用及住宿飯店取消費用<br />\r\n	4、免費行李服務包含：每人20公斤內托運行李及一件10公斤內手提行李；超重部分按航空公司現場規定，費用敬請自理。且機上不含餐飲，需要者請於機上向空服員訂購付費。加購行李：需於開票以前加購確認，於開票時一併作業，請開票前告知業務，開票後或在國外恕無法受理。<br />\r\n	5、【嬰兒、孩童票售價】因航空公司考量飛安因素，即日起限制嬰兒不佔位席次。如欲偕同嬰兒報名，敬請先行向客服人員確認正確報價，但無座位且不提供任何免費托運或手提行李件數服務，嬰兒可免費托運嬰兒推車一台，亦無提供嬰兒搖籃全程需父母抱著，一位成人旅客只能抱一位嬰幼兒坐在膝上。嬰兒或兒童佔位，售價與大人相同。<br />\r\n	6、樂桃航空登機流程參考：<br />\r\n	一、樂桃航空登機手續為自助式辦理，所有旅客皆須持電子機票/條碼利用機場自動報到機辦理登機手續。請務必提前2~3小時到場辦理，以免無法搭機。<br />\r\n	二、台灣桃園機場第一航廈出境大廳1號櫃檯(地面代理為長榮航空)。<br />\r\n	三、沖繩那霸機場於那霸國際機場LCC航廈起降與辦理相關手續。(LCC航廈資訊) 請注意，那霸國際機場LCC航廈僅限專用接駁巴士與部分指定的租車公司接送巴士進入。為避免人多混雜，建議您最遲在航班起飛的70分鐘前搭接駁巴士前往LCC航廈。</p>\r\n	</li>\r\n</ol>\r\n', 4, '<ul>\r\n	<li>\r\n	<p>上述需加價日期，小孩不佔床售價亦須加價。5～10月份單售來回機票每人為7500元起。</p>\r\n	</li>\r\n	<li>\r\n	<p>訂位艙等：樂桃航空經濟艙；訂位時需附上護照影本。『樂桃航空網址』<a href="http://www.flypeach.com/tw/home.aspx" rel="noopener noreferrer" target="_blank">http://www.flypeach.com/tw/home.aspx</a></p>\r\n	</li>\r\n	<li>\r\n	<p>機位一經確認後需立即付清全額款項，無法更改日期、航點、名單、取消並無法退費/退票，且開完票後此機票完全無退票價值。取消會收全額機票費用。</p>\r\n	</li>\r\n	<li>\r\n	<p>【嬰兒、孩童票售價】因航空公司考量飛安因素，即日起限制嬰兒不佔位席次。如欲偕同嬰兒報名，敬請先行向客服人員確認正確報價，但無座位且不提供任何免費托運或手提行李件數服務，嬰兒可免費托運嬰兒推車一台，亦無提供嬰兒搖籃全程需父母抱著，一位成人旅客只能抱一位嬰幼兒坐在膝上。嬰兒或兒童佔位，售價與大人相同。</p>\r\n	</li>\r\n	<li>\r\n	<p>免費行李服務包含：每人20公斤內托運行李及一件10公斤內手提行李；超重部分按航空公司現場規定，費用敬請自理。且機上不含餐飲，需要者請於機上向空服員訂購付費。加購行李：需於開票以前加購確認，於開票時一併作業，請開票前告知業務，開票後或在國外恕無法受理。 而嬰兒票恕不提供任何免費託運或手提行李件數。</p>\r\n	</li>\r\n	<li>\r\n	<p>樂桃航空之航班在所有航程全面禁止旅客攜帶Samsung Note 7手機登機，包括隨身行李、托運行李或貨運寄送皆不可攜帶，如有查獲違規攜帶之事宜，將拒絕該名旅客登機，敬請注意。</p>\r\n	</li>\r\n	<li>\r\n	<p>若因天災等不可抗力之因素致班機延誤，航空公司不提供旅客轉乘其他航空公司班機之服務、亦不提供安排旅客住宿之服務，且不負任何行程取消延誤責任，敬請留意。</p>\r\n	</li>\r\n</ul>\r\n', '<p>1. 桃園／東京成田／桃園，捷星航空機票<br />\r\n2. 機票內含20公斤托運行李（不限件數）及一件7公斤手提行李（限制長56公分．寬36公分．高23公分以內），如需加價購多件托運行李請於訂位前告知，否則請恕無法做變更。<br />\r\n3. 飯店住宿（住宿內容依照選購產品為準）<br />\r\n4. 桃園、成田兩地機場稅，以及航空公司燃油附加費<br />\r\n5. 200萬契約責任險 + 20萬意外醫療險</p>\r\n', '<h4>行程不包含</h4>\r\n\r\n<p>1. 機上餐食或行程中餐食、交通、票券<br />\r\n2. 行李小費<br />\r\n3. 辦理個人護照、簽證費用<br />\r\n4. 私人開銷，如行李超重費、飯店房間內飲料酒類、洗衣、電話、快遞&hellip;等支出。</p>\r\n', 1, '', '2017-03-02 13:31:29', '2017-06-07 15:02:53', NULL, ''),
(4, 8, 0, 0, '0000-00-00', '0000-00-00', '澎湖自由行3天(航空)', '1,2,3,4', '3,4', '', '2', 0, 200, '17060601313990443.jpg', '', 3, '<h2><strong>你可以這樣玩</strong></h2>\r\n\r\n<p>您或許有自己想住的地方，或許有買了住宿券，或許已經訂好房！我們精選您有興趣的活動，全日遊,搭配小活動，剩下的一天還可以好好去環島拍照，活動時間你可以自行決定要在哪一天進行，完全享有自由行的自在。</p>\r\n', '<ul>\r\n	<li>澎湖來回機票\r\n	<p>&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>機車或轎車使用48小時</p>\r\n	</li>\r\n	<li>\r\n	<p>東海戀夏全日遊活動卷</p>\r\n	</li>\r\n	<li>\r\n	<p>海洋牧場碳烤鮮蚵吃到飽活動券</p>\r\n	</li>\r\n	<li>\r\n	<p>夜釣小管活動券</p>\r\n	</li>\r\n	<li>\r\n	<p>200萬旅遊責任險附加20萬意外醫療險</p>\r\n	</li>\r\n</ul>\r\n', '<ul>\r\n	<li>\r\n	<p>飯店住宿-請自行安排或由我們為您代訂</p>\r\n	</li>\r\n	<li>\r\n	<p>機車或轎車油料費用</p>\r\n	</li>\r\n	<li>\r\n	<p>個人行李超重運費</p>\r\n	</li>\r\n	<li>\r\n	<p>馬公機場來回接送</p>\r\n	</li>\r\n</ul>\r\n', 0, '', '2017-06-06 01:31:39', '2017-06-06 02:43:04', NULL, ''),
(5, 8, 0, 0, '0000-00-00', '0000-00-00', '澎湖優惠機票', '', '', '', '3', 0, 0, '17060601374298472.jpg', '<ul>\r\n	<li>專案機票訂位完成需於開票期限內完成付款開票，開票需付清全額票款。</li>\r\n	<li>機票效期：限當日當班次有效(不得更換班次)逾期未使用不可退票，不可補票差至全額票搭乘。</li>\r\n	<li>訂位開票後，出發前30天前取消，收取退票手續費10%。</li>\r\n	<li>訂位開票後，出發前7~30天內取消，收取退票手續費20%。</li>\r\n	<li>訂位開票後，出發前2~7天內取消，收取退票手續費1000元。</li>\r\n	<li>如遇天候不可抗力影響停飛或航班取消，可全額退費。</li>\r\n	<li>登機報到：憑報到通知單至機場遠航櫃檯以身分證件辦理登機手續。</li>\r\n</ul>\r\n', 5, '<h2><strong>早去晚回-你或許不知道的困擾</strong></h2>\r\n\r\n<p>大家總覺得出門玩一定要早去晚回，天沒亮就出發，最後一刻到家最划算，總是指定要早上8點以前出發，晚上8點以後回來！但是早去晚回，其實辛苦又痛苦！</p>\r\n\r\n<h2><strong>晚去早回-沒有您想的那麼不堪</strong></h2>\r\n\r\n<p>早去晚回班機，既然都被團體包走，被不知道誰留走了，自己怎麼訂都只有晚去早回的班機，那當然要選擇最優惠的價錢買機票！省下的票價差，拿來多住一晚飯店都划算，足足節省至少30%</p>\r\n\r\n<h2><strong>你有更好的選擇</strong></h2>\r\n\r\n<p>遠東航空公司限定班次專案，讓你一樣有充足的時間玩，但是票價優惠更有感！都是MD機型的160~165人座的噴射客機，而不是螺旋槳的小飛機。</p>\r\n', '', '', 0, '0', '2017-06-06 01:37:42', '2017-09-22 23:39:02', NULL, ''),
(6, 8, 0, 0, '0000-00-00', '0000-00-00', '', '', '', '', '', 0, 0, '', '', 1, '', '', '', 1, '0', '2017-09-21 11:25:34', '2017-09-21 11:25:52', '2017-09-21 03:25:52', '');

-- --------------------------------------------------------

--
-- 資料表結構 `hotel`
--

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int(11) NOT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL COMMENT '說明',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `location_id`, `rate`, `file1`, `file2`, `file3`, `file4`, `file5`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '東新宿E飯店', 10, '5', '17030118044132994.jpg', '17030118044114583.jpg', '17030118044167515.jpg', '17030118044135758.jpg', '17030118044145049.jpg', '<p>飯店位於地鐵「東新宿車站」A1出口走出來旁邊即可到達。東新宿E飯店的一樓即有24小時LAWSON商店，還有飯店直營的Tully&#39;s Coffee，附近還有超市，就算是女性旅客或是長期投宿的旅客也能安心入住的優質飯店。</p>\r\n', '2017-03-01 18:04:41', '2017-05-11 20:44:55', NULL),
(2, '福華飯店', 8, '3', '17031012285478583.jpg', '17031012285419916.jpg', '17031012285429380.jpg', '17031012285492957.jpg', '17031012285482565.jpg', '<p>aaaa</p>\r\n', '2017-03-10 12:28:54', '2017-03-15 17:48:19', NULL),
(3, '澎湖安一海景大飯店', 8, '3', '17031514423915158.jpg', '17031514423957977.jpg', '17031514423912809.jpg', '17031514423993584.jpg', '17031514423941145.jpg', '<p>澎湖安一海景大飯店</p>\r\n\r\n<p>澎湖安一海景大飯店</p>\r\n\r\n<p>澎湖安一海景大飯店</p>\r\n\r\n<p>澎湖安一海景大飯店</p>\r\n\r\n<p>澎湖安一海景大飯店</p>\r\n', '2017-03-15 14:42:39', '2018-01-12 16:20:35', NULL),
(4, '利時達酒店', 10, '5', '17031520594926708.jpg', '17031520594947544.jpg', '17031520594961729.jpg', '17031520594945347.jpg', '17031520594936116.jpg', '<p>距離新宿車站僅有一站的距離，飯店內設有美容中心，芳香療法1按摩能讓旅客舒解旅途疲勞的壓力。全館皆有WIFI系統連接，也有客房電視也設有VOD付費頻道。櫃台也有販售紙箱、化妝水卸妝油、雨傘等物品。</p>\r\n', '2017-03-15 20:59:49', '2017-05-11 20:45:50', NULL),
(5, '沖繩大飯店', 11, '3', '17031521073719249.jpg', '', '', '', '', '<p>a</p>\r\n', '2017-03-15 21:07:37', '2017-03-15 21:07:37', NULL),
(6, '沖繩大飯店', 11, '3', '17031521073714577.jpg', '', '', '', '', '<p>a</p>\r\n', '2017-03-15 21:07:37', '2017-03-31 16:02:22', '2017-03-31 08:02:22');

-- --------------------------------------------------------

--
-- 資料表結構 `hotel_class`
--

CREATE TABLE `hotel_class` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `hotel_class`
--

INSERT INTO `hotel_class` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '日本福岡', '2017-03-01 16:57:18', '2017-03-01 16:57:18', NULL),
(2, '日本北海道', '2017-03-01 16:57:29', '2017-03-01 16:57:29', NULL),
(3, '日本東京', '2017-03-01 16:57:38', '2017-03-01 16:57:38', NULL),
(4, '高雄', '2017-03-01 16:57:45', '2017-03-01 16:57:45', NULL),
(5, '阿里山', '2017-03-01 16:57:53', '2017-03-01 16:57:53', NULL),
(6, '台北', '2017-03-01 16:58:01', '2017-03-01 16:58:01', NULL),
(7, '義大利', '2017-03-01 16:58:08', '2017-03-01 16:58:08', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `hotel_room`
--

CREATE TABLE `hotel_room` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `child_max_quantity` int(10) NOT NULL,
  `max_quantity` int(11) NOT NULL,
  `child_age` int(10) NOT NULL,
  `price` int(11) NOT NULL,
  `child_price` int(11) NOT NULL,
  `extra_bed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extra_bed_price` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `file1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ext` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `hotel_room`
--

INSERT INTO `hotel_room` (`id`, `hotel_id`, `name`, `quantity`, `child_max_quantity`, `max_quantity`, `child_age`, `price`, `child_price`, `extra_bed`, `extra_bed_price`, `cost`, `file1`, `file2`, `file3`, `file4`, `file5`, `ext`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, '山景雙人房', 2, 0, 0, 12, 2500, 1500, '0', 0, 3000, '17030118363672010.jpg', '17030118363657646.jpg', '17030118363635069.jpg', '17030118363678279.jpg', '17030118363653466.jpg', '<p>a</p>\r\n', '2017-03-01 18:36:36', '2017-04-24 15:15:12', NULL),
(2, 1, 'E雙人房', 2, 0, 0, 0, 6000, 3000, '3000', 0, 8000, '17031012295582359.jpg', '17031012295598171.jpg', '17031012295548087.jpg', '17031012295520865.jpg', '17031012295568980.jpg', '<p>aaaa</p>\r\n', '2017-03-10 12:29:55', '2017-05-11 21:37:57', NULL),
(3, 3, '雙人房', 2, 1, 2, 12, 5000, 2000, '0', 0, 4000, '17031514435924791.jpg', '17031514435931553.jpg', '17031514435972854.jpg', '17031514435917659.jpg', '17031514435950040.jpg', '<p>說明說明</p>\r\n', '2017-03-15 14:43:59', '2018-01-30 16:07:23', NULL),
(4, 4, '雙人房', 2, 0, 0, 0, 4000, 2000, '3000', 0, 3000, '17031521004482147.jpg', '', '', '', '', '<p>雙人房</p>\r\n', '2017-03-15 21:00:44', '2017-03-15 21:00:44', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `hotel_room_increase`
--

CREATE TABLE `hotel_room_increase` (
  `id` int(10) NOT NULL,
  `room_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `price` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `hotel_room_increase`
--

INSERT INTO `hotel_room_increase` (`id`, `room_id`, `date`, `price`) VALUES
(1, 1, '2017-05-01', 500);

-- --------------------------------------------------------

--
-- 資料表結構 `img_rotation`
--

CREATE TABLE `img_rotation` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `alt` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `alt_title` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `img_rotation`
--

INSERT INTO `img_rotation` (`id`, `title`, `file1`, `sort`, `alt`, `created_at`, `updated_at`, `deleted_at`, `alt_title`) VALUES
(3, '你怎麼說話 決定你是誰', '16100521364989084.jpg', 1, 'test', '2016-10-05 13:39:23', '2016-10-05 13:39:23', NULL, 'test'),
(4, '讓最了解課程的人幫你選擇課程', '16100521371027856.jpg', 2, 'test', '2016-10-05 13:39:29', '2016-10-05 13:39:29', NULL, 'test'),
(5, '培訓大師研討講座開始報名囉', '16100521372144894.jpg', 3, 'teSt', '2016-10-05 13:40:13', '2016-10-05 13:40:13', NULL, 'test'),
(15, 'test', '16100521373645274.jpg', 4, 'test', '2016-10-05 13:37:36', '2016-10-05 13:37:36', NULL, 'test'),
(16, 'test', '16100521374885934.jpg', 5, 'test', '2016-10-05 13:37:48', '2016-10-05 13:37:48', NULL, 'test');

-- --------------------------------------------------------

--
-- 資料表結構 `index_banner`
--

CREATE TABLE `index_banner` (
  `id` int(11) NOT NULL,
  `sort` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `index_banner`
--

INSERT INTO `index_banner` (`id`, `sort`, `name`, `link`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '了解更多', 'http://www.yahoo.com.tw', '18020510154449685.jpg', '2018-02-05 02:15:44', '2018-02-05 02:15:44', NULL),
(2, 2, 'Hanzo', 'http://www.yahoo.com.tw', '17052217284097306.jpg', '2017-05-22 09:49:07', '2017-05-22 09:49:07', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `index_marketing`
--

CREATE TABLE `index_marketing` (
  `id` int(11) NOT NULL,
  `product1` int(11) NOT NULL,
  `product2` int(11) NOT NULL,
  `product3` int(11) NOT NULL,
  `product4` int(11) NOT NULL,
  `product5` int(11) NOT NULL,
  `product6` int(11) NOT NULL,
  `product7` int(11) NOT NULL,
  `product8` int(11) NOT NULL,
  `product9` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `index_marketing`
--

INSERT INTO `index_marketing` (`id`, `product1`, `product2`, `product3`, `product4`, `product5`, `product6`, `product7`, `product8`, `product9`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2018-01-22 07:23:01', '0000-00-00 00:00:00', '2018-01-22 15:23:01');

-- --------------------------------------------------------

--
-- 資料表結構 `item_img`
--

CREATE TABLE `item_img` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `item_img`
--

INSERT INTO `item_img` (`id`, `name`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '票位', '18020715302270605.jpg', '2018-02-07 12:25:21', '2018-02-07 15:30:22', NULL),
(2, '飯店', '18020714530698855.jpg', '2018-02-07 12:25:21', '2018-02-07 14:53:06', NULL),
(3, '活動', '18020715303361423.jpg', '2018-02-07 12:25:21', '2018-02-07 15:30:33', NULL),
(4, '當地交通', '18020715304580238.png', '2018-02-07 12:25:21', '2018-02-07 15:30:45', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `license`
--

CREATE TABLE `license` (
  `id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `license`
--

INSERT INTO `license` (`id`, `file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '17030816080336048.jpg', '2017-03-06 16:12:20', '2017-03-08 16:08:03', NULL),
(2, '17030816080938525.jpg', '2017-03-06 16:13:38', '2017-03-08 16:08:09', NULL),
(3, '17030816081538384.jpg', '2017-03-08 16:08:15', '2017-03-08 16:08:15', NULL),
(4, '17030816082135164.jpg', '2017-03-08 16:08:21', '2017-03-08 16:08:21', NULL),
(5, '17030816082757712.jpg', '2017-03-08 16:08:27', '2017-03-08 16:08:27', NULL),
(6, '17030816083421389.jpg', '2017-03-08 16:08:34', '2017-03-08 16:08:34', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `location`
--

INSERT INTO `location` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '沖繩機場', '2017-03-01 11:34:42', '2017-06-06 01:44:31', NULL),
(2, '高雄小港機場', '2017-03-01 12:24:41', '2017-03-01 12:24:41', NULL),
(3, '桃園國際機場', '2017-03-31 14:53:56', '2017-03-31 14:53:56', NULL),
(4, '嘉義布袋漁港', '2017-04-17 14:22:44', '2017-04-17 14:22:44', NULL),
(5, '澎湖馬公港', '2017-04-17 14:22:55', '2017-04-17 14:22:55', NULL),
(6, '蘭嶼機場', '2017-05-11 20:50:26', '2017-05-11 20:50:26', NULL),
(7, '台東機場', '2017-05-11 20:50:33', '2017-05-11 20:50:33', NULL),
(8, '澎湖機場', '2017-06-06 01:32:28', '2017-06-06 01:32:28', NULL),
(9, '金門機場', '2017-06-06 01:32:28', '2017-06-06 01:33:12', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name`, `address`, `tel`, `email`, `remarks`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '海之星', 'a', 'a', 'a', '<p>a</p>\r\n', '2017-06-05 11:51:00', '2017-06-05 11:51:00', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `menu_class`
--

CREATE TABLE `menu_class` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `font_awesome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `menu_class`
--

INSERT INTO `menu_class` (`id`, `name`, `sort`, `font_awesome`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, '帳號管理', 0, 'fa fa-user', '2016-10-13 03:23:14', '2016-10-13 03:23:14', NULL),
(6, '系統設定', 100, 'fa fa-steam', '2017-02-08 03:45:52', '2017-02-08 03:45:52', NULL),
(27, '網頁管理', 1, 'fa fa-cog', '2017-02-22 03:22:58', '2017-02-22 03:22:58', NULL),
(28, '項目管理', 2, 'fa fa-cog', '2017-02-24 06:40:45', '2017-02-24 06:40:45', NULL),
(29, '票位管理', 4, 'fa fa-cog', '2017-02-24 08:55:18', '2017-02-24 08:55:18', NULL),
(30, '飯店管理', 5, 'fa fa-cog', '2017-03-01 08:47:24', '2017-03-01 08:47:24', NULL),
(31, '商品管理', 6, 'fa fa-cog', '2018-01-10 03:01:19', '2018-01-10 03:01:19', NULL),
(32, '訂單管理', 7, 'fa fa-cog', '2017-05-09 10:59:13', '2017-05-09 10:59:13', NULL),
(33, '商家管理', 8, 'fa fa-flag-o', '2017-05-23 06:38:06', '2017-05-23 06:38:06', NULL),
(34, '旅客名單', 9, 'fa fa-user-md', '2018-01-10 03:02:22', '2018-01-10 03:02:22', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `menu_trip_list`
--

CREATE TABLE `menu_trip_list` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `set_trip_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `free_trip_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `menu_trip_list`
--

INSERT INTO `menu_trip_list` (`id`, `title`, `set_trip_id`, `free_trip_id`, `img`, `color`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '澎湖', '1', '1,2,3,4,5', '17052216444651071.png', '#ecab10', '2017-09-22 15:41:33', '2017-09-22 15:41:33', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_10_22_104638_create_posts_table', 1),
('2015_10_22_110150_add_email2_in_users_table', 2),
('2015_10_22_110926_add_email_in_users_table', 3),
('2015_10_23_032702_create_posts_table', 4),
('2015_10_23_054331_create_comments_table', 4),
('2015_10_23_085023_create_comments_table', 5),
('2015_11_30_122108_create_url_option_table', 6),
('2015_11_30_123458_add_url_option_table', 7),
('2015_12_02_115139_create_roles_table', 8),
('2015_12_02_125318_add_users_table', 9),
('2015_12_03_092241_create_country_table', 10),
('2015_12_12_123939_add_program_registration_table', 11),
('2015_12_12_124206_add_product_category_table', 12),
('2015_12_12_133000_add_menu_class_table', 13),
('2015_12_12_133117_add_menu_class_table', 14),
('2015_12_12_133524_add_roles_table', 15),
('2015_12_12_133851_add_users_table', 16),
('2015_12_12_134005_add_role_controls_table', 17),
('2015_12_12_135607_add_product_items_table', 18),
('2015_12_13_132835_add_menu_class_table', 19),
('2015_12_14_135412_add_product_category_table', 20),
('2015_12_15_101837_add_menu_class_table', 21),
('2015_12_15_152408_add_product_items_table', 22),
('2015_12_15_165304_add_role_controls_table', 23),
('2015_12_27_102941_DB', 24),
('2016_05_03_061953_add_font-awesome_menu_class_table', 25),
('2016_05_03_072537_create_qrcodes_table', 25),
('2016_05_03_110240_add_status_in_qrcodes_table', 25),
('2016_05_04_124254_create_pet_qrocde_table', 25),
('2016_05_04_134956_create_pet_qrocdes_table', 25),
('2016_05_04_152544_create_pet_qrocde_table', 26),
('2016_05_04_164547_create_pet_qrocdes_gps_table', 26),
('2016_05_05_201208_create_contacts_table', 26),
('2016_05_26_155838_add_product_product_explanation_table', 27),
('2016_05_27_112512_create_guides_table', 28),
('2016_06_01_155619_add_product_in_upload_image_table', 29),
('2016_06_02_174325_add_orders_detail_img_table', 30),
('2016_06_03_150305_add_postal_code_in_orders_table', 31),
('2016_06_04_140538_add_product_table', 32),
('2016_06_06_161447_add_tables_in_orders_detail', 33),
('2016_06_27_161430_add_county_in_orders_table', 34),
('2016_07_04_172249_add_pay_time_to_orders', 35),
('2016_08_04_144644_create_pay_way_table', 36),
('2016_08_09_104839_add_accompanied_link_table', 37),
('2016_08_09_170325_add_origin_table', 38),
('2016_08_09_170352_add_writings_table', 38),
('2016_08_09_170427_add_directors_supervisors_table', 38),
('2016_08_10_104218_add_directors_supervisors_class_table', 38),
('2016_08_11_180946_add_affairs_table', 39),
('2016_08_11_180958_add_affairs_class_table', 39),
('2016_08_12_150636_add_event_class_table', 40),
('2016_08_12_154127_add_event_table', 41),
('2016_08_19_105618_create_category_table', 42),
('2016_08_19_105957_create_product_table', 42),
('2016_08_19_114109_create_family_table', 42),
('2016_08_19_115614_create_shipping_fee_table', 42),
('2016_08_19_120533_create_family_file_table', 42),
('2016_08_19_121012_create_pay_way_table', 42),
('2016_08_25_171037_add_code_to_product_table', 43),
('2016_08_25_171340_create_stock_table', 44),
('2016_08_25_171400_create_warehouse_table', 44),
('2016_08_25_171418_create_shelf_table', 44),
('2016_08_26_113437_create_company_table', 45),
('2016_08_29_103942_create_recv_body_table', 46),
('2016_08_29_104006_create_recv_head_table', 46),
('2016_08_29_104048_create_vendor_table', 46);

-- --------------------------------------------------------

--
-- 資料表結構 `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `news`
--

INSERT INTO `news` (`id`, `title`, `body`, `file`, `created_at`, `updated_at`, `deleted_at`, `date`) VALUES
(1, '春天賞花團', '<p>我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！最新消息內文最新消息內文最新消息內文最新消息內文我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！最新消息內文最新消息內文最新消息內文最新消息內文我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！最新消息內文最新消息內文最新消息內文最新消息內文最新消息內文最新消息內文最新消息內文最新消息內文我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！最新消息內文最新消息內文最新消息內文最新消息內文</p>\r\n', '17030816440289105.jpg', '2017-02-13 15:24:48', '2017-03-08 16:54:08', NULL, '2017-04-19'),
(2, '犒賞自己趁現在', '<p>犒賞自己趁現在</p>\r\n', '17030816445113972.jpg', '2017-03-08 16:44:51', '2017-03-08 16:44:51', NULL, '2017-03-31');

-- --------------------------------------------------------

--
-- 資料表結構 `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `trip_type` int(10) NOT NULL,
  `ticket_hotel` int(10) DEFAULT NULL,
  `trip` int(10) NOT NULL,
  `trip_tour_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `contact_person_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `adult_quantity` int(11) NOT NULL,
  `child_quantity` int(11) NOT NULL,
  `whit_baby` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `check_user_id` int(11) NOT NULL,
  `pdf` text COLLATE utf8_unicode_ci NOT NULL,
  `return_ext` text COLLATE utf8_unicode_ci NOT NULL,
  `retain_days` datetime NOT NULL,
  `remark_id` int(11) DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` int(11) NOT NULL,
  `payment_item` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_account` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_time` datetime NOT NULL,
  `room_type` int(11) DEFAULT NULL,
  `shuttle_bus_type` int(11) NOT NULL,
  `shuttle_bus_id` int(11) NOT NULL,
  `order_member` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `order`
--

INSERT INTO `order` (`id`, `trip_type`, `ticket_hotel`, `trip`, `trip_tour_id`, `start_date`, `end_date`, `contact_person_name`, `contact_person_tel`, `contact_person_email`, `total`, `adult_quantity`, `child_quantity`, `whit_baby`, `status`, `user_id`, `check_user_id`, `pdf`, `return_ext`, `retain_days`, `remark_id`, `payment_method`, `payment_amount`, `payment_item`, `payment_account`, `payment_time`, `room_type`, `shuttle_bus_type`, `shuttle_bus_id`, `order_member`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', 'xxx', '0987654321', 'ab7895123@hotmail.com', 30000, 10, 0, 0, 0, 21, 5, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '2311312132', '2018-01-15 09:32:44', '2018-01-15 09:32:44', NULL),
(104, 0, NULL, 1, 4, '0000-00-00', '0000-00-00', '', '', '', 90000, 30, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, 'xxxxxx', '2018-01-22 05:58:55', '2018-01-22 05:58:55', NULL),
(105, 1, NULL, 0, 0, '0000-00-00', '0000-00-00', '', '', '', 42500, 0, 0, 0, 1, 21, 0, '', '', '0000-00-00 00:00:00', 1, 'ATM轉帳', 5000, '訂金', '12345', '2018-02-07 00:00:00', NULL, 0, 0, '', '2018-01-24 09:50:39', '2018-01-24 14:09:42', NULL),
(106, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, 'ATM轉帳', 5000, '訂金', '123456', '2018-02-10 00:00:00', NULL, 0, 0, '', '2018-01-24 14:19:02', '2018-01-24 14:19:02', NULL),
(107, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-05 05:32:00', '2018-02-05 05:32:00', NULL),
(108, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:34:19', '2018-02-09 02:34:19', NULL),
(109, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:34:56', '2018-02-09 02:34:56', NULL),
(110, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:35:04', '2018-02-09 02:35:04', NULL),
(111, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:35:21', '2018-02-09 02:35:21', NULL),
(112, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:36:01', '2018-02-09 02:36:01', NULL),
(113, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:36:26', '2018-02-09 02:36:26', NULL),
(114, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:36:55', '2018-02-09 02:36:55', NULL),
(115, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:37:23', '2018-02-09 02:37:23', NULL),
(116, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:37:30', '2018-02-09 02:37:30', NULL),
(117, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:39:16', '2018-02-09 02:39:16', NULL),
(118, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:40:28', '2018-02-09 02:40:28', NULL),
(119, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:41:32', '2018-02-09 02:41:32', NULL),
(120, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:42:39', '2018-02-09 02:42:39', NULL),
(121, 1, NULL, 0, 0, '0000-00-00', '0000-00-00', '', '', '', 3000, 0, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:48:23', '2018-02-09 02:48:23', NULL),
(122, 1, NULL, 0, 0, '0000-00-00', '0000-00-00', '', '', '', 3000, 0, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:49:18', '2018-02-09 02:49:18', NULL),
(123, 1, NULL, 0, 0, '0000-00-00', '0000-00-00', '', '', '', 0, 0, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:51:46', '2018-02-09 02:51:46', NULL),
(124, 1, NULL, 0, 0, '0000-00-00', '0000-00-00', '', '', '', 6000, 0, 0, 0, 0, 21, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-09 02:51:54', '2018-02-09 02:51:54', NULL),
(125, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 5, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-26 05:32:22', '2018-02-26 05:32:22', NULL),
(126, 0, NULL, 1, 3, '0000-00-00', '0000-00-00', '', '', '', 3000, 1, 0, 0, 3, 5, 0, '', '', '0000-00-00 00:00:00', NULL, '', 0, '', '', '0000-00-00 00:00:00', NULL, 0, 0, '', '2018-02-26 05:34:08', '2018-02-26 05:34:08', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `order_activity`
--

CREATE TABLE `order_activity` (
  `id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `activity_id` int(10) NOT NULL,
  `adult_quantity` int(10) NOT NULL,
  `child_quantity` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `time` time DEFAULT NULL,
  `manufacturers_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `cost` int(10) NOT NULL DEFAULT '0',
  `ext` text COLLATE utf8_unicode_ci NOT NULL,
  `check` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `order_activity`
--

INSERT INTO `order_activity` (`id`, `order_id`, `activity_id`, `adult_quantity`, `child_quantity`, `date`, `time`, `manufacturers_id`, `supplier_id`, `cost`, `ext`, `check`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 10, 0, NULL, NULL, 0, 0, 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 105, 1, 1, 0, '2018-01-24 10:30:00', NULL, 1, 1, 0, '', 0, '0000-00-00 00:00:00', '2018-01-24 20:41:03', NULL),
(3, 105, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 0, '0000-00-00 00:00:00', '2018-01-24 21:02:50', '2018-01-24 13:02:50'),
(4, 105, 1, 1, 0, '1900-12-13 09:00:00', NULL, 1, 1, 0, '', 0, '0000-00-00 00:00:00', '2018-01-24 22:04:42', NULL),
(5, 105, 1, 1, 0, '2018-01-10 06:00:00', NULL, 1, 1, 0, '', 0, '0000-00-00 00:00:00', '2018-01-24 22:04:51', NULL),
(6, 107, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 107, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 108, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, 108, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 109, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(11, 109, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(12, 110, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 110, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(14, 111, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(15, 111, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 112, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(17, 112, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(18, 113, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(19, 113, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(20, 114, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(21, 114, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(22, 115, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(23, 115, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(24, 116, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(25, 116, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(26, 117, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(27, 117, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(28, 118, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(29, 118, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(30, 119, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(31, 119, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(32, 120, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(33, 120, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(34, 125, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(35, 125, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(36, 126, 1, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(37, 126, 2, 1, 0, NULL, NULL, 0, 0, 0, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 1, 24, '2018-01-19 15:04:26', '2018-01-19 15:04:26', NULL),
(32, 104, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(33, 116, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(34, 118, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(35, 119, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(36, 120, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `order_hotel`
--

CREATE TABLE `order_hotel` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `manufacturers_id` int(10) NOT NULL,
  `supplier_id` int(10) NOT NULL,
  `cost` int(10) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL,
  `check` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `order_hotel`
--

INSERT INTO `order_hotel` (`id`, `order_id`, `hotel_id`, `room_id`, `status`, `start_date`, `end_date`, `created_at`, `updated_at`, `deleted_at`, `manufacturers_id`, `supplier_id`, `cost`, `quantity`, `check`) VALUES
(1, 1, 3, 3, 0, '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0, 0, 5, 0),
(94, 104, 3, 3, 0, '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0, 0, 15, 0),
(95, 105, 0, 1, 0, '2018-01-24', '2018-01-27', '0000-00-00 00:00:00', '2018-01-24 21:53:02', NULL, 1, 1, 0, 2, 0),
(96, 106, 3, 3, 0, '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0, 0, 1, 0),
(97, 107, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-05 13:32:00', '2018-02-05 13:32:00', NULL, 0, 0, 0, 1, 1),
(98, 108, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:34:19', '2018-02-09 10:34:19', NULL, 0, 0, 0, 1, 1),
(99, 109, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:34:57', '2018-02-09 10:34:57', NULL, 0, 0, 0, 1, 1),
(100, 110, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:35:04', '2018-02-09 10:35:04', NULL, 0, 0, 0, 1, 1),
(101, 111, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:35:21', '2018-02-09 10:35:21', NULL, 0, 0, 0, 1, 1),
(102, 112, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:36:01', '2018-02-09 10:36:01', NULL, 0, 0, 0, 1, 1),
(103, 113, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:36:26', '2018-02-09 10:36:26', NULL, 0, 0, 0, 1, 1),
(104, 114, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:36:55', '2018-02-09 10:36:55', NULL, 0, 0, 0, 1, 1),
(105, 115, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:37:24', '2018-02-09 10:37:24', NULL, 0, 0, 0, 1, 1),
(106, 116, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:37:30', '2018-02-09 10:37:30', NULL, 0, 0, 0, 1, 1),
(107, 117, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:39:16', '2018-02-09 10:39:16', NULL, 0, 0, 0, 1, 1),
(108, 118, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:40:29', '2018-02-09 10:40:29', NULL, 0, 0, 0, 1, 1),
(109, 119, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:41:33', '2018-02-09 10:41:33', NULL, 0, 0, 0, 1, 1),
(110, 120, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-09 10:42:39', '2018-02-09 10:42:39', NULL, 0, 0, 0, 1, 1),
(111, 125, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-26 13:32:22', '2018-02-26 13:32:22', NULL, 0, 0, 0, 1, 1),
(112, 126, 3, 3, 0, '2018-01-17', '2018-01-19', '2018-02-26 13:34:09', '2018-02-26 13:34:09', NULL, 0, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `order_img`
--

CREATE TABLE `order_img` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `order_img`
--

INSERT INTO `order_img` (`id`, `order_id`, `file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 107, '2018020789713.jpg', '2018-02-07 16:57:48', '2018-02-08 11:34:32', NULL),
(2, 107, '2018020712263.jpg', '2018-02-07 16:57:48', '2018-02-08 11:34:37', NULL),
(3, 107, '2018020779985.jpg', '2018-02-07 16:57:48', '2018-02-08 11:34:42', NULL),
(4, 107, '2018020788061.jpg', '2018-02-07 16:57:48', '2018-02-08 11:34:45', NULL),
(5, 105, '2018020740293.jpg', '2018-02-07 16:59:50', '2018-02-08 11:34:50', NULL),
(6, 105, '2018020783873.jpg', '2018-02-07 16:59:50', '2018-02-08 11:34:54', NULL),
(7, 105, '2018020762768.jpg', '2018-02-07 17:00:39', '2018-02-08 11:34:58', NULL),
(8, 105, '2018020791574.jpg', '2018-02-07 17:01:01', '2018-02-08 11:35:02', NULL),
(9, 105, '18020811590145008.jpg', '2018-02-08 11:59:01', '2018-02-08 11:59:01', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `order_remark`
--

CREATE TABLE `order_remark` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `order_remark`
--

INSERT INTO `order_remark` (`id`, `name`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '國內', '國內', '2017-09-08 11:31:33', '2017-09-08 11:55:33', NULL),
(2, '國外', 'xxxxx', '2017-09-08 11:32:18', '2017-09-08 11:32:18', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `order_service`
--

CREATE TABLE `order_service` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `service_id` int(10) NOT NULL,
  `adult_quantity` int(10) NOT NULL,
  `child_quantity` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `manufacturers_id` int(10) NOT NULL,
  `supplier_id` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `time` time DEFAULT NULL,
  `cost` int(11) NOT NULL,
  `check` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `order_service`
--

INSERT INTO `order_service` (`id`, `order_id`, `service_id`, `adult_quantity`, `child_quantity`, `created_at`, `updated_at`, `deleted_at`, `manufacturers_id`, `supplier_id`, `date`, `time`, `cost`, `check`) VALUES
(1, 1, 1, 10, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-15 09:32:44', 0, 0, NULL, NULL, 0, 0),
(2, 105, 3, 1, 0, '0000-00-00 00:00:00', '2018-01-24 22:07:05', '2018-01-24 09:50:40', 1, 1, '2018-11-29 00:00:00', NULL, 0, 0),
(3, 107, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-05 05:32:00', 0, 0, NULL, NULL, 0, 1),
(4, 107, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-05 05:32:00', 0, 0, NULL, NULL, 0, 1),
(5, 108, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:34:19', 0, 0, NULL, NULL, 0, 1),
(6, 108, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:34:19', 0, 0, NULL, NULL, 0, 1),
(7, 109, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:34:57', 0, 0, NULL, NULL, 0, 1),
(8, 109, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:34:57', 0, 0, NULL, NULL, 0, 1),
(9, 110, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:35:04', 0, 0, NULL, NULL, 0, 1),
(10, 110, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:35:04', 0, 0, NULL, NULL, 0, 1),
(11, 111, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:35:21', 0, 0, NULL, NULL, 0, 1),
(12, 111, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:35:21', 0, 0, NULL, NULL, 0, 1),
(13, 112, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:36:01', 0, 0, NULL, NULL, 0, 1),
(14, 112, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:36:01', 0, 0, NULL, NULL, 0, 1),
(15, 113, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:36:26', 0, 0, NULL, NULL, 0, 1),
(16, 113, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:36:26', 0, 0, NULL, NULL, 0, 1),
(17, 114, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:36:55', 0, 0, NULL, NULL, 0, 1),
(18, 114, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:36:55', 0, 0, NULL, NULL, 0, 1),
(19, 115, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:37:23', 0, 0, NULL, NULL, 0, 1),
(20, 115, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:37:23', 0, 0, NULL, NULL, 0, 1),
(21, 116, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:37:30', 0, 0, NULL, NULL, 0, 1),
(22, 116, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:37:30', 0, 0, NULL, NULL, 0, 1),
(23, 117, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:39:16', 0, 0, NULL, NULL, 0, 1),
(24, 117, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:39:16', 0, 0, NULL, NULL, 0, 1),
(25, 118, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:40:28', 0, 0, NULL, NULL, 0, 1),
(26, 118, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:40:28', 0, 0, NULL, NULL, 0, 1),
(27, 119, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:41:33', 0, 0, NULL, NULL, 0, 1),
(28, 119, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:41:33', 0, 0, NULL, NULL, 0, 1),
(29, 120, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:42:39', 0, 0, NULL, NULL, 0, 1),
(30, 120, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-09 02:42:39', 0, 0, NULL, NULL, 0, 1),
(31, 125, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-26 05:32:22', 0, 0, NULL, NULL, 0, 1),
(32, 125, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-26 05:32:22', 0, 0, NULL, NULL, 0, 1),
(33, 126, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-26 05:34:09', 0, 0, NULL, NULL, 0, 1),
(34, 126, 3, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-26 05:34:09', 0, 0, NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `order_ticket`
--

CREATE TABLE `order_ticket` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `ticket_departure_date_id` int(10) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cost` int(11) NOT NULL,
  `check` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `order_ticket`
--

INSERT INTO `order_ticket` (`id`, `order_id`, `ticket_id`, `ticket_departure_date_id`, `quantity`, `created_at`, `updated_at`, `deleted_at`, `cost`, `check`) VALUES
(1, '1', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0),
(3, '104', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0),
(4, '105', 6, 300, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0),
(5, '106', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0),
(6, '107', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(7, '108', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(8, '109', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(9, '110', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(10, '111', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(11, '112', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(12, '113', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(13, '114', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(14, '115', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(15, '116', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(16, '117', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(17, '118', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(18, '119', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(19, '120', 2, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(20, '121', 6, 300, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0),
(21, '122', 6, 300, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0),
(22, '124', 6, 300, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 0),
(23, '125', 3, 388, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1),
(24, '126', 3, 388, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `orientation_config`
--

CREATE TABLE `orientation_config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content1` text COLLATE utf8_unicode_ci NOT NULL,
  `content2` text COLLATE utf8_unicode_ci NOT NULL,
  `content3` text COLLATE utf8_unicode_ci NOT NULL,
  `content4` text COLLATE utf8_unicode_ci NOT NULL,
  `content5` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `orientation_config`
--

INSERT INTO `orientation_config` (`id`, `name`, `content1`, `content2`, `content3`, `content4`, `content5`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '兩天一夜', '123', '123', '123', '123', '123', '2017-06-09 09:51:17', '2017-06-09 09:51:17', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_msg` text COLLATE utf8_unicode_ci NOT NULL,
  `reads` int(11) DEFAULT '0' COMMENT '0:未讀取 1:已經讀取',
  `user_read` int(11) NOT NULL DEFAULT '0',
  `sales_msg` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `post`
--

INSERT INTO `post` (`id`, `user_id`, `order_id`, `user_msg`, `reads`, `user_read`, `sales_msg`, `created_at`, `updated_at`) VALUES
(59, 21, 104, 'xxxx', 1, 1, '已經幫您處裡', '2018-01-22 06:48:37', '2018-01-22 14:49:16'),
(60, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 13:01:52', '2018-01-24 09:01:52'),
(61, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 13:03:44', '2018-01-24 09:03:44'),
(62, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 13:04:20', '2018-01-24 09:04:20'),
(63, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 13:04:51', '2018-01-24 09:04:51'),
(64, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 13:17:55', '2018-01-24 09:17:55'),
(65, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 13:27:08', '2018-01-24 09:27:08'),
(66, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 13:29:18', '2018-01-24 09:29:18'),
(67, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 13:29:46', '2018-01-24 09:29:46'),
(68, 5, 105, '', 0, 0, '[異動飯店項目]\n房間: 山景雙人房 => E雙人房\n', '2018-01-24 13:53:02', '2018-01-24 09:53:02'),
(69, 5, 105, '', 0, 0, '[異動飯店項目]\n房間: 山景雙人房 => 山景雙人房\n', '2018-01-24 13:54:09', '2018-01-24 09:54:09'),
(70, 5, 105, '', 0, 0, '[異動飯店項目]\n房間: 山景雙人房 => 山景雙人房\n', '2018-01-24 13:54:17', '2018-01-24 09:54:17'),
(71, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 金沙灘水上活動不限次數 => 南海-七美島.望安島跳島全日遊-不玩水\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 14:04:29', '2018-01-24 10:04:29'),
(72, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 14:04:34', '2018-01-24 10:04:34'),
(73, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 南海-七美島.望安島跳島全日遊-不玩水 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 14:04:42', '2018-01-24 10:04:42'),
(74, 5, 105, '', 0, 0, '[異動活動項目]\n活動名稱: 東海水上活動全日遊 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 14:04:51', '2018-01-24 10:04:51'),
(75, 21, 116, 'xxx', 1, 1, 'xxx', '2018-01-24 14:31:09', '2018-01-24 22:31:46'),
(76, 21, 116, 'xxx', 0, 1, NULL, '2018-01-24 14:32:51', NULL),
(77, 21, 116, 'xxx', 1, 1, 'xxx', '2018-01-24 14:33:22', '2018-01-24 22:33:47'),
(78, 5, 116, '', 0, 0, '[新增團員名冊]\n', '2018-01-24 14:36:38', '2018-01-24 10:36:38'),
(79, 5, 116, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 14:40:31', '2018-01-24 10:40:31'),
(80, 5, 116, '', 0, 0, '[異動活動項目]\n活動名稱: 吉貝島水上活動無限暢玩 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 14:40:44', '2018-01-24 10:40:44'),
(81, 5, 116, '', 0, 0, '[異動活動項目]\n活動名稱: 南海-七美島.望安島跳島全日遊-不玩水 => 吉貝島水上活動無限暢玩\n大人人數: 1 => 1\n小孩人數: 0 => 0\n', '2018-01-24 14:43:23', '2018-01-24 10:43:23'),
(82, 5, 116, '', 0, 0, '[異動飯店項目]\n房間: 雙人房 => 雙人房\n', '2018-01-24 14:44:26', '2018-01-24 10:44:26'),
(83, 5, 117, '', 0, 0, '[異動飯店項目]\n房間: 山景雙人房 => 山景雙人房\n', '2018-01-24 16:45:25', '2018-01-25 12:45:25');

-- --------------------------------------------------------

--
-- 資料表結構 `privacy`
--

CREATE TABLE `privacy` (
  `id` int(11) NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `privacy`
--

INSERT INTO `privacy` (`id`, `body`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '<p>隱私權聲明</p>\r\n', '2016-10-26 03:58:11', '2016-10-26 03:58:11', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `program_registration`
--

CREATE TABLE `program_registration` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `program_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `menu_class_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `menu` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `top_program_id` int(11) NOT NULL,
  `top_program_id_is_show` int(11) NOT NULL,
  `program_id_is_show` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `program_registration`
--

INSERT INTO `program_registration` (`id`, `name`, `program_name`, `menu_class_id`, `sort`, `menu`, `created_at`, `updated_at`, `deleted_at`, `top_program_id`, `top_program_id_is_show`, `program_id_is_show`) VALUES
(2, '帳號管理', 'BackUserManagement', '2', 1, 'Y', '2016-10-24 09:08:55', '2016-05-27 09:48:25', NULL, 0, 0, 0),
(3, '角色創建', 'BackRole', '2', 2, 'Y', '2016-09-04 08:26:54', '2015-12-09 01:54:14', NULL, 0, 0, 0),
(4, '角色維護', 'BackRoleControls', '2', 3, 'Y', '2016-10-24 09:09:31', '2016-10-24 09:09:31', NULL, 0, 0, 0),
(5, '選單分類', 'BackMenuClass', '6', 3, 'Y', '2017-02-22 03:23:21', '2015-12-09 02:30:16', NULL, 0, 0, 0),
(6, '程式註冊', 'BackProgramRegistration', '6', 5, 'Y', '2017-02-22 03:23:24', '2015-12-09 02:28:40', NULL, 0, 0, 0),
(7, '回首頁', 'BackHome', '6', 0, 'Y', '2017-02-22 03:23:27', '0000-00-00 00:00:00', NULL, 0, 0, 0),
(8, '輪播圖管理', 'BackIndexBanner', '27', 4, 'Y', '2017-02-22 03:23:31', '2016-10-24 09:14:52', NULL, 0, 0, 0),
(9, '聯絡我們', 'BackContact', '27', 7, 'Y', '2017-02-22 06:36:53', '2017-02-22 06:36:53', NULL, 0, 0, 0),
(10, '收信帳號設定', 'BackEmail', '27', 20, 'Y', '2018-01-24 15:56:49', '2016-10-13 09:23:24', NULL, 0, 0, 0),
(11, '關於我們', 'BackAbout', '27', 5, 'Y', '2017-02-22 06:36:07', '2017-02-22 06:36:07', NULL, 0, 0, 0),
(12, '隱私權聲明', 'BackPrivacy', '27', 8, 'Y', '2018-01-24 15:56:17', '2018-01-24 15:56:17', '2018-01-24 15:56:17', 0, 0, 0),
(13, '最新消息', 'BackNews', '27', 6, 'Y', '2017-02-22 06:36:30', '2017-02-22 06:36:30', NULL, 0, 0, 0),
(14, '刪除圖片', 'BackRemove', '24', 15, 'N', '2017-02-22 03:23:52', '2017-02-15 02:35:53', NULL, 0, 0, 0),
(15, '活動類別', 'BackActivityClass', '29', 9, 'Y', '2018-01-22 08:28:15', '2018-01-22 08:28:15', '2018-01-22 08:28:15', 0, 0, 0),
(16, '活動管理', 'BackActivity', '29', 10, 'Y', '2018-01-10 08:35:58', '2018-01-10 08:35:58', NULL, 30, 0, 1),
(17, '當地交通', 'BackTrafficServices', '29', 11, 'Y', '2018-01-10 08:36:34', '2018-01-10 08:36:34', NULL, 30, 0, 1),
(18, '交通服務細項', 'BackTrafficServicesRead', '28', 12, 'N', '2017-02-24 08:07:08', '2017-02-24 08:07:08', NULL, 0, 0, 0),
(19, '票位公司管理', 'BackTransportationCompany', '29', 14, 'Y', '2018-01-24 16:02:16', '2018-01-10 08:38:23', NULL, 0, 0, 0),
(20, '票位地點管理', 'BackLocation', '29', 15, 'Y', '2018-01-24 16:02:18', '2018-01-10 08:40:42', NULL, 0, 0, 0),
(21, '票位管理', 'BackTicket', '29', 13, 'Y', '2018-01-24 16:02:13', '2017-03-01 04:07:43', NULL, 0, 0, 0),
(22, '票位假日加價', 'BackTicketRead', '29', 0, 'N', '2018-01-10 08:35:35', '2018-01-10 08:35:35', '2018-01-10 08:35:35', 21, 0, 1),
(23, '飯店類別', 'BackHotelClass', '30', 16, 'Y', '2017-03-15 06:35:38', '2017-03-15 06:35:38', '2017-03-15 06:35:38', 0, 0, 0),
(24, '飯店管理', 'BackHotel', '29', 12, 'Y', '2018-01-24 16:01:38', '2018-01-12 08:18:26', NULL, 30, 0, 1),
(25, '飯店房型管理', 'BackHotelRead', '30', 0, 'N', '2017-12-27 07:37:23', '2017-12-27 07:37:23', NULL, 24, 1, 1),
(26, '自由行管理', 'BackFreeExercise', '31', 18, 'N', '2017-12-27 07:36:46', '2017-12-27 07:36:46', NULL, 30, 0, 1),
(27, '行程設定', 'BackSetTrip', '31', 19, 'Y', '2018-01-24 16:03:38', '2018-01-10 03:03:18', NULL, 30, 0, 1),
(28, '公司相關證照', 'BackLicense', '27', 6, 'Y', '2018-01-24 15:55:14', '2018-01-24 15:55:14', '2018-01-24 15:55:14', 0, 0, 0),
(29, '行程國別', 'BackTripClass', '31', 21, 'Y', '2017-03-15 06:18:45', '2017-03-15 06:18:45', '2017-03-15 06:18:45', 0, 0, 0),
(30, '行程地點', 'BackTripLocation', '29', 22, 'Y', '2018-01-24 18:06:04', '2018-01-24 18:06:04', NULL, 0, 0, 0),
(31, '飯店加價', 'BackHotelReadRead', '30', 87, 'N', '2017-03-15 08:18:32', '2017-03-15 08:18:32', NULL, 0, 0, 0),
(32, '首頁行銷版位', 'BackIndexMarketing', '27', 7, 'Y', '2018-01-24 15:55:42', '2018-01-10 08:39:09', NULL, 0, 0, 0),
(33, '機票出發日期', 'BackTicketRead2', '29', 101, 'N', '2017-03-31 09:37:50', '2017-03-31 09:37:50', NULL, 0, 0, 0),
(34, '正式訂單管理', 'BackOrder', '32', 1, 'Y', '2018-01-24 16:13:51', '2018-01-10 08:42:58', NULL, 0, 0, 0),
(35, '訂單機票', 'BackOrderRead3', '32', 24, 'N', '2017-05-09 11:43:21', '2017-05-09 11:43:21', NULL, 0, 0, 0),
(36, '訂單飯店', 'BackOrderRead4', '32', 25, 'N', '2017-05-09 11:43:47', '2017-05-09 11:43:47', NULL, 0, 0, 0),
(37, '訂單交通活動', 'BackOrderRead5', '32', 25, 'N', '2017-05-09 11:44:20', '2017-05-09 11:44:20', NULL, 0, 0, 0),
(38, '訂單加購項目', 'BackOrderRead6', '32', 26, 'N', '2017-05-09 11:44:49', '2017-05-09 11:44:49', NULL, 0, 0, 0),
(39, '需求單管理', 'BackQUO', '32', 2, 'Y', '2018-01-24 16:14:00', '2018-01-10 08:42:25', NULL, 0, 0, 0),
(40, '行程成員', 'BackOrderDetail', '32', 27, 'N', '2017-05-10 06:42:35', '2017-05-10 06:42:35', NULL, 0, 0, 0),
(41, '加購項目訂單', 'BackOrderActivityDetail', '32', 28, 'N', '2017-05-10 06:49:05', '2017-05-10 06:49:05', NULL, 0, 0, 0),
(42, '客服紀錄', 'customer_service', '32', 29, 'N', '2017-05-10 07:05:11', '2017-05-10 07:05:11', NULL, 0, 0, 0),
(43, '訂單交通服務', 'BackOrderTrafficDetail', '31', 30, 'N', '2017-05-10 07:08:06', '2017-05-10 07:08:06', NULL, 0, 0, 0),
(44, '訂單飯店項目', 'BackOrderHotelDetail', '32', 31, 'N', '2017-05-10 07:08:42', '2017-05-10 07:08:42', NULL, 0, 0, 0),
(45, '訂單詢問', 'BackOrderCS', '32', 32, 'N', '2017-06-05 11:40:43', '2017-06-05 11:40:43', NULL, 0, 0, 0),
(46, '票位類型', 'BackTicketType', '29', 16, 'Y', '2018-01-24 16:02:20', '2017-05-22 05:44:00', NULL, 0, 0, 0),
(47, 'Menu行程列表', 'BackMenuTripList', '31', 18, 'Y', '2018-01-22 07:21:41', '2018-01-22 07:21:41', '2018-01-22 07:21:41', 0, 0, 0),
(48, '供應商管理', 'BackSupplier', '29', 33, 'Y', '2018-01-24 11:19:32', '2018-01-24 11:19:32', NULL, 0, 0, 0),
(49, '廠商管理', 'BackManufacturers', '29', 34, 'Y', '2018-01-24 11:21:10', '2018-01-24 11:21:10', NULL, 0, 0, 0),
(50, '行前說明', 'BackOrientation', '32', 0, 'N', '2017-06-01 05:24:17', '2017-06-01 05:24:17', NULL, 0, 0, 0),
(51, '供應商報表', 'BackSupplierRead', '33', 0, 'N', '2018-01-10 09:38:50', '2018-01-10 09:38:50', '2018-01-10 09:38:50', 0, 0, 0),
(52, '行前通知單範本設定', 'BackOrientationConfig', '32', 100, 'Y', '2017-06-09 08:59:08', '2017-06-09 08:59:08', NULL, 0, 0, 0),
(53, '票位種類', 'BackTicketClass', '29', 20, 'Y', '2018-01-10 08:41:42', '2018-01-10 08:41:42', '2018-01-10 08:41:42', 0, 0, 0),
(54, '套裝行程房型設定', 'BackRoomType', '31', 10, 'Y', '2018-01-10 08:38:47', '2018-01-10 08:38:47', '2018-01-10 08:38:47', 0, 0, 0),
(55, '接駁車', 'BackShuttleBus', '31', 30, 'Y', '2018-01-24 09:35:00', '2018-01-24 09:35:00', '2018-01-24 09:35:00', 0, 0, 0),
(56, '訂單備註設定', 'BackOrderRemark', '32', 20, 'Y', '2017-09-22 17:25:26', '2017-09-22 17:25:26', NULL, 0, 0, 0),
(57, '套裝行程旅行團', 'BackSetTripTour', '31', 0, 'N', '2018-01-10 06:06:21', '2018-01-10 06:06:21', NULL, 0, 0, 0),
(58, '旅客名單', 'BackMember', '34', 4, 'Y', '2018-01-10 09:32:07', '2018-01-10 09:32:07', NULL, 0, 0, 0),
(59, '單購項目代表圖片', 'BackItemImg', '27', 40, 'Y', '2018-02-07 04:11:51', '2018-02-07 04:11:51', NULL, 0, 0, 0),
(60, '訂單圖片', 'BackOrderImg', '32', 10, 'N', '2018-02-08 03:29:59', '2018-02-08 03:29:59', NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- 資料表結構 `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sort` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`, `sort`, `deleted_at`) VALUES
(2, '超級管理員', '<p>Full access to create, edit, and update companies, and orders.</p>\r\n', '2016-05-26 08:24:42', '2018-01-24 15:47:25', 0, NULL),
(3, '一般管理員', '<p>Ability to create new companies and orders, or edit and update any existing ones.</p>\r\n', '2016-05-26 08:24:42', '2018-01-24 15:47:34', 0, NULL),
(5, '一般會員', '<p>A standard user that can have a licence assigned to them. No administrative features.</p>\r\n', '2016-05-26 08:24:42', '2018-01-24 15:50:56', 0, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `role_controls`
--

CREATE TABLE `role_controls` (
  `id` int(11) NOT NULL,
  `create` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `update` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `read` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `delete` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `program_registration_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `role_controls`
--

INSERT INTO `role_controls` (`id`, `create`, `update`, `read`, `delete`, `program_registration_id`, `role_id`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '0', '0', '0', '0', 2, 2, 0, '2017-02-22 03:29:11', '2017-02-10 04:17:39', NULL),
(2, '0', '0', '0', '0', 3, 2, 0, '2017-02-22 03:29:14', '2017-02-10 05:50:44', NULL),
(3, '0', '0', '0', '0', 4, 2, 0, '2017-02-22 03:29:16', '2017-02-10 05:51:39', NULL),
(4, '0', '0', '0', '0', 5, 2, 0, '2017-02-22 03:29:18', '2017-02-10 07:08:11', NULL),
(5, '0', '0', '0', '0', 6, 2, 0, '2017-02-22 03:29:21', '2017-02-10 07:08:18', NULL),
(6, '0', '0', '0', '0', 7, 2, 0, '2017-02-22 03:29:24', '2017-02-10 07:08:25', NULL),
(7, '0', '0', '0', '0', 8, 2, 0, '2017-02-22 03:29:27', '2017-02-10 08:45:15', NULL),
(8, '0', '0', '0', '0', 9, 2, 0, '2017-02-22 03:29:30', '2017-02-10 08:45:21', NULL),
(9, '0', '0', '0', '0', 10, 2, 0, '2017-02-22 03:29:33', '2017-02-13 03:13:42', NULL),
(10, '0', '0', '0', '0', 11, 2, 0, '2017-02-22 03:29:36', '2017-02-13 07:17:52', NULL),
(11, '0', '0', '0', '0', 12, 2, 0, '2017-02-22 03:29:38', '2017-02-13 08:35:27', NULL),
(12, '0', '0', '0', '0', 13, 2, 0, '2017-02-22 03:29:41', '2017-02-13 08:35:35', NULL),
(13, '0', '0', '0', '0', 14, 2, 0, '2017-02-22 03:29:43', '2017-02-15 02:58:26', NULL),
(14, '0', '0', '0', '0', 159, 2, 0, '2017-02-22 03:28:54', '2017-02-20 14:29:30', NULL),
(15, '0', '0', '0', '0', 15, 2, 0, '2017-02-24 06:48:46', '2017-02-24 06:48:46', NULL),
(16, '0', '0', '0', '0', 16, 2, 0, '2017-02-24 07:21:32', '2017-02-24 07:21:32', NULL),
(17, '0', '0', '0', '0', 17, 2, 0, '2017-02-24 07:48:54', '2017-02-24 07:48:54', NULL),
(18, '0', '0', '0', '0', 18, 2, 0, '2017-02-24 08:07:16', '2017-02-24 08:07:16', NULL),
(19, '0', '0', '0', '0', 19, 2, 0, '2017-02-24 08:58:23', '2017-02-24 08:58:23', NULL),
(20, '0', '0', '0', '0', 20, 2, 0, '2017-03-01 03:31:55', '2017-03-01 03:31:55', NULL),
(21, '0', '0', '0', '0', 21, 2, 0, '2017-03-01 04:08:03', '2017-03-01 04:08:03', NULL),
(22, '0', '0', '0', '0', 22, 2, 0, '2017-03-01 07:23:22', '2017-03-01 07:23:22', NULL),
(23, '0', '0', '0', '0', 23, 2, 0, '2017-03-01 08:50:22', '2017-03-01 08:50:22', NULL),
(24, '0', '0', '0', '0', 24, 2, 0, '2017-03-01 08:50:27', '2017-03-01 08:50:27', NULL),
(25, '0', '0', '0', '0', 25, 2, 0, '2017-03-01 10:13:41', '2017-03-01 10:13:41', NULL),
(26, '0', '0', '0', '0', 26, 2, 0, '2017-03-02 03:15:28', '2017-03-02 03:15:28', NULL),
(27, '0', '0', '0', '0', 27, 2, 0, '2017-03-02 03:15:34', '2017-03-02 03:15:34', NULL),
(28, '0', '0', '0', '0', 28, 2, 0, '2017-03-06 08:06:11', '2017-03-06 08:06:11', NULL),
(29, '0', '0', '0', '0', 29, 2, 0, '2017-03-09 08:22:08', '2017-03-09 08:22:08', NULL),
(30, '0', '0', '0', '0', 30, 2, 0, '2017-03-09 08:31:26', '2017-03-09 08:31:26', NULL),
(31, '0', '0', '0', '0', 31, 2, 0, '2017-03-15 08:18:43', '2017-03-15 08:18:43', NULL),
(32, '0', '0', '0', '0', 32, 2, 0, '2017-03-17 07:42:17', '2017-03-17 07:42:17', NULL),
(33, '0', '0', '0', '0', 33, 2, 0, '2017-03-31 09:37:59', '2017-03-31 09:37:59', NULL),
(34, '0', '0', '0', '0', 34, 2, 0, '2017-05-09 11:00:44', '2017-05-09 11:00:44', NULL),
(35, '0', '0', '0', '0', 35, 2, 0, '2017-05-09 11:45:01', '2017-05-09 11:45:01', NULL),
(36, '0', '0', '0', '0', 36, 2, 0, '2017-05-09 11:45:08', '2017-05-09 11:45:08', NULL),
(37, '0', '0', '0', '0', 37, 2, 0, '2017-05-09 11:45:15', '2017-05-09 11:45:15', NULL),
(38, '0', '0', '0', '0', 38, 2, 0, '2017-05-09 11:45:22', '2017-05-09 11:45:22', NULL),
(39, '0', '0', '0', '0', 39, 2, 0, '2017-05-10 06:35:43', '2017-05-10 06:35:43', NULL),
(40, '0', '0', '0', '0', 40, 2, 0, '2017-05-10 06:42:47', '2017-05-10 06:42:47', NULL),
(41, '0', '0', '0', '0', 41, 2, 0, '2017-05-10 06:48:42', '2017-05-10 06:48:42', NULL),
(42, '0', '0', '0', '0', 42, 2, 0, '2017-05-10 07:05:21', '2017-05-10 07:05:21', NULL),
(43, '0', '0', '0', '0', 44, 2, 0, '2017-05-10 07:08:58', '2017-05-10 07:08:58', NULL),
(44, '0', '0', '0', '0', 45, 2, 0, '2017-05-10 09:40:48', '2017-05-10 09:40:48', NULL),
(45, '0', '0', '0', '0', 46, 2, 0, '2017-05-22 05:44:22', '2017-05-22 05:44:22', NULL),
(46, '0', '0', '0', '0', 47, 2, 0, '2017-05-22 08:35:05', '2017-05-22 08:35:05', NULL),
(47, '0', '0', '0', '0', 48, 2, 0, '2017-05-23 06:40:36', '2017-05-23 06:40:36', NULL),
(48, '0', '0', '0', '0', 49, 2, 0, '2017-05-23 06:40:44', '2017-05-23 06:40:44', NULL),
(49, '0', '0', '0', '0', 50, 2, 0, '2017-06-01 05:24:30', '2017-06-01 05:24:30', NULL),
(50, '0', '0', '0', '0', 51, 2, 0, '2017-06-05 04:46:39', '2017-06-05 04:46:39', NULL),
(51, '0', '0', '0', '0', 34, 3, 0, '2017-06-09 08:13:21', '2017-06-09 08:13:21', NULL),
(52, '0', '0', '0', '0', 43, 3, 0, '2017-06-09 08:13:42', '2017-06-09 08:13:42', NULL),
(53, '0', '0', '0', '0', 35, 3, 0, '2017-06-09 08:13:58', '2017-06-09 08:13:58', NULL),
(54, '0', '0', '0', '0', 36, 3, 0, '2017-06-09 08:14:12', '2017-06-09 08:14:12', NULL),
(55, '0', '0', '0', '0', 42, 3, 0, '2017-06-09 08:14:54', '2017-06-09 08:14:54', NULL),
(56, '0', '0', '0', '0', 37, 3, 0, '2017-06-09 08:15:12', '2017-06-09 08:15:12', NULL),
(57, '0', '0', '0', '0', 45, 3, 0, '2017-06-09 08:26:34', '2017-06-09 08:26:34', NULL),
(58, '0', '0', '0', '0', 40, 3, 0, '2017-06-09 08:27:14', '2017-06-09 08:27:14', NULL),
(59, '0', '0', '0', '0', 38, 3, 0, '2017-06-09 08:27:57', '2017-06-09 08:27:57', NULL),
(60, '0', '0', '0', '0', 52, 2, 0, '2017-06-09 09:05:37', '2017-06-09 09:05:37', NULL),
(61, '0', '0', '0', '0', 53, 2, 0, '2017-08-28 07:49:25', '2017-08-28 07:49:25', NULL),
(62, '0', '0', '0', '0', 54, 2, 0, '2017-09-18 09:47:23', '2017-09-18 09:47:23', NULL),
(63, '0', '0', '0', '0', 55, 2, 0, '2017-09-21 02:57:08', '2017-09-21 02:57:08', NULL),
(64, '0', '0', '0', '0', 56, 2, 0, '2017-09-22 17:24:15', '2017-09-22 17:24:15', NULL),
(65, '0', '0', '0', '0', 57, 2, 0, '2018-01-10 06:06:34', '2018-01-10 06:06:34', NULL),
(66, '0', '0', '0', '0', 58, 2, 0, '2018-01-10 08:46:11', '2018-01-10 08:46:11', NULL),
(67, '0', '0', '0', '0', 59, 2, 0, '2018-02-07 04:12:21', '2018-02-07 04:12:21', NULL),
(68, '0', '0', '0', '0', 60, 2, 0, '2018-02-08 03:30:26', '2018-02-08 03:30:26', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `room_type`
--

CREATE TABLE `room_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `set_trip_id` int(11) NOT NULL,
  `add_price` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `room_type`
--

INSERT INTO `room_type` (`id`, `name`, `set_trip_id`, `add_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '單人房', 1, 200, '2017-09-18 17:50:44', '2017-09-18 17:58:13', NULL),
(2, '雙人房', 1, 0, '2017-09-18 17:58:04', '2017-09-18 17:58:04', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `search`
--

CREATE TABLE `search` (
  `id` int(11) NOT NULL,
  `controller` text COLLATE utf8_unicode_ci NOT NULL,
  `string` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `search_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `search`
--

INSERT INTO `search` (`id`, `controller`, `string`, `created_at`, `updated_at`, `deleted_at`, `search_id`) VALUES
(1, 'BackProgramRegistration', '27,Y,關於我們,BackAbout,5', '2017-02-22 06:36:07', '2017-02-22 06:36:07', NULL, 11),
(2, 'BackProgramRegistration', '27,Y,最新消息,BackNews,6', '2017-02-22 06:36:30', '2017-02-22 06:36:30', NULL, 13),
(3, 'BackProgramRegistration', '27,Y,聯絡我們,BackContact,7', '2017-02-22 06:36:53', '2017-02-22 06:36:53', NULL, 9),
(4, 'BackProgramRegistration', '27,Y,隱私權聲明,BackPrivacy,8', '2017-02-22 06:37:08', '2017-02-22 06:37:08', NULL, 12),
(5, 'BackMenuClass', '項目管理,fa fa-cog,2', '2017-02-24 06:40:45', '2017-02-24 06:40:45', NULL, 28),
(6, 'BackProgramRegistration', '29,Y,活動類別,BackActivityClass,9', '2018-01-10 08:37:10', '2018-01-10 08:37:10', NULL, 15),
(7, 'BackActivityClass', '玩水類', '2017-02-24 06:51:51', '2017-02-24 06:51:51', NULL, 1),
(8, 'BackActivityClass', '生態體驗', '2017-02-24 06:52:50', '2017-02-24 06:52:50', NULL, 2),
(9, 'BackProgramRegistration', '29,Y,活動管理,BackActivity,10', '2018-01-10 08:35:58', '2018-01-10 08:35:58', NULL, 16),
(10, 'BackActivity', '9,吉貝島水上活動無限暢玩,8000,1000,', '2018-02-02 10:36:34', '2018-02-02 10:36:34', NULL, 1),
(11, 'BackProgramRegistration', '29,Y,當地交通,BackTrafficServices,11', '2018-01-10 08:36:34', '2018-01-10 08:36:34', NULL, 17),
(12, 'BackTrafficServices', '3,1000,500,600,200,<p>aaa</p>\r\n', '2017-03-15 07:13:01', '2017-03-15 07:13:01', NULL, 1),
(13, 'BackProgramRegistration', '28,N,交通服務細項,BackTrafficServicesRead,12', '2017-02-24 08:07:08', '2017-02-24 08:07:08', NULL, 18),
(14, 'BackTrafficServicesRead', '1,City Bus,1000,500,600,200,<p>aaa</p>\r\n', '2017-03-14 06:30:10', '2017-03-14 06:30:10', NULL, 1),
(15, 'BackTrafficServices', '10,,<p>日北沖繩</p>\r\n', '2017-03-15 07:11:59', '2017-03-15 07:11:59', NULL, 2),
(16, 'BackTrafficServicesRead', '1,觀光交通車,1000,500,600,300,<p>aaa</p>\r\n', '2017-03-14 06:35:49', '2017-03-14 06:35:49', NULL, 2),
(17, 'BackMenuClass', '票位管理,fa fa-cog,4', '2017-02-24 08:55:18', '2017-02-24 08:55:18', NULL, 29),
(18, 'BackProgramRegistration', '29,Y,票位公司管理,BackTransportationCompany,13', '2018-01-10 08:38:24', '2018-01-10 08:38:24', NULL, 19),
(19, 'BackTransportationCompany', '長榮航空,0800098666', '2017-05-31 12:41:45', '2017-05-31 12:41:45', NULL, 1),
(20, 'BackProgramRegistration', '29,Y,票位地點管理,BackLocation,14', '2018-01-10 08:40:42', '2018-01-10 08:40:42', NULL, 20),
(21, 'BackLocation', '沖繩機場', '2017-06-05 17:44:31', '2017-06-05 17:44:31', NULL, 1),
(22, 'BackProgramRegistration', '29,Y,票位管理,BackTicket,15', '2017-03-01 04:07:43', '2017-03-01 04:07:43', NULL, 21),
(23, 'BackLocation', '高雄小港機場', '2017-03-01 04:24:41', '2017-03-01 04:24:41', NULL, 2),
(24, 'BackTicket', '東京來回機票,1,2,1,3,1,', '2017-08-28 08:05:41', '2017-08-28 08:05:41', NULL, 1),
(25, 'BackTicket', '長榮航空-(高雄至澎湖),1,1,1,2,8,', '2017-08-28 08:05:25', '2017-08-28 08:05:25', NULL, 2),
(26, 'BackProgramRegistration', '29,N,票位假日加價,BackTicketRead,0', '2017-03-01 07:23:12', '2017-03-01 07:23:12', NULL, 22),
(27, 'BackMenuClass', '飯店管理,fa fa-cog,5', '2017-03-01 08:47:24', '2017-03-01 08:47:24', NULL, 30),
(28, 'BackProgramRegistration', '30,Y,飯店類別,BackHotelClass,16', '2017-03-01 08:48:38', '2017-03-01 08:48:38', NULL, 23),
(29, 'BackProgramRegistration', '29,Y,飯店管理,BackHotel,17', '2018-01-12 08:18:26', '2018-01-12 08:18:26', NULL, 24),
(30, 'BackHotelClass', '日本福岡', '2017-03-01 08:57:18', '2017-03-01 08:57:18', NULL, 1),
(31, 'BackHotelClass', '日本北海道', '2017-03-01 08:57:29', '2017-03-01 08:57:29', NULL, 2),
(32, 'BackHotelClass', '日本東京', '2017-03-01 08:57:38', '2017-03-01 08:57:38', NULL, 3),
(33, 'BackHotelClass', '高雄', '2017-03-01 08:57:45', '2017-03-01 08:57:45', NULL, 4),
(34, 'BackHotelClass', '阿里山', '2017-03-01 08:57:53', '2017-03-01 08:57:53', NULL, 5),
(35, 'BackHotelClass', '台北', '2017-03-01 08:58:01', '2017-03-01 08:58:01', NULL, 6),
(36, 'BackHotelClass', '義大利', '2017-03-01 08:58:08', '2017-03-01 08:58:08', NULL, 7),
(37, 'BackHotel', '10,5,東新宿E飯店,<p>飯店位於地鐵「東新宿車站」A1出口走出來旁邊即可到達。東新宿E飯店的一樓即有24小時LAWSON商店，還有飯店直營的Tully&#39;s Coffee，附近還有超市，就算是女性旅客或是長期投宿的旅客也能安心入住的優質飯店。</p>\r\n', '2017-05-11 12:44:55', '2017-05-11 12:44:55', NULL, 1),
(38, 'BackProgramRegistration', '30,N,飯店房型管理,BackHotelRead,0', '2017-03-01 10:13:33', '2017-03-01 10:13:33', NULL, 25),
(39, 'BackHotelRead', '2,山景雙人房,0,0,2,0,12,2500,1500,3000,<p>a</p>\r\n', '2017-04-24 07:15:12', '2017-04-24 07:15:12', NULL, 1),
(40, 'BackHotelRead', '1,E雙人房,0,3000,3,0,0,6000,3000,8000,<p>aaaa</p>\r\n', '2017-05-11 13:37:57', '2017-05-11 13:37:57', NULL, 2),
(41, 'BackHotelRead', '3,雙人房,0,0,2,1,12,5000,2000,4000,<p>說明說明</p>\r\n', '2018-01-30 08:07:23', '2018-01-30 08:07:23', NULL, 3),
(42, 'BackHotelRead', '4,雙人房,0,3000,3,4000,2000,3000,<p>雙人房</p>\r\n', '2017-03-15 13:00:44', '2017-03-15 13:00:44', NULL, 4),
(43, 'BackHotelRead', '1,a,0,123,123,23,213,213,<p>123</p>\r\n', '2017-03-01 10:45:59', '2017-03-01 10:45:59', NULL, 5),
(44, 'BackHotel', '8,3,福華飯店,<p>aaaa</p>\r\n', '2017-03-15 09:48:19', '2017-03-15 09:48:19', NULL, 2),
(45, 'BackMenuClass', '商品管理,fa fa-cog,6', '2018-01-10 03:01:19', '2018-01-10 03:01:19', NULL, 31),
(46, 'BackProgramRegistration', '31,N,自由行管理,BackFreeExercise,18', '2017-12-21 03:14:09', '2017-12-21 03:14:09', NULL, 26),
(47, 'BackProgramRegistration', '31,Y,套裝行程,BackSetTrip,19', '2018-01-10 03:03:18', '2018-01-10 03:03:18', NULL, 27),
(48, 'BackFreeExercise', '澎湖自由行3天(船),3,1,8,1,0,0,0,<p>行程特色</p>\r\n,<p>費用包含</p>\r\n,<p>未包含</p>\r\n,<p>最好玩</p>\r\n', '2017-09-21 04:35:23', '2017-09-21 04:35:23', NULL, 1),
(49, 'BackFreeExercise', '蘭嶼自由行,0,1,9,1,0,0,<p><strong>蘭嶼可說是台灣離島最特殊的島嶼，整個島的達悟族文化保留完整，也不商業化，特別海景與海底景緻<br />\r\n更是優美，山區間的珠光鳳蝶與蘭嶼角鴞是蘭嶼特有種，不管文化、生態與景緻都是台灣極為特別的一<br />\r\n個地方～</strong></p>\r\n\r\n<p><strong>開元港的海水，清澈見底，藍寶石般的顏色映著陽光閃閃發亮，帶著愉快的心情，展開蘭嶼的旅程吧</strong></p>\r\n\r\n<p><strong>蘭嶼處處美景　騎車環島時要小心，路上隨時會有準備過馬路的山羊、逛大街的豬仔、寄居蟹<br />\r\n可別光顧著賞景　讓動物受傷喔</strong></p>\r\n,<p>☆台北／台東／火車費用、台東／台北機票費用。<br />\r\n☆台東火車站／富岡碼頭接送服務。<br />\r\n☆台東富岡碼頭&rarr;蘭嶼、蘭嶼&rarr;台東富岡碼頭　<br />\r\n☆蘭嶼住宿兩晚費用。<br />\r\n<strong>（蘭嶼為離島中的離島，住宿環境不比本島，無法接受請勿報名）</strong><br />\r\n☆蘭嶼機車費用兩人一部<br />\r\n☆含飯店早餐２次(視住宿民宿提供，若無提供會退早餐餐費)。<br />\r\n<strong>因提供的機車為１２５ＣＣ所以須持有重型機車駕照，外籍人士也須備有國際駕照，<br />\r\n以免到當地無法使用交通工具。若因無攜帶駕照而導致無法使用機車代步，費用恕不退費。</strong><br />\r\n☆贈送蘭嶼浮潛１次</p>\r\n,<p>☆個人旅行平安保險。<br />\r\n☆其他個人額外支出。</p>\r\n,<p>☆行程注意事項<br />\r\n１.蘭嶼、離島地區，住宿等級無法與本島相比，若無法接受者請勿報名，敬請見諒<br />\r\n２.此自由行行程，蘭嶼、皆要自行騎機車，無法提供其他交通工具<br />\r\n3.若要加購自費行程，請於報名時一併告知客服人員，以利作業<br />\r\n<br />\r\n☆蘭嶼小叮嚀：</p>\r\n\r\n<p>１.蘭嶼為原住民達悟族居住地，島上請勿直接拿著相機對著人們拍照，容易引起糾紛。<br />\r\n2.拼板舟為達悟族的生財器具，請勿隨意觸碰或過於靠近。<br />\r\n3.蘭嶼全島只有一間加油站，抵達時請先前往加油。</p>\r\n', '2017-06-07 07:02:47', '2017-06-07 07:02:47', NULL, 2),
(50, 'BackFreeExercise', '沖繩自由行-四天三夜,4,1,10,2,0,0,<ul>\r\n	<li>\r\n	<p>上述需加價日期，小孩不佔床售價亦須加價。5～10月份單售來回機票每人為7500元起。</p>\r\n	</li>\r\n	<li>\r\n	<p>訂位艙等：樂桃航空經濟艙；訂位時需附上護照影本。『樂桃航空網址』<a href="http://www.flypeach.com/tw/home.aspx" rel="noopener noreferrer" target="_blank">http://www.flypeach.com/tw/home.aspx</a></p>\r\n	</li>\r\n	<li>\r\n	<p>機位一經確認後需立即付清全額款項，無法更改日期、航點、名單、取消並無法退費/退票，且開完票後此機票完全無退票價值。取消會收全額機票費用。</p>\r\n	</li>\r\n	<li>\r\n	<p>【嬰兒、孩童票售價】因航空公司考量飛安因素，即日起限制嬰兒不佔位席次。如欲偕同嬰兒報名，敬請先行向客服人員確認正確報價，但無座位且不提供任何免費托運或手提行李件數服務，嬰兒可免費托運嬰兒推車一台，亦無提供嬰兒搖籃全程需父母抱著，一位成人旅客只能抱一位嬰幼兒坐在膝上。嬰兒或兒童佔位，售價與大人相同。</p>\r\n	</li>\r\n	<li>\r\n	<p>免費行李服務包含：每人20公斤內托運行李及一件10公斤內手提行李；超重部分按航空公司現場規定，費用敬請自理。且機上不含餐飲，需要者請於機上向空服員訂購付費。加購行李：需於開票以前加購確認，於開票時一併作業，請開票前告知業務，開票後或在國外恕無法受理。 而嬰兒票恕不提供任何免費託運或手提行李件數。</p>\r\n	</li>\r\n	<li>\r\n	<p>樂桃航空之航班在所有航程全面禁止旅客攜帶Samsung Note 7手機登機，包括隨身行李、托運行李或貨運寄送皆不可攜帶，如有查獲違規攜帶之事宜，將拒絕該名旅客登機，敬請注意。</p>\r\n	</li>\r\n	<li>\r\n	<p>若因天災等不可抗力之因素致班機延誤，航空公司不提供旅客轉乘其他航空公司班機之服務、亦不提供安排旅客住宿之服務，且不負任何行程取消延誤責任，敬請留意。</p>\r\n	</li>\r\n</ul>\r\n,<p>1. 桃園／東京成田／桃園，捷星航空機票<br />\r\n2. 機票內含20公斤托運行李（不限件數）及一件7公斤手提行李（限制長56公分．寬36公分．高23公分以內），如需加價購多件托運行李請於訂位前告知，否則請恕無法做變更。<br />\r\n3. 飯店住宿（住宿內容依照選購產品為準）<br />\r\n4. 桃園、成田兩地機場稅，以及航空公司燃油附加費<br />\r\n5. 200萬契約責任險 + 20萬意外醫療險</p>\r\n,<h4>行程不包含</h4>\r\n\r\n<p>1. 機上餐食或行程中餐食、交通、票券<br />\r\n2. 行李小費<br />\r\n3. 辦理個人護照、簽證費用<br />\r\n4. 私人開銷，如行李超重費、飯店房間內飲料酒類、洗衣、電話、快遞&hellip;等支出。</p>\r\n,<ol>\r\n	<li>\r\n	<p><strong>廉價航空非一般傳統航空，機票使用規定較為特殊且嚴苛，請同意航空公司相關規定後再行訂購報名。</strong></p>\r\n\r\n	<p>1、此機票為廉價航空~樂桃航空經濟艙四天團體票，須配合團體機位去、回程日期恕無法更改。<br />\r\n	2、機位一經確認OK即需繳付全額機票款訂金，無法更改日期、取消並無法退費，並請提供正確護照英文名單以利訂房作業，待飯店OK另行通知，敬請注意。訂金：平日每人NT8,000、卡連續假日每人NT12,000。<br />\r\n	一、飯店訂房需要保證入住，請付訂金並提供護照影本方可為您處理後續作業，3~5個工作日內回覆您飯店預訂狀況。若因天災等不可抗力之因素致班機延誤，樂桃航空不提供旅客轉乘其他航空公司班機之服務、亦不提供安排旅客住宿之服務，且不負任何行程取消延誤責任，敬請留意。<br />\r\n	★貼心提醒：SEMI-DBL(S/D)意指較小房型、一張雙人床(床寬約120 CM~140 CM，視各飯店狀況而定) ，恕無法接受嬰兒或小孩不佔床之需求，您可加價選購住宿標準雙人房型，敬請注意。<br />\r\n	★貼心提醒：日本飯店訂房規定，六歲以上小孩皆需佔床，恕無法接受不佔床需求，敬請注意。<br />\r\n	二、機票開票，一經開票不可更改姓名亦無退票價值。前列事項恕無法取消，若取消需收全額費用，敬請注意。機位及飯店一經確認即不可取消或變更；此商品若取消，將收取全額機票費用及住宿飯店取消費用<br />\r\n	4、免費行李服務包含：每人20公斤內托運行李及一件10公斤內手提行李；超重部分按航空公司現場規定，費用敬請自理。且機上不含餐飲，需要者請於機上向空服員訂購付費。加購行李：需於開票以前加購確認，於開票時一併作業，請開票前告知業務，開票後或在國外恕無法受理。<br />\r\n	5、【嬰兒、孩童票售價】因航空公司考量飛安因素，即日起限制嬰兒不佔位席次。如欲偕同嬰兒報名，敬請先行向客服人員確認正確報價，但無座位且不提供任何免費托運或手提行李件數服務，嬰兒可免費托運嬰兒推車一台，亦無提供嬰兒搖籃全程需父母抱著，一位成人旅客只能抱一位嬰幼兒坐在膝上。嬰兒或兒童佔位，售價與大人相同。<br />\r\n	6、樂桃航空登機流程參考：<br />\r\n	一、樂桃航空登機手續為自助式辦理，所有旅客皆須持電子機票/條碼利用機場自動報到機辦理登機手續。請務必提前2~3小時到場辦理，以免無法搭機。<br />\r\n	二、台灣桃園機場第一航廈出境大廳1號櫃檯(地面代理為長榮航空)。<br />\r\n	三、沖繩那霸機場於那霸國際機場LCC航廈起降與辦理相關手續。(LCC航廈資訊) 請注意，那霸國際機場LCC航廈僅限專用接駁巴士與部分指定的租車公司接送巴士進入。為避免人多混雜，建議您最遲在航班起飛的70分鐘前搭接駁巴士前往LCC航廈。</p>\r\n	</li>\r\n</ol>\r\n,', '2017-06-07 07:02:53', '2017-06-07 07:02:53', NULL, 3),
(51, 'BackSetTrip', '澎湖搭船三天,2017-03-01,2017-03-31,3,399,3000,1000,2000,499,<p>澎湖搭船三天</p>\r\n', '2017-03-09 08:54:27', '2017-03-09 08:54:27', NULL, 1),
(52, 'BackAbout', '<h3 style="text-align:center"><strong><span style="color:#0033cc"><span style="font-size:20px">關於自由先生</span></span></strong></h3>\r\n\r\n<p style="text-align:center"><span style="font-size:16px">我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！&nbsp;<br />\r\n澎湖旅遊.澎湖自由行.迷你小團.蘭嶼東京.大阪.沖繩.自由行. 國鼎旅行社有限公司&nbsp;<br />\r\n我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！&nbsp;<br />\r\n澎湖旅遊.澎湖自由行.迷你小團.蘭嶼東京.大阪.沖繩.自由行</span></p>\r\n,<h3 style="text-align:center"><strong><span style="color:#0033ff"><span style="font-size:20px">公司概覽</span></span></strong></h3>\r\n\r\n<p style="text-align:center"><span style="font-size:16px">國鼎旅行社有限公司&nbsp;<br />\r\n成立於1986年，屬於甲種旅行社，經營項目包含國際機票、各國簽證、各地自由行、員工旅遊、學生畢旅、會議假期、獎勵旅遊&nbsp;<br />\r\n&nbsp;緣起：國鼎旅行社成立已逾20年，20年來歷經旅客消費習慣的改變，從早年難得出國，一次就要走很多國家，泰港新馬8日遊，瑞士、義大利、法國17日，這種大堆頭的走馬看花行程，演變成單島、單點的定點休閒行程，例如：北海道五日、峇里島五日、長灘島、韓國、濟州、九州、東京、大阪，這種深度旅遊。&nbsp;<br />\r\n近年來，由於網路資訊的發達，商務往返的頻繁，旅遊節目的盛行，旅客對於自由行越來越有自信，越來越喜歡自己安排自己想去的地方！&nbsp;<br />\r\n&nbsp;想要的休閒方式，純粹旅遊或出差兼旅遊的需求越來越多，國鼎旅行社以Mr.Free自由先生的假期名稱，代表著自由自在，無拘無束的概念，加強在旅客自由行的部份，單訂機票、單訂飯店、機票加飯店、機票加飯店加活動、、、任何可能性，讓您自由自在的安排您的國內或國外假期！</span></p>\r\n,國鼎旅行社有限公司,董事長 陳善國,104 台北市中山區林森北路100號9樓之1,1988-02-16,02-25230946   02-25319832,23151866,NT$6,000,000元,乙式,02-25234101 02-25231052,NT$1,500,000,063700,NT$5,000,000,北123,甲種旅行業,http://www.yahoo.com.tw,http://www.yahoo.com.tw', '2017-03-08 07:44:42', '2017-03-08 07:44:42', NULL, 1),
(53, 'BackProgramRegistration', '27,Y,公司相關證照,BackLicense,6', '2017-03-06 08:06:01', '2017-03-06 08:06:01', NULL, 28),
(54, 'BackLicense', '0', '2017-03-08 08:08:03', '2017-03-08 08:08:03', NULL, 1),
(55, 'BackLicense', '0', '2017-03-08 08:08:09', '2017-03-08 08:08:09', NULL, 2),
(56, 'BackLicense', '0', '2017-03-08 08:08:15', '2017-03-08 08:08:15', NULL, 3),
(57, 'BackLicense', '0', '2017-03-08 08:08:21', '2017-03-08 08:08:21', NULL, 4),
(58, 'BackLicense', '0', '2017-03-08 08:08:27', '2017-03-08 08:08:27', NULL, 5),
(59, 'BackLicense', '0', '2017-03-08 08:08:34', '2017-03-08 08:08:34', NULL, 6),
(60, 'BackNews', '2017-04-19,春天賞花團,<p>我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！最新消息內文最新消息內文最新消息內文最新消息內文我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！最新消息內文最新消息內文最新消息內文最新消息內文我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！最新消息內文最新消息內文最新消息內文最新消息內文最新消息內文最新消息內文最新消息內文最新消息內文我們很專心的服務自由行這一個領域！如果您也喜歡以自由行的方式旅遊，希望有機會能為您服務！最新消息內文最新消息內文最新消息內文最新消息內文</p>\r\n', '2017-03-08 08:54:08', '2017-03-08 08:54:08', NULL, 1),
(61, 'BackNews', '2017-03-31,犒賞自己趁現在,<p>犒賞自己趁現在</p>\r\n', '2017-03-08 08:44:51', '2017-03-08 08:44:51', NULL, 2),
(62, 'BackProgramRegistration', '31,Y,行程國別,BackTripClass,21', '2017-03-12 08:46:33', '2017-03-12 08:46:33', NULL, 29),
(63, 'BackTripClass', '國內', '2017-03-09 08:26:16', '2017-03-09 08:26:16', NULL, 1),
(64, 'BackTripClass', '國外', '2017-03-09 08:26:22', '2017-03-09 08:26:22', NULL, 2),
(65, 'BackProgramRegistration', '29,Y,行程地點,BackTripLocation,22', '2018-01-24 18:06:04', '2018-01-24 18:06:04', NULL, 30),
(66, 'BackTripLocation', '1,花蓮', '2017-03-09 08:39:06', '2017-03-09 08:39:06', NULL, 1),
(67, 'BackTripLocation', '1,台東', '2017-03-09 08:52:38', '2017-03-09 08:52:38', NULL, 2),
(68, 'BackTripLocation', '1,台北', '2017-03-09 08:52:44', '2017-03-09 08:52:44', NULL, 3),
(69, 'BackTripLocation', '1,墾丁', '2017-03-09 08:52:59', '2017-03-09 08:52:59', NULL, 4),
(70, 'BackSetTrip', '台北一日遊,2017-03-01,2017-03-31,3,300,3000,1000,1500,500,<p>aaaaa</p>\r\n', '2017-03-09 09:12:32', '2017-03-09 09:12:32', NULL, 2),
(71, 'BackTripLocation', '2,東京', '2017-03-09 09:12:55', '2017-03-09 09:12:55', NULL, 5),
(72, 'BackTripLocation', '2,北海道', '2017-03-09 09:13:05', '2017-03-09 09:13:05', NULL, 6),
(73, 'BackTripLocation', '2,首爾', '2017-03-09 09:13:15', '2017-03-09 09:13:15', NULL, 7),
(74, 'BackSetTrip', '首爾韓劇三日遊,2017-03-01,2017-03-23,1,300,3000,1000,1500,500,<p>AAAA</p>\r\n', '2017-03-09 09:14:22', '2017-03-09 09:14:22', NULL, 3),
(75, 'BackTripLocation', '澎湖', '2018-01-30 08:58:49', '2018-01-30 08:58:49', NULL, 8),
(76, 'BackTripLocation', '蘭嶼', '2017-03-15 06:21:23', '2017-03-15 06:21:23', NULL, 9),
(77, 'BackTripLocation', '日本', '2017-03-31 04:32:39', '2017-03-31 04:32:39', NULL, 10),
(78, 'BackHotel', '8,澎湖安一海景大飯店,<p>澎湖安一海景大飯店</p>\r\n\r\n<p>澎湖安一海景大飯店</p>\r\n\r\n<p>澎湖安一海景大飯店</p>\r\n\r\n<p>澎湖安一海景大飯店</p>\r\n\r\n<p>澎湖安一海景大飯店</p>\r\n', '2018-01-12 08:20:35', '2018-01-12 08:20:35', NULL, 3),
(79, 'BackTrafficServices', '8,機車,500,200,500,0,<p>123</p>\r\n', '2017-05-08 06:44:10', '2017-05-08 06:44:10', NULL, 3),
(80, 'BackProgramRegistration', '30,N,飯店加價,BackHotelReadRead,87', '2017-03-15 08:18:32', '2017-03-15 08:18:32', NULL, 31),
(81, 'BackTripLocation', '沖繩', '2017-03-15 12:49:57', '2017-03-15 12:49:57', NULL, 11),
(82, 'BackFreeExercise', '澎湖自由行3天(航空),3,8,0,0,200,<h2><strong>你可以這樣玩</strong></h2>\r\n\r\n<p>您或許有自己想住的地方，或許有買了住宿券，或許已經訂好房！我們精選您有興趣的活動，全日遊,搭配小活動，剩下的一天還可以好好去環島拍照，活動時間你可以自行決定要在哪一天進行，完全享有自由行的自在。</p>\r\n,<ul>\r\n	<li>澎湖來回機票\r\n	<p>&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>機車或轎車使用48小時</p>\r\n	</li>\r\n	<li>\r\n	<p>東海戀夏全日遊活動卷</p>\r\n	</li>\r\n	<li>\r\n	<p>海洋牧場碳烤鮮蚵吃到飽活動券</p>\r\n	</li>\r\n	<li>\r\n	<p>夜釣小管活動券</p>\r\n	</li>\r\n	<li>\r\n	<p>200萬旅遊責任險附加20萬意外醫療險</p>\r\n	</li>\r\n</ul>\r\n,<ul>\r\n	<li>\r\n	<p>飯店住宿-請自行安排或由我們為您代訂</p>\r\n	</li>\r\n	<li>\r\n	<p>機車或轎車油料費用</p>\r\n	</li>\r\n	<li>\r\n	<p>個人行李超重運費</p>\r\n	</li>\r\n	<li>\r\n	<p>馬公機場來回接送</p>\r\n	</li>\r\n</ul>\r\n,', '2017-06-05 17:31:39', '2017-06-05 17:31:39', NULL, 4),
(83, 'BackHotel', '10,5,利時達酒店,<p>距離新宿車站僅有一站的距離，飯店內設有美容中心，芳香療法1按摩能讓旅客舒解旅途疲勞的壓力。全館皆有WIFI系統連接，也有客房電視也設有VOD付費頻道。櫃台也有販售紙箱、化妝水卸妝油、雨傘等物品。</p>\r\n', '2017-05-11 12:45:50', '2017-05-11 12:45:50', NULL, 4),
(84, 'BackHotel', '11,3,沖繩大飯店,<p>a</p>\r\n', '2017-03-15 13:07:37', '2017-03-15 13:07:37', NULL, 5),
(85, 'BackHotel', '11,3,沖繩大飯店,<p>a</p>\r\n', '2017-03-15 13:07:37', '2017-03-15 13:07:37', NULL, 6),
(86, 'BackProgramRegistration', '27,Y,首頁行銷,BackIndexMarketing,7', '2018-01-10 08:39:09', '2018-01-10 08:39:09', NULL, 32),
(87, 'BackIndexMarketing', '1,1,1,1,1,1,1,1,1', '2018-01-22 07:23:01', '2018-01-22 07:23:01', NULL, 1),
(88, 'BackSetTrip', '東京三日遊,3,2017-03-31,2017-06-30,10,0,8000,3000,4000,1000,<p>東京三日遊</p>\r\n\r\n<p>東京三日遊</p>\r\n\r\n<p>東京三日遊</p>\r\n\r\n<p>東京三日遊</p>\r\n\r\n<p>東京三日遊</p>\r\n,<p>第一天行程</p>\r\n\r\n<p>第一天行程</p>\r\n\r\n<p>第一天行程</p>\r\n\r\n<p>第一天行程</p>\r\n,<p>第二天行程</p>\r\n\r\n<p>第二天行程</p>\r\n\r\n<p>第二天行程</p>\r\n\r\n<p>第二天行程</p>\r\n,<p>第三天行程</p>\r\n\r\n<p>第三天行程</p>\r\n\r\n<p>第三天行程</p>\r\n\r\n<p>第三天行程</p>\r\n\r\n<p>第三天行程</p>\r\n,,', '2017-03-31 04:40:58', '2017-03-31 04:40:58', NULL, 4),
(89, 'BackTransportationCompany', '中華航空', '2017-03-31 06:52:58', '2017-03-31 06:52:58', NULL, 2),
(90, 'BackLocation', '桃園國際機場', '2017-03-31 06:53:56', '2017-03-31 06:53:56', NULL, 3),
(91, 'BackProgramRegistration', '29,N,機票出發日期,BackTicketRead2,101', '2017-03-31 09:37:50', '2017-03-31 09:37:50', NULL, 33),
(92, 'BackIndexBanner', '1', '2017-05-22 09:49:01', '2017-05-22 09:49:01', NULL, 1),
(93, 'BackIndexBanner', '2', '2017-05-22 09:49:07', '2017-05-22 09:49:07', NULL, 2),
(94, 'BackTransportationCompany', '滿天星航運股份有限公司', '2017-04-17 06:22:24', '2017-04-17 06:22:24', NULL, 3),
(95, 'BackLocation', '嘉義布袋漁港', '2017-04-17 06:22:44', '2017-04-17 06:22:44', NULL, 4),
(96, 'BackLocation', '澎湖馬公港', '2017-04-17 06:22:55', '2017-04-17 06:22:55', NULL, 5),
(97, 'BackTicket', '嘉義-澎湖-來回船票,2,1,3,4,5,報到方式：憑身分證件 或健保卡，至船公司或航空公司櫃台直接辦理\r\n1.敬請提前60分鐘抵達辦理登機、登船手續。\r\n2.請記得攜帶身分證件或健保卡，孩童可準備戶籍謄本。', '2017-08-28 08:05:36', '2017-08-28 08:05:36', NULL, 3),
(98, 'BackTransportationCompany', '凱旋船運', '2017-04-24 04:01:17', '2017-04-24 04:01:17', NULL, 4),
(99, 'BackTicket', '8,嘉義-澎湖-來回船票,2,1,4,4,5,', '2018-01-16 06:16:01', '2018-01-16 06:16:01', NULL, 4),
(100, 'BackMenuClass', '訂單管理,fa fa-cog,7', '2017-05-09 10:59:13', '2017-05-09 10:59:13', NULL, 32),
(101, 'BackProgramRegistration', '32,Y,正式訂單管理,BackOrder,23', '2018-01-10 08:42:58', '2018-01-10 08:42:58', NULL, 34),
(102, 'BackOrder', '0,1,2017-05-11,2017-05-13,hazho,0987654321,18125,1,1,1,0', '2017-05-09 11:37:42', '2017-05-09 11:37:42', NULL, 61),
(103, 'BackOrder', '1,1,2017-04-20,0000-00-00,周杰倫,0987654321,3100,1,0,1,0', '2017-05-09 11:37:52', '2017-05-09 11:37:52', NULL, 86),
(104, 'BackProgramRegistration', '32,N,訂單機票,BackOrderRead3,24', '2017-05-09 11:43:21', '2017-05-09 11:43:21', NULL, 35),
(105, 'BackProgramRegistration', '32,N,訂單飯店,BackOrderRead4,25', '2017-05-09 11:43:48', '2017-05-09 11:43:48', NULL, 36),
(106, 'BackProgramRegistration', '32,N,訂單交通活動,BackOrderRead5,25', '2017-05-09 11:44:20', '2017-05-09 11:44:20', NULL, 37),
(107, 'BackProgramRegistration', '32,N,訂單加購項目,BackOrderRead6,26', '2017-05-09 11:44:49', '2017-05-09 11:44:49', NULL, 38),
(108, 'BackOrderRead3', '3', '2017-05-09 12:41:53', '2017-05-09 12:41:53', NULL, 5),
(109, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:37:35', '2017-06-09 07:37:35', NULL, 25),
(110, 'BackOrderRead5', '2', '2017-05-09 13:53:49', '2017-05-09 13:53:49', NULL, 25),
(111, 'BackOrderRead5', '1,1,0', '2017-05-10 08:06:53', '2017-05-10 08:06:53', NULL, 33),
(112, 'BackOrderRead5', '1', '2017-05-10 06:05:48', '2017-05-10 06:05:48', NULL, 34),
(113, 'BackProgramRegistration', '32,Y,需求單管理,BackQUO,26', '2018-01-10 08:42:25', '2018-01-10 08:42:25', NULL, 39),
(114, 'BackProgramRegistration', '32,N,行程成員,BackOrderDetail,27', '2017-05-10 06:42:35', '2017-05-10 06:42:35', NULL, 40),
(115, 'BackProgramRegistration', '32,N,加購項目訂單,BackOrderActivityDetail,28', '2017-05-10 06:49:05', '2017-05-10 06:49:05', NULL, 41),
(116, 'BackProgramRegistration', '32,N,客服紀錄,customer_service,29', '2017-05-10 07:05:11', '2017-05-10 07:05:11', NULL, 42),
(117, 'BackProgramRegistration', '31,N,訂單交通服務,BackOrderTrafficDetail,30', '2017-05-10 07:08:07', '2017-05-10 07:08:07', NULL, 43),
(118, 'BackProgramRegistration', '32,N,訂單飯店項目,BackOrderHotelDetail,31', '2017-05-10 07:08:42', '2017-05-10 07:08:42', NULL, 44),
(119, 'BackOrderRead6', '1', '2017-05-10 07:52:30', '2017-05-10 07:52:30', NULL, 41),
(120, 'BackOrderRead6', '1', '2017-05-10 07:52:56', '2017-05-10 07:52:56', NULL, 42),
(121, 'BackOrderRead6', '1,1,0', '2017-05-10 07:56:57', '2017-05-10 07:56:57', NULL, 43),
(122, 'BackOrderRead6', '1,1,0', '2017-05-10 07:59:29', '2017-05-10 07:59:29', NULL, 44),
(123, 'BackProgramRegistration', '32,N,訂單詢問,BackOrderCS,32', '2017-06-05 11:40:30', '2017-06-05 11:40:30', NULL, 45),
(124, 'BackOrderCS', '61,aaa,aaa', '2017-05-10 09:44:17', '2017-05-10 09:44:17', NULL, 19),
(125, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:40:39', '2017-06-09 07:40:39', NULL, 29),
(126, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:39:41', '2017-06-09 07:39:41', NULL, 26),
(127, 'BackOrderCS', '91,5256,fuck', '2017-05-10 11:23:35', '2017-05-10 11:23:35', NULL, 20),
(128, 'BackSetTrip', '日本自由行,5,2017-05-09,2017-05-13,10,,,,,,<p>123</p>\r\n,<p>123</p>\r\n,,<p>123</p>\r\n,<p>123</p>\r\n,<p>123</p>\r\n,<p>213</p>\r\n,<p>123</p>\r\n,<p>123</p>\r\n', '2017-05-11 03:21:05', '2017-05-11 03:21:05', NULL, 5),
(129, 'BackOrderCS', '99,aaa,asd', '2017-05-11 06:04:20', '2017-05-11 06:04:20', NULL, 22),
(130, 'BackOrderRead4', '190,2,1,1,1', '2017-06-09 09:26:53', '2017-06-09 09:26:53', NULL, 36),
(131, 'BackOrderRead4', '190,2,1,1,1', '2017-06-09 09:26:56', '2017-06-09 09:26:56', NULL, 37),
(132, 'BackOrderRead4', '190,2,1,1,1', '2017-06-09 09:26:59', '2017-06-09 09:26:59', NULL, 38),
(133, 'BackOrderRead4', ',3,3,0', '2017-05-11 09:51:56', '2017-05-11 09:51:56', NULL, 39),
(134, 'BackOrderRead4', '103,2,1,1', '2017-05-11 09:54:39', '2017-05-11 09:54:39', NULL, 40),
(135, 'BackOrderRead4', '103,3,1,1', '2017-05-11 09:57:29', '2017-05-11 09:57:29', NULL, 41),
(136, 'BackOrderRead4', '102,2,1,1', '2017-05-11 09:58:38', '2017-05-11 09:58:38', NULL, 42),
(137, 'BackLocation', '蘭嶼機場', '2017-05-11 12:50:26', '2017-05-11 12:50:26', NULL, 6),
(138, 'BackLocation', '台東機場', '2017-05-11 12:50:33', '2017-05-11 12:50:33', NULL, 7),
(139, 'BackTicket', '台東-蘭嶼來回機票,1,1,1,7,6,', '2017-08-28 08:05:31', '2017-08-28 08:05:31', NULL, 5),
(140, 'BackProgramRegistration', '29,Y,票位類型,BackTicketType,14', '2017-05-22 05:44:00', '2017-05-22 05:44:00', NULL, 46),
(141, 'BackTicketType', '機票', '2017-05-22 05:51:26', '2017-05-22 05:51:26', NULL, 1),
(142, 'BackTicketType', '船票', '2017-05-22 05:52:26', '2017-05-22 05:52:26', NULL, 2),
(143, 'BackProgramRegistration', '31,Y,Menu行程列表,BackMenuTripList,18', '2017-05-22 08:34:51', '2017-05-22 08:34:51', NULL, 47),
(144, 'BackMenuTripList', '澎湖自由行', '2017-05-22 08:44:46', '2017-05-22 08:44:46', NULL, 1),
(145, 'BackMenuClass', '商家管理,fa fa-flag-o,8', '2017-05-23 06:38:06', '2017-05-23 06:38:06', NULL, 33),
(146, 'BackProgramRegistration', '29,Y,供應商管理,BackSupplier,33', '2018-01-24 11:19:32', '2018-01-24 11:19:32', NULL, 48),
(147, 'BackProgramRegistration', '29,Y,廠商管理,BackManufacturers,34', '2018-01-24 11:21:10', '2018-01-24 11:21:10', NULL, 49),
(148, 'BackManufacturers', '海之星,a,a,a,<p>a</p>\r\n', '2017-06-05 11:51:00', '2017-06-05 11:51:00', NULL, 1),
(149, 'BackOrderDetail', 'hanzo,hanzi,o100221111,0,2017-05-16', '2017-05-31 13:39:25', '2017-05-31 13:39:25', NULL, 129),
(150, 'BackProgramRegistration', '32,N,行前說明,BackOrientation,0', '2017-06-01 05:24:17', '2017-06-01 05:24:17', NULL, 50),
(151, 'BackSupplier', '供應商黃先生,ab7895123@hotmail.com,新竹市西大路500號1F,0978945612,<p>23131231</p>\r\n', '2017-06-05 11:50:41', '2017-06-05 11:50:41', NULL, 1),
(152, 'BackOrderRead6', '123,1,1,0,2017-05-14,,1,1', '2017-06-03 05:34:24', '2017-06-03 05:34:24', NULL, 65),
(153, 'BackOrderRead4', '123,1,2,1,1,1', '2017-06-03 06:49:10', '2017-06-03 06:49:10', NULL, 81),
(154, 'BackOrderRead4', '123,1,2,1,1,1', '2017-06-03 06:49:15', '2017-06-03 06:49:15', NULL, 82),
(155, 'BackOrderRead5', '1,2017-05-15,12:30pm,1,0,1,1', '2017-06-03 08:19:56', '2017-06-03 08:19:56', NULL, 75),
(156, 'BackOrderRead5', '2,2017-05-14,6:00am,1,0,1,1', '2017-06-03 08:20:48', '2017-06-03 08:20:48', NULL, 76),
(157, 'BackOrderRead5', '3,2017-05-14,5:00pm,1,0,1,1', '2017-06-03 08:20:55', '2017-06-03 08:20:55', NULL, 77),
(158, 'BackProgramRegistration', '33,N,供應商報表,BackSupplierRead,0', '2017-06-05 04:46:26', '2017-06-05 04:46:26', NULL, 51),
(159, 'BackActivity', '8,2,南海-七美島.望安島跳島全日遊-不玩水,500,20', '2018-01-16 06:04:30', '2018-01-16 06:04:30', NULL, 2),
(160, 'BackOrderRead6', '119,2,1,0,2017-06-01,3:00am,1,1', '2017-06-05 11:59:48', '2017-06-05 11:59:48', NULL, 68),
(161, 'BackOrderRead6', '128,1,10,10,2017-06-16,1:00am,1,1', '2017-06-05 12:53:54', '2017-06-05 12:53:54', NULL, 69),
(162, 'BackTicket', '8,嘉義-澎湖-來回船票,2,1,5,4,5,', '2018-01-16 06:15:51', '2018-01-16 06:15:51', NULL, 6),
(163, 'BackTransportationCompany', '太吉之星,09888', '2017-06-05 17:12:09', '2017-06-05 17:12:09', NULL, 5),
(164, 'BackActivity', '8,1,金沙灘水上活動不限次數,50,100', '2018-01-16 06:04:24', '2018-01-16 06:04:24', NULL, 3),
(165, 'BackActivity', '8,東海水上活動全日遊,30,200,<p>xxxx</p>\r\n', '2018-01-30 07:56:10', '2018-01-30 07:56:10', NULL, 4),
(166, 'BackLocation', '澎湖機場', '2017-06-05 17:32:28', '2017-06-05 17:32:28', NULL, 8),
(167, 'BackLocation', '金門機場', '2017-06-05 17:33:12', '2017-06-05 17:33:12', NULL, 9),
(168, 'BackFreeExercise', '澎湖優惠機票,5,0,0,8,0,0,0,<h2><strong>早去晚回-你或許不知道的困擾</strong></h2>\r\n\r\n<p>大家總覺得出門玩一定要早去晚回，天沒亮就出發，最後一刻到家最划算，總是指定要早上8點以前出發，晚上8點以後回來！但是早去晚回，其實辛苦又痛苦！</p>\r\n\r\n<h2><strong>晚去早回-沒有您想的那麼不堪</strong></h2>\r\n\r\n<p>早去晚回班機，既然都被團體包走，被不知道誰留走了，自己怎麼訂都只有晚去早回的班機，那當然要選擇最優惠的價錢買機票！省下的票價差，拿來多住一晚飯店都划算，足足節省至少30%</p>\r\n\r\n<h2><strong>你有更好的選擇</strong></h2>\r\n\r\n<p>遠東航空公司限定班次專案，讓你一樣有充足的時間玩，但是票價優惠更有感！都是MD機型的160~165人座的噴射客機，而不是螺旋槳的小飛機。</p>\r\n,,,<ul>\r\n	<li>專案機票訂位完成需於開票期限內完成付款開票，開票需付清全額票款。</li>\r\n	<li>機票效期：限當日當班次有效(不得更換班次)逾期未使用不可退票，不可補票差至全額票搭乘。</li>\r\n	<li>訂位開票後，出發前30天前取消，收取退票手續費10%。</li>\r\n	<li>訂位開票後，出發前7~30天內取消，收取退票手續費20%。</li>\r\n	<li>訂位開票後，出發前2~7天內取消，收取退票手續費1000元。</li>\r\n	<li>如遇天候不可抗力影響停飛或航班取消，可全額退費。</li>\r\n	<li>登機報到：憑報到通知單至機場遠航櫃檯以身分證件辦理登機手續。</li>\r\n</ul>\r\n,,', '2017-10-12 03:21:44', '2017-10-12 03:21:44', NULL, 5),
(169, 'BackTransportationCompany', '樂桃航空,0800', '2017-06-05 17:42:45', '2017-06-05 17:42:45', NULL, 6),
(170, 'BackTicket', '8,樂桃航空-早去晚回航班,1,2,6,3,1,樂桃航空之航班在所有航程全面禁止旅客攜帶Samsung Note 7手機登機，包括隨身行李、托運行李或貨運寄送皆不可攜帶，如有查獲違規攜帶之事宜，將拒絕該名旅客登機，敬請注意。', '2018-01-16 06:14:36', '2018-01-16 06:14:36', NULL, 7),
(171, 'BackOrderCS', '131,aa,bbb', '2017-06-05 18:31:58', '2017-06-05 18:31:58', NULL, 1),
(172, 'BackTrafficServices', '8,重型機車,1200,800,600,300,<p>test</p>\r\n', '2018-01-30 08:05:17', '2018-01-30 08:05:17', NULL, 4),
(173, 'BackOrderRead5', '4,2017-06-20,12:00am,10,5,1,1', '2017-06-05 18:44:02', '2017-06-05 18:44:02', NULL, 3),
(174, 'BackOrderRead5', '105,3,2018-11-29 00:00,1,0,1,1', '2018-01-24 14:07:05', '2018-01-24 14:07:05', NULL, 2),
(175, 'BackOrderRead5', '131,4,2017-06-20,12:00am,10,0,1,1', '2017-06-05 18:46:47', '2017-06-05 18:46:47', NULL, 4),
(176, 'BackOrderRead6', '1,1,0,2018-01-24 10:30:00,1,1', '2018-01-24 12:53:49', '2018-01-24 12:53:49', NULL, 2),
(177, 'BackOrderRead6', '131,2,10,5,2017-06-20,1:30am,1,1', '2017-06-05 18:48:40', '2017-06-05 18:48:40', NULL, 3),
(178, 'BackOrderRead6', '1,1,0,1900-12-13 09:00,1,1', '2018-01-24 14:04:42', '2018-01-24 14:04:42', NULL, 4),
(179, 'BackOrderRead6', '1,1,0,2018-01-10 06:00,1,1', '2018-01-24 14:04:51', '2018-01-24 14:04:51', NULL, 5),
(180, 'BackOrderRead4', '131,2,1,1,1,1', '2017-06-05 18:52:23', '2017-06-05 18:52:23', NULL, 3),
(181, 'BackOrderRead6', '6,1,3,0,2017-10-01,,1,1', '2017-09-28 05:25:21', '2017-09-28 05:25:21', NULL, 6),
(182, 'BackOrderRead6', '133,2,1,0,2017-06-20,12:00am,1,1', '2017-06-06 02:57:38', '2017-06-06 02:57:38', NULL, 7),
(183, 'BackOrderRead3', '223', '2017-06-09 05:55:48', '2017-06-09 05:55:48', NULL, 50),
(184, 'BackOrderRead3', '242', '2017-06-09 06:07:05', '2017-06-09 06:07:05', NULL, 52),
(185, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:33:21', '2017-06-09 07:33:21', NULL, 17),
(186, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:35:01', '2017-06-09 07:35:01', NULL, 18),
(187, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:35:06', '2017-06-09 07:35:06', NULL, 19),
(188, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:35:25', '2017-06-09 07:35:25', NULL, 20),
(189, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:35:35', '2017-06-09 07:35:35', NULL, 21),
(190, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:36:19', '2017-06-09 07:36:19', NULL, 22),
(191, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:36:35', '2017-06-09 07:36:35', NULL, 23),
(192, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:37:25', '2017-06-09 07:37:25', NULL, 24),
(193, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:39:51', '2017-06-09 07:39:51', NULL, 27),
(194, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:40:34', '2017-06-09 07:40:34', NULL, 28),
(195, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:43:26', '2017-06-09 07:43:26', NULL, 30),
(196, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:43:41', '2017-06-09 07:43:41', NULL, 31),
(197, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:43:58', '2017-06-09 07:43:58', NULL, 32),
(198, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 07:44:16', '2017-06-09 07:44:16', NULL, 33),
(199, 'BackOrderRead3', '2', '2017-06-09 08:01:24', '2017-06-09 08:01:24', NULL, 68),
(200, 'BackOrderRead4', '162,3,3,1,1', '2017-06-09 08:01:38', '2017-06-09 08:01:38', NULL, 34),
(201, 'BackOrderRead4', '162,2,1,1,1', '2017-06-09 08:01:44', '2017-06-09 08:01:44', NULL, 35),
(202, 'BackUserManagement', '周杰倫,ab7895123,123,123,ab7895123@hotmail.com,3', '2017-06-09 08:16:27', '2017-06-09 08:16:27', NULL, 6),
(203, 'BackOrderRead3', '1,1000', '2017-06-09 08:52:32', '2017-06-09 08:52:32', NULL, 41),
(204, 'BackProgramRegistration', '32,Y,行前通知單範本設定,BackOrientationConfig,100', '2017-06-09 08:59:09', '2017-06-09 08:59:09', NULL, 52),
(205, 'BackOrderRead3', '232', '2017-06-09 09:27:13', '2017-06-09 09:27:13', NULL, 124),
(206, 'BackOrderRead5', '190,1,2017-06-13,12:00am,1,0,1,1', '2017-06-09 09:27:29', '2017-06-09 09:27:29', NULL, 39),
(207, 'BackOrientationConfig', '兩天一夜,123,123,123,123,123', '2017-06-09 09:51:18', '2017-06-09 09:51:18', NULL, 1),
(208, 'BackReturnOrder', '客戶修改,0', '2017-08-25 09:17:42', '2017-08-25 09:17:42', NULL, 190),
(209, 'BackQUO', '1,1,2017-06-13,2017-06-15,周杰倫,0987654321,2017-08-31 01:45:00', '2017-08-28 04:11:46', '2017-08-28 04:11:46', NULL, 193),
(210, 'BackQUO', '0,1,2017-06-13,2017-06-15,HANZO,0987654321,2017-08-29 09:25:00', '2017-08-28 04:03:41', '2017-08-28 04:03:41', NULL, 192),
(211, 'BackProgramRegistration', '29,Y,票位種類,BackTicketClass,20', '2017-08-28 07:48:41', '2017-08-28 07:48:41', NULL, 53),
(212, 'BackTicketClass', '國內', '2017-08-28 08:00:02', '2017-08-28 08:00:02', NULL, 1),
(213, 'BackTicketClass', '國外', '2017-08-28 08:00:09', '2017-08-28 08:00:09', NULL, 2),
(214, 'BackOrderCS', '194,123,321', '2017-09-06 02:56:27', '2017-09-06 02:56:27', NULL, 32),
(215, 'BackProgramRegistration', '31,Y,套裝行程房型設定,BackRoomType,10', '2017-09-18 09:47:12', '2017-09-18 09:47:12', NULL, 54),
(216, 'BackRoomType', '單人房,200,1', '2017-09-18 09:58:13', '2017-09-18 09:58:13', NULL, 1),
(217, 'BackRoomType', '雙人房,0,1', '2017-09-18 09:58:04', '2017-09-18 09:58:04', NULL, 2),
(218, 'BackProgramRegistration', '31,Y,接駁車,BackShuttleBus,30', '2017-09-21 02:56:56', '2017-09-21 02:56:56', NULL, 55),
(219, 'BackShuttleBus', '台北轉運站,700,1100', '2017-09-21 03:15:01', '2017-09-21 03:15:01', NULL, 1),
(220, 'BackShuttleBus', '三重重陽站,700,1100', '2017-09-21 03:15:36', '2017-09-21 03:15:36', NULL, 2),
(221, 'BackShuttleBus', '桃園南崁站,700,1100', '2017-09-21 03:15:46', '2017-09-21 03:15:46', NULL, 3),
(222, 'BackShuttleBus', '台中中港轉運站,400,600', '2017-09-21 03:16:11', '2017-09-21 03:16:11', NULL, 4),
(223, 'BackShuttleBus', '彰化交流道,400,600', '2017-09-21 03:16:21', '2017-09-21 03:16:21', NULL, 5),
(224, 'BackShuttleBus', '員林交流道,400,600', '2017-09-21 03:16:31', '2017-09-21 03:16:31', NULL, 6),
(225, 'BackShuttleBus', '台中車站,400,600', '2017-09-21 03:16:55', '2017-09-21 03:16:55', NULL, 7),
(226, 'BackFreeExercise', ',1,1,8,0,0,0,,,,,', '2017-09-21 03:25:34', '2017-09-21 03:25:34', NULL, 6),
(227, 'BackProgramRegistration', '32,Y,訂單備註設定,BackOrderRemark,20', '2017-09-22 17:25:27', '2017-09-22 17:25:27', NULL, 56),
(228, 'BackOrderRead6', '216,1,1,0,2017-10-01,12:00am,1,1', '2017-09-22 17:49:27', '2017-09-22 17:49:27', NULL, 96),
(229, 'BackOrderRead4', '12,2,1,2,1,1', '2017-10-16 03:54:41', '2017-10-16 03:54:41', NULL, 10),
(230, 'BackSetTrip', ',test,5,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>tset</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n,<p>test</p>\r\n', '2018-01-24 16:05:08', '2018-01-24 16:05:08', NULL, 8),
(231, 'BackMenuClass', '旅客名單,fa fa-user-md,9', '2018-01-10 03:02:22', '2018-01-10 03:02:22', NULL, 34),
(232, 'BackProgramRegistration', '31,N,套裝行程旅行團,BackSetTripTour,0', '2018-01-10 06:06:21', '2018-01-10 06:06:21', NULL, 57),
(233, 'BackSetTripTour', '8,東京親子遊歡樂５日～歡暢迪士尼．八景島海洋樂園．螃蟹吃到飽 1/30出發,2018-01-30,8,30,386,2,30000,30000', '2018-01-10 08:24:30', '2018-01-10 08:24:30', NULL, 1),
(234, 'BackSetTripTour', '8,東京親子遊歡樂５日～歡暢迪士尼．八景島海洋樂園．螃蟹吃到飽 1/30出發,2018-01-30,8,30,386,2,30000,30000', '2018-01-10 08:24:59', '2018-01-10 08:24:59', NULL, 2),
(235, 'BackProgramRegistration', '34,Y,旅客名單,BackMember,4', '2018-01-10 09:32:07', '2018-01-10 09:32:07', NULL, 58),
(236, 'BackMember', 'Joe Liu,facebook_1630396273640762,,,joaner99@gmail.com', '2018-01-10 08:59:14', '2018-01-10 08:59:14', NULL, 18),
(237, 'BackSetTripTour', ',春節前最後一檔,2018-01-17,3,30,379,3,3000,1500', '2018-01-11 09:14:28', '2018-01-11 09:14:28', NULL, 3),
(238, 'BackSetTripTour', '1,,冬天的澎湖,2018-01-18,3,30,1,3,3000,500', '2018-01-11 09:18:46', '2018-01-11 09:18:46', NULL, 4),
(239, 'BackUserManagement', 'xxx,ab7895123@hotmail.com,123456,123456,ab7895123@hotmail.com,5', '2018-01-15 04:56:25', '2018-01-15 04:56:25', NULL, 21),
(240, 'BackOrderDetail', '97,24', '2018-01-18 09:54:57', '2018-01-18 09:54:57', NULL, 3),
(241, 'BackOrderDetail', '97,24', '2018-01-18 09:55:42', '2018-01-18 09:55:42', NULL, 4),
(242, 'BackOrderDetail', '97,24', '2018-01-19 05:59:59', '2018-01-19 05:59:59', NULL, 5),
(243, 'BackOrderDetail', '97,24', '2018-01-19 06:01:07', '2018-01-19 06:01:07', NULL, 6),
(244, 'BackOrderDetail', '97,24', '2018-01-19 06:02:09', '2018-01-19 06:02:09', NULL, 7),
(245, 'BackOrderDetail', '97,24', '2018-01-19 06:02:15', '2018-01-19 06:02:15', NULL, 8),
(246, 'BackOrderDetail', '97,14', '2018-01-19 06:03:21', '2018-01-19 06:03:21', NULL, 9),
(247, 'BackOrderDetail', '1,24', '2018-01-19 07:04:26', '2018-01-19 07:04:26', NULL, 10),
(248, 'BackOrderDetail', '97,14', '2018-01-19 07:24:48', '2018-01-19 07:24:48', NULL, 11),
(249, 'BackOrderDetail', '97,25', '2018-01-19 07:40:14', '2018-01-19 07:40:14', NULL, 12),
(250, 'BackOrderDetail', '97,25', '2018-01-19 07:40:23', '2018-01-19 07:40:23', NULL, 13),
(251, 'BackOrderDetail', '97,27', '2018-01-19 07:49:14', '2018-01-19 07:49:14', NULL, 14),
(252, 'BackMember', 'hen,ab7895123@hotmail.com,,0000-00-00,,,ab7895123@hotmail.com', '2018-01-19 07:50:09', '2018-01-19 07:50:09', NULL, 21),
(253, 'BackOrderCS', '104,xxxx,xxxxxx', '2018-01-22 06:45:09', '2018-01-22 06:45:09', NULL, 57),
(254, 'BackOrderCS', '104,xxxx,cccc', '2018-01-22 06:47:06', '2018-01-22 06:47:06', NULL, 58),
(255, 'BackOrderCS', '104,xxxx,已經幫您處裡', '2018-01-22 06:49:16', '2018-01-22 06:49:16', NULL, 59),
(256, 'BackOrderRead4', '105,1,1,1', '2018-01-24 13:54:08', '2018-01-24 13:54:08', NULL, 95),
(257, 'BackQUO', '1,,,0000-00-00,0000-00-00,,,0000-00-00 00:00:00,ATM轉帳,全部旅費,5000,123456,2008-11-30 00:00,1', '2018-01-24 14:09:42', '2018-01-24 14:09:42', NULL, 105),
(258, 'BackOrderCS', '116,xxx,xxx', '2018-01-24 14:31:46', '2018-01-24 14:31:46', NULL, 75),
(259, 'BackOrderCS', '116,xxx,xxx', '2018-01-24 14:33:47', '2018-01-24 14:33:47', NULL, 77),
(260, 'BackOrderDetail', '116,28', '2018-01-24 14:36:38', '2018-01-24 14:36:38', NULL, 38),
(261, 'BackOrderRead6', '1,1,0,1900-12-01 00:00,1,1', '2018-01-24 14:40:44', '2018-01-24 14:40:44', NULL, 20),
(262, 'BackOrderRead6', '1,1,0,2018-01-26 15:30,1,1', '2018-01-24 14:43:23', '2018-01-24 14:43:23', NULL, 21),
(263, 'BackOrderRead4', '116,3,1,1', '2018-01-24 14:44:26', '2018-01-24 14:44:26', NULL, 106),
(264, 'BackOrderRead5', '116,1,2018-01-09 10:30,1,0,1,1', '2018-01-24 14:45:07', '2018-01-24 14:45:07', NULL, 17),
(265, 'BackQUO', '0,1,3,0000-00-00,0000-00-00,,,0000-00-00 00:00:00,ATM轉帳,全部旅費,50000,123456,2018-11-30 00:00,1', '2018-01-24 14:45:45', '2018-01-24 14:45:45', NULL, 116),
(266, 'BackOrderRead5', '116,3,2018-01-16 09:15,1,0,1,1', '2018-01-24 14:48:49', '2018-01-24 14:48:49', NULL, 18),
(267, 'BackReturnOrder', 'xxxxx,0', '2018-01-24 14:49:34', '2018-01-24 14:49:34', NULL, 115),
(268, 'BackReturnOrder', 'xxxx,0', '2018-01-24 14:50:10', '2018-01-24 14:50:10', NULL, 116),
(269, 'BackRole', '超級管理員,<p>Full access to create, edit, and update companies, and orders.</p>\r\n', '2018-01-24 15:47:25', '2018-01-24 15:47:25', NULL, 2),
(270, 'BackRole', '一般管理員,<p>Ability to create new companies and orders, or edit and update any existing ones.</p>\r\n', '2018-01-24 15:47:34', '2018-01-24 15:47:34', NULL, 3),
(271, 'BackRole', '一般會員,<p>A standard user that can have a licence assigned to them. No administrative features.</p>\r\n', '2018-01-24 15:50:56', '2018-01-24 15:50:56', NULL, 5),
(272, 'BackOrderRead4', '117,1,1,1', '2018-01-24 16:45:25', '2018-01-24 16:45:25', NULL, 107),
(273, 'BackMember', 'c,a22222222,0000-00-00,joaner99@gmail.com,xxxx', '2018-02-01 04:32:53', '2018-02-01 04:32:53', NULL, 14),
(274, 'BackMember', ',,,', '2018-01-24 18:45:51', '2018-01-24 18:45:51', NULL, 30),
(275, 'BackOrderRead5', '117,1,0000-00-00 00:00:00,1,0,1,1', '2018-01-30 08:54:35', '2018-01-30 08:54:35', NULL, 19),
(276, 'BackProgramRegistration', '27,Y,單購項目代表圖片,BackItemImg,40', '2018-02-07 04:11:51', '2018-02-07 04:11:51', NULL, 59),
(277, 'BackItemImg', '0', '2018-02-07 07:30:22', '2018-02-07 07:30:22', NULL, 1),
(278, 'BackItemImg', '0', '2018-02-07 06:53:06', '2018-02-07 06:53:06', NULL, 2),
(279, 'BackItemImg', '0', '2018-02-07 07:30:33', '2018-02-07 07:30:33', NULL, 3),
(280, 'BackItemImg', '0', '2018-02-07 07:30:46', '2018-02-07 07:30:46', NULL, 4),
(281, 'BackProgramRegistration', '32,N,訂單圖片,BackOrderImg,10', '2018-02-08 03:29:59', '2018-02-08 03:29:59', NULL, 60),
(282, 'BackOrderImg', '105', '2018-02-08 03:59:01', '2018-02-08 03:59:01', NULL, 9);

-- --------------------------------------------------------

--
-- 資料表結構 `set_trip`
--

CREATE TABLE `set_trip` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `traffic_services` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ticket` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `insurance_price` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `trip_location_id` int(11) NOT NULL,
  `trip_class_id` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `day1` text COLLATE utf8_unicode_ci NOT NULL,
  `day2` text COLLATE utf8_unicode_ci NOT NULL,
  `day3` text COLLATE utf8_unicode_ci NOT NULL,
  `day4` text COLLATE utf8_unicode_ci NOT NULL,
  `day5` text COLLATE utf8_unicode_ci NOT NULL,
  `day6` text COLLATE utf8_unicode_ci NOT NULL,
  `day7` text COLLATE utf8_unicode_ci NOT NULL,
  `day8` text COLLATE utf8_unicode_ci NOT NULL,
  `day1_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day2_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day3_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day4_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day5_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day6_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day7_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day8_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `features` text COLLATE utf8_unicode_ci NOT NULL,
  `features_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contain` text COLLATE utf8_unicode_ci NOT NULL,
  `traffic_information` text COLLATE utf8_unicode_ci NOT NULL,
  `ext` text COLLATE utf8_unicode_ci NOT NULL,
  `show_shuttle_bus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_list` text COLLATE utf8_unicode_ci NOT NULL,
  `adult_price` int(11) NOT NULL,
  `child_price` int(11) NOT NULL,
  `oldman_price` int(11) NOT NULL,
  `other_price` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `set_trip`
--

INSERT INTO `set_trip` (`id`, `name`, `detail`, `traffic_services`, `hotel`, `activity`, `ticket`, `price`, `insurance_price`, `file`, `description`, `start_date`, `end_date`, `trip_location_id`, `trip_class_id`, `days`, `day1`, `day2`, `day3`, `day4`, `day5`, `day6`, `day7`, `day8`, `day1_file`, `day2_file`, `day3_file`, `day4_file`, `day5_file`, `day6_file`, `day7_file`, `day8_file`, `features`, `features_file`, `contain`, `traffic_information`, `ext`, `show_shuttle_bus`, `price_list`, `adult_price`, `child_price`, `oldman_price`, `other_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '澎湖搭船三天', '', '', '', '', '', 3000, 399, '18012215312080029.jpg', '<p>澎湖來回船票</p>\r\n\r\n<p>機車或轎車使用48小時</p>\r\n\r\n<p>吉貝島水上活動不限次數一日券</p>\r\n\r\n<p>吉貝島來回船票含島上機車2人一部</p>\r\n\r\n<p>海洋牧場碳烤鮮蚵吃到飽活動券</p>\r\n\r\n<p>夜釣小管活動券</p>\r\n\r\n<p>200萬旅遊責任險附加20萬意外醫療險</p>\r\n\r\n<h3>費用未包含</h3>\r\n\r\n<p>飯店住宿-請自行安排或由我們為您代訂</p>\r\n\r\n<p>機車或轎車油料費用</p>\r\n\r\n<p>個人行李超重運費</p>\r\n\r\n<p>往返嘉義布袋港碼頭交通</p>\r\n', '2017-03-01', '2017-03-31', 8, 1, 3, '<p>嘉義布袋港搭船前往澎湖馬公，抵達後步行2分鐘，前往自由先生集合地點辦理報到領取機車以及相關活動票券後，隨即展開此次精彩的澎湖自由行之旅</p>\r\n\r\n<p>【湖西奎壁山之旅】行程約2～3小時</p>\r\n\r\n<p>免稅店奎壁山體驗摩西分紅海奇觀，隨著海水退去，可踏浪至海中間的赤嶼，是您不能錯過的天然景觀！</p>\r\n\r\n<p>【夜釣小管】行程約2.5~3小時</p>\r\n\r\n<p>乘著海風，在銀色的月光下，搭乘打著集魚燈的專業夜釣船，出海體驗漁夫夜間辛勤工作及夜釣小管的過程，人手一竿，實際感受夜釣小管的樂趣，現釣現料理，親嚐尚青、尚甘甜的海中極品－小管生魚片。並且可以品嘗澎湖在地人的小管麵線（是手工的哦）。（此活動若遇花火節施放的夜晚，會優先安排以海上賞花火釣小管為主，已報名先後安排。）</p>\r\n', '<p>【吉貝沙灘水上活動】行程約4小時</p>\r\n\r\n<p>包含水上機車、香蕉船、飛行沙發、鴛鴦飛艇、拖曳甜甜圈、海上飛碟、魔法飛毯、海上曼波、浮潛等不限次數(含來回船票+島上機車2人一部)</p>\r\n\r\n<p>【北環島之旅】行程約4小時</p>\r\n\r\n<p>中屯風車園區、跨海大橋、許家村壁畫、通樑古榕樹、易家仙人掌冰、鯨魚洞、大義宮、二崁聚落、大義宮、大菓葉石壁、西嶼落霞</p>\r\n', '<p>【海洋牧場-碳烤生蠔無限量吃到飽】行程約3小時</p>\r\n\r\n<p>（1）海上休閒平台：「牡蠣」無限碳烤，讓您吃到飽、古早味之「海鮮粥」無限供應</p>\r\n\r\n<p>（2）海洋牧場：鬥海鱺（海梨仔）、溜花枝（海中變色龍）、戲弄黃臘鰺（號稱食人魚）</p>\r\n\r\n<p>（3）親子生態觸摸池：多樣化之海中生物讓您直呼驚奇，享受與魚兒親近的樂趣，來一趟意義非凡之「親子生態教育」之樂哦。</p>\r\n\r\n<p>【市區古蹟觀光】行程約2小時</p>\r\n\r\n<p>中央老街、天后宮、四眼井、篤行眷村園區、張雨生故事館、外婆澎湖灣、潘安邦舊居</p>\r\n', '', '', '', '', '', '', '', '', '17031216255513411.jpg', '17031216255595336.jpg', '', '', '', '<p>行程特色</p>\r\n', '', '<p>船票</p>\r\n\r\n<p>水上活動</p>\r\n\r\n<p>住宿</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>交通資訊</p>\n\n<p>交通資訊</p>\n\n<p>交通資訊</p>\n', '', '1', '', 0, 0, 0, 0, '2017-03-02 16:56:40', '2018-01-22 15:31:20', NULL),
(5, '東京親子遊歡樂５日～歡暢迪士尼．八景島海洋樂園．螃蟹吃到飽', '<p>123</p>\r\n', '', '', '', '', 10000, 100, '17051111215462093.jpg', '<p>１.若遇當地特殊情況或不可抗力之因素，如塞車、觀光景點休假等因素，為讓行程能流暢進行，本公司保留變更順序之權利與義務。但絕對忠於行前說明會資料，飯店、景點、餐食將不會有所遺漏。<br />\r\n２.為顧及消費者旅遊安全，並為維護團體旅遊品質與行程順暢，本行程恕無法接受行動不便或年滿70歲以上之貴賓單獨報名；如有行動不便或年滿70歲以上之貴賓欲參與本團體行程，必須至少有一位以上可相互照料之同行家人或友人一同參團，且經雙方確認後，確實並無不適於參加本團體行程之虞者，方可接受報名，感謝您的配合。<br />\r\n３.本行程設定為團體旅遊行程，故為顧及旅客於出遊期間之人身安全及相關問題，於旅遊行程期間，恕無法接受脫隊之要求；若因此而無法滿足您的旅遊需求，建議您可另行選購團體自由行或航空公司套裝自由行，不便之處，尚祈鑒諒。<br />\r\n４.日本國土交通省於平成24年6月(2012年)27日發布最新規定，每日行車時間不得超過10小時(計算方式以車庫實際發車時間至返回車庫時間為基準)，以有效防止巴士司機因過(疲)勞駕駛所衍生之交通狀況。<br />\r\n５.<a href="http://www.mlit.go.jp/report/press/jidosha02_hh_000095.html">日本國土交通省相關連結《１》</a>&nbsp;<br />\r\n６.<a href="http://www.mlit.go.jp/common/000215058.pdf">日本國土交通省相關連結《２》</a>&nbsp;<br />\r\n７.日本新入境審查手續於2007年11月20日起實施，前往日本旅客入境時需提供本人指紋和拍攝臉部照片並接受入境審查官之審查，拒絕配合者將不獲准入境，敬請瞭解。<br />\r\n８.自2008年6月1日起，日本交通法規定，基於安全考量，所有乘客（包含後座）均需繫上安全帶。</p>\r\n', '2017-05-09', '2017-05-13', 10, 0, 8, '<p>台北桃園國際機場／成田國際空港&rarr;住宿飯店</p>\r\n\r\n<p>本日為下午抵達成田之班機，到達成田國際空港後，即前往飯店休息，敬請了解！</p>\r\n', '<p>住宿飯店&rarr;遠眺東京晴空塔&rarr;八景島海島樂園&rarr;河口湖散策&rarr;富士五湖飯店</p>\r\n', '<h2>富士五湖溫泉或石和溫泉&rarr;箱根登山鐵道深度愜意行（搭乘三項交通工具：斜面登山纜車、空中纜車、蘆之湖海賊船）&rarr;箱根神社&rarr;免稅店&rarr;台場DIVER CITY&rarr;住宿飯店</h2>\r\n', '<p>東京&rarr;一票到底(備註)～迪士尼海洋樂園或迪士尼樂園&rarr;東京</p>\r\n', '<p>東京&rarr;成田國際空港／桃園國際機場</p>\r\n', '', '', '', '17051121011655417.jpg', '17051121011667019.jpg', '17051121011624704.jpg', '17051121011666536.jpg', '17051121011619295.jpg', '', '', '', '<p>螃蟹吃到飽123</p>\r\n', '', '<p>１.本行程最低出團人數為16人以上(含)；最多為45人以下(含)，台灣地區將派遣合格領隊隨行服務。<br />\r\n２.本報價已含兩地機場稅、燃油附加費、200萬意外責任險暨20萬意外醫療險。<br />\r\n３.本報價不含行程中未標示之純私人消費：如行李超重費、飲料酒類、洗衣、電話、電報及非團體行程中包含交通費與餐食等。<br />\r\n４.本報價是以雙人入住一房計算，如遇單人報名時，同團無法覓得可合住的同性旅客，需補單人房差。<br />\r\n５.若為包(加)班機行程，依包(加)班機航空公司作業條件，作業方式將不受國外旅遊定型化契約書中第二十七條規範，如因旅客個人因素取消旅遊、變更日期或行程之履行，則訂金將不予退還，請注意您的旅遊規劃。</p>\r\n', '', '', '1', '', 0, 0, 0, 0, '2017-05-11 11:21:04', '2018-01-16 10:37:35', '2018-01-16 02:37:35'),
(6, '東京親子遊歡樂５日～歡暢迪士尼．八景島海洋樂園．螃蟹吃到飽', '', '0,3', '1,4', '1', '1', 10000, 0, '17051111215462093.jpg', '<p>１.若遇當地特殊情況或不可抗力之因素，如塞車、觀光景點休假等因素，為讓行程能流暢進行，本公司保留變更順序之權利與義務。但絕對忠於行前說明會資料，飯店、景點、餐食將不會有所遺漏。<br />\r\n２.為顧及消費者旅遊安全，並為維護團體旅遊品質與行程順暢，本行程恕無法接受行動不便或年滿70歲以上之貴賓單獨報名；如有行動不便或年滿70歲以上之貴賓欲參與本團體行程，必須至少有一位以上可相互照料之同行家人或友人一同參團，且經雙方確認後，確實並無不適於參加本團體行程之虞者，方可接受報名，感謝您的配合。<br />\r\n３.本行程設定為團體旅遊行程，故為顧及旅客於出遊期間之人身安全及相關問題，於旅遊行程期間，恕無法接受脫隊之要求；若因此而無法滿足您的旅遊需求，建議您可另行選購團體自由行或航空公司套裝自由行，不便之處，尚祈鑒諒。<br />\r\n４.日本國土交通省於平成24年6月(2012年)27日發布最新規定，每日行車時間不得超過10小時(計算方式以車庫實際發車時間至返回車庫時間為基準)，以有效防止巴士司機因過(疲)勞駕駛所衍生之交通狀況。<br />\r\n５.<a href="http://www.mlit.go.jp/report/press/jidosha02_hh_000095.html">日本國土交通省相關連結《１》</a>&nbsp;<br />\r\n６.<a href="http://www.mlit.go.jp/common/000215058.pdf">日本國土交通省相關連結《２》</a>&nbsp;<br />\r\n７.日本新入境審查手續於2007年11月20日起實施，前往日本旅客入境時需提供本人指紋和拍攝臉部照片並接受入境審查官之審查，拒絕配合者將不獲准入境，敬請瞭解。<br />\r\n８.自2008年6月1日起，日本交通法規定，基於安全考量，所有乘客（包含後座）均需繫上安全帶。</p>\r\n', '2017-05-09', '2017-05-13', 10, 0, 5, '<p>台北桃園國際機場／成田國際空港&rarr;住宿飯店</p>\r\n\r\n<p>本日為下午抵達成田之班機，到達成田國際空港後，即前往飯店休息，敬請了解！</p>\r\n', '<p>住宿飯店&rarr;遠眺東京晴空塔&rarr;八景島海島樂園&rarr;河口湖散策&rarr;富士五湖飯店</p>\r\n', '<h2>富士五湖溫泉或石和溫泉&rarr;箱根登山鐵道深度愜意行（搭乘三項交通工具：斜面登山纜車、空中纜車、蘆之湖海賊船）&rarr;箱根神社&rarr;免稅店&rarr;台場DIVER CITY&rarr;住宿飯店</h2>\r\n', '<p>東京&rarr;一票到底(備註)～迪士尼海洋樂園或迪士尼樂園&rarr;東京</p>\r\n', '<p>東京&rarr;成田國際空港／桃園國際機場</p>\r\n', '', '', '', '17051121011655417.jpg', '17051121011667019.jpg', '17051121011624704.jpg', '17051121011666536.jpg', '17051121011619295.jpg', '', '', '', '<p>螃蟹吃到飽</p>\r\n', '17091820081598795.jpg', '<p>１.本行程最低出團人數為16人以上(含)；最多為45人以下(含)，台灣地區將派遣合格領隊隨行服務。<br />\r\n２.本報價已含兩地機場稅、燃油附加費、200萬意外責任險暨20萬意外醫療險。<br />\r\n３.本報價不含行程中未標示之純私人消費：如行李超重費、飲料酒類、洗衣、電話、電報及非團體行程中包含交通費與餐食等。<br />\r\n４.本報價是以雙人入住一房計算，如遇單人報名時，同團無法覓得可合住的同性旅客，需補單人房差。<br />\r\n５.若為包(加)班機行程，依包(加)班機航空公司作業條件，作業方式將不受國外旅遊定型化契約書中第二十七條規範，如因旅客個人因素取消旅遊、變更日期或行程之履行，則訂金將不予退還，請注意您的旅遊規劃。</p>\r\n', '', '', '1', '', 0, 0, 0, 0, '0000-00-00 00:00:00', '2018-01-16 10:37:49', '2018-01-16 02:37:49'),
(7, '東京親子遊歡樂５日～歡暢迪士尼．八景島海洋樂園．螃蟹吃到飽', '', '0,3', '1,4', '1', '1', 10000, 0, '17051111215462093.jpg', '<p>１.若遇當地特殊情況或不可抗力之因素，如塞車、觀光景點休假等因素，為讓行程能流暢進行，本公司保留變更順序之權利與義務。但絕對忠於行前說明會資料，飯店、景點、餐食將不會有所遺漏。<br />\r\n２.為顧及消費者旅遊安全，並為維護團體旅遊品質與行程順暢，本行程恕無法接受行動不便或年滿70歲以上之貴賓單獨報名；如有行動不便或年滿70歲以上之貴賓欲參與本團體行程，必須至少有一位以上可相互照料之同行家人或友人一同參團，且經雙方確認後，確實並無不適於參加本團體行程之虞者，方可接受報名，感謝您的配合。<br />\r\n３.本行程設定為團體旅遊行程，故為顧及旅客於出遊期間之人身安全及相關問題，於旅遊行程期間，恕無法接受脫隊之要求；若因此而無法滿足您的旅遊需求，建議您可另行選購團體自由行或航空公司套裝自由行，不便之處，尚祈鑒諒。<br />\r\n４.日本國土交通省於平成24年6月(2012年)27日發布最新規定，每日行車時間不得超過10小時(計算方式以車庫實際發車時間至返回車庫時間為基準)，以有效防止巴士司機因過(疲)勞駕駛所衍生之交通狀況。<br />\r\n５.<a href="http://www.mlit.go.jp/report/press/jidosha02_hh_000095.html">日本國土交通省相關連結《１》</a>&nbsp;<br />\r\n６.<a href="http://www.mlit.go.jp/common/000215058.pdf">日本國土交通省相關連結《２》</a>&nbsp;<br />\r\n７.日本新入境審查手續於2007年11月20日起實施，前往日本旅客入境時需提供本人指紋和拍攝臉部照片並接受入境審查官之審查，拒絕配合者將不獲准入境，敬請瞭解。<br />\r\n８.自2008年6月1日起，日本交通法規定，基於安全考量，所有乘客（包含後座）均需繫上安全帶。</p>\r\n', '2017-05-09', '2017-05-13', 10, 0, 5, '<p>台北桃園國際機場／成田國際空港&rarr;住宿飯店</p>\r\n\r\n<p>本日為下午抵達成田之班機，到達成田國際空港後，即前往飯店休息，敬請了解！</p>\r\n', '<p>住宿飯店&rarr;遠眺東京晴空塔&rarr;八景島海島樂園&rarr;河口湖散策&rarr;富士五湖飯店</p>\r\n', '<h2>富士五湖溫泉或石和溫泉&rarr;箱根登山鐵道深度愜意行（搭乘三項交通工具：斜面登山纜車、空中纜車、蘆之湖海賊船）&rarr;箱根神社&rarr;免稅店&rarr;台場DIVER CITY&rarr;住宿飯店</h2>\r\n', '<p>東京&rarr;一票到底(備註)～迪士尼海洋樂園或迪士尼樂園&rarr;東京</p>\r\n', '<p>東京&rarr;成田國際空港／桃園國際機場</p>\r\n', '', '', '', '17051121011655417.jpg', '17051121011667019.jpg', '17051121011624704.jpg', '17051121011666536.jpg', '17051121011619295.jpg', '', '', '', '<p>螃蟹吃到飽</p>\r\n', '17091820081598795.jpg', '<p>１.本行程最低出團人數為16人以上(含)；最多為45人以下(含)，台灣地區將派遣合格領隊隨行服務。<br />\r\n２.本報價已含兩地機場稅、燃油附加費、200萬意外責任險暨20萬意外醫療險。<br />\r\n３.本報價不含行程中未標示之純私人消費：如行李超重費、飲料酒類、洗衣、電話、電報及非團體行程中包含交通費與餐食等。<br />\r\n４.本報價是以雙人入住一房計算，如遇單人報名時，同團無法覓得可合住的同性旅客，需補單人房差。<br />\r\n５.若為包(加)班機行程，依包(加)班機航空公司作業條件，作業方式將不受國外旅遊定型化契約書中第二十七條規範，如因旅客個人因素取消旅遊、變更日期或行程之履行，則訂金將不予退還，請注意您的旅遊規劃。</p>\r\n', '', '', '1', '', 0, 0, 0, 0, '0000-00-00 00:00:00', '2018-01-16 10:37:43', '2018-01-16 02:37:43'),
(8, 'test', '<p>test</p>\r\n', '', '', '', '', 11111, 0, '18020514150148521.jpg', '<p>test</p>\r\n', '0000-00-00', '0000-00-00', 8, 1, 5, '<p>test</p>\r\n', '<p>test</p>\r\n', '<p>test</p>\r\n', '<p>tset</p>\r\n', '<p>test</p>\r\n', '', '', '', '', '', '', '', '', '', '', '', '<p>test</p>\r\n', '', '<p>test</p>\r\n', '<p>test</p>\r\n', '<p>test</p>\r\n', '', '', 1, 1, 1, 1, '2018-01-25 00:05:08', '2018-02-26 11:58:17', NULL),
(9, '東京親子遊歡樂５日～歡暢迪士尼．八景島海洋樂園．螃蟹吃到飽', '', '', '', '', '', 10000, 0, '17051111215462093.jpg', '<p>１.若遇當地特殊情況或不可抗力之因素，如塞車、觀光景點休假等因素，為讓行程能流暢進行，本公司保留變更順序之權利與義務。但絕對忠於行前說明會資料，飯店、景點、餐食將不會有所遺漏。<br />\r\n２.為顧及消費者旅遊安全，並為維護團體旅遊品質與行程順暢，本行程恕無法接受行動不便或年滿70歲以上之貴賓單獨報名；如有行動不便或年滿70歲以上之貴賓欲參與本團體行程，必須至少有一位以上可相互照料之同行家人或友人一同參團，且經雙方確認後，確實並無不適於參加本團體行程之虞者，方可接受報名，感謝您的配合。<br />\r\n３.本行程設定為團體旅遊行程，故為顧及旅客於出遊期間之人身安全及相關問題，於旅遊行程期間，恕無法接受脫隊之要求；若因此而無法滿足您的旅遊需求，建議您可另行選購團體自由行或航空公司套裝自由行，不便之處，尚祈鑒諒。<br />\r\n４.日本國土交通省於平成24年6月(2012年)27日發布最新規定，每日行車時間不得超過10小時(計算方式以車庫實際發車時間至返回車庫時間為基準)，以有效防止巴士司機因過(疲)勞駕駛所衍生之交通狀況。<br />\r\n５.<a href="http://www.mlit.go.jp/report/press/jidosha02_hh_000095.html">日本國土交通省相關連結《１》</a>&nbsp;<br />\r\n６.<a href="http://www.mlit.go.jp/common/000215058.pdf">日本國土交通省相關連結《２》</a>&nbsp;<br />\r\n７.日本新入境審查手續於2007年11月20日起實施，前往日本旅客入境時需提供本人指紋和拍攝臉部照片並接受入境審查官之審查，拒絕配合者將不獲准入境，敬請瞭解。<br />\r\n８.自2008年6月1日起，日本交通法規定，基於安全考量，所有乘客（包含後座）均需繫上安全帶。</p>\r\n', '2017-05-09', '2017-05-13', 10, 0, 8, '<p>台北桃園國際機場／成田國際空港&rarr;住宿飯店</p>\r\n\r\n<p>本日為下午抵達成田之班機，到達成田國際空港後，即前往飯店休息，敬請了解！</p>\r\n', '<p>住宿飯店&rarr;遠眺東京晴空塔&rarr;八景島海島樂園&rarr;河口湖散策&rarr;富士五湖飯店</p>\r\n', '<h2>富士五湖溫泉或石和溫泉&rarr;箱根登山鐵道深度愜意行（搭乘三項交通工具：斜面登山纜車、空中纜車、蘆之湖海賊船）&rarr;箱根神社&rarr;免稅店&rarr;台場DIVER CITY&rarr;住宿飯店</h2>\r\n', '<p>東京&rarr;一票到底(備註)～迪士尼海洋樂園或迪士尼樂園&rarr;東京</p>\r\n', '<p>東京&rarr;成田國際空港／桃園國際機場</p>\r\n', '', '', '', '17051121011655417.jpg', '17051121011667019.jpg', '17051121011624704.jpg', '17051121011666536.jpg', '17051121011619295.jpg', '', '', '', '<p>螃蟹吃到飽123</p>\r\n', '', '<p>１.本行程最低出團人數為16人以上(含)；最多為45人以下(含)，台灣地區將派遣合格領隊隨行服務。<br />\r\n２.本報價已含兩地機場稅、燃油附加費、200萬意外責任險暨20萬意外醫療險。<br />\r\n３.本報價不含行程中未標示之純私人消費：如行李超重費、飲料酒類、洗衣、電話、電報及非團體行程中包含交通費與餐食等。<br />\r\n４.本報價是以雙人入住一房計算，如遇單人報名時，同團無法覓得可合住的同性旅客，需補單人房差。<br />\r\n５.若為包(加)班機行程，依包(加)班機航空公司作業條件，作業方式將不受國外旅遊定型化契約書中第二十七條規範，如因旅客個人因素取消旅遊、變更日期或行程之履行，則訂金將不予退還，請注意您的旅遊規劃。</p>\r\n', '\n', '', '1', '', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, '東京親子遊歡樂５日～歡暢迪士尼．八景島海洋樂園．螃蟹吃到飽', '', '', '', '', '', 10000, 0, '17051111215462093.jpg', '<p>１.若遇當地特殊情況或不可抗力之因素，如塞車、觀光景點休假等因素，為讓行程能流暢進行，本公司保留變更順序之權利與義務。但絕對忠於行前說明會資料，飯店、景點、餐食將不會有所遺漏。<br />\r\n２.為顧及消費者旅遊安全，並為維護團體旅遊品質與行程順暢，本行程恕無法接受行動不便或年滿70歲以上之貴賓單獨報名；如有行動不便或年滿70歲以上之貴賓欲參與本團體行程，必須至少有一位以上可相互照料之同行家人或友人一同參團，且經雙方確認後，確實並無不適於參加本團體行程之虞者，方可接受報名，感謝您的配合。<br />\r\n３.本行程設定為團體旅遊行程，故為顧及旅客於出遊期間之人身安全及相關問題，於旅遊行程期間，恕無法接受脫隊之要求；若因此而無法滿足您的旅遊需求，建議您可另行選購團體自由行或航空公司套裝自由行，不便之處，尚祈鑒諒。<br />\r\n４.日本國土交通省於平成24年6月(2012年)27日發布最新規定，每日行車時間不得超過10小時(計算方式以車庫實際發車時間至返回車庫時間為基準)，以有效防止巴士司機因過(疲)勞駕駛所衍生之交通狀況。<br />\r\n５.<a href="http://www.mlit.go.jp/report/press/jidosha02_hh_000095.html">日本國土交通省相關連結《１》</a>&nbsp;<br />\r\n６.<a href="http://www.mlit.go.jp/common/000215058.pdf">日本國土交通省相關連結《２》</a>&nbsp;<br />\r\n７.日本新入境審查手續於2007年11月20日起實施，前往日本旅客入境時需提供本人指紋和拍攝臉部照片並接受入境審查官之審查，拒絕配合者將不獲准入境，敬請瞭解。<br />\r\n８.自2008年6月1日起，日本交通法規定，基於安全考量，所有乘客（包含後座）均需繫上安全帶。</p>\r\n', '2017-05-09', '2017-05-13', 10, 0, 8, '<p>台北桃園國際機場／成田國際空港&rarr;住宿飯店</p>\r\n\r\n<p>本日為下午抵達成田之班機，到達成田國際空港後，即前往飯店休息，敬請了解！</p>\r\n', '<p>住宿飯店&rarr;遠眺東京晴空塔&rarr;八景島海島樂園&rarr;河口湖散策&rarr;富士五湖飯店</p>\r\n', '<h2>富士五湖溫泉或石和溫泉&rarr;箱根登山鐵道深度愜意行（搭乘三項交通工具：斜面登山纜車、空中纜車、蘆之湖海賊船）&rarr;箱根神社&rarr;免稅店&rarr;台場DIVER CITY&rarr;住宿飯店</h2>\r\n', '<p>東京&rarr;一票到底(備註)～迪士尼海洋樂園或迪士尼樂園&rarr;東京</p>\r\n', '<p>東京&rarr;成田國際空港／桃園國際機場</p>\r\n', '', '', '', '17051121011655417.jpg', '17051121011667019.jpg', '17051121011624704.jpg', '17051121011666536.jpg', '17051121011619295.jpg', '', '', '', '<p>螃蟹吃到飽123</p>\r\n', '', '<p>１.本行程最低出團人數為16人以上(含)；最多為45人以下(含)，台灣地區將派遣合格領隊隨行服務。<br />\r\n２.本報價已含兩地機場稅、燃油附加費、200萬意外責任險暨20萬意外醫療險。<br />\r\n３.本報價不含行程中未標示之純私人消費：如行李超重費、飲料酒類、洗衣、電話、電報及非團體行程中包含交通費與餐食等。<br />\r\n４.本報價是以雙人入住一房計算，如遇單人報名時，同團無法覓得可合住的同性旅客，需補單人房差。<br />\r\n５.若為包(加)班機行程，依包(加)班機航空公司作業條件，作業方式將不受國外旅遊定型化契約書中第二十七條規範，如因旅客個人因素取消旅遊、變更日期或行程之履行，則訂金將不予退還，請注意您的旅遊規劃。</p>\r\n', '', '', '1', '', 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `set_trip_tour`
--

CREATE TABLE `set_trip_tour` (
  `id` int(11) NOT NULL,
  `tour_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `set_trip_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `remaining_quantity` int(11) NOT NULL,
  `end_date` date NOT NULL,
  `days` int(11) NOT NULL,
  `activity_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `ticket_departure_date_id` int(11) NOT NULL,
  `traffic_services_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `adult_price` int(11) NOT NULL,
  `child_price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `set_trip_tour`
--

INSERT INTO `set_trip_tour` (`id`, `tour_no`, `set_trip_id`, `date`, `name`, `quantity`, `remaining_quantity`, `end_date`, `days`, `activity_id`, `ticket_id`, `ticket_departure_date_id`, `traffic_services_id`, `hotel_id`, `room_id`, `adult_price`, `child_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'S321548789', 1, '2018-01-17', '春節前最後一檔', 30, 19, '2018-01-19', 3, '1,2', 0, 388, '1,3', 0, 3, 3000, 1500, '2018-01-11 09:14:28', '2018-02-09 02:42:43', NULL),
(4, 'XC32WT433', 1, '2018-01-18', '冬天的澎湖', 30, 30, '2018-01-20', 3, '', 1, 1, '0', 0, 3, 3000, 500, '2018-01-11 09:18:46', '2018-01-22 05:59:07', NULL),
(5, 'ts000001', 8, '2018-01-11', 'test', 1, 1, '2018-01-31', 3, '', 0, 300, '4', 0, 1, 5000, 500, '2018-01-24 16:12:25', '2018-02-26 03:11:27', NULL),
(6, 'ts000001', 8, '2018-01-11', 'test複製', 1, 1, '2018-01-31', 3, '', 0, 300, '4', 0, 1, 5000, 500, '2018-01-24 16:12:25', '2018-02-26 03:11:32', NULL),
(7, 'S321548789', 8, '2018-02-05', 'xxxx', 1, 0, '2018-02-16', 1, '2', 0, 300, '3', 0, 1, 3333, 3333, '2018-02-26 03:12:05', '2018-02-26 03:12:05', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `shuttle_bus`
--

CREATE TABLE `shuttle_bus` (
  `id` int(11) NOT NULL,
  `station` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prices` int(11) NOT NULL COMMENT '來回',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `shuttle_bus`
--

INSERT INTO `shuttle_bus` (`id`, `station`, `price`, `prices`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '台北轉運站', '700', 1100, '2017-09-21 11:01:00', '2017-09-21 11:15:01', NULL),
(2, '三重重陽站', '700', 1100, '2017-09-21 11:01:39', '2017-09-21 11:15:36', NULL),
(3, '桃園南崁站', '700', 1100, '2017-09-21 11:02:18', '2017-09-21 11:15:46', NULL),
(4, '台中車站', '400', 600, '2017-09-21 11:16:55', '2017-09-21 11:16:55', NULL),
(5, '彰化交流道', '400', 600, '2017-09-21 11:03:08', '2017-09-21 11:16:21', NULL),
(6, '員林交流道', '400', 600, '2017-09-21 11:04:14', '2017-09-21 11:16:31', NULL),
(7, '台中中港轉運站', '400', 600, '2017-09-21 11:02:43', '2017-09-21 11:16:11', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `address`, `tel`, `email`, `remarks`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '供應商黃先生', '新竹市西大路500號1F', '0978945612', 'ab7895123@hotmail.com', '<p>23131231</p>\r\n', '2017-06-05 11:50:41', '2017-06-05 11:50:41', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `temporary_order_hotel`
--

CREATE TABLE `temporary_order_hotel` (
  `id` int(11) NOT NULL,
  `order_id` int(10) NOT NULL,
  `hotel_id` int(10) NOT NULL,
  `room_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `temporary_order_hotel`
--

INSERT INTO `temporary_order_hotel` (`id`, `order_id`, `hotel_id`, `room_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 143, 0, 0, '2017-06-08 08:24:10', '0000-00-00 00:00:00', NULL),
(2, 145, 0, 0, '2017-06-08 08:24:48', '0000-00-00 00:00:00', NULL),
(3, 146, 0, 0, '2017-06-08 08:25:12', '0000-00-00 00:00:00', NULL),
(6, 190, 2, 1, '2017-06-09 07:03:49', '0000-00-00 00:00:00', NULL),
(7, 190, 2, 1, '2017-06-09 07:03:49', '0000-00-00 00:00:00', NULL),
(8, 194, 2, 1, '2017-08-28 08:39:28', '0000-00-00 00:00:00', NULL),
(9, 194, 2, 1, '2017-08-28 08:39:28', '0000-00-00 00:00:00', NULL),
(10, 211, 2, 1, '2017-09-14 10:11:41', '0000-00-00 00:00:00', NULL),
(11, 211, 2, 1, '2017-09-14 10:11:41', '0000-00-00 00:00:00', NULL),
(12, 1, 2, 1, '2017-09-22 17:58:57', '0000-00-00 00:00:00', NULL),
(13, 1, 2, 1, '2017-09-22 17:58:57', '0000-00-00 00:00:00', NULL),
(14, 2, 2, 1, '2017-09-22 18:11:35', '0000-00-00 00:00:00', NULL),
(15, 3, 2, 1, '2017-09-22 18:43:09', '0000-00-00 00:00:00', NULL),
(16, 3, 2, 1, '2017-09-22 18:43:09', '0000-00-00 00:00:00', NULL),
(17, 4, 2, 1, '2017-09-22 18:47:53', '0000-00-00 00:00:00', NULL),
(18, 4, 2, 1, '2017-09-22 18:47:53', '0000-00-00 00:00:00', NULL),
(19, 5, 2, 1, '2017-09-27 06:11:23', '0000-00-00 00:00:00', NULL),
(20, 5, 2, 1, '2017-09-27 06:11:24', '0000-00-00 00:00:00', NULL),
(21, 7, 2, 1, '2017-10-13 08:04:13', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `ticket_class_id` int(11) NOT NULL,
  `ticket_type_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transportation_company_id` int(11) NOT NULL,
  `departure_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `adult_cost` int(11) NOT NULL,
  `adult_price` int(11) NOT NULL,
  `child_cost` int(11) NOT NULL,
  `child_price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `holiday_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `departure_date_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `ext` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `ticket`
--

INSERT INTO `ticket` (`id`, `location_id`, `ticket_class_id`, `ticket_type_id`, `name`, `transportation_company_id`, `departure_id`, `destination_id`, `adult_cost`, `adult_price`, `child_cost`, `child_price`, `quantity`, `start_date`, `end_date`, `holiday_file`, `departure_date_file`, `remarks`, `ext`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 2, 1, '東京來回機票', 1, 3, 1, 2000, 25799, 20000, 25799, 193, '2017-03-01', '2017-03-31', '2017060642241.xlsx.xlsx', '2017060948809.xlsx', '', '', '2017-03-01 12:25:19', '2017-08-28 16:05:41', NULL),
(2, 0, 1, 1, '長榮航空-(高雄至澎湖)', 1, 2, 8, 1000, 2000, 500, 1000, 0, '2017-03-01', '2017-03-31', '2017041783436.xlsx', '2018011242705.xlsx', '', '', '2017-03-01 12:25:56', '2018-01-12 15:42:44', NULL),
(3, 0, 1, 2, '嘉義-澎湖-來回船票', 3, 4, 5, 1500, 1950, 700, 975, 200, '2017-04-01', '2017-04-30', '2017040521883.xlsx', '2017122794948.xlsx', '報到方式：憑身分證件 或健保卡，至船公司或航空公司櫃台直接辦理\r\n1.敬請提前60分鐘抵達辦理登機、登船手續。\r\n2.請記得攜帶身分證件或健保卡，孩童可準備戶籍謄本。', '', '2017-04-17 14:25:15', '2017-12-27 13:27:17', NULL),
(4, 8, 1, 2, '嘉義-澎湖-來回船票', 4, 4, 5, 1500, 1950, 975, 500, 0, '0000-00-00', '0000-00-00', '', '', '', '', '2017-04-24 12:02:33', '2018-01-16 14:16:01', NULL),
(5, 0, 1, 1, '台東-蘭嶼來回機票', 1, 7, 6, 2000, 3000, 1500, 2000, 0, '0000-00-00', '0000-00-00', '', '2017051167065.xlsx', '', '', '2017-05-11 20:51:19', '2017-08-28 16:05:31', NULL),
(6, 8, 1, 2, '嘉義-澎湖-來回船票', 5, 4, 5, 500, 1500, 600, 1200, 0, '0000-00-00', '0000-00-00', '', '2018011657617.xlsx', '', '', '2017-06-06 01:10:24', '2018-01-16 17:41:31', NULL),
(7, 8, 2, 1, '樂桃航空-早去晚回航班', 6, 3, 1, 10000, 12000, 8000, 10000, 0, '0000-00-00', '0000-00-00', '2017060623936.xlsx.xlsx', '2018022653062.xlsx', '樂桃航空之航班在所有航程全面禁止旅客攜帶Samsung Note 7手機登機，包括隨身行李、托運行李或貨運寄送皆不可攜帶，如有查獲違規攜帶之事宜，將拒絕該名旅客登機，敬請注意。', '', '2017-06-06 01:44:14', '2018-02-26 10:31:30', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `ticket_class`
--

CREATE TABLE `ticket_class` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `ticket_class`
--

INSERT INTO `ticket_class` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '國內', '2017-08-28 16:00:02', '2017-08-28 16:00:02', NULL),
(2, '國外', '2017-08-28 16:00:09', '2017-08-28 16:00:09', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `ticket_departure_date`
--

CREATE TABLE `ticket_departure_date` (
  `id` int(11) NOT NULL,
  `no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticket_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `arrival_date` date NOT NULL,
  `arrival_time` time NOT NULL,
  `return_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_date` date NOT NULL,
  `return_time` time NOT NULL,
  `return_arrival_date` date NOT NULL,
  `return_arrival_time` time NOT NULL,
  `price` int(10) NOT NULL,
  `cost` int(10) NOT NULL,
  `stoct` int(10) NOT NULL,
  `ext` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `ticket_departure_date`
--

INSERT INTO `ticket_departure_date` (`id`, `no`, `ticket_id`, `date`, `time`, `arrival_date`, `arrival_time`, `return_no`, `return_date`, `return_time`, `return_arrival_date`, `return_arrival_time`, `price`, `cost`, `stoct`, `ext`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'a123456789', 2, '2017-05-31', '16:00:00', '2017-06-20', '04:00:00', 'a987654231', '2017-06-21', '15:00:00', '2017-06-21', '17:00:00', 1000, 0, 9, 'xxxxxxx', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(2, 'a123456789', 2, '2017-06-20', '16:00:00', '2017-06-20', '04:00:00', 'a987654231', '2017-06-21', '15:00:00', '2017-06-21', '17:00:00', 1000, 0, 9, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(278, '', 1, '1905-07-09', '16:00:00', '0000-00-00', '00:00:00', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'xxx', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(279, '', 1, '2017-06-30', '16:00:00', '0000-00-00', '00:00:00', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'xxx', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(290, NULL, 7, '2017-10-01', '16:00:00', '2017-10-23', '16:00:11', NULL, '2017-10-24', '16:00:11', '2017-10-24', '16:00:11', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(291, NULL, 7, '2017-10-02', '16:00:00', '2017-10-22', '16:00:10', NULL, '2017-10-23', '16:00:10', '2017-10-23', '16:00:10', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(292, NULL, 7, '2017-10-03', '16:00:00', '2017-10-21', '16:00:09', NULL, '2017-10-22', '16:00:09', '2017-10-22', '16:00:09', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(293, NULL, 7, '2017-10-04', '16:00:00', '2017-10-20', '16:00:08', NULL, '2017-10-21', '16:00:08', '2017-10-21', '16:00:08', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(294, NULL, 7, '2017-10-05', '16:00:00', '2017-10-19', '16:00:07', NULL, '2017-10-20', '16:00:07', '2017-10-20', '16:00:07', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(295, NULL, 7, '2017-10-06', '16:00:00', '2017-10-18', '16:00:06', NULL, '2017-10-19', '16:00:06', '2017-10-19', '16:00:06', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(296, NULL, 7, '2017-10-07', '16:00:00', '2017-10-17', '16:00:05', NULL, '2017-10-18', '16:00:05', '2017-10-18', '16:00:05', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(297, NULL, 7, '2017-10-08', '16:00:00', '2017-10-16', '16:00:04', NULL, '2017-10-17', '16:00:04', '2017-10-17', '16:00:04', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(298, NULL, 7, '2017-10-13', '16:00:00', '2017-10-13', '16:00:00', NULL, '2017-10-13', '16:00:00', '2017-10-13', '16:00:00', 3000, 0, 100, 'ext', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(299, NULL, 7, '2017-10-11', '16:00:00', '2017-10-13', '16:00:01', NULL, '2017-10-14', '16:00:01', '2017-10-14', '16:00:01', 3000, 0, 100, 'ext', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(300, NULL, 6, '2018-10-19', '16:00:00', '2018-10-20', '00:00:00', '', '2018-10-24', '00:00:00', '2018-10-25', '00:00:00', 3000, 0, 100, 'xxx', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(301, NULL, 6, '2018-10-18', '16:00:00', '2018-10-19', '00:00:00', '', '2018-10-23', '00:00:00', '2018-10-24', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(302, NULL, 6, '2018-10-17', '16:00:00', '2018-10-18', '00:00:00', '', '2018-10-22', '00:00:00', '2018-10-23', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(303, NULL, 6, '2018-10-16', '16:00:00', '2018-10-17', '00:00:00', '', '2018-10-21', '00:00:00', '2018-10-22', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(304, NULL, 6, '2018-10-15', '16:00:00', '2018-10-16', '00:00:00', '', '2018-10-20', '00:00:00', '2018-10-21', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(305, NULL, 6, '2018-10-14', '16:00:00', '2018-10-15', '00:00:00', '', '2018-10-19', '00:00:00', '2018-10-20', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(306, NULL, 6, '2018-10-13', '16:00:00', '2018-10-14', '00:00:00', '', '2018-10-18', '00:00:00', '2018-10-19', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(307, NULL, 6, '2018-10-12', '16:00:00', '2018-10-13', '00:00:00', '', '2018-10-17', '00:00:00', '2018-10-18', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(308, NULL, 6, '2018-10-11', '16:00:00', '2018-10-12', '00:00:00', '', '2018-10-16', '00:00:00', '2018-10-17', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(309, NULL, 6, '2018-10-10', '16:00:00', '2018-10-11', '00:00:00', '', '2018-10-15', '00:00:00', '2018-10-16', '00:00:00', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(379, NULL, 3, '2018-10-18', '16:00:00', '2018-10-19', '02:00:00', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(380, NULL, 3, '2018-10-19', '16:00:00', '2018-10-20', '03:00:00', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(381, NULL, 3, '2018-10-20', '16:00:00', '2018-10-21', '04:00:00', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(382, NULL, 3, '2018-10-21', '16:00:00', '2018-10-22', '05:00:00', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(383, NULL, 3, '2018-10-22', '16:00:00', '2018-10-23', '06:00:00', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(384, NULL, 3, '2018-10-23', '16:00:00', '2018-10-24', '07:00:00', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(385, NULL, 3, '2018-10-24', '16:00:00', '2018-10-25', '14:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(386, NULL, 3, '2018-10-25', '16:00:00', '2018-10-26', '15:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(387, NULL, 3, '2018-10-26', '16:00:00', '2018-10-27', '16:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(388, NULL, 3, '2018-10-27', '16:00:00', '2018-10-28', '17:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(389, NULL, 3, '2018-10-28', '16:00:00', '2018-10-29', '18:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(390, NULL, 3, '2018-10-29', '16:00:00', '2018-10-30', '19:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(391, NULL, 3, '2018-10-30', '16:00:00', '2018-10-31', '20:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(392, NULL, 3, '2018-10-31', '16:00:00', '2018-11-01', '21:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(393, NULL, 3, '2018-11-01', '16:00:00', '2018-11-02', '22:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(394, NULL, 3, '2018-11-02', '16:00:00', '2018-11-03', '23:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(395, 'a2', 3, '2018-11-03', '16:00:00', '2018-11-04', '00:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(396, 'a1', 3, '2018-11-04', '16:00:00', '2018-11-05', '01:28:16', '', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 3000, 0, 100, 'aaa', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(397, NULL, 7, '2017-10-01', '16:00:00', '2017-10-24', '16:00:12', NULL, '2017-10-25', '16:00:12', '2017-10-25', '16:00:12', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(398, NULL, 7, '2017-10-09', '16:00:00', '2017-10-15', '16:00:03', NULL, '2017-10-16', '16:00:03', '2017-10-16', '16:00:03', 3000, 0, 100, 'ext', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(399, NULL, 7, '2017-10-10', '16:00:00', '2017-10-14', '16:00:02', NULL, '2017-10-15', '16:00:02', '2017-10-15', '16:00:02', 3000, 0, 100, 'ext', '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(400, NULL, 7, '2017-10-01', '16:00:00', '2017-10-24', '16:00:12', NULL, '2017-10-25', '16:00:12', '2017-10-25', '16:00:12', 3000, 0, 100, NULL, '2018-02-26 10:15:55', '2018-02-26 10:15:55', NULL),
(401, NULL, 7, '2017-10-01', '16:00:00', '2017-10-24', '16:00:12', NULL, '2017-10-25', '16:00:12', '2017-10-25', '16:00:12', 3000, 0, 100, NULL, '2018-02-26 10:16:05', '2018-02-26 10:16:05', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `ticket_holiday_price`
--

CREATE TABLE `ticket_holiday_price` (
  `id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `adult_price` int(11) NOT NULL,
  `child_price` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `ticket_holiday_price`
--

INSERT INTO `ticket_holiday_price` (`id`, `ticket_id`, `adult_price`, `child_price`, `date`) VALUES
(1, 7, 1000, 500, '2017-09-18');

-- --------------------------------------------------------

--
-- 資料表結構 `ticket_type`
--

CREATE TABLE `ticket_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `ticket_type`
--

INSERT INTO `ticket_type` (`id`, `name`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '機票', '17052213512694249.png', '2017-05-22 05:51:26', '2017-05-22 05:51:26', NULL),
(2, '船票', '17052213522690801.png', '2017-05-22 05:52:26', '2017-05-22 05:52:26', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `traffic_services_read`
--

CREATE TABLE `traffic_services_read` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adult_price` int(11) NOT NULL,
  `adult_cost` int(11) NOT NULL,
  `child_price` int(11) NOT NULL,
  `child_cost` int(11) NOT NULL,
  `ext` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `traffic_services_read`
--

INSERT INTO `traffic_services_read` (`id`, `location_id`, `name`, `adult_price`, `adult_cost`, `child_price`, `child_cost`, `ext`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'City Bus', 1000, 500, 600, 200, '<p>aaa</p>\r\n', '2017-03-14 14:30:10', '2017-03-14 14:30:10', NULL),
(2, 3, '觀光交通車', 1000, 500, 600, 300, '<p>aaa</p>\r\n', '2017-03-14 14:35:49', '2017-03-14 14:35:49', NULL),
(3, 8, '機車', 500, 200, 500, 0, '<p>123</p>\r\n', '2017-03-15 15:21:41', '2017-05-08 14:44:10', NULL),
(4, 8, '重型機車', 1200, 800, 600, 300, '<p>test</p>\r\n', '2017-06-06 02:42:20', '2018-01-30 16:05:17', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `transportation_company`
--

CREATE TABLE `transportation_company` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `transportation_company`
--

INSERT INTO `transportation_company` (`id`, `name`, `tel`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '長榮航空', '0800098666', '2017-03-01 11:07:55', '2017-05-31 20:41:45', NULL),
(2, '中華航空', '', '2017-03-31 14:52:58', '2017-03-31 14:52:58', NULL),
(3, '滿天星航運股份有限公司', '', '2017-04-17 14:21:57', '2017-04-17 14:22:24', NULL),
(4, '凱旋船運', '', '2017-04-24 12:01:17', '2017-04-24 12:01:17', NULL),
(5, '太吉之星', '09888', '2017-06-06 01:12:09', '2017-06-06 01:12:09', NULL),
(6, '樂桃航空', '0800', '2017-06-06 01:42:45', '2017-06-06 01:42:45', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `trip_class`
--

CREATE TABLE `trip_class` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `trip_class`
--

INSERT INTO `trip_class` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '國內', '2017-03-09 16:26:16', '2017-03-09 16:26:16', NULL),
(2, '國外', '2017-03-09 16:26:22', '2017-03-09 16:26:22', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `trip_location`
--

CREATE TABLE `trip_location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `trip_location`
--

INSERT INTO `trip_location` (`id`, `name`, `img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, '澎湖', '18013016584954774.jpg', '2017-03-10 11:34:00', '2018-01-30 16:58:49', NULL),
(9, '蘭嶼', '18013016584954774.jpg', '2017-03-15 14:21:23', '2017-03-15 14:21:23', NULL),
(10, '日本', '18013016584954774.jpg', '2017-03-15 14:39:32', '2017-03-31 12:32:39', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `oauth_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `oauth_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `birthday` date NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sex` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profession` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '職業',
  `education_level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annual_income` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ext` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `en_name`, `account`, `tel`, `mobile`, `password`, `remember_token`, `role_id`, `sort`, `type`, `oauth_type`, `oauth_id`, `birthday`, `address`, `sex`, `profession`, `education_level`, `annual_income`, `fax`, `country`, `identity`, `img`, `ext`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, NULL, 'Kobe', '', 'bryant', '12345', '23456', '$2y$10$fOD/6FPoQjImOm9whQ/Lp.1zSM9QhZTxcJmmNPMmSXsEUEOqw.5yq', 'JraHIsEvtRQVInTzudhxjfQ3OXl04wY2XoedROVMIJTS0W1cuHCbhKyUJKe9', 2, 1, 'user', 'user', 'user', '0000-00-00', '', '', '0', '', '', '', '', '', '', '', '2016-04-03 03:38:00', '2016-08-16 13:44:40', NULL),
(5, NULL, '管理員', '', 'admin', '', '', '$2y$10$zxI9SmnVv62w5xfgPDireuHpPE/.4fPXYoWG5AFfpH0vmXQ2RZ2Mi', 'qXhPtm8cenHlYv2TeOXUJEPAwdpFnFLNCP8yrFlTJtl6AVA7X2CogK7tMC2t', 2, 1, 'admin', 'user', 'user', '0000-00-00', '', '', '0', '', '', '', '', '', '', '', '2016-05-06 03:11:06', '2018-02-26 05:38:12', NULL),
(14, 'joaner99@gmail.com', 'c', '', 'joaner99@gmail.com', NULL, NULL, '$2y$10$jSXjKILWdmG72jJQNvewHesRZqU9dEPb1GAevqKvLfkhDNF0zLP7O', 'c0ra0dh3eON4nXRAAdE53srd2rnxv3ndAaXjycKlWevcsGmZreONTppJwszX', 5, 0, 'user', 'user', 'user', '0000-00-00', '', '', '0', '', '', '', '', 'a22222222', '18020510245978925.jpg', 'xxxx', '2017-03-18 13:26:09', '2018-02-05 02:24:59', NULL),
(18, 'joaner99@gmail.com', 'Joe Liu', '', 'facebook_1630396273640762', NULL, NULL, '$2y$10$wX7.yEERcqFY1qEcaDzGtuckFzI5Du5p5ChWtkFetWaxhD1B0spdW', 'vbMGdiHrIb8mKUvi8CskiHAaqwk56v1CbsnghS2OO7iIiaqzgVdoPycJAg6y', 5, 0, 'user', 'facebook', '1630396273640762', '0000-00-00', '', '', '0', '', '', '', '', '', '', '', '2017-03-18 16:53:13', '2018-01-10 09:00:10', NULL),
(20, 'joaner99@yahoo.com.tw', 'a', '', 'joaner99@yahoo.com.tw', NULL, NULL, '$2y$10$gXbF8GNtM4cLz2zWDc/10.zDS.IGV2tTHGEW2qmCD4pQ7zGBNP1ue', 'cR9fDXrOp0zIosgCzbIfjlaI9hH6Otd0uc4mnvx3tg4mMyuJA9Sj3e6G8SuB', 5, 0, 'user', 'user', 'user', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '2017-05-12 03:30:18', '2017-05-12 03:30:31', NULL),
(21, 'ab7895123@hotmail.com', 'hen', '', 'ab7895123@hotmail.com', '0987654321', NULL, '$2y$10$HTXKkTPLYcjCTSDDzet6eep8zpZh5g1JY3CwSqt18EkolZuKYK7Km', 'YSZZPwla3RSdFB28Fz3Uxmh3h0bNChI0n0YiJF9mmd172yqxwZamndFixAkB', 5, 0, 'user', 'user', 'user', '1989-07-31', 'xxxxx', '', '', '', '', '', '', 'A123456789', '', '', '2017-09-14 08:30:34', '2018-02-09 03:24:01', NULL),
(23, 'hanzo@test.com', 'b', '', 'hanzo@test.com', '$2y$10$9IT.MlzwEq9EEvNX2tluOOn', NULL, '$2y$10$TN7bTkYiD6RO3OPtDmEifOpJuTcYQwgoK8nW3D3hr0dATN342F48e', 'FreDF6WKrjhAXxrAWqJVnG1x0tkZ22R9EpM9aGRXEw1vqKtrzMMwPgNlNLHu', 5, 0, 'user', 'user', 'user', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '2017-09-22 17:58:56', '2017-09-28 04:44:36', NULL),
(24, NULL, 'Hanzo', '', 'ab7895123@hotmail.com', '0987654321', NULL, '$2y$10$4rw3g3nQs2Tv4kcfUvW5eenvp7bqKrBtmRLkMVkSvLB8PWaYL4sx.', NULL, 5, 0, 'user', 'user', 'user', '0000-00-00', '', '', '', '', '', '', '', 'A123456789', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(25, NULL, 'xxx', '', 'ab7895123@hotmail.com', '0987654321', NULL, '$2y$10$xh138qJJQvr4uVqecXr.2.QHo.GwogmnOrZAWtvQeJ0VijEEjqZhS', NULL, 5, 0, 'user', 'user', 'user', '0000-00-00', '', '', '', '', '', '', '', 'A123456789', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(26, 'ab7895123@hotmail.com', '321', '', 'ab7895123@hotmail.com', '0987654321', NULL, '$2y$10$byFf4NoFBDyA.WrWkAQAxeKqBHkatJn5fkOOuCU9wc9MxUURCQQya', NULL, 5, 0, 'user', 'user', 'user', '0000-00-00', '', '', '', '', '', '', '', 'A123654789', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(27, 'ab7895123@hotmail.com', 'huang', '', 'ab7895123@hotmail.com', '0987654321', NULL, '$2y$10$XZE0QB0GoQeDbDLHDt/yUexBJt4aLo85CUKT.c/8GcnF0w16Shuf6', NULL, 5, 0, 'user', 'user', 'user', '2018-01-08', '', '', '', '', '', '', '', 'A123123456', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(28, 'ab7895123@hotmail.com', 'cca', '', 'ab7895123@hotmail.com', '0987654321', NULL, '$2y$10$hV2gLpM1hguzMGIFACLgneMVnblMY1QwqZjHqgz2UWeOp.YerFh/y', NULL, 5, 0, 'user', 'user', 'user', '2018-01-15', '', '', '', '', '', '', '', 'a22222222', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(29, 'ab7895123@hotmail', 'hanzo', '', 'ab7895123@hotmail', '0987654321', NULL, '$2y$10$K7V7ky8GWPWSmmY2AX1fyes3owrzFd/nkY9avaFw6UMs0jx6j1xrC', NULL, 5, 0, 'user', 'user', 'user', '2018-01-08', '', '', '', '', '', '', '', 'a22222222234', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(30, '', '', '', NULL, NULL, NULL, '', NULL, 0, 0, 'user', 'user', 'user', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '2018-01-24 18:45:51', '2018-01-24 18:45:51', NULL);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `free_exercise`
--
ALTER TABLE `free_exercise`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `hotel_class`
--
ALTER TABLE `hotel_class`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `hotel_room`
--
ALTER TABLE `hotel_room`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `hotel_room_increase`
--
ALTER TABLE `hotel_room_increase`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `img_rotation`
--
ALTER TABLE `img_rotation`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `index_banner`
--
ALTER TABLE `index_banner`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `index_marketing`
--
ALTER TABLE `index_marketing`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `item_img`
--
ALTER TABLE `item_img`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `license`
--
ALTER TABLE `license`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `menu_class`
--
ALTER TABLE `menu_class`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `menu_trip_list`
--
ALTER TABLE `menu_trip_list`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order_activity`
--
ALTER TABLE `order_activity`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order_hotel`
--
ALTER TABLE `order_hotel`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order_img`
--
ALTER TABLE `order_img`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order_remark`
--
ALTER TABLE `order_remark`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order_service`
--
ALTER TABLE `order_service`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `order_ticket`
--
ALTER TABLE `order_ticket`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `orientation_config`
--
ALTER TABLE `orientation_config`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `program_registration`
--
ALTER TABLE `program_registration`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- 資料表索引 `role_controls`
--
ALTER TABLE `role_controls`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `set_trip`
--
ALTER TABLE `set_trip`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `set_trip_tour`
--
ALTER TABLE `set_trip_tour`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `shuttle_bus`
--
ALTER TABLE `shuttle_bus`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `temporary_order_hotel`
--
ALTER TABLE `temporary_order_hotel`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ticket_class`
--
ALTER TABLE `ticket_class`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ticket_departure_date`
--
ALTER TABLE `ticket_departure_date`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ticket_holiday_price`
--
ALTER TABLE `ticket_holiday_price`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ticket_type`
--
ALTER TABLE `ticket_type`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `traffic_services_read`
--
ALTER TABLE `traffic_services_read`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `transportation_company`
--
ALTER TABLE `transportation_company`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `trip_class`
--
ALTER TABLE `trip_class`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `trip_location`
--
ALTER TABLE `trip_location`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_type` (`oauth_type`),
  ADD KEY `oauth_id` (`oauth_id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用資料表 AUTO_INCREMENT `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- 使用資料表 AUTO_INCREMENT `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `free_exercise`
--
ALTER TABLE `free_exercise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用資料表 AUTO_INCREMENT `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用資料表 AUTO_INCREMENT `hotel_class`
--
ALTER TABLE `hotel_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- 使用資料表 AUTO_INCREMENT `hotel_room`
--
ALTER TABLE `hotel_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用資料表 AUTO_INCREMENT `hotel_room_increase`
--
ALTER TABLE `hotel_room_increase`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `img_rotation`
--
ALTER TABLE `img_rotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- 使用資料表 AUTO_INCREMENT `index_banner`
--
ALTER TABLE `index_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用資料表 AUTO_INCREMENT `index_marketing`
--
ALTER TABLE `index_marketing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `item_img`
--
ALTER TABLE `item_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用資料表 AUTO_INCREMENT `license`
--
ALTER TABLE `license`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用資料表 AUTO_INCREMENT `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- 使用資料表 AUTO_INCREMENT `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `menu_class`
--
ALTER TABLE `menu_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- 使用資料表 AUTO_INCREMENT `menu_trip_list`
--
ALTER TABLE `menu_trip_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用資料表 AUTO_INCREMENT `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
--
-- 使用資料表 AUTO_INCREMENT `order_activity`
--
ALTER TABLE `order_activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- 使用資料表 AUTO_INCREMENT `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- 使用資料表 AUTO_INCREMENT `order_hotel`
--
ALTER TABLE `order_hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- 使用資料表 AUTO_INCREMENT `order_img`
--
ALTER TABLE `order_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- 使用資料表 AUTO_INCREMENT `order_remark`
--
ALTER TABLE `order_remark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用資料表 AUTO_INCREMENT `order_service`
--
ALTER TABLE `order_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- 使用資料表 AUTO_INCREMENT `order_ticket`
--
ALTER TABLE `order_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- 使用資料表 AUTO_INCREMENT `orientation_config`
--
ALTER TABLE `orientation_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- 使用資料表 AUTO_INCREMENT `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `program_registration`
--
ALTER TABLE `program_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- 使用資料表 AUTO_INCREMENT `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- 使用資料表 AUTO_INCREMENT `role_controls`
--
ALTER TABLE `role_controls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- 使用資料表 AUTO_INCREMENT `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用資料表 AUTO_INCREMENT `search`
--
ALTER TABLE `search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;
--
-- 使用資料表 AUTO_INCREMENT `set_trip`
--
ALTER TABLE `set_trip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- 使用資料表 AUTO_INCREMENT `set_trip_tour`
--
ALTER TABLE `set_trip_tour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- 使用資料表 AUTO_INCREMENT `shuttle_bus`
--
ALTER TABLE `shuttle_bus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- 使用資料表 AUTO_INCREMENT `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `temporary_order_hotel`
--
ALTER TABLE `temporary_order_hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- 使用資料表 AUTO_INCREMENT `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- 使用資料表 AUTO_INCREMENT `ticket_class`
--
ALTER TABLE `ticket_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用資料表 AUTO_INCREMENT `ticket_departure_date`
--
ALTER TABLE `ticket_departure_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=402;
--
-- 使用資料表 AUTO_INCREMENT `ticket_holiday_price`
--
ALTER TABLE `ticket_holiday_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用資料表 AUTO_INCREMENT `ticket_type`
--
ALTER TABLE `ticket_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用資料表 AUTO_INCREMENT `traffic_services_read`
--
ALTER TABLE `traffic_services_read`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用資料表 AUTO_INCREMENT `transportation_company`
--
ALTER TABLE `transportation_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用資料表 AUTO_INCREMENT `trip_class`
--
ALTER TABLE `trip_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用資料表 AUTO_INCREMENT `trip_location`
--
ALTER TABLE `trip_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- 使用資料表 AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
